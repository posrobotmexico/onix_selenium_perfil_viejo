
package onix_selenium;


import Core.*;
import Operaciones.*;
import static Operaciones.OperacionesGenerales.*;
import Regresivas.AdministracionOferta;
import Regresivas.AtencionAlCliente;
import Regresivas.Batch;
import Regresivas.CV54;
import Regresivas.CasosPymes;
import Regresivas.ConsolidacionCesiondeDerechos;
import Regresivas.CuentasPorCobrar;
import Regresivas.FacturacionCobranza;
import Regresivas.InfoCliente;
import Regresivas.ListaNegra;
import Regresivas.MigracionReno;
import Regresivas.ReporteRobo;
import Regresivas.SuspencionReactivacion;
import Regresivas.ValidacionErrores;
import Regresivas.Ventas;
//import static Regresivas.*;
import java.awt.Color;
import java.io.IOException;
import java.util.logging.Level;
import java.util.logging.Logger;


public class Menu extends javax.swing.JFrame {
   
  
    public Menu() {
        
        initComponents();  
        
    }
    
    String Caso       ="";
    String Explorador ="";
    
    @SuppressWarnings("unchecked")
    // <editor-fold defaultstate="collapsed" desc="Generated Code">//GEN-BEGIN:initComponents
    private void initComponents() {

        jLabel2 = new javax.swing.JLabel();
        ComboBox1 = new javax.swing.JComboBox<>();
        ComboBox2 = new javax.swing.JComboBox<>();
        Aceptar = new javax.swing.JButton();
        jLabel3 = new javax.swing.JLabel();
        jLabel6 = new javax.swing.JLabel();
        jSeparator1 = new javax.swing.JSeparator();
        Resultado = new javax.swing.JLabel();
        jButton1 = new javax.swing.JButton();
        jLabel8 = new javax.swing.JLabel();
        jLabel1 = new javax.swing.JLabel();
        jButton2 = new javax.swing.JButton();
        jLabel7 = new javax.swing.JLabel();
        jLabel9 = new javax.swing.JLabel();
        Aceptar1 = new javax.swing.JButton();
        jCheckBox2 = new javax.swing.JCheckBox();

        setDefaultCloseOperation(javax.swing.WindowConstants.DO_NOTHING_ON_CLOSE);
        setTitle("Practia Automatización");
        setBackground(new java.awt.Color(255, 255, 255));
        setBounds(new java.awt.Rectangle(0, 0, 0, 0));
        setForeground(new java.awt.Color(255, 255, 255));
        setLocationByPlatform(true);
        setResizable(false);

        jLabel2.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel2.setText("Selecciona el caso a ejecutar");

        ComboBox1.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        ComboBox1.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "------------ATENCION AL CLIENTE-------------", "Cambio de SIM (Sin Costo) - Prepago", "Cambio de SIM (Con Costo) - Prepago", "Cambio de SIM (Sin Costo) - Pospago", "Cambio de SIM (Con Costo) - Hibrido", "Agregar Numero Frecuente - Prepago", "Agregar Numero Frecuente(Con Costo) - Prepago", "Eliminar Numero Frecuente", "Recarga (CAC-Pago Efectivo) Prepago-Hibrido", "Incremento DN Prepago", "Decremento DN Prepago", "Cambiar numero de DN", "Complemento de pago Envio de E-mail", "Movistar Refresh Pospago", "Movistar Refresh Hibrido", "Apertura Caja", "Cierre Caja", "Incremento de Balance de Caja", "Decremento de Balance de Caja", "Payment-Paying - Hibrido", "Reporte Diario", "Pago de Deposito", "Liberacion de Deposito", "------------INFORMACION DEL CLIENTE----------------", "Consulta la Información de Cliente - Prepago", "Consulta la Información de Cuenta - Prepago", "Consulta la Información del Suscriptor - Prepago", "Consulta de Saldo - Prepago", "Cambiar Info Cuenta (OrdenesPendientes) - Prepago", "Cambiar Informacion de Cuenta - Hibrido", "Cambiar Informacion de Suscriptor - Hibrido", "-------------LISTA ROJA----------------", "Agregar Lista Roja", "Eliminar Lista Roja", "----------VALIDACION DE ERRORES----------", "Generar RFC", "Valida PIPE Info Cuenta", "Valida PIPE Direccion de Entrega", "Valida PIPE Calle Direccion Fiscal", "Valida PIPE Num Int y Ext Direccion Fiscal", "Valida RFC Existente", "----------VALIDACION DE ERRORES PYMES----------", "Generar RFC - Pymes", "Valida PIPE Info Cuenta - Pymes", "Valida PIPE Direccion de Entrega - Pymes", "Valida PIPE Calle Direccion Fiscal - Pymes", "Valida PIPE Num Int y Ext Direccion Fiscal - Pymes", "Valida RFC Existente - Pymes", "-----------------UTILIDADES BATCH------------------", "Batch actualizacion en precio de handsets venta", "Batch actualizacion en precio de handsets renovacion", "Batch Changing Customer Category", "Batch Update Payment Method - Store Card Number", "--------------SUSPENCION-------------------", "Suspend on specific day", "Suspension Bidireccional - Hibrido", "Reactivacion Bidireccional - Hibrido", "Barring pospaid subscriber - Pospago", "Unarring pospaid subscriber - Pospago", "-----------REPORTE ROBO Y EXTRAVIO--------------", "Reporte de Robo y Extravio - Prepago", "Cancelacion Reporte de Robo y Extravio - Prepago", "Reporte de Robo y Extravio - Hibrido", "Cancelacion Reporte de Robo y Extravio - Hibrido", "Reporte de Robo y Extravio con IMEI", "-----------ADMINISTRACION DE OFERTA--------------", "Cambio Oferta Primaria - Prepago", "Cambio Oferta Primaria (Upgrade) - Pospago", "Agregar Oferta Suplementaria - Prepago", "Agregar Oferta Suplementaria - Pospago", "Eliminar Oferta Suplementaria - Hibrido", "Carga De Oferta Suplementaria Por Batch", "Batch descuento variable oferta primaria", "Batch descuento variable oferta suplementaria", "-----------MIGRACION Y RENOVACION--------------", "Migración de Prepago a Híbrido", "Migración de Prepago a Pospago", "Migración de Híbrido a Pospago", "Migración de Pospago a Híbrido", "Migración de Híbrido a Prepago", "Migración de Pospago a Prepago", "Renovacion de Contrato (Upgrade) - Hibrido", "Renovacion de Contrato (Con Cambio de Oferta) - Pospago", "-------------------------VENTAS--------------------------", "Venta de Solo SIM Con Factura (Cliente Nuevo) - Prepago", "Venta de Solo SIM Con Factura (Cliente Existente) - Prepago", "Venta de Solo SIM (Cliente Nuevo) - Pospago", "Venta de Solo SIM (Cliente Nuevo) - Hibrido", "Venta de SIM Con Handset (Cliente Nuevo) - Prepago", "Agregar Nueva Linea - Pospago", "Liquidacion de Terminal", "Venta de Equipo a Credito (Cliente Nuevo) - Pospago", "Venta de Equipo a Credito (Cliente Nuevo) - Hibrido", "Venta Pospago Solo SIM Especialista", "-----------CONSOLIDACIÓN CESIÓN DE DERECHOS--------", "Consolidacón de cuenta - Pospago", "Desconsolidacón de cuenta - Hibrido", "Cesión de Derechos - Híbrido", "------------------------LISTA NEGRA------------------------", "Gestionar Lista Negra (Agregar)", "Nuevo Cliente Pospago (Lista Negra)", "Cambiar Informacion Cliente (Lista Negra)", "MNP Port in Pospago (Lista Negra)", "Venta de Equipo a Cliente Existente (Lista Negra)", "Cesion de derechos (Lista Negra)", "Cambiar Informacion de Cuenta (Lista Negra)", "Migracion Prepago a Pospago (Lista Negra)", "Migracion Prepago a Hibrido (Lista Negra)", "Quitar RFC (Lista Negra)", "Nuevo Cliente Pospago (Quitar Lista Negra)", "Cambiar Informacion Cliente (Quitar Lista Negra)", "MNP Port in Pospago (Quitar Lista Negra)", "Venta de Equipo a Cliente Existente (Quitar Lista Negra)", "Cesion de derechos (Quitar Lista Negra)", "Cambiar Informacion de Cuenta (Quitar Lista Negra)", "Migracion Prepago a Hibrido (Quitar Lista Negra)", "Migracion Prepago a Pospago (Quitar Lista Negra)", "------------------------LISTA NEGRA FINALIZADO------------------------", "Cambiar Información del Suscriptor Lista Negra", "Cambio de SIM (Sin Costo) Lista Negra", "Cambio Oferta Primaria Lista Negra", "Agregar Oferta Suplementaria Lista Negra", "Suspension Lista Negra", "Reactivacion Lista Negra", "Reporte de Robo y Extravio Lista Negra", "Cancelacion Reporte de Robo y Extravio Lista Negra", "Agregar Numero Frecuente - Prepago Lista Negra", "Payment-Paying - Hibrido Lista Negra", "Migración de Pospago a Híbrido Lista Negra", "Migración de Pospago a Prepago Lista Negra", "Migración de Híbrido a Pospago Lista Negra", "Migración de Híbrido a Prepago Lista Negra", "------------------------CASOS PYMES------------------------", "Activacion pymes pospago solo sim cliente PFAE", "Activacion pymes pospago solo sim cliente PM", "Activacion pymes hibrido con equipo cliente PFAE", "RFC con caracteres durante la venta lo permite (&)", "RFC con caracteres durante la venta lo permite (ñ)", "Desabilita boton genera RFC con cliente PM", "Adicion cliente pymes a una cuenta B2C", "Venta nueva pymes con RFC de cliente B2B ya existente", "Venta nueva pymes con RFC de cliente PM ya existente", "Venta nueva pymes con RFC de cliente PFAE ya exitente", "Exclusividad de oferta pymes cambio de oferta", "Exclusividad de oferta pymes migracion de Pospago a Hibrido", "Agegar RFC a lista negra con formato PM", "Agegar RFC a lista negra con formato PM caracter especia (&)", "Agegar RFC a lista negra con formatoPM letra (ñ)", "Quitar RFC De Lista Negra", "Lista negra error Cambiar informacion de cliente", "Lista negra error cambiar informacion de cuenta", "Lista negra error venta de equipo a cliente existente", "Lista negra error nueva venta cliente pymes", "Lista negra despues de sacar lista Cambiar informacion de cliente", "Lista negra despues de sacar lista venta de equipo a cliente existente", "Lista negra despues de sacar lista cambiar informacion de cuenta", "Lista negra despues de sacar lista nueva venta cliente pymes", "-----------------------Facturacion y Cobranza------------------------", "Consultar-Imprimir factura", "Consultar-Imprimir Nota de Credito", "Cambio de Ciclo de facturacion Pospago", "Cambiar el metodo de pago (Efectivo a Tarjeta de Credito)", "Cambiar Medio de Facturacion", "Cambiar el metodo de pago (Tarjeta de Credito y Departamental a Efectivo)", "Refacturacion", "Refacturación con Documento Ciclo", "Ordenes de Trabajo en DC por deuda", "Validacion Dias de Reloj con Ordenes de Trabajo", "Nota de Credito a Nivel Detalle - Monto Exacto", "Nota de Credito a Nivel Detalle - Monto Menor", "Nota de Credito a Nivel Detalle - Monto Mayor", "-----------------------Casos CV54------------------------", "Pago de orden con metodo de pago flap", "Pago de orden de venta con metodo de pago flap" }));
        ComboBox1.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ComboBox1ItemStateChanged(evt);
            }
        });
        ComboBox1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBox1ActionPerformed(evt);
            }
        });

        ComboBox2.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        ComboBox2.setModel(new javax.swing.DefaultComboBoxModel<>(new String[] { "Firefox", "Chrome", "Edge", " " }));
        ComboBox2.addItemListener(new java.awt.event.ItemListener() {
            public void itemStateChanged(java.awt.event.ItemEvent evt) {
                ComboBox2ItemStateChanged(evt);
            }
        });
        ComboBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                ComboBox2ActionPerformed(evt);
            }
        });

        Aceptar.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        Aceptar.setText("Ejecutar");
        Aceptar.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                AceptarActionPerformed(evt);
            }
        });

        jLabel3.setFont(new java.awt.Font("Times New Roman", 0, 10)); // NOI18N
        jLabel3.setText("Practia © ");

        jLabel6.setFont(new java.awt.Font("Times New Roman", 0, 10)); // NOI18N
        jLabel6.setText("Mexico");

        Resultado.setBorder(javax.swing.BorderFactory.createTitledBorder("Resultados"));

        jButton1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jButton1.setText("Salir");
        jButton1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton1ActionPerformed(evt);
            }
        });

        jLabel8.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jLabel8.setText("Selecciona el Explorador");

        jLabel1.setFont(new java.awt.Font("Tahoma", 0, 14)); // NOI18N
        jLabel1.setIcon(new javax.swing.ImageIcon(getClass().getResource("/Imagenes/Practia - Logo.png"))); // NOI18N
        jLabel1.setMaximumSize(new java.awt.Dimension(200, 95));
        jLabel1.setMinimumSize(new java.awt.Dimension(200, 95));
        jLabel1.setPreferredSize(new java.awt.Dimension(200, 95));

        jButton2.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        jButton2.setText("Carpeta Reporte");
        jButton2.setToolTipText("");
        jButton2.setActionCommand("Caroeta Reporte");
        jButton2.setMaximumSize(new java.awt.Dimension(79, 23));
        jButton2.setMinimumSize(new java.awt.Dimension(79, 23));
        jButton2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jButton2ActionPerformed(evt);
            }
        });

        jLabel7.setFont(new java.awt.Font("Verdana", 0, 36)); // NOI18N
        jLabel7.setText("ONIX");

        jLabel9.setFont(new java.awt.Font("Verdana", 0, 18)); // NOI18N
        jLabel9.setText("Automatización");

        Aceptar1.setFont(new java.awt.Font("Verdana", 0, 11)); // NOI18N
        Aceptar1.setText("Archivo");
        Aceptar1.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                Aceptar1ActionPerformed(evt);
            }
        });

        jCheckBox2.setFont(new java.awt.Font("Verdana", 0, 12)); // NOI18N
        jCheckBox2.setText("Abrir reporte al finalizar la prueba");
        jCheckBox2.addActionListener(new java.awt.event.ActionListener() {
            public void actionPerformed(java.awt.event.ActionEvent evt) {
                jCheckBox2ActionPerformed(evt);
            }
        });

        javax.swing.GroupLayout layout = new javax.swing.GroupLayout(getContentPane());
        getContentPane().setLayout(layout);
        layout.setHorizontalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 524, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addGroup(layout.createSequentialGroup()
                        .addGap(69, 69, 69)
                        .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 166, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(61, 61, 61)
                        .addComponent(ComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 94, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(46, 46, 46)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addComponent(Resultado, javax.swing.GroupLayout.PREFERRED_SIZE, 426, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 465, javax.swing.GroupLayout.PREFERRED_SIZE))))
                .addGap(0, 59, Short.MAX_VALUE))
            .addGroup(layout.createSequentialGroup()
                .addContainerGap()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                        .addGap(0, 0, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addComponent(jLabel7, javax.swing.GroupLayout.PREFERRED_SIZE, 124, javax.swing.GroupLayout.PREFERRED_SIZE)
                            .addComponent(jLabel9))
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 173, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(29, 29, 29))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(103, 103, 103)
                        .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 231, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addContainerGap(javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE))
                    .addGroup(layout.createSequentialGroup()
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.TRAILING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, javax.swing.GroupLayout.DEFAULT_SIZE, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addGap(26, 26, 26)
                                .addComponent(Aceptar1, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE))
                            .addComponent(jCheckBox2))
                        .addGap(18, 18, 18)
                        .addComponent(Aceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 84, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addGap(18, 18, 18)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 88, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, javax.swing.GroupLayout.DEFAULT_SIZE, Short.MAX_VALUE)
                        .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                            .addGroup(layout.createSequentialGroup()
                                .addComponent(jLabel6)
                                .addGap(19, 19, 19))
                            .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createSequentialGroup()
                                .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 48, javax.swing.GroupLayout.PREFERRED_SIZE)
                                .addContainerGap())))))
        );
        layout.setVerticalGroup(
            layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
            .addGroup(layout.createSequentialGroup()
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addGroup(layout.createSequentialGroup()
                        .addContainerGap()
                        .addComponent(jLabel1, javax.swing.GroupLayout.PREFERRED_SIZE, 112, javax.swing.GroupLayout.PREFERRED_SIZE))
                    .addGroup(layout.createSequentialGroup()
                        .addGap(20, 20, 20)
                        .addComponent(jLabel7)
                        .addGap(18, 18, 18)
                        .addComponent(jLabel9)))
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jSeparator1, javax.swing.GroupLayout.PREFERRED_SIZE, 10, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED)
                .addComponent(jLabel2, javax.swing.GroupLayout.PREFERRED_SIZE, 28, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(ComboBox1, javax.swing.GroupLayout.PREFERRED_SIZE, 37, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addGap(18, 18, 18)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                    .addComponent(jLabel8, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE)
                    .addComponent(ComboBox2, javax.swing.GroupLayout.PREFERRED_SIZE, 29, javax.swing.GroupLayout.PREFERRED_SIZE))
                .addGap(18, 18, 18)
                .addComponent(Resultado, javax.swing.GroupLayout.PREFERRED_SIZE, 44, javax.swing.GroupLayout.PREFERRED_SIZE)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.UNRELATED)
                .addComponent(jCheckBox2)
                .addPreferredGap(javax.swing.LayoutStyle.ComponentPlacement.RELATED, 52, Short.MAX_VALUE)
                .addGroup(layout.createParallelGroup(javax.swing.GroupLayout.Alignment.LEADING)
                    .addComponent(jLabel6, javax.swing.GroupLayout.Alignment.TRAILING)
                    .addGroup(javax.swing.GroupLayout.Alignment.TRAILING, layout.createParallelGroup(javax.swing.GroupLayout.Alignment.BASELINE)
                        .addComponent(jButton1, javax.swing.GroupLayout.PREFERRED_SIZE, 42, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Aceptar, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jButton2, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(jLabel3, javax.swing.GroupLayout.PREFERRED_SIZE, 13, javax.swing.GroupLayout.PREFERRED_SIZE)
                        .addComponent(Aceptar1, javax.swing.GroupLayout.PREFERRED_SIZE, 39, javax.swing.GroupLayout.PREFERRED_SIZE)))
                .addContainerGap())
        );

        jLabel2.getAccessibleContext().setAccessibleName("jLabel");
        jLabel8.getAccessibleContext().setAccessibleName("Explorador");
        jLabel8.getAccessibleContext().setAccessibleDescription("");

        getAccessibleContext().setAccessibleDescription("");

        pack();
    }// </editor-fold>//GEN-END:initComponents

    private void ComboBox1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBox1ActionPerformed

    }//GEN-LAST:event_ComboBox1ActionPerformed

    private void ComboBox1ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ComboBox1ItemStateChanged

        
    }//GEN-LAST:event_ComboBox1ItemStateChanged

    private void AceptarActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_AceptarActionPerformed
        Report reporte = null;
        
        try {             
            String Estado,Or,Error;
            String Url="http://10.225.241.136:8080/login.action?language=es_MX&username=&loginType=Account&msisdn=";
            //String Url="http://google.com";            
            Explorador=ComboBox2.getSelectedItem().toString();            
            OperacionesGenerales.CierraNavegadores(Explorador);
            Thread.sleep(1000);
            WebDriv WebDrive= new WebDriv(Url,Explorador);
            WebAction SelenElem=new WebAction();//Objeto para ocupar funciones WebAction

            Caso=ComboBox1.getSelectedItem().toString();  //Se obtiene el texto del combo 
            reporte = new Report(Caso); //Se inicializa el reporte
            
            
            String DNNSMS,Ord,Orden,Cambio,FechaIn,FechaFin,Operador,Cuenta,Ciclo,DNCesion,Oferta,DNFact,SIM,DN,Segento,Bono,CodigoOferta,IMEI,CategoriaAct,RFC,NuevaCategoria,NomArchivo,CodigoCu,Codigo,ICC,Causa,Dia,Mes,Anio,Fecha,Edo,Ope, VOZ11, SMS02, OTRAS_COMP, VOZ_LDI, Recarga, Monto, Comentario,TipAjuste,User,Contra,No_Orden,IMEI_Act,IMEI_New,Equipo,Plan,Importe,SKU,Meses,Tipo,PrecioActual,PrecioNuevo,BusinessType;                     
            switch(Caso)
            { 
                case "Cambio de SIM (Sin Costo) - Prepago":   
                    DN      = CargaVariables.LeerCSV(0, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    Codigo  = CargaVariables.LeerCSV(1, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    ICC     = CargaVariables.LeerCSV(2, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    Causa   = CargaVariables.LeerCSV(3, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    Edo     = CargaVariables.LeerCSV(4, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    Ope     = CargaVariables.LeerCSV(5, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    AtencionAlCliente.CambioDeSimSinCosto(WebDrive, SelenElem, DN, Codigo, ICC,Causa, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Cambio de SIM (Con Costo) - Prepago":   
                    DN      = CargaVariables.LeerCSV(0, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    Codigo  = CargaVariables.LeerCSV(1, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    ICC     = CargaVariables.LeerCSV(2, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    Causa   = CargaVariables.LeerCSV(3, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    Edo     = CargaVariables.LeerCSV(4, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    Ope     = CargaVariables.LeerCSV(5, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    AtencionAlCliente.CambioDeSimSinCosto(WebDrive, SelenElem, DN,Codigo, ICC,Causa, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                case "Cambio de SIM (Sin Costo) - Pospago":   
                    DN      = CargaVariables.LeerCSV(0, "AttnCliente", "Cambio de SIM (Sin Costo) - Pospago");
                    Codigo  = CargaVariables.LeerCSV(1, "AttnCliente", "Cambio de SIM (Sin Costo) - Pospago");
                    ICC     = CargaVariables.LeerCSV(2, "AttnCliente", "Cambio de SIM (Sin Costo) - Pospago");
                    Causa   = CargaVariables.LeerCSV(3, "AttnCliente", "Cambio de SIM (Sin Costo) - Pospago");
                    Edo     = CargaVariables.LeerCSV(4, "AttnCliente", "Cambio de SIM (Sin Costo) - Pospago");
                    Ope     = CargaVariables.LeerCSV(5, "AttnCliente", "Cambio de SIM (Sin Costo) - Pospago");
                    AtencionAlCliente.CambioDeSimSinCostoPospago(WebDrive, SelenElem, DN, Codigo, ICC,Causa, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                case "Cambio de SIM (Con Costo) - Hibrido":   
                    DN      = CargaVariables.LeerCSV(0, "AttnCliente", "Cambio de SIM (Con Costo) - Hibrido");
                    Codigo  = CargaVariables.LeerCSV(1, "AttnCliente", "Cambio de SIM (Con Costo) - Hibrido");
                    ICC     = CargaVariables.LeerCSV(2, "AttnCliente", "Cambio de SIM (Con Costo) - Hibrido");
                    Causa   = CargaVariables.LeerCSV(3, "AttnCliente", "Cambio de SIM (Con Costo) - Hibrido");
                    Edo     = CargaVariables.LeerCSV(4, "AttnCliente", "Cambio de SIM (Con Costo) - Hibrido");
                    Ope     = CargaVariables.LeerCSV(5, "AttnCliente", "Cambio de SIM (Con Costo) - Hibrido");
                    AtencionAlCliente.CambioDeSimConCosto(WebDrive, SelenElem, DN, Codigo, ICC,Causa, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                case "Agregar Numero Frecuente - Prepago": 
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Agregar Numero Frecuente - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "AttnCliente", "Agregar Numero Frecuente - Prepago");
                    VOZ11       = CargaVariables.LeerCSV(2, "AttnCliente", "Agregar Numero Frecuente - Prepago");
                    Edo         = CargaVariables.LeerCSV(3, "AttnCliente", "Agregar Numero Frecuente - Prepago");
                    Ope         = CargaVariables.LeerCSV(4, "AttnCliente", "Agregar Numero Frecuente - Prepago");
                    AtencionAlCliente.AgregarNumeroFrecuente(WebDrive, SelenElem, DN, Codigo, VOZ11, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Agregar Numero Frecuente(Con Costo) - Prepago": 
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Agregar Numero Frecuente (Con Costo) - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "AttnCliente", "Agregar Numero Frecuente (Con Costo) - Prepago");
                    SMS02       = CargaVariables.LeerCSV(2, "AttnCliente", "Agregar Numero Frecuente (Con Costo) - Prepago");
                    Edo         = CargaVariables.LeerCSV(3, "AttnCliente", "Agregar Numero Frecuente (Con Costo) - Prepago");
                    Ope         = CargaVariables.LeerCSV(4, "AttnCliente", "Agregar Numero Frecuente (Con Costo) - Prepago");
                    AtencionAlCliente.AgregarNumeroFrecuenteCosto(WebDrive, SelenElem, DN, Codigo, SMS02, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Eliminar Numero Frecuente":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Eliminar Numero Frecuente");
                    Codigo      = CargaVariables.LeerCSV(1, "AttnCliente", "Eliminar Numero Frecuente");
                    Edo         = CargaVariables.LeerCSV(2, "AttnCliente", "Eliminar Numero Frecuente");
                    Ope         = CargaVariables.LeerCSV(3, "AttnCliente", "Eliminar Numero Frecuente");
                    AtencionAlCliente.EliminarNumeroFrecuente(WebDrive, SelenElem, DN, Codigo, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Recarga (CAC-Pago Efectivo) Prepago-Hibrido":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Recarga (CAC-Pago Efectivo) Prepago-Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "AttnCliente", "Recarga (CAC-Pago Efectivo) Prepago-Hibrido");
                    Recarga     = CargaVariables.LeerCSV(2, "AttnCliente", "Recarga (CAC-Pago Efectivo) Prepago-Hibrido");
                    Edo         = CargaVariables.LeerCSV(3, "AttnCliente", "Recarga (CAC-Pago Efectivo) Prepago-Hibrido");
                    Ope         = CargaVariables.LeerCSV(4, "AttnCliente", "Recarga (CAC-Pago Efectivo) Prepago-Hibrido");
                    AtencionAlCliente.RecargaCAC(WebDrive, SelenElem, reporte, DN, Codigo, Recarga); 
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Incremento DN Prepago":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Incremento DN-Prepago");
                    TipAjuste   = CargaVariables.LeerCSV(1, "AttnCliente", "Incremento DN-Prepago");
                    Monto       = CargaVariables.LeerCSV(2, "AttnCliente", "Incremento DN-Prepago");
                    Comentario  = CargaVariables.LeerCSV(3, "AttnCliente", "Incremento DN-Prepago");
                    Edo         = CargaVariables.LeerCSV(4, "AttnCliente", "Incremento DN-Prepago");
                    Ope         = CargaVariables.LeerCSV(5, "AttnCliente", "Incremento DN-Prepago");
                    AtencionAlCliente.IncrementoDN(WebDrive, SelenElem, reporte, DN, TipAjuste, Monto, Comentario, "Incremento");                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Decremento DN Prepago":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Decremento DN-Prepago");
                    TipAjuste   = CargaVariables.LeerCSV(1, "AttnCliente", "Decremento DN-Prepago");
                    Monto       = CargaVariables.LeerCSV(2, "AttnCliente", "Decremento DN-Prepago");
                    Comentario  = CargaVariables.LeerCSV(3, "AttnCliente", "Decremento DN-Prepago");
                    Edo         = CargaVariables.LeerCSV(4, "AttnCliente", "Decremento DN-Prepago");
                    Ope         = CargaVariables.LeerCSV(5, "AttnCliente", "Decremento DN-Prepago");
                    AtencionAlCliente.IncrementoDN(WebDrive, SelenElem, reporte, DN, TipAjuste, Monto, Comentario, "Decremento");                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Cambiar numero de DN":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Cambiar numero de DN");
                    Codigo      = CargaVariables.LeerCSV(1, "AttnCliente", "Cambiar numero de DN");
                    Comentario  = CargaVariables.LeerCSV(2, "AttnCliente", "Cambiar numero de DN");
                    Edo         = CargaVariables.LeerCSV(3, "AttnCliente", "Cambiar numero de DN");
                    Ope         = CargaVariables.LeerCSV(4, "AttnCliente", "Cambiar numero de DN");
                    AtencionAlCliente.CambioDN(WebDrive, SelenElem, reporte, DN, Codigo, Comentario);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Complemento de pago Envio de E-mail":                     
                    No_Orden    = CargaVariables.LeerCSV(0, "AttnCliente", "ComplementoPago_EnvioEmail");
                    Edo         = CargaVariables.LeerCSV(1, "AttnCliente", "ComplementoPago_EnvioEmail");
                    Ope         = CargaVariables.LeerCSV(2, "AttnCliente", "ComplementoPago_EnvioEmail");
                    AtencionAlCliente.ComplementoEmail(WebDrive, SelenElem, reporte, No_Orden);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Movistar Refresh Pospago":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Movistar Refresh Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "AttnCliente", "Movistar Refresh Pospago");
                    IMEI_Act    = CargaVariables.LeerCSV(2, "AttnCliente", "Movistar Refresh Pospago");
                    IMEI_New    = CargaVariables.LeerCSV(3, "AttnCliente", "Movistar Refresh Pospago");
                    Equipo      = CargaVariables.LeerCSV(4, "AttnCliente", "Movistar Refresh Pospago");
                    Plan        = CargaVariables.LeerCSV(5, "AttnCliente", "Movistar Refresh Pospago");
                    Edo         = CargaVariables.LeerCSV(6, "AttnCliente", "Movistar Refresh Pospago");
                    Ope         = CargaVariables.LeerCSV(7, "AttnCliente", "Movistar Refresh Pospago");
                    AtencionAlCliente.MovRefresh(WebDrive, SelenElem, reporte, DN, Codigo, IMEI_Act, IMEI_New, Equipo,Plan);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Movistar Refresh Hibrido":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Movistar Refresh Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "AttnCliente", "Movistar Refresh Pospago");
                    IMEI_Act    = CargaVariables.LeerCSV(2, "AttnCliente", "Movistar Refresh Pospago");
                    IMEI_New    = CargaVariables.LeerCSV(3, "AttnCliente", "Movistar Refresh Pospago");
                    Equipo      = CargaVariables.LeerCSV(4, "AttnCliente", "Movistar Refresh Pospago");
                    Plan        = CargaVariables.LeerCSV(5, "AttnCliente", "Movistar Refresh Pospago");
                    Edo         = CargaVariables.LeerCSV(6, "AttnCliente", "Movistar Refresh Pospago");
                    Ope         = CargaVariables.LeerCSV(7, "AttnCliente", "Movistar Refresh Pospago");
                    AtencionAlCliente.MovRefresh(WebDrive, SelenElem, reporte, DN, Codigo, IMEI_Act, IMEI_New, Equipo,Plan);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                  
                case "Apertura Caja":                     
                    User        = CargaVariables.LeerCSV(0, "AttnCliente", "Apertura Caja");
                    Contra      = CargaVariables.LeerCSV(1, "AttnCliente", "Apertura Caja");
                    Monto       = CargaVariables.LeerCSV(2, "AttnCliente", "Apertura Caja");
                    Edo         = CargaVariables.LeerCSV(3, "AttnCliente", "Apertura Caja");
                    Ope         = CargaVariables.LeerCSV(4, "AttnCliente", "Apertura Caja");
                    AtencionAlCliente.AbrirCaja(WebDrive, SelenElem, reporte, User, Contra, Monto);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Cierre Caja": 
                    User        = CargaVariables.LeerCSV(0, "AttnCliente", "Cierre Caja");
                    Contra      = CargaVariables.LeerCSV(1, "AttnCliente", "Cierre Caja");
                    Edo         = CargaVariables.LeerCSV(3, "AttnCliente", "Cierre Caja");
                    Ope         = CargaVariables.LeerCSV(4, "AttnCliente", "Cierre Caja");
                    AtencionAlCliente.CierreCaja(WebDrive, SelenElem, reporte, User, Contra);                    
                    Resultado.setText("En Ejecucion");
                    break;
                    
                case "Payment-Paying - Hibrido":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Payment-Paying - Hibrido");
                    Edo         = CargaVariables.LeerCSV(1, "AttnCliente", "Payment-Paying - Hibrido");
                    Ope         = CargaVariables.LeerCSV(2, "AttnCliente", "Payment-Paying - Hibrido");
                    AtencionAlCliente.Payment(WebDrive, SelenElem, reporte, DN);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                
                case "Pago de Deposito":                     
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Pago de Deposito");
                    Importe     = CargaVariables.LeerCSV(1, "AttnCliente", "Pago de Deposito");
                    Edo         = CargaVariables.LeerCSV(2, "AttnCliente", "Pago de Deposito");
                    Ope         = CargaVariables.LeerCSV(3, "AttnCliente", "Pago de Deposito");
                    AtencionAlCliente.PagoDeposito(WebDrive, SelenElem, reporte, DN, Importe);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Liberacion de Deposito":                    
                    DN          = CargaVariables.LeerCSV(0, "AttnCliente", "Liberacion de Deposito");
                    Edo         = CargaVariables.LeerCSV(1, "AttnCliente", "Liberacion de Deposito");
                    Ope         = CargaVariables.LeerCSV(2, "AttnCliente", "Liberacion de Deposito");
                    AtencionAlCliente.LiberacionDeposito(WebDrive, SelenElem, reporte, DN);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                
                case "Agregar Lista Roja":                     
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Roja", "Agregar Lista Roja");
                    Edo         = CargaVariables.LeerCSV(1, "Lista_Roja", "Agregar Lista Roja");
                    Ope         = CargaVariables.LeerCSV(2, "Lista_Roja", "Agregar Lista Roja");
                    AtencionAlCliente.AgregaListaRoja(WebDrive, SelenElem, reporte, RFC);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Eliminar Lista Roja":                     
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Roja", "Eliminar Lista Roja");
                    Edo         = CargaVariables.LeerCSV(1, "Lista_Roja", "Eliminar Lista Roja");
                    Ope         = CargaVariables.LeerCSV(2, "Lista_Roja", "Eliminar Lista Roja");
                    AtencionAlCliente.EliminarListaRoja(WebDrive, SelenElem, reporte, RFC);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
            //////////////////////////INFO CLIENTE ////////////////
                case "Consulta la Información de Cliente - Prepago":                     
                    DN          = CargaVariables.LeerCSV(0, "Info_Cliente", "Consulta Info Cliente");
                    Codigo      = CargaVariables.LeerCSV(1, "Info_Cliente", "Consulta Info Cliente");
                    Edo         = CargaVariables.LeerCSV(2, "Info_Cliente", "Consulta Info Cliente");
                    Ope         = CargaVariables.LeerCSV(3, "Info_Cliente", "Consulta Info Cliente");
                    AtencionAlCliente.ConsultaInfoCliente(WebDrive, SelenElem, reporte, DN, Codigo);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                      
                case "Consulta la Información de Cuenta - Prepago":                     
                    DN          = CargaVariables.LeerCSV(0, "Info_Cliente", "Consulta Info Cuenta");
                    Codigo      = CargaVariables.LeerCSV(1, "Info_Cliente", "Consulta Info Cuenta");
                    Edo         = CargaVariables.LeerCSV(2, "Info_Cliente", "Consulta Info Cuenta");
                    Ope         = CargaVariables.LeerCSV(3, "Info_Cliente", "Consulta Info Cuenta");
                    AtencionAlCliente.ConsultaInfoCuenta(WebDrive, SelenElem, reporte, DN, Codigo);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Consulta la Información del Suscriptor - Prepago":                     
                    DN          = CargaVariables.LeerCSV(0, "Info_Cliente", "Consulta Info Suscriptor");
                    Codigo      = CargaVariables.LeerCSV(1, "Info_Cliente", "Consulta Info Suscriptor");
                    Edo         = CargaVariables.LeerCSV(2, "Info_Cliente", "Consulta Info Suscriptor");
                    Ope         = CargaVariables.LeerCSV(3, "Info_Cliente", "Consulta Info Suscriptor");
                    AtencionAlCliente.ConsultaInfoSuscriptor(WebDrive, SelenElem, reporte, DN, Codigo);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Consulta de Saldo - Prepago":                     
                    DN          = CargaVariables.LeerCSV(0, "Info_Cliente", "Consulta Saldo - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "Info_Cliente", "Consulta Saldo - Prepago");
                    Edo         = CargaVariables.LeerCSV(2, "Info_Cliente", "Consulta Saldo - Prepago");
                    Ope         = CargaVariables.LeerCSV(3, "Info_Cliente", "Consulta Saldo - Prepago");
                    AtencionAlCliente.ConsultaSaldo(WebDrive, SelenElem, reporte, DN, Codigo);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Cambiar Info Cuenta (OrdenesPendientes) - Prepago":                     
                    DN          = CargaVariables.LeerCSV(0, "Info_Cliente", "Cambiar Info Cuenta (OrdenesPendientes) - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "Info_Cliente", "Cambiar Info Cuenta (OrdenesPendientes) - Prepago");
                    Edo         = CargaVariables.LeerCSV(2, "Info_Cliente", "Cambiar Info Cuenta (OrdenesPendientes) - Prepago");
                    Ope         = CargaVariables.LeerCSV(3, "Info_Cliente", "Cambiar Info Cuenta (OrdenesPendientes) - Prepago");
                    InfoCliente.CambiaInfoCuentaOrdPend(WebDrive, SelenElem, reporte, DN, Codigo);   
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                   
                case "Incremento de Balance de Caja":                     
                    User        = CargaVariables.LeerCSV(0, "AttnCliente", "Incremento Balance de Caja");
                    Contra      = CargaVariables.LeerCSV(1, "AttnCliente", "Incremento Balance de Caja");
                    Monto       = CargaVariables.LeerCSV(2, "AttnCliente", "Incremento Balance de Caja");
                    Edo         = CargaVariables.LeerCSV(3, "AttnCliente", "Incremento Balance de Caja");
                    Ope         = CargaVariables.LeerCSV(4, "AttnCliente", "Incremento Balance de Caja");
                    AtencionAlCliente.IncrementoBalanceCaja(WebDrive, SelenElem, reporte, User, Contra, Monto);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Decremento de Balance de Caja":                     
                    User        = CargaVariables.LeerCSV(0, "AttnCliente", "DecrementoBalance de Caja");
                    Contra      = CargaVariables.LeerCSV(1, "AttnCliente", "DecrementoBalance de Caja");
                    Monto       = CargaVariables.LeerCSV(2, "AttnCliente", "DecrementoBalance de Caja");
                    Edo         = CargaVariables.LeerCSV(3, "AttnCliente", "DecrementoBalance de Caja");
                    Ope         = CargaVariables.LeerCSV(4, "AttnCliente", "DecrementoBalance de Caja");
                    AtencionAlCliente.DecrementoBalanceCaja(WebDrive, SelenElem, reporte, User, Contra, Monto);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Reporte Diario":                     
                    Dia        = CargaVariables.LeerCSV(0, "AttnCliente", "Reporte Diario");
                    Mes        = CargaVariables.LeerCSV(1, "AttnCliente", "Reporte Diario");
                    Anio       = CargaVariables.LeerCSV(2, "AttnCliente", "Reporte Diario");
                    Edo        = CargaVariables.LeerCSV(3, "AttnCliente", "Reporte Diario");
                    Ope        = CargaVariables.LeerCSV(4, "AttnCliente", "Reporte Diario");
                    Fecha = Dia+"."+Mes+"."+Anio;
                    AtencionAlCliente.ReporteDiario(WebDrive, SelenElem, reporte, Fecha);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
            /////////Validacion Errores///////////        
                case "Generar RFC":       
                    ValidacionErrores.GenerarRFC(WebDrive, SelenElem, reporte, "Pospago");                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                
                case "Valida PIPE Info Cuenta":       
                    ValidacionErrores.ValidaInfoCliPIPE(WebDrive, SelenElem, reporte, "Pospago");                   
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                   
                case "Valida PIPE Direccion de Entrega":       
                    ValidacionErrores.ValidaInfoDireccionPIPE(WebDrive, SelenElem, reporte, "Pospago");                     
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Valida PIPE Calle Direccion Fiscal":       
                    ValidacionErrores.ValidaInfoCalleDireccionPIPE(WebDrive, SelenElem, reporte, "Pospago");                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                
                case "Valida PIPE Num Int y Ext Direccion Fiscal":       
                    ValidacionErrores.ValidaInfoCalleDireccionPIPE(WebDrive, SelenElem, reporte, "Pospago");                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Valida RFC Existente":       
                    ValidacionErrores.RFCExistente(WebDrive, SelenElem, reporte, "Pospago");                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                //////////Pymes//////////////
                case "Generar RFC - Pymes":       
                    ValidacionErrores.GenerarRFC(WebDrive, SelenElem, reporte, "Pymes");                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                
                case "Valida PIPE Info Cuenta - Pymes":         
                    ValidacionErrores.ValidaInfoCliPIPE(WebDrive, SelenElem, reporte, "Pymes");                     
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                   
                case "Valida PIPE Direccion de Entrega - Pymes":        
                    ValidacionErrores.ValidaInfoDireccionPIPE(WebDrive, SelenElem, reporte, "Pymes");                     
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Valida PIPE Calle Direccion Fiscal - Pymes":        
                    ValidacionErrores.ValidaInfoCalleDireccionPIPE(WebDrive, SelenElem, reporte, "Pymes");                     
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                
                case "Valida PIPE Num Int y Ext Direccion Fiscal - Pymes":        
                    ValidacionErrores.ValidaInfoCalleDireccionPIPE(WebDrive, SelenElem, reporte, "Pymes");                     
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Valida RFC Existente - Pymes":         
                    ValidacionErrores.RFCExistente(WebDrive, SelenElem, reporte, "Pymes");                     
                    Resultado.setText("Ejecucion Exitosa");
                    break;
     /////////////////////////////////////////////////////////////////////////               
                case "Cambiar Informacion de Cuenta - Hibrido":
                    DN          = CargaVariables.LeerCSV(0, "Info_Cliente", "Cambio Info Cuenta - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "Info_Cliente", "Cambio Info Cuenta - Hibrido");
                    Edo         = CargaVariables.LeerCSV(2, "Info_Cliente", "Cambio Info Cuenta - Hibrido");
                    Ope         = CargaVariables.LeerCSV(3, "Info_Cliente", "Cambio Info Cuenta - Hibrido");
                    InfoCliente.CambiaInfoCuenta(WebDrive, SelenElem, reporte, DN, Codigo);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                 
                case "Cambiar Informacion de Suscriptor - Hibrido":
                    DN          = CargaVariables.LeerCSV(0, "Info_Cliente", "Cambio Info Suscriptor - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "Info_Cliente", "Cambio Info Suscriptor - Hibrido");
                    Edo         = CargaVariables.LeerCSV(2, "Info_Cliente", "Cambio Info Suscriptor - Hibrido");
                    Ope         = CargaVariables.LeerCSV(3, "Info_Cliente", "Cambio Info Suscriptor - Hibrido");
                    InfoCliente.CambiaInfoSuscriptor(WebDrive, SelenElem, reporte, DN, Codigo);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Batch actualizacion en precio de handsets venta":
                    Equipo          = CargaVariables.LeerCSV(0, "Batch", "Batch actualizacion en precio de handsets venta");
                    SKU             = CargaVariables.LeerCSV(1, "Batch", "Batch actualizacion en precio de handsets venta");
                    Meses           = CargaVariables.LeerCSV(2, "Batch", "Batch actualizacion en precio de handsets venta");
                    CodigoOferta    = CargaVariables.LeerCSV(3, "Batch", "Batch actualizacion en precio de handsets venta");
                    Tipo            = CargaVariables.LeerCSV(4, "Batch", "Batch actualizacion en precio de handsets venta");
                    PrecioActual    = CargaVariables.LeerCSV(5, "Batch", "Batch actualizacion en precio de handsets venta");
                    PrecioNuevo     = CargaVariables.LeerCSV(6, "Batch", "Batch actualizacion en precio de handsets venta");
                    NomArchivo      = CargaVariables.LeerCSV(7, "Batch", "Batch actualizacion en precio de handsets venta");
                    DN              = CargaVariables.LeerCSV(8, "Batch", "Batch actualizacion en precio de handsets venta");
                    Edo             = CargaVariables.LeerCSV(9, "Batch", "Batch actualizacion en precio de handsets venta");
                    Ope             = CargaVariables.LeerCSV(10,"Batch", "Batch actualizacion en precio de handsets venta");
                    Batch.BatchPrecioVenta(WebDrive, SelenElem, reporte, Equipo, SKU, Meses, CodigoOferta, Tipo, PrecioNuevo, DN);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Batch actualizacion en precio de handsets renovacion":
                    Equipo          = CargaVariables.LeerCSV(0, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    SKU             = CargaVariables.LeerCSV(1, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    Meses           = CargaVariables.LeerCSV(2, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    CodigoOferta    = CargaVariables.LeerCSV(3, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    Tipo            = CargaVariables.LeerCSV(4, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    PrecioActual    = CargaVariables.LeerCSV(5, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    PrecioNuevo     = CargaVariables.LeerCSV(6, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    NomArchivo      = CargaVariables.LeerCSV(7, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    DN              = CargaVariables.LeerCSV(8, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    Edo             = CargaVariables.LeerCSV(9, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    Ope             = CargaVariables.LeerCSV(10, "Batch", "Batch actualizacion en precio de handsets renovacion");
                    Batch.BatchPrecioReno(WebDrive, SelenElem, reporte, Equipo, SKU, Meses, CodigoOferta, Tipo, PrecioNuevo, DN);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
     
                case "Batch Changing Customer Category":
                    DN              = CargaVariables.LeerCSV(0, "Batch", "Batch Changing Customer Category");
                    Codigo          = CargaVariables.LeerCSV(1, "Batch", "Batch Changing Customer Category");
                    CodigoCu        = CargaVariables.LeerCSV(2, "Batch", "Batch Changing Customer Category");
                    CategoriaAct    = CargaVariables.LeerCSV(3, "Batch", "Batch Changing Customer Category");
                    NuevaCategoria  = CargaVariables.LeerCSV(4, "Batch", "Batch Changing Customer Category");
                    NomArchivo      = CargaVariables.LeerCSV(5, "Batch", "Batch Changing Customer Category");
                    Edo             = CargaVariables.LeerCSV(6, "Batch", "Batch Changing Customer Category");
                    Ope             = CargaVariables.LeerCSV(7, "Batch", "Batch Changing Customer Category");
                    Batch.BatchCostumerCategory(WebDrive, SelenElem, reporte, DN, Codigo, NuevaCategoria);
                    Resultado.setText("Ejecucion Exitosa");
                    break;

                case "Batch Update Payment Method - Store Card Number":
                    DN          = CargaVariables.LeerCSV(0, "Batch", "Batch Update Payment MethodStore Card Number");
                    Codigo      = CargaVariables.LeerCSV(1, "Batch", "Batch Update Payment MethodStore Card Number");
                    CodigoCu    = CargaVariables.LeerCSV(2, "Batch", "Batch Update Payment MethodStore Card Number");
                    NomArchivo  = CargaVariables.LeerCSV(3, "Batch", "Batch Update Payment MethodStore Card Number");
                    Edo         = CargaVariables.LeerCSV(4, "Batch", "Batch Update Payment MethodStore Card Number");
                    Ope         = CargaVariables.LeerCSV(5, "Batch", "Batch Update Payment MethodStore Card Number");
                    Batch.BatchPymentMethod(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Suspend on specific day":
                    DN          = CargaVariables.LeerCSV(0, "Suspencion", "Suspencion en dia espesifico");
                    Codigo      = CargaVariables.LeerCSV(1, "Suspencion", "Suspencion en dia espesifico");
                    Fecha       = CargaVariables.LeerCSV(2, "Suspencion", "Suspencion en dia espesifico");
                    Edo         = CargaVariables.LeerCSV(3, "Suspencion", "Suspencion en dia espesifico");
                    Ope         = CargaVariables.LeerCSV(4, "Suspencion", "Suspencion en dia espesifico");
                    SuspencionReactivacion.SuspencionDiaEspesifico(WebDrive, SelenElem, reporte, DN, Codigo, Fecha,true);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Suspension Bidireccional - Hibrido":
                    DN          = CargaVariables.LeerCSV(0, "Suspencion", "Suspension Bidireccional - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "Suspencion", "Suspension Bidireccional - Hibrido");
                    Edo         = CargaVariables.LeerCSV(2, "Suspencion", "Suspension Bidireccional - Hibrido");
                    Ope         = CargaVariables.LeerCSV(3, "Suspencion", "Suspension Bidireccional - Hibrido");
                    SuspencionReactivacion.SuspencionDiaEspesifico(WebDrive, SelenElem, reporte, DN, Codigo, "",false);
                    Resultado.setText("En ejecucion");                    
                    break;
                 
                case "Reactivacion Bidireccional - Hibrido":
                    DN          = CargaVariables.LeerCSV(0, "Suspencion", "Reactivacion Bidireccional - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "Suspencion", "Reactivacion Bidireccional - Hibrido");
                    Edo         = CargaVariables.LeerCSV(2, "Suspencion", "Reactivacion Bidireccional - Hibrido");
                    Ope         = CargaVariables.LeerCSV(3, "Suspencion", "Reactivacion Bidireccional - Hibrido");
                    SuspencionReactivacion.Reactivacion(WebDrive, SelenElem, reporte, DN, Codigo, "",false);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Barring pospaid subscriber - Pospago":
                    DN          = CargaVariables.LeerCSV(0, "Suspencion", "Barring pospaid subscriber - Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "Suspencion", "Barring pospaid subscriber - Pospago");
                    Edo         = CargaVariables.LeerCSV(2, "Suspencion", "Barring pospaid subscriber - Pospago");
                    Ope         = CargaVariables.LeerCSV(3, "Suspencion", "Barring pospaid subscriber - Pospago");
                    SuspencionReactivacion.BarringPospago(WebDrive, SelenElem, reporte, DN);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Unarring pospaid subscriber - Pospago":
                    DN          = CargaVariables.LeerCSV(0, "Suspencion", "Unbarring pospaid subscriber - Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "Suspencion", "Unbarring pospaid subscriber - Pospago");
                    Edo         = CargaVariables.LeerCSV(2, "Suspencion", "Unbarring pospaid subscriber - Pospago");
                    Ope         = CargaVariables.LeerCSV(3, "Suspencion", "Unbarring pospaid subscriber - Pospago");
                    SuspencionReactivacion.UnbarringPospago(WebDrive, SelenElem, reporte, DN);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Reporte de Robo y Extravio - Prepago":
                    DN          = CargaVariables.LeerCSV(0, "ReporteRobo", "Reporte de Robo y Extravio - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "ReporteRobo", "Reporte de Robo y Extravio - Prepago");
                    Edo         = CargaVariables.LeerCSV(2, "ReporteRobo", "Reporte de Robo y Extravio - Prepago");
                    Ope         = CargaVariables.LeerCSV(3, "ReporteRobo", "Reporte de Robo y Extravio - Prepago");
                    ReporteRobo.ReportePrepago(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Cancelacion Reporte de Robo y Extravio - Prepago":
                    DN          = CargaVariables.LeerCSV(0, "ReporteRobo", "Cancelacion Reporte de Robo y Extravio - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "ReporteRobo", "Cancelacion Reporte de Robo y Extravio - Prepago");
                    Edo         = CargaVariables.LeerCSV(2, "ReporteRobo", "Cancelacion Reporte de Robo y Extravio - Prepago");
                    Ope         = CargaVariables.LeerCSV(3, "ReporteRobo", "Cancelacion Reporte de Robo y Extravio - Prepago");
                    ReporteRobo.CancelarReportePrepago(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Reporte de Robo y Extravio - Hibrido":
                    DN          = CargaVariables.LeerCSV(0, "ReporteRobo", "Reporte de Robo y Extravio - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "ReporteRobo", "Reporte de Robo y Extravio - Hibrido");
                    Edo         = CargaVariables.LeerCSV(2, "ReporteRobo", "Reporte de Robo y Extravio - Hibrido");
                    Ope         = CargaVariables.LeerCSV(3, "ReporteRobo", "Reporte de Robo y Extravio - Hibrido");
                    ReporteRobo.ReporteHibrido(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Cancelacion Reporte de Robo y Extravio - Hibrido":
                    DN          = CargaVariables.LeerCSV(0, "ReporteRobo", "Cancelacion Reporte de Robo y Extravio - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "ReporteRobo", "Cancelacion Reporte de Robo y Extravio - Hibrido");
                    Edo         = CargaVariables.LeerCSV(2, "ReporteRobo", "Cancelacion Reporte de Robo y Extravio - Hibrido");
                    Ope         = CargaVariables.LeerCSV(3, "ReporteRobo", "Cancelacion Reporte de Robo y Extravio - Hibrido");
                    ReporteRobo.CancelarReportePrepago(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Reporte de Robo y Extravio con IMEI":
                    DN          = CargaVariables.LeerCSV(0, "ReporteRobo", "Reporte de Robo y Extravio con IMEI");
                    Codigo      = CargaVariables.LeerCSV(1, "ReporteRobo", "Reporte de Robo y Extravio con IMEI");
                    IMEI        = CargaVariables.LeerCSV(2, "ReporteRobo", "Reporte de Robo y Extravio con IMEI");
                    Edo         = CargaVariables.LeerCSV(3, "ReporteRobo", "Reporte de Robo y Extravio con IMEI");
                    Ope         = CargaVariables.LeerCSV(4, "ReporteRobo", "Reporte de Robo y Extravio con IMEI");
                    ReporteRobo.ReporteIMEI(WebDrive, SelenElem, reporte, DN, Codigo, IMEI);
                    Resultado.setText("En ejecucion"); 
                    break;
                
                case "Cambio Oferta Primaria - Prepago":
                    DN          = CargaVariables.LeerCSV(0, "AdminOfer", "Cambio Oferta Primaria - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "AdminOfer", "Cambio Oferta Primaria - Prepago");
                    Plan        = CargaVariables.LeerCSV(2, "AdminOfer", "Cambio Oferta Primaria - Prepago");
                    Edo         = CargaVariables.LeerCSV(3, "AdminOfer", "Cambio Oferta Primaria - Prepago");
                    Ope         = CargaVariables.LeerCSV(4, "AdminOfer", "Cambio Oferta Primaria - Prepago");
                    AdministracionOferta.CambioOferPrimPrepago(WebDrive, SelenElem, reporte, DN, Codigo, Plan);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Cambio Oferta Primaria (Upgrade) - Pospago":
                    DN          = CargaVariables.LeerCSV(0, "AdminOfer", "Cambio Oferta Primaria - Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "AdminOfer", "Cambio Oferta Primaria - Pospago");
                    Plan        = CargaVariables.LeerCSV(2, "AdminOfer", "Cambio Oferta Primaria - Pospago");
                    Edo         = CargaVariables.LeerCSV(3, "AdminOfer", "Cambio Oferta Primaria - Pospago");
                    Ope         = CargaVariables.LeerCSV(4, "AdminOfer", "Cambio Oferta Primaria - Pospago");
                    AdministracionOferta.CambioOferPrimPrepago(WebDrive, SelenElem, reporte, DN, Codigo, Plan);
                    Resultado.setText("En ejecucion");
                    break;
                /////**************************************************************
                case "Agregar Oferta Suplementaria - Prepago":
                    DN          = CargaVariables.LeerCSV(0, "AdminOfer", "Agregar Oferta Suplementaria - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "AdminOfer", "Agregar Oferta Suplementaria - Prepago");
                    Plan        = CargaVariables.LeerCSV(2, "AdminOfer", "Agregar Oferta Suplementaria - Prepago");
                    Edo         = CargaVariables.LeerCSV(3, "AdminOfer", "Agregar Oferta Suplementaria - Prepago");
                    Ope         = CargaVariables.LeerCSV(4, "AdminOfer", "Agregar Oferta Suplementaria - Prepago");
                    AdministracionOferta.AgregarOferSupl(WebDrive, SelenElem, reporte, DN, Codigo, Plan);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Agregar Oferta Suplementaria - Pospago":
                    DN          = CargaVariables.LeerCSV(0, "AdminOfer", "Agregar Oferta Suplementaria - Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "AdminOfer", "Agregar Oferta Suplementaria - Pospago");
                    Plan        = CargaVariables.LeerCSV(2, "AdminOfer", "Agregar Oferta Suplementaria - Pospago");
                    Edo         = CargaVariables.LeerCSV(3, "AdminOfer", "Agregar Oferta Suplementaria - Pospago");
                    Ope         = CargaVariables.LeerCSV(4, "AdminOfer", "Agregar Oferta Suplementaria - Pospago");
                    AdministracionOferta.AgregarOferSuplPos(WebDrive, SelenElem, reporte, DN, Codigo, Plan);
                    Resultado.setText("En ejecucion");
                    break;
 //////////////////////////////******************************///////////////////////                 
                case "Eliminar Oferta Suplementaria - Hibrido":
                    DN          = CargaVariables.LeerCSV(0, "AdminOfer", "Eliminar Oferta Suplementaria - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "AdminOfer", "Eliminar Oferta Suplementaria - Hibrido");
                    Plan        = CargaVariables.LeerCSV(2, "AdminOfer", "Eliminar Oferta Suplementaria - Hibrido");
                    Edo         = CargaVariables.LeerCSV(3, "AdminOfer", "Eliminar Oferta Suplementaria - Hibrido");
                    Ope         = CargaVariables.LeerCSV(4, "AdminOfer", "Eliminar Oferta Suplementaria - Hibrido");
                    AdministracionOferta.EliminarOferHibrido(WebDrive, SelenElem, reporte, DN, Codigo, Plan);
                    Resultado.setText("En ejecucion");
                    break;
                ////****************************************    
                case "Carga De Oferta Suplementaria Por Batch":
                    DN          = CargaVariables.LeerCSV(0, "AdminOfer", "Carga De Oferta Suplementaria Por Batch");
                    Segento     = CargaVariables.LeerCSV(1, "AdminOfer", "Carga De Oferta Suplementaria Por Batch");
                    Plan        = CargaVariables.LeerCSV(2, "AdminOfer", "Carga De Oferta Suplementaria Por Batch");
                    Edo         = CargaVariables.LeerCSV(3, "AdminOfer", "Carga De Oferta Suplementaria Por Batch");
                    Ope         = CargaVariables.LeerCSV(4, "AdminOfer", "Carga De Oferta Suplementaria Por Batch");
                    AdministracionOferta.Cambia_Oferta_Sup(WebDrive, SelenElem, reporte, DN, Segento,Plan);
                    Resultado.setText("En ejecucion");
                    break;
                     
                case "Batch descuento variable oferta primaria":
                    DN          = CargaVariables.LeerCSV(0, "AdminOfer", "Batch descuento variable oferta primaria");
                    Segento     = CargaVariables.LeerCSV(1, "AdminOfer", "Batch descuento variable oferta primaria");
                    Bono        = CargaVariables.LeerCSV(2, "AdminOfer", "Batch descuento variable oferta primaria");
                    Monto       = CargaVariables.LeerCSV(3, "AdminOfer", "Batch descuento variable oferta primaria");
                    AdministracionOferta.Desc_Oferta_Pri(WebDrive, SelenElem, reporte, DN, Segento,Bono,Monto);
                    Resultado.setText("En ejecucion");
                    break;
                
                case "Batch descuento variable oferta suplementaria":
                    DN          = CargaVariables.LeerCSV(0, "AdminOfer", "Batch descuento variable oferta suplementaria");
                    Segento     = CargaVariables.LeerCSV(1, "AdminOfer", "Batch descuento variable oferta suplementaria");
                    Bono        = CargaVariables.LeerCSV(2, "AdminOfer", "Batch descuento variable oferta suplementaria");
                    Monto       = CargaVariables.LeerCSV(3, "AdminOfer", "Batch descuento variable oferta suplementaria");
                    AdministracionOferta.Desc_Oferta_Pri(WebDrive, SelenElem, reporte, DN, Segento,Bono,Monto);
                    Resultado.setText("En ejecucion");
                    break;
                    
//////////////////////////////Migraciones////////////////////////////////////////////////////////////////
                    
                case "Migración de Prepago a Híbrido":
                    DN          = CargaVariables.LeerCSV(0, "MigraReno", "Migración de Prepago a Híbrido");
                    Codigo      = CargaVariables.LeerCSV(1, "MigraReno", "Migración de Prepago a Híbrido");
                    DNFact      = CargaVariables.LeerCSV(2, "MigraReno", "Migración de Prepago a Híbrido");
                    Plan        = CargaVariables.LeerCSV(3, "MigraReno", "Migración de Prepago a Híbrido");
                    MigracionReno.MigraPrepHibri(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Migración de Prepago a Pospago":
                    DN          = CargaVariables.LeerCSV(0, "MigraReno", "Migración de Prepago a Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "MigraReno", "Migración de Prepago a Pospago");
                    DNFact      = CargaVariables.LeerCSV(2, "MigraReno", "Migración de Prepago a Pospago");
                    Plan        = CargaVariables.LeerCSV(3, "MigraReno", "Migración de Prepago a Pospago");
                    MigracionReno.MigraPrepPosp(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Migración de Híbrido a Pospago":
                    DN          = CargaVariables.LeerCSV(0, "MigraReno", "Migración de Híbrido a Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "MigraReno", "Migración de Híbrido a Pospago");
                    DNFact      = CargaVariables.LeerCSV(2, "MigraReno", "Migración de Híbrido a Pospago");
                    Plan        = CargaVariables.LeerCSV(3, "MigraReno", "Migración de Híbrido a Pospago");
                    MigracionReno.MigraHibriPosp(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break; 
                     
                case "Migración de Pospago a Híbrido":
                    DN          = CargaVariables.LeerCSV(0, "MigraReno", "Migración de Pospago a Híbrido");
                    Codigo      = CargaVariables.LeerCSV(1, "MigraReno", "Migración de Pospago a Híbrido");
                    DNFact      = CargaVariables.LeerCSV(2, "MigraReno", "Migración de Pospago a Híbrido");
                    Plan        = CargaVariables.LeerCSV(3, "MigraReno", "Migración de Pospago a Híbrido");
                    MigracionReno.MigraPospHibr(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                case "Migración de Híbrido a Prepago":
                    DN          = CargaVariables.LeerCSV(0, "MigraReno", "Migración de Híbrido a Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "MigraReno", "Migración de Híbrido a Prepago");
                    DNFact      = CargaVariables.LeerCSV(2, "MigraReno", "Migración de Híbrido a Prepago");
                    Plan        = CargaVariables.LeerCSV(3, "MigraReno", "Migración de Híbrido a Prepago");
                    MigracionReno.MigraHibriPrep(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Migración de Pospago a Prepago":
                    DN          = CargaVariables.LeerCSV(0, "MigraReno", "Migración de Pospago a Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "MigraReno", "Migración de Pospago a Prepago");
                    DNFact      = CargaVariables.LeerCSV(2, "MigraReno", "Migración de Pospago a Prepago");
                    Plan        = CargaVariables.LeerCSV(3, "MigraReno", "Migración de Pospago a Prepago");
                    MigracionReno.MigraPosPrep(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Renovacion de Contrato (Upgrade) - Hibrido":
                    DN          = CargaVariables.LeerCSV(0, "MigraReno", "Renovacion de Contrato (Upgrade) - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "MigraReno", "Renovacion de Contrato (Upgrade) - Hibrido");
                    Plan        = CargaVariables.LeerCSV(2, "MigraReno", "Renovacion de Contrato (Upgrade) - Hibrido");
                    Equipo      = CargaVariables.LeerCSV(3, "MigraReno", "Renovacion de Contrato (Upgrade) - Hibrido");
                    IMEI        = CargaVariables.LeerCSV(4, "MigraReno", "Renovacion de Contrato (Upgrade) - Hibrido");
                    MigracionReno.RenoUpgrade(WebDrive, SelenElem, reporte, DN, Codigo,Plan,Equipo,IMEI,"HIBRIDO");
                    Resultado.setText("En ejecucion");
                    break;
                     
                case "Renovacion de Contrato (Con Cambio de Oferta) - Pospago":
                    DN          = CargaVariables.LeerCSV(0, "MigraReno", "Renovacion de Contrato (Con Cambio de Oferta) - Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "MigraReno", "Renovacion de Contrato (Con Cambio de Oferta) - Pospago");
                    Plan        = CargaVariables.LeerCSV(2, "MigraReno", "Renovacion de Contrato (Con Cambio de Oferta) - Pospago");
                    Equipo      = CargaVariables.LeerCSV(3, "MigraReno", "Renovacion de Contrato (Con Cambio de Oferta) - Pospago");
                    IMEI        = CargaVariables.LeerCSV(4, "MigraReno", "Renovacion de Contrato (Con Cambio de Oferta) - Pospago");
                    MigracionReno.RenoUpgrade(WebDrive, SelenElem, reporte, DN, Codigo,Plan,Equipo,IMEI,"POSPAGO");
                    Resultado.setText("En ejecucion");
                    break;
  //////////////////////////////VENTAS////////////////////////////////////////////////////////////////
                case "Venta de Solo SIM Con Factura (Cliente Nuevo) - Prepago":
                    SIM         = CargaVariables.LeerCSV(0, "Ventas", "Venta de Solo SIM Con Factura (Cliente Nuevo) - Prepago");
                    Ventas.SoloSIMFact(WebDrive, SelenElem, reporte,SIM);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Venta de Solo SIM Con Factura (Cliente Existente) - Prepago":
                    DN          = CargaVariables.LeerCSV(0, "Ventas", "Venta de Solo SIM Con Factura (Cliente Existente) - Prepago");
                    SIM         = CargaVariables.LeerCSV(1, "Ventas", "Venta de Solo SIM Con Factura (Cliente Existente) - Prepago");
                    Ventas.SoloSIMFactCliEx(WebDrive, SelenElem, reporte,DN, SIM);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Venta de Solo SIM (Cliente Nuevo) - Pospago":
                    SIM         = CargaVariables.LeerCSV(0, "Ventas", "Venta de Solo SIM (Cliente Nuevo) - Pospago");
                    Ventas.SoloSIMPosp(WebDrive, SelenElem, reporte, SIM);
                    Resultado.setText("En ejecucion");
                    break;
                //  Flujo masivo   oferta como variable 
                case "Venta de Solo SIM (Cliente Nuevo) - Hibrido":
                    SIM         = CargaVariables.LeerCSV(0, "Ventas", "Venta de Solo SIM (Cliente Nuevo) - Hibrido");
                    Ventas.SoloSIMHibri(WebDrive, SelenElem, reporte, SIM);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Venta de SIM Con Handset (Cliente Nuevo) - Prepago":
                    SIM         = CargaVariables.LeerCSV(0, "Ventas", "Venta de SIM Con Handset (Cliente Nuevo) - Prepago");
                    Equipo      = CargaVariables.LeerCSV(1, "Ventas", "Venta de SIM Con Handset (Cliente Nuevo) - Prepago");
                    IMEI        = CargaVariables.LeerCSV(2, "Ventas", "Venta de SIM Con Handset (Cliente Nuevo) - Prepago");
                    Ventas.VentaSimconHandset(WebDrive, SelenElem, reporte,SIM,Equipo,IMEI);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Agregar Nueva Linea - Pospago":
                    DN          = CargaVariables.LeerCSV(0, "Ventas", "Agregar Nueva Linea - Pospago");
                    SIM         = CargaVariables.LeerCSV(1, "Ventas", "Agregar Nueva Linea - Pospago");
                    Ventas.AdicionLineaPosp(WebDrive, SelenElem, reporte,DN,SIM);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Liquidacion de Terminal":
                    DN          = CargaVariables.LeerCSV(0, "Ventas", "Liquidacion de Terminal");
                    Codigo      = CargaVariables.LeerCSV(1, "Ventas", "Liquidacion de Terminal");
                    SIM         = CargaVariables.LeerCSV(2, "Ventas", "Liquidacion de Terminal");
                    Ventas.Liquidacion(WebDrive, SelenElem, reporte,DN,Codigo,SIM);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Venta de Equipo a Credito (Cliente Nuevo) - Pospago":
                    SIM          = CargaVariables.LeerCSV(0, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Pospago");
                    Plan         = CargaVariables.LeerCSV(1, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Pospago");
                    Equipo       = CargaVariables.LeerCSV(2, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Pospago");
                    IMEI         = CargaVariables.LeerCSV(3, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Pospago");
                    Meses        = CargaVariables.LeerCSV(4, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Pospago");
                    Ventas.VentaCrediPosp(WebDrive, SelenElem, reporte,SIM,Plan,Equipo,IMEI,Meses);
                    Resultado.setText("En ejecucion");
                    break;
                          
                case "Venta de Equipo a Credito (Cliente Nuevo) - Hibrido":
                    SIM          = CargaVariables.LeerCSV(0, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Hibrido");
                    Plan         = CargaVariables.LeerCSV(1, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Hibrido");
                    Equipo       = CargaVariables.LeerCSV(2, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Hibrido");
                    IMEI         = CargaVariables.LeerCSV(3, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Hibrido");
                    Meses        = CargaVariables.LeerCSV(4, "Ventas", "Venta de Equipo a Credito (Cliente Nuevo) - Hibrido");
                    Ventas.VentaCrediHibr(WebDrive, SelenElem, reporte,SIM,Plan,Equipo,IMEI,Meses);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Venta Pospago Solo SIM Especialista":
                    SIM          = CargaVariables.LeerCSV(0, "Ventas", "Venta Pospago Solo SIM Especialista");
                    Plan         = CargaVariables.LeerCSV(1, "Ventas", "Venta Pospago Solo SIM Especialista");
                    Operador     = CargaVariables.LeerCSV(2, "Ventas", "Venta Pospago Solo SIM Especialista");
                    Ventas.SoloSIMESP(WebDrive, SelenElem, reporte,SIM,Plan,Operador);
                    Resultado.setText("En ejecucion");
                    break; 
    //////////////////////////////Consolidacion y Cesion de derechos////////////////////////////////////////////////////////////////
                case "Consolidacón de cuenta - Pospago":
                    DN         = CargaVariables.LeerCSV(0, "ConDesCes", "Consolidacón de cuenta - Pospago");
                    Codigo     = CargaVariables.LeerCSV(1, "ConDesCes", "Consolidacón de cuenta - Pospago");
                    Cuenta     = CargaVariables.LeerCSV(2, "ConDesCes", "Consolidacón de cuenta - Pospago");
                    Comentario = CargaVariables.LeerCSV(3, "ConDesCes", "Consolidacón de cuenta - Pospago");
                    ConsolidacionCesiondeDerechos.ConsolidacionPosp(WebDrive, SelenElem, reporte,DN,Codigo,Cuenta,Comentario);
                    Resultado.setText("En ejecucion");
                    break;   
                  
                case "Desconsolidacón de cuenta - Hibrido":
                    DN         = CargaVariables.LeerCSV(0, "ConDesCes", "Desconsolidacón de cuenta - Hibrido");
                    Codigo     = CargaVariables.LeerCSV(1, "ConDesCes", "Desconsolidacón de cuenta - Hibrido");
                    Comentario = CargaVariables.LeerCSV(2, "ConDesCes", "Desconsolidacón de cuenta - Hibrido");
                    Ciclo      = CargaVariables.LeerCSV(3, "ConDesCes", "Desconsolidacón de cuenta - Hibrido");
                    ConsolidacionCesiondeDerechos.DesonsolidacionHib(WebDrive, SelenElem, reporte,DN,Codigo,Comentario,Ciclo);
                    Resultado.setText("En ejecucion");
                    break;
                     
                case "Cesión de Derechos - Híbrido":
                    DN         = CargaVariables.LeerCSV(0, "ConDesCes", "Cesión de Derechos - Híbrido");
                    Codigo     = CargaVariables.LeerCSV(1, "ConDesCes", "Cesión de Derechos - Híbrido");
                    DNCesion = CargaVariables.LeerCSV(2, "ConDesCes", "Cesión de Derechos - Híbrido");
                    Ciclo      = CargaVariables.LeerCSV(3, "ConDesCes", "Cesión de Derechos - Híbrido");
                    ConsolidacionCesiondeDerechos.CesionDerechos(WebDrive, SelenElem, reporte,DN,Codigo,DNCesion,Ciclo);
                    Resultado.setText("En ejecucion");
                    break;
    ////////////////////////////// Lista Negra ////////////////////////////////////////////////////////////////                
                case "Gestionar Lista Negra (Agregar)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Gestionar Lista Negra (Agregar)");
                    ListaNegra.Agregar(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                case "Nuevo Cliente Pospago (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Nuevo Cliente Pospago (Lista Negra)");
                    ListaNegra.NewClientPosp(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break;
                     
                case "Cambiar Informacion Cliente (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Cambiar Informacion Cliente (Lista Negra)");
                    ListaNegra.CambiarInfoCli(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break;
                     
                case "MNP Port in Pospago (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "MNP Port in Pospago (Lista Negra)");
                    DN          = CargaVariables.LeerCSV(1, "Lista_Negra", "MNP Port in Pospago (Lista Negra)");
                    ListaNegra.PortaListaNegra(WebDrive, SelenElem, reporte, RFC, DN);
                    Resultado.setText("En ejecucion");
                    break;     
                    
                case "Venta de Equipo a Cliente Existente (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Venta de Equipo a Cliente Existente (Lista Negra)");
                    ListaNegra.VentaListaNegra(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                case "Cesion de derechos (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Cesion de derechos (Lista Negra)");
                    ListaNegra.CesionListaNegra(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Cambiar Informacion de Cuenta (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Cambiar Informacion de Cuenta (Lista Negra)");
                    Cuenta      = CargaVariables.LeerCSV(1, "Lista_Negra", "Cambiar Informacion de Cuenta (Lista Negra)");
                    ListaNegra.CambiarInfoCuenta(WebDrive, SelenElem, reporte, RFC, Cuenta);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Migracion Prepago a Pospago (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Migracion Prepago a Pospago (Lista Negra)");
                    ListaNegra.MigraListaNegra(WebDrive, SelenElem, reporte, RFC,"Migracion PRE-POS");
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Migracion Prepago a Hibrido (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Migracion Prepago a Hibrido (Lista Negra)");
                    ListaNegra.MigraListaNegra(WebDrive, SelenElem, reporte, RFC,"Migracion PRE-CTRL");
                    Resultado.setText("En ejecucion");
                    break;
                  
                case "Quitar RFC (Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Quitar RFC (Lista Negra)");
                    ListaNegra.Eliminar(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Nuevo Cliente Pospago (Quitar Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Nuevo Cliente Pospago (Lista Negra)");
                    ListaNegra.NewClientPosp2(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Cambiar Informacion Cliente (Quitar Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Cambiar Informacion Cliente (Lista Negra)");
                    ListaNegra.CambiarInfoCli2(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break;
                     //Pendiente para revisar von bypass
                case "MNP Port in Pospago (Quitar Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "MNP Port in Pospago (Lista Negra)");
                    DN          = CargaVariables.LeerCSV(1, "Lista_Negra", "MNP Port in Pospago (Lista Negra)");
                    ListaNegra.PortaListaNegra2(WebDrive, SelenElem, reporte, RFC, DN);
                    Resultado.setText("En ejecucion");
                    break;

                case "Venta de Equipo a Cliente Existente (Quitar Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Venta de Equipo a Cliente Existente (Lista Negra)");
                    ListaNegra.VentaListaNegra2(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                case "Cesion de derechos (Quitar Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Cesion de derechos (Lista Negra)");
                    ListaNegra.CesionListaNegra2(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break;                    
                
                case "Cambiar Informacion de Cuenta (Quitar Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Cambiar Informacion de Cuenta (Lista Negra)");
                    Cuenta      = CargaVariables.LeerCSV(1, "Lista_Negra", "Cambiar Informacion de Cuenta (Lista Negra)");
                    ListaNegra.CambiarInfoCuenta2(WebDrive, SelenElem, reporte, RFC, Cuenta);
                    Resultado.setText("En ejecucion");
                    break;  
                    
                case "Migracion Prepago a Pospago (Quitar Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Migracion Prepago a Pospago (Lista Negra)");
                    ListaNegra.MigraListaNegra2(WebDrive, SelenElem, reporte, RFC,"Migracion PRE-POS");
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Migracion Prepago a Hibrido (Quitar Lista Negra)":
                    RFC         = CargaVariables.LeerCSV(0, "Lista_Negra", "Migracion Prepago a Hibrido (Lista Negra)");
                    ListaNegra.MigraListaNegra2(WebDrive, SelenElem, reporte, RFC,"Migracion PRE-CTRL");
                    Resultado.setText("En ejecucion");
                    break;
            ////////////////////////////////// Lista Negra Finalizado////////////////////////////////////////////////////////////////  
                 case "Cambiar Información del Suscriptor Lista Negra":
                    DN         = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Cambio Info Suscriptor");
                    Codigo     = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Cambio Info Suscriptor");
                    InfoCliente.CambiaInfoCuenta(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");
                    break;    
                    
                    case "Cambio de SIM (Sin Costo) Lista Negra":   
                    DN      = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Cambio de SIM (Sin Costo)");
                    Codigo  = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Cambio de SIM (Sin Costo)");
                    ICC     = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Cambio de SIM (Sin Costo)");
                    Causa   = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Cambio de SIM (Sin Costo)");
                    Edo     = CargaVariables.LeerCSV(4, "ListaNegraFinalizado", "Cambio de SIM (Sin Costo)");
                    Ope     = CargaVariables.LeerCSV(5, "ListaNegraFinalizado", "Cambio de SIM (Sin Costo)");
                    AtencionAlCliente.CambioDeSimSinCostoPospago(WebDrive, SelenElem, DN, Codigo, ICC,Causa, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Cambio Oferta Primaria Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Cambio Oferta Primaria");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Cambio Oferta Primaria");
                    Plan        = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Cambio Oferta Primaria");
                    Edo         = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Cambio Oferta Primaria");
                    Ope         = CargaVariables.LeerCSV(4, "ListaNegraFinalizado", "Cambio Oferta Primaria");
                    AdministracionOferta.CambioOferPrimPrepago(WebDrive, SelenElem, reporte, DN, Codigo, Plan);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Agregar Oferta Suplementaria Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Agregar Oferta Suplementaria");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Agregar Oferta Suplementaria");
                    Plan        = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Agregar Oferta Suplementaria");
                    Edo         = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Agregar Oferta Suplementaria");
                    Ope         = CargaVariables.LeerCSV(4, "ListaNegraFinalizado", "Agregar Oferta Suplementaria");
                    AdministracionOferta.AgregarOferSupl(WebDrive, SelenElem, reporte, DN, Codigo, Plan);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Suspension Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Suspension Bidireccional - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Suspension Bidireccional - Hibrido");
                    Edo         = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Suspension Bidireccional - Hibrido");
                    Ope         = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Suspension Bidireccional - Hibrido");
                    SuspencionReactivacion.SuspencionDiaEspesifico(WebDrive, SelenElem, reporte, DN, Codigo, "",false);
                    Resultado.setText("En ejecucion");                    
                    break;
                 
                case "Reactivacion Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Reactivacion Bidireccional - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Reactivacion Bidireccional - Hibrido");
                    Edo         = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Reactivacion Bidireccional - Hibrido");
                    Ope         = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Reactivacion Bidireccional - Hibrido");
                    SuspencionReactivacion.Reactivacion(WebDrive, SelenElem, reporte, DN, Codigo, "",false);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Reporte de Robo y Extravio Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Reporte de Robo y Extravio");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Reporte de Robo y Extravio");
                    Edo         = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Reporte de Robo y Extravio");
                    Ope         = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Reporte de Robo y Extravio");
                    ReporteRobo.ReportePrepago(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");                    
                    break;
                    
                case "Cancelacion Reporte de Robo y Extravio Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Cancelacion Reporte de Robo y Extravio");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Cancelacion Reporte de Robo y Extravio");
                    Edo         = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Cancelacion Reporte de Robo y Extravio");
                    Ope         = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Cancelacion Reporte de Robo y Extravio");
                    ReporteRobo.CancelarReportePrepago(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion"); 
                    
                case "Agregar Numero Frecuente - Prepago Lista Negra": 
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Agregar Numero Frecuente - Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Agregar Numero Frecuente - Prepago");
                    VOZ11       = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Agregar Numero Frecuente - Prepago");
                    Edo         = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Agregar Numero Frecuente - Prepago");
                    Ope         = CargaVariables.LeerCSV(4, "ListaNegraFinalizado", "Agregar Numero Frecuente - Prepago");
                    AtencionAlCliente.AgregarNumeroFrecuente(WebDrive, SelenElem, DN, Codigo, VOZ11, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    
                case "Payment-Paying - Hibrido Lista Negra":                     
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Payment-Paying");
                    Edo         = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Payment-Paying");
                    Ope         = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Payment-Paying");
                    AtencionAlCliente.Payment(WebDrive, SelenElem, reporte, DN);                   
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                case "Migración de Pospago a Híbrido Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Migración de Pospago a Híbrido");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Migración de Pospago a Híbrido");
                    DNFact      = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Migración de Pospago a Híbrido");
                    Plan        = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Migración de Pospago a Híbrido");
                    MigracionReno.MigraPospHibr(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    
                case "Migración de Pospago a Prepago Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Migración de Pospago a Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Migración de Pospago a Prepago");
                    DNFact      = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Migración de Pospago a Prepago");
                    Plan        = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Migración de Pospago a Prepago");
                    MigracionReno.MigraPosPrep(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Migración de Híbrido a Pospago Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Migración de Híbrido a Pospago");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Migración de Híbrido a Pospago");
                    DNFact      = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Migración de Híbrido a Pospago");
                    Plan        = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Migración de Híbrido a Pospago");
                    MigracionReno.MigraHibriPosp(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                case "Migración de Híbrido a Prepago Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Migración de Híbrido a Prepago");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Migración de Híbrido a Prepago");
                    DNFact      = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Migración de Híbrido a Prepago");
                    Plan        = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Migración de Híbrido a Prepago");
                    MigracionReno.MigraHibriPrep(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break;
                    
                case "Consolidacón de cuenta - Lista Negra":
                    DN         = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Consolidacón de cuenta - Pospago");
                    Codigo     = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Consolidacón de cuenta - Pospago");
                    Cuenta     = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Consolidacón de cuenta - Pospago");
                    Comentario = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Consolidacón de cuenta - Pospago");
                    ConsolidacionCesiondeDerechos.ConsolidacionPosp(WebDrive, SelenElem, reporte,DN,Codigo,Cuenta,Comentario);
                    Resultado.setText("En ejecucion");
                    break;   
                  
                case "Desconsolidacón de cuenta - Lista Negra":
                    DN         = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Desconsolidacón de cuenta - Hibrido");
                    Codigo     = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Desconsolidacón de cuenta - Hibrido");
                    Comentario = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Desconsolidacón de cuenta - Hibrido");
                    Ciclo      = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Desconsolidacón de cuenta - Hibrido");
                    ConsolidacionCesiondeDerechos.DesonsolidacionHib(WebDrive, SelenElem, reporte,DN,Codigo,Comentario,Ciclo);
                    Resultado.setText("En ejecucion");
                     
                case "Renovacion de Contrato (Upgrade) - Lista Negra":
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Renovacion de Contrato (Upgrade) - Hibrido");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Renovacion de Contrato (Upgrade) - Hibrido");
                    Plan        = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Renovacion de Contrato (Upgrade) - Hibrido");
                    Equipo      = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Renovacion de Contrato (Upgrade) - Hibrido");
                    IMEI        = CargaVariables.LeerCSV(4, "ListaNegraFinalizado", "Renovacion de Contrato (Upgrade) - Hibrido");
                    MigracionReno.RenoUpgrade(WebDrive, SelenElem, reporte, DN, Codigo,Plan,Equipo,IMEI,"HIBRIDO");
                    Resultado.setText("En ejecucion");
                    
                case "Cambiar numero de DN - Lista Negra":                     
                    DN          = CargaVariables.LeerCSV(0, "ListaNegraFinalizado", "Cambiar numero de DN");
                    Codigo      = CargaVariables.LeerCSV(1, "ListaNegraFinalizado", "Cambiar numero de DN");
                    Comentario  = CargaVariables.LeerCSV(2, "ListaNegraFinalizado", "Cambiar numero de DN");
                    Edo         = CargaVariables.LeerCSV(3, "ListaNegraFinalizado", "Cambiar numero de DN");
                    Ope         = CargaVariables.LeerCSV(4, "ListaNegraFinalizado", "Cambiar numero de DN");
                    AtencionAlCliente.CambioDN(WebDrive, SelenElem, reporte, DN, Codigo, Comentario);                    
                    Resultado.setText("Ejecucion Exitosa");
                    break;
 /////////////////////////Casos nuevos Pymes//////////////////////////////
                    
                    case "Activacion pymes pospago solo sim cliente PFAE":
                    SIM          = CargaVariables.LeerCSV(0, "Pymes", "Activacion pymes pospago solo sim cliente PM");
                    Oferta       = CargaVariables.LeerCSV(1, "Pymes", "Activacion pymes pospago solo sim cliente PM");
                    CasosPymes.SoloSIMPosp(WebDrive, SelenElem, reporte,SIM,Oferta,"PFAE");
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Activacion pymes pospago solo sim cliente PM":
                    SIM          = CargaVariables.LeerCSV(0, "Pymes", "Activacion pymes pospago solo sim cliente PM");
                    Oferta       = CargaVariables.LeerCSV(1, "Pymes", "Activacion pymes pospago solo sim cliente PM");
                    CasosPymes.SoloSIMPosp(WebDrive, SelenElem, reporte,SIM,Oferta,"PM");
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Activacion pymes hibrido con equipo cliente PFAE":
                    SIM          = CargaVariables.LeerCSV(0, "Pymes", "Activacio pymes hibrido con equipo cliente PFAE");
                    Oferta       = CargaVariables.LeerCSV(1, "Pymes", "Activacio pymes hibrido con equipo cliente PFAE");
                    Equipo       = CargaVariables.LeerCSV(2, "Pymes", "Activacio pymes hibrido con equipo cliente PFAE");
                    IMEI         = CargaVariables.LeerCSV(3, "Pymes", "Activacio pymes hibrido con equipo cliente PFAE");
                    Meses        = CargaVariables.LeerCSV(4, "Pymes", "Activacio pymes hibrido con equipo cliente PFAE");
                    CasosPymes.VentaSimconHandset(WebDrive, SelenElem, reporte,SIM,Oferta,Equipo,IMEI,Meses,"PFAE");
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                                        
                    case "Activacio pymes hibrido con equipo cliente PM":
                    SIM          = CargaVariables.LeerCSV(0, "Pymes", "Activacio pymes hibrido con equipo cliente PM");
                    Oferta       = CargaVariables.LeerCSV(1, "Pymes", "Activacio pymes hibrido con equipo cliente PM");
                    Equipo       = CargaVariables.LeerCSV(2, "Pymes", "Activacio pymes hibrido con equipo cliente PM");
                    IMEI         = CargaVariables.LeerCSV(3, "Pymes", "Activacio pymes hibrido con equipo cliente PM");
                    Meses        = CargaVariables.LeerCSV(4, "Pymes", "Activacio pymes hibrido con equipo cliente PM");
                    CasosPymes.VentaSimconHandset(WebDrive, SelenElem, reporte,SIM,Oferta,Equipo,IMEI,Meses,"PM");
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "RFC con caracteres durante la venta lo permite (&)":
                    RFC          = CargaVariables.LeerCSV(0, "Pymes", "RFC con caracteres durante la venta lo permite (&)");
                    CasosPymes.ValidaRFC(WebDrive, SelenElem, reporte,RFC);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "RFC con caracteres durante la venta lo permite (ñ)":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "RFC con caracteres durante la venta lo permite (ñ)");
                    CasosPymes.ValidaRFC(WebDrive, SelenElem, reporte,RFC);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Desabilita boton genera RFC con cliente PM":
                    CasosPymes.ValidaBtnRFC(WebDrive, SelenElem, reporte);
                    Resultado.setText("Ejecucion Exitosa");
                    break;

                    case "Adicion cliente pymes a una cuenta B2C":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Adicion cliente pymes a una cuenta B2C");
                    CasosPymes.AdicionCuent(WebDrive, SelenElem, reporte,RFC);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Venta nueva pymes con RFC de cliente B2B ya existente":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Venta nueva pymes con RFC de cliente B2B ya existente");
                    CasosPymes.VentaCliExi(WebDrive, SelenElem, reporte,RFC);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Venta nueva pymes con RFC de cliente PM ya existente":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Venta nueva pymes con RFC de cliente PM ya existente");
                    CasosPymes.VentaCliExi(WebDrive, SelenElem, reporte,RFC);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Venta nueva pymes con RFC de cliente PFAE ya exitente":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Venta nueva pymes con RFC de cliente PFAE ya exitente");
                    CasosPymes.VentaCliExi(WebDrive, SelenElem, reporte,RFC);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                                        
                    case "Agegar RFC a lista negra con formato PM":
                    RFC         = CargaVariables.LeerCSV2(0, "Pymes", "Agegar RFC a lista negra con formato PM");
                    CasosPymes.AgregarRFC(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                    case "Agegar RFC a lista negra con formato PM caracter especia (&)":
                    RFC         = CargaVariables.LeerCSV2(0, "Pymes", "Agegar RFC a lista negra con formato PM caracter especia (&)");
                    CasosPymes.AgregarRFCSim1(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                    case "Agegar RFC a lista negra con formatoPM letra (ñ)":
                    RFC         = CargaVariables.LeerCSV2(0, "Pymes", "Agegar RFC a lista negra con formatoPM letra (ñ)");
                    CasosPymes.AgregarRFCSim2(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                    case "Quitar RFC De Lista Negra":
                    RFC         = CargaVariables.LeerCSV2(0, "Pymes", "Quitar RFC De Lista Negra");
                    ListaNegra.Eliminar(WebDrive, SelenElem, reporte, RFC);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Lista negra error Cambiar informacion de cliente":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Lista negra error Cambiar informacion de cliente");
                    Codigo       = CargaVariables.LeerCSV2(1, "Pymes", "Lista negra error Cambiar informacion de cliente");
                    CasosPymes.CambiarInfoClien(WebDrive, SelenElem, reporte,RFC,Codigo);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                         
                    case "Lista negra error venta de equipo a cliente existente":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Lista negra error venta de equipo a cliente existente");
                    CasosPymes.VentaCliExi(WebDrive, SelenElem, reporte,RFC);
                    Resultado.setText("Ejecucion Exitosa");
                    break;//
                    
                    case "Lista negra error cambiar informacion de cuenta":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Lista negra error cambiar informacion de cuenta");
                    Cuenta       = CargaVariables.LeerCSV2(1, "Pymes", "Lista negra error cambiar informacion de cuenta");
                    ListaNegra.CambiarInfoCuenta(WebDrive, SelenElem, reporte,RFC,Cuenta);
                    Resultado.setText("Ejecucion Exitosa");
                    break;//
                    
                    case "Lista negra error nueva venta cliente pymes":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Lista negra error nueva venta cliente pymes");
                    CasosPymes.VentaNewClientPosp(WebDrive, SelenElem, reporte,RFC);
                    Resultado.setText("Ejecucion Exitosa");//
                    
                    case "Lista negra despues de sacar lista Cambiar informacion de cliente":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Lista negra despues de sacar lista Cambiar informacion de cliente");
                    Codigo       = CargaVariables.LeerCSV2(1, "Pymes", "Lista negra despues de sacar lista Cambiar informacion de cliente");
                    CasosPymes.CambiarInfoClien(WebDrive, SelenElem, reporte,RFC,"");
                    Resultado.setText("Ejecucion Exitosa");
                    
                    case "Lista negra despues de sacar lista cambiar informacion de cuenta":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Lista negra despues de sacar lista cambiar informacion de cuenta");
                    Codigo       = CargaVariables.LeerCSV2(1, "Pymes", "Lista negra despues de sacar lista cambiar informacion de cuenta");
                    CasosPymes.CambiarInfoCuent(WebDrive, SelenElem, reporte,RFC,Codigo);
                    Resultado.setText("Ejecucion Exitosa");
                    
                    case "Lista negra despues de sacar lista venta de equipo a cliente existente":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Lista negra despues de sacar lista venta de equipo a cliente existente");
                    Codigo       = CargaVariables.LeerCSV2(1, "Pymes", "Lista negra despues de sacar lista venta de equipo a cliente existente");
                    Equipo       = CargaVariables.LeerCSV2(2, "Pymes", "Lista negra despues de sacar lista venta de equipo a cliente existente");
                    IMEI         = CargaVariables.LeerCSV2(3, "Pymes", "Lista negra despues de sacar lista venta de equipo a cliente existente");
                    CasosPymes.VentaConHandsetCliExist(WebDrive, SelenElem, reporte,RFC,Codigo,Equipo,IMEI);
                    Resultado.setText("Ejecucion Exitosa"); 
                    
                    case "Lista negra despues de sacar lista nueva venta cliente pymes":
                    RFC          = CargaVariables.LeerCSV2(0, "Pymes", "Lista negra despues de sacar lista nueva venta cliente pymes");
                    Codigo       = CargaVariables.LeerCSV2(1, "Pymes", "Lista negra despues de sacar lista nueva venta cliente pymes");
                    SIM          = CargaVariables.LeerCSV2(2, "Pymes", "Lista negra despues de sacar lista nueva venta cliente pymes");
                    Oferta       = CargaVariables.LeerCSV2(3, "Pymes", "Lista negra despues de sacar lista nueva venta cliente pymes");
                    CasosPymes.NuevaVentaCliExist(WebDrive, SelenElem, reporte,RFC,Codigo,SIM,Oferta);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Exclusividad de oferta pymes cambio de oferta":
                    DN          = CargaVariables.LeerCSV2(0, "Pymes", "Exclusividad de oferta pymes cambio de oferta");
                    Codigo      = CargaVariables.LeerCSV2(1, "Pymes", "Exclusividad de oferta pymes cambio de oferta");
                    Plan        = CargaVariables.LeerCSV2(2, "Pymes", "Exclusividad de oferta pymes cambio de oferta");
                    AdministracionOferta.CambioOferPrimPrepago(WebDrive, SelenElem, reporte, DN, Codigo, Plan);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Exclusividad de oferta pymes migracion de Pospago a Hibrido":
                    DN          = CargaVariables.LeerCSV2(0, "Pymes", "Exclusividad de oferta pymes migracion de Pospago a Hibrido");
                    Codigo      = CargaVariables.LeerCSV2(1, "Pymes", "Exclusividad de oferta pymes migracion de Pospago a Hibrido");
                    DNFact      = CargaVariables.LeerCSV2(2, "Pymes", "Exclusividad de oferta pymes migracion de Pospago a Hibrido");
                    Plan        = CargaVariables.LeerCSV2(3, "Pymes", "Exclusividad de oferta pymes migracion de Pospago a Hibrido");
                    MigracionReno.MigraPospHibr(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Exclusividad de oferta pymes migracion de Hibrido a Pospago":
                    DN          = CargaVariables.LeerCSV2(0, "Pymes", "Exclusividad de oferta pymes migracion de Hibrido a Pospago");
                    Codigo      = CargaVariables.LeerCSV2(1, "Pymes", "Exclusividad de oferta pymes migracion de Hibrido a Pospago");
                    DNFact      = CargaVariables.LeerCSV2(2, "Pymes", "Exclusividad de oferta pymes migracion de Hibrido a Pospago");
                    Plan        = CargaVariables.LeerCSV2(3, "Pymes", "Exclusividad de oferta pymes migracion de Hibrido a Pospago");
                    MigracionReno.MigraHibriPosp(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break; 
                    
                    case "Migracion de clinte pymes a prepago (guardar cuenta y nombre cliente)":
                    DN          = CargaVariables.LeerCSV2(0, "Pymes", "Migracion de clinte pymes a prepago (guardar cuenta y nombre cliente)");
                    Codigo      = CargaVariables.LeerCSV2(1, "Pymes", "Migracion de clinte pymes a prepago (guardar cuenta y nombre cliente)");
                    DNFact      = CargaVariables.LeerCSV2(2, "Pymes", "Migracion de clinte pymes a prepago (guardar cuenta y nombre cliente)");
                    Plan        = CargaVariables.LeerCSV2(3, "Pymes", "Migracion de clinte pymes a prepago (guardar cuenta y nombre cliente)");
                    CasosPymes.MigraPymPrep(WebDrive, SelenElem, reporte, DN, Codigo,Plan,DNFact);
                    Resultado.setText("En ejecucion");
                    break;
                    
  ///////////////////Facturaion y Cobranza ////////////////////////
                    case "Consultar-Imprimir factura":
                    Ord         = CargaVariables.LeerCSV2(0, "Facturacion", "ConsultarImprimirFactura");
                    FechaIn     = CargaVariables.LeerCSV2(1, "Facturacion", "ConsultarImprimirFactura");
                    FechaFin    = CargaVariables.LeerCSV2(2, "Facturacion", "ConsultarImprimirFactura");
                    FacturacionCobranza.ConsultaImprimir(WebDrive, SelenElem, reporte,Ord,FechaIn,FechaFin,"Factura");
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Consultar-Imprimir Nota de Credito":
                    Ord         = CargaVariables.LeerCSV2(0, "Facturacion", "Consultar-Imprimir Nota de Credito");
                    FechaIn     = CargaVariables.LeerCSV2(1, "Facturacion", "Consultar-Imprimir Nota de Credito");
                    FechaFin    = CargaVariables.LeerCSV2(2, "Facturacion", "Consultar-Imprimir Nota de Credito");
                    FacturacionCobranza.ConsultaImprimir(WebDrive, SelenElem, reporte,Ord,FechaIn,FechaFin,"Nota de Crédito");
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Cambio de Ciclo de facturacion Pospago":
                    DN         = CargaVariables.LeerCSV2(0, "Facturacion", "Cambio de Ciclo de facturacion Pospago");
                    Codigo     = CargaVariables.LeerCSV2(1, "Facturacion", "Cambio de Ciclo de facturacion Pospago");
                    Ciclo      = CargaVariables.LeerCSV2(2, "Facturacion", "Cambio de Ciclo de facturacion Pospago");
                    FacturacionCobranza.CambioCiclo(WebDrive, SelenElem, reporte, DN, Codigo, Ciclo);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Cambiar el metodo de pago (Efectivo a Tarjeta de Credito)":
                    DN         = CargaVariables.LeerCSV2(0, "Facturacion", "Cambiar el metodo de pago (Efectivo a Tarjeta de Credito)");
                    Codigo     = CargaVariables.LeerCSV2(1, "Facturacion", "Cambiar el metodo de pago (Efectivo a Tarjeta de Credito)");
                    FacturacionCobranza.CambioPagoaTarjeta(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Cambiar Medio de Facturacion":
                    DN         = CargaVariables.LeerCSV2(0, "Facturacion", "Cambiar Medio de Facturacion");
                    Codigo     = CargaVariables.LeerCSV2(1, "Facturacion", "Cambiar Medio de Facturacion");
                    Cambio     = CargaVariables.LeerCSV2(2, "Facturacion", "Cambiar Medio de Facturacion");
                    FacturacionCobranza.CambioMedioFact(WebDrive, SelenElem, reporte, DN, Codigo, Cambio);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Cambiar el metodo de pago (Tarjeta de Credito y Departamental a Efectivo)":
                    DN         = CargaVariables.LeerCSV2(0, "Facturacion", "Cambiar el metodo de pago (Tarjeta de Credito y Departamental a Efectivo)");
                    Codigo     = CargaVariables.LeerCSV2(1, "Facturacion", "Cambiar el metodo de pago (Tarjeta de Credito y Departamental a Efectivo)");
                    FacturacionCobranza.CambioTarjetaAEfectivo(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Refacturacion":
                    DN          = CargaVariables.LeerCSV2(0, "Facturacion", "Refacturacion");
                    FechaIn     = CargaVariables.LeerCSV2(1, "Facturacion", "Refacturacion");
                    FechaFin    = CargaVariables.LeerCSV2(2, "Facturacion", "Refacturacion");
                    Monto       = CargaVariables.LeerCSV2(3, "Facturacion", "Refacturacion");
                    FacturacionCobranza.Refacturacion(WebDrive, SelenElem, reporte,DN,FechaIn,FechaFin,Monto);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Refacturación con Documento Ciclo":
                    DN          = CargaVariables.LeerCSV2(0, "Facturacion", "Refacturación con Documento Ciclo");
                    Codigo      = CargaVariables.LeerCSV2(1, "Facturacion", "Refacturación con Documento Ciclo");
                    FechaIn     = CargaVariables.LeerCSV2(2, "Facturacion", "Refacturación con Documento Ciclo");
                    Monto       = CargaVariables.LeerCSV2(3, "Facturacion", "Refacturación con Documento Ciclo");
                    FacturacionCobranza.RefacturacionDocCiclo(WebDrive, SelenElem, reporte,DN,Codigo,FechaIn,Monto);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Ordenes de Trabajo en DC por deuda":
                    DN          = CargaVariables.LeerCSV2(0, "Facturacion", "Ordenes de Trabajo en DC por deuda");
                    Codigo      = CargaVariables.LeerCSV2(1, "Facturacion", "Ordenes de Trabajo en DC por deuda");
                    FechaIn     = CargaVariables.LeerCSV2(2, "Facturacion", "Ordenes de Trabajo en DC por deuda");
                    FacturacionCobranza.OrdenesDCDeuda(WebDrive, SelenElem, reporte,DN,Codigo,FechaIn);
                    Resultado.setText("En ejecucion");
                    break;
                     
                    case "Validacion Dias de Reloj con Ordenes de Trabajo":
                    DN          = CargaVariables.LeerCSV2(0, "Facturacion", "Validacion Dias de Reloj con Ordenes de Trabajo");
                    Codigo      = CargaVariables.LeerCSV2(1, "Facturacion", "Validacion Dias de Reloj con Ordenes de Trabajo");
                    FacturacionCobranza.ValidacionDiasReloj(WebDrive, SelenElem, reporte,DN,Codigo);
                    Resultado.setText("En ejecucion");
                    break;
                    case "Nota de Credito a Nivel Detalle - Monto Exacto":
                    DN         = CargaVariables.LeerCSV2(0, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Exacto");
                    Codigo     = CargaVariables.LeerCSV2(1, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Exacto");
                    //Importe    = CargaVariables.LeerCSV2(2, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Exacto");
                    FacturacionCobranza.NotaCrediExacto(WebDrive, SelenElem, reporte, DN, Codigo);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Nota de Credito a Nivel Detalle - Monto Menor":
                    DN         = CargaVariables.LeerCSV2(0, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Exacto");
                    Codigo     = CargaVariables.LeerCSV2(1, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Exacto");
                    Importe    = CargaVariables.LeerCSV2(2, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Exacto");
                    FacturacionCobranza.NotaCrediMenor(WebDrive, SelenElem, reporte, DN, Codigo, Importe);
                    Resultado.setText("En ejecucion");
                    break;
                    
                    case "Nota de Credito a Nivel Detalle - Monto Mayor":
                    DN         = CargaVariables.LeerCSV2(0, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Mayor");
                    Codigo     = CargaVariables.LeerCSV2(1, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Mayor");
                    Importe    = CargaVariables.LeerCSV2(2, "Facturacion", "Nota de Credito a Nivel Detalle - Monto Mayor");
                    FacturacionCobranza.NotaCrediMayor(WebDrive, SelenElem, reporte, DN, Codigo, Importe);
                    Resultado.setText("En ejecucion");
                    break;
    ///////////////////////////////////Nuevos Casos CV 54 ////////////////////////////////////////                
                    case "Cambio de SIM Pospago sin costo sin By-pass ICC Dealer": 
                    DN      = CargaVariables.LeerCSV(0, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    Codigo  = CargaVariables.LeerCSV(1, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    ICC     = CargaVariables.LeerCSV(2, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    Causa   = CargaVariables.LeerCSV(3, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    Edo     = CargaVariables.LeerCSV(4, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    Ope     = CargaVariables.LeerCSV(5, "AttnCliente", "Cambio de SIM (Sin Costo) - Prepago");
                    AtencionAlCliente.CambioDeSimSinCosto(WebDrive, SelenElem, DN, Codigo, ICC,Causa, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Cambio de SIM Pospago con costo sin By-pass ICC Dealer":  
                    DN      = CargaVariables.LeerCSV(0, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    Codigo  = CargaVariables.LeerCSV(1, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    ICC     = CargaVariables.LeerCSV(2, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    Causa   = CargaVariables.LeerCSV(3, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    Edo     = CargaVariables.LeerCSV(4, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    Ope     = CargaVariables.LeerCSV(5, "AttnCliente", "Cambio de SIM (Con Costo) - Prepago");
                    AtencionAlCliente.CambioDeSimSinCosto(WebDrive, SelenElem, DN,Codigo, ICC,Causa, reporte);               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Pago de orden con metodo de pago flap":
                    Orden  = CargaVariables.LeerCSV2(0, "CV54", "Pago de orden con metodo de pago flap");
                    CV54.PagoFlap(WebDrive, SelenElem, reporte, Orden);
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                    
                    case "Pago de orden de venta con metodo de pago flap":  
                    DN      = CargaVariables.LeerCSV(0, "CV54", "Pago de orden de venta con metodo de pago flap");
                    Codigo  = CargaVariables.LeerCSV(1, "CV54", "Pago de orden de venta con metodo de pago flap");
                    Equipo  = CargaVariables.LeerCSV(2, "CV54", "Pago de orden de venta con metodo de pago flap");
                    IMEI    = CargaVariables.LeerCSV(3, "CV54", "Pago de orden de venta con metodo de pago flap");
                    CV54.PagoVentaFlap(WebDrive, SelenElem,reporte, DN,Codigo,Equipo,IMEI );               
                    Resultado.setText("Ejecucion Exitosa");
                    break;
                default:
                    Resultado.setText("Flujo no valido, por favor valide su caso a ejecutar");
            }
        } catch (Exception ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
            reporte.writeError("La prueba FALLO favor de reintentar, si el problema perciste comunicarse con los desarrolladores");
            Resultado.setText("La prueba FALLO favor de reintentar");
        }finally{
            reporte.Export();
            //Resultado.setText("Finalizo la ejecucion"); 
        }
        
        if(jCheckBox2.isSelected())
        {
            if(reporte.getNamefile() != null)
                (new SystemA()).OpenFile(reporte.getNamefile());
        }
                    
    }//GEN-LAST:event_AceptarActionPerformed

    private void jButton1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton1ActionPerformed
           
        System.exit(0);
        
    }//GEN-LAST:event_jButton1ActionPerformed

    private void ComboBox2ItemStateChanged(java.awt.event.ItemEvent evt) {//GEN-FIRST:event_ComboBox2ItemStateChanged
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboBox2ItemStateChanged

    private void ComboBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_ComboBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_ComboBox2ActionPerformed

    private void jButton2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jButton2ActionPerformed
        
        //(new SystemA()).OpenFile(ruta);
        (new SystemA()).OpenFolder("A:\\Reportes");
    }//GEN-LAST:event_jButton2ActionPerformed

    private void Aceptar1ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_Aceptar1ActionPerformed
        // TODO add your handling code here:
        String ruta = null;
        String Caso = ComboBox1.getSelectedItem().toString();
        Resultado.setText("");
        switch(Caso)
        { 
            case "Cambio de SIM (Sin Costo) - Prepago":  
                ruta = "A:\\DataOnix\\AttnCliente\\Cambio de SIM (Sin Costo) - Prepago.csv";               
                break;

            case "Cambio de SIM (Con Costo) - Prepago":  
                ruta = "A:\\DataOnix\\AttnCliente\\Cambio de SIM (Con Costo) - Prepago.csv";          
                break;
                
            case "Cambio de SIM (Sin Costo) - Pospago":  
                ruta = "A:\\DataOnix\\AttnCliente\\Cambio de SIM (Sin Costo) - Pospago.csv";               
                break;

            case "Cambio de SIM (Con Costo) - Hibrido":  
                ruta = "A:\\DataOnix\\AttnCliente\\Agregar Numero Frecuente - Prepago.csv";         
                break;
            
            case "Agregar Numero Frecuente - Prepago":  
                ruta = "A:\\DataOnix\\AttnCliente\\Agregar Numero Frecuente - Prepago.csv";               
                break;

            case "Agregar Numero Frecuente(Con Costo) - Prepago":  
                ruta = "A:\\DataOnix\\AttnCliente\\Agregar Numero Frecuente (Con Costo) - Prepago.csv";         
                break;
                
            case "Eliminar Numero Frecuente":  
                ruta = "A:\\DataOnix\\AttnCliente\\Eliminar Numero Frecuente.csv";               
                break;

            case "Recarga (CAC-Pago Efectivo) Prepago-Hibrido":  
                ruta = "A:\\DataOnix\\AttnCliente\\Recarga (CAC-Pago Efectivo) Prepago-Hibrido.csv";          
                break;
               
            case "Incremento DN Prepago":    
                ruta = "A:\\DataOnix\\AttnCliente\\Incremento DN-Prepago.csv";               
                break;

            case "Decremento DN Prepago":   
                ruta = "A:\\DataOnix\\AttnCliente\\Decremento DN-Prepago.csv";         
                break;
                
            case "Cambiar numero de DN":  
                ruta = "A:\\DataOnix\\AttnCliente\\Cambiar numero de DN.csv";               
                break;

            case "Complemento de pago Envio de E-mail":  
                ruta = "A:\\DataOnix\\AttnCliente\\ComplementoPago_EnvioEmail.csv";          
                break;
            
            case "Movistar Refresh Pospago":    
                ruta = "A:\\DataOnix\\AttnCliente\\Movistar Refresh Pospago.csv";               
                break;

            case "Movistar Refresh Hibrido":   
                ruta = "A:\\DataOnix\\AttnCliente\\Movistar Refresh Pospago.csv";         
                break;
                
           case "Apertura Caja":
                ruta = "A:\\DataOnix\\AttnCliente\\Apertura Caja.csv";               
                break;

            case "Cierre Caja":  
                ruta = "A:\\DataOnix\\AttnCliente\\Cierre Caja.csv";          
                break;
            
            case "Payment-Paying - Hibrido":  
                ruta = "A:\\DataOnix\\AttnCliente\\Payment-Paying - Hibrido.csv";          
                break;
            
            case "Pago de Deposito":    
                ruta = "A:\\DataOnix\\AttnCliente\\Pago de Deposito.csv";               
                break;

            case "Liberacion de Deposito":   
                ruta = "A:\\DataOnix\\AttnCliente\\Liberacion de Deposito.csv";         
                break;
                
            case "Agregar Lista Roja":
                ruta = "A:\\DataOnix\\Lista_Roja\\Agregar Lista Roja.csv";               
                break;
                
            case "Eliminar Lista Roja":
                ruta = "A:\\DataOnix\\Lista_Roja\\Eliminar Lista Roja.csv";               
                break;

            case "Consulta la Información de Cliente - Prepago":    
                ruta = "A:\\DataOnix\\Info_Cliente\\Consulta Info Cliente.csv";               
                break;

            case "Consulta la Información de Cuenta - Prepago":   
                ruta = "A:\\DataOnix\\Info_Cliente\\Consulta Info Cuenta.csv";         
                break;
                
            case "Consulta la Información del Suscriptor - Prepago": 
                ruta = "A:\\DataOnix\\Info_Cliente\\Consulta Info Suscriptor.csv";               
                break;
                
            case "Consulta de Saldo - Prepago":   
                ruta = "A:\\DataOnix\\Info_Cliente\\Consulta Saldo - Prepago.csv";               
                break;
                
            case "Cambiar Info Cuenta (OrdenesPendientes) - Prepago":   
                ruta = "A:\\DataOnix\\Info_Cliente\\Cambiar Info Cuenta (OrdenesPendientes) - Prepago.csv";               
                break;
            
            case "Incremento de Balance de Caja":
                ruta = "A:\\DataOnix\\AttnCliente\\Incremento Balance de Caja.csv";               
                break;
                
             case "Decremento de Balance de Caja":   
                ruta = "A:\\DataOnix\\AttnCliente\\DecrementoBalance de Caja.csv";               
                break;
                
            case "Reporte Diario":   
                ruta = "A:\\DataOnix\\AttnCliente\\Reporte Diario.csv";               
                break;
                
            case "Generar RFC": 
                ruta = "A:\\DataOnix\\ConfigOnix\\CliNuevo.csv";               
                break;
                
            case "Valida PIPE Info Cuenta": 
                Resultado.setText("El flujo no necesita archivo de ejecucion");             
                break;
                
            case "Valida PIPE Direccion de Entrega":
                Resultado.setText("El flujo no necesita archivo de ejecucion");             
                break;
                
            case "Valida PIPE Calle Direccion Fiscal": 
                Resultado.setText("El flujo no necesita archivo de ejecucion");             
                break;
                
            case "Valida PIPE Num Int y Ext Direccion Fiscal":
                Resultado.setText("El flujo no necesita archivo de ejecucion");             
                break;
            
            case "Valida RFC Existente": 
                ruta = "A:\\DataOnix\\ConfigOnix\\CliNuevo.csv";               
                break;
            
            case "Generar RFC - Pymes": 
                ruta = "A:\\DataOnix\\ConfigOnix\\CliNuevo.csv";               
                break;
            
            case "Valida PIPE Info Cuenta - Pymes": 
                Resultado.setText("El flujo no necesita archivo de ejecucion");             
                break;
                
            case "Valida PIPE Direccion de Entrega - Pymes":
                Resultado.setText("El flujo no necesita archivo de ejecucion");             
                break;
                
            case "Valida PIPE Calle Direccion Fiscal - Pymes": 
                Resultado.setText("El flujo no necesita archivo de ejecucion");             
                break;
                
            case "Valida PIPE Num Int y Ext Direccion Fiscal - Pymes":
                Resultado.setText("El flujo no necesita archivo de ejecucion");             
                break;
            
            case "Valida RFC Existente - Pymes": 
                ruta = "A:\\DataOnix\\ConfigOnix\\CliNuevo.csv";               
                break; 
            
            case "Cambiar Informacion de Cuenta - Hibrido":  
                ruta = "A:\\DataOnix\\Info_Cliente\\Cambio Info Cuenta - Hibrido.csv";               
                break;
                
            case "Cambiar Informacion de Suscriptor - Hibrido":   
                ruta = "A:\\DataOnix\\Info_Cliente\\Cambio Info Suscriptor - Hibrido.csv";               
                break;    
                
            case "Batch actualizacion en precio de handsets venta":   
                ruta = "A:\\DataOnix\\Batch\\Batch actualizacion en precio de handsets venta.csv";               
                break;    
                
            case "Batch actualizacion en precio de handsets renovacion":   
                ruta = "A:\\DataOnix\\Batch\\Batch actualizacion en precio de handsets renovacion.csv";               
                break;    
                
            case "Batch Changing Customer Category":   
                ruta = "A:\\DataOnix\\Batch\\Batch Changing Customer Category.csv";               
                break;    
                
            case "Batch Update Payment Method - Store Card Number":  
                ruta = "A:\\DataOnix\\Batch\\Batch Update Payment MethodStore Card Number.csv";               
                break;    
                
            case "Suspend on specific day":
                ruta = "A:\\DataOnix\\Suspencion\\Suspencion en dia espesifico.csv";               
                break;    
                
            case "Suspension Bidireccional - Hibrido":
                ruta = "A:\\DataOnix\\Suspencion\\Suspension Bidireccional - Hibrido.csv";               
                break;    
                
            case "Reactivacion Bidireccional - Hibrido":
                ruta = "A:\\DataOnix\\Suspencion\\Reactivacion Bidireccional - Hibrido.csv";               
                break;    
                
            case "Barring pospaid subscriber - Pospago":
                ruta = "A:\\DataOnix\\Suspencion\\Barring pospaid subscriber - Pospago.csv";               
                break;    
                
            case "Unarring pospaid subscriber - Pospago":
                ruta = "A:\\DataOnix\\Suspencion\\Unbarring pospaid subscriber - Pospago.csv";               
                break;    
                
            case "Reporte de Robo y Extravio - Prepago":
                ruta = "A:\\DataOnix\\ReporteRobo\\Reporte de Robo y Extravio - Prepago.csv";               
                break;    
                
            case "Cancelacion Reporte de Robo y Extravio - Prepago":
                ruta = "A:\\DataOnix\\ReporteRobo\\Cancelacion Reporte de Robo y Extravio - Prepago.csv";               
                break;    
                
            case "Reporte de Robo y Extravio - Hibrido":
                ruta = "A:\\DataOnix\\ReporteRobo\\Reporte de Robo y Extravio - Hibrido.csv";               
                break;    
                
            case "Cancelacion Reporte de Robo y Extravio - Hibrido":
                ruta = "A:\\DataOnix\\ReporteRobo\\Cancelacion Reporte de Robo y Extravio - Hibrido.csv";               
                break;    
                
            case "Reporte de Robo y Extravio con IMEI":
                ruta = "A:\\DataOnix\\ReporteRobo\\Reporte de Robo y Extravio con IMEI.csv";               
                break; 
                
            case "Cambio Oferta Primaria - Prepago":
                ruta = "A:\\DataOnix\\AdminOfer\\Cambio Oferta Primaria - Prepago.csv";               
                break;    
                
            case "Cambio Oferta Primaria (Upgrade) - Pospago":
                ruta = "A:\\DataOnix\\AdminOfer\\Cambio Oferta Primaria - Pospago.csv";               
                break;    
                
            case "Agregar Oferta Suplementaria - Prepago":
                ruta = "A:\\DataOnix\\AdminOfer\\Agregar Oferta Suplementaria - Prepago.csv";               
                break;    
                
            case "Agregar Oferta Suplementaria - Pospago":
                ruta = "A:\\DataOnix\\AdminOfer\\Agregar Oferta Suplementaria - Pospago.csv";               
                break;    
                
            case "Eliminar Oferta Suplementaria - Hibrido":
                ruta = "A:\\DataOnix\\AdminOfer\\Eliminar Oferta Suplementaria - Hibrido.csv";               
                break;    
                
            case "Carga De Oferta Suplementaria Por Batch":
                ruta = "A:\\DataOnix\\AdminOfer\\Carga De Oferta Suplementaria Por Batch.csv";               
                break;    
                
            case "Batch descuento variable oferta primaria":
                ruta = "A:\\DataOnix\\AdminOfer\\Batch descuento variable oferta primaria.csv";               
                break;    
                
            case "Batch descuento variable oferta suplementaria":
                ruta = "A:\\DataOnix\\AdminOfer\\Batch descuento variable oferta suplementaria.csv";               
                break;
                
            case "Migración de Prepago a Híbrido":
                ruta = "A:\\DataOnix\\MigraReno\\Migración de Prepago a Híbrido.csv";               
                break;    
                
            case "Migración de Prepago a Pospago":
                ruta = "A:\\DataOnix\\MigraReno\\Migración de Prepago a Pospago.csv";               
                break;    
                
            case "Migración de Híbrido a Pospago":
                ruta = "A:\\DataOnix\\MigraReno\\Migración de Híbrido a Pospago.csv";               
                break;    
                
            case "Migración de Pospago a Híbrido":
                ruta = "A:\\DataOnix\\MigraReno\\Migración de Pospago a Híbrido.csv";               
                break;    
                
            case "Migración de Híbrido a Prepago":
                ruta = "A:\\DataOnix\\MigraReno\\Migración de Híbrido a Prepago.csv";               
                break;    
                
            case "Migración de Pospago a Prepago":
                ruta = "A:\\DataOnix\\MigraReno\\Migración de Pospago a Prepago.csv";               
                break;    
                
            case "Renovacion de Contrato (Upgrade) - Hibrido":
                ruta = "A:\\DataOnix\\MigraReno\\Renovacion de Contrato (Upgrade) - Hibrido.csv";               
                break;    
                
            case "Renovacion de Contrato (Con Cambio de Oferta) - Pospago":
                ruta = "A:\\DataOnix\\MigraReno\\Renovacion de Contrato (Con Cambio de Oferta) - Pospago.csv";               
                break;  
            ///////////////////////   VENTAS  \\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\\
            case "Venta de Solo SIM Con Factura (Cliente Nuevo) - Prepago":
                ruta = "A:\\DataOnix\\Ventas\\Venta de Solo SIM Con Factura (Cliente Nuevo) - Prepago.csv";               
                break;   
            
            case "Venta de Solo SIM Con Factura (Cliente Existente) - Prepago":
                ruta = "A:\\DataOnix\\Ventas\\Venta de Solo SIM Con Factura (Cliente Existente) - Prepago.csv";               
                break;   
            
            case "Venta de Solo SIM (Cliente Nuevo) - Pospago":
                ruta = "A:\\DataOnix\\Ventas\\Venta de Solo SIM (Cliente Nuevo) - Pospago.csv";               
                break;   
            
            case "Venta de Solo SIM (Cliente Nuevo) - Hibrido":
                ruta = "A:\\DataOnix\\Ventas\\Venta de Solo SIM (Cliente Nuevo) - Hibrido.csv";               
                break; 
                               
            case "Venta de SIM Con Handset (Cliente Nuevo) - Prepago":
                ruta = "A:\\DataOnix\\Ventas\\Venta de SIM Con Handset (Cliente Nuevo) - Prepago.csv";               
                break;
            
            case "Agregar Nueva Linea - Pospago":
                ruta = "A:\\DataOnix\\Ventas\\Agregar Nueva Linea - Pospago.csv";               
                break; 
            ///Pendientes los casos de venta a credito 
                
            case "Consolidacón de cuenta - Pospago":
                ruta = "A:\\DataOnix\\ConDesCes\\Agregar Nueva Linea - Pospago.csv";               
                break; 
            
            case "Desconsolidacón de cuenta - Hibrido":
                ruta = "A:\\DataOnix\\ConDesCes\\Agregar Nueva Linea - Pospago.csv";               
                break;
                
            case "Cesión de Derechos - Híbrido":
                ruta = "A:\\DataOnix\\ConDesCes\\Agregar Nueva Linea - Pospago.csv";               
                break;
            //Lista Negra
            case "Gestionar Lista Negra (Agregar)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Gestionar Lista Negra (Agregar).csv";               
                break; 
            
            case "Nuevo Cliente Pospago (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Nuevo Cliente Pospago (Lista Negra).csv";               
                break;
                
            case "Cambiar Informacion Cliente (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Cambiar Informacion Cliente (Lista Negra).csv";               
                break;
                
            case "MNP Port in Pospago (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\MNP Port in Pospago (Lista Negra).csv";               
                break; 
            
            case "Venta de Equipo a Cliente Existente (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Venta de Equipo a Cliente Existente (Lista Negra).csv";               
                break;
                
            case "Cesion de derechos (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Cesion de derechos (Lista Negra).csv";               
                break;
                
            case "Cambiar Informacion de Cuenta (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Cambiar Informacion de Cuenta (Lista Negra).csv";               
                break; 
            
            case "Migracion Prepago a Pospago (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Migracion Prepago a Pospago (Lista Negra).csv";               
                break;
                
            case "Migracion Prepago a Hibrido (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Migracion Prepago a Hibrido (Lista Negra).csv";               
                break;
                
            case "Quitar RFC (Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Quitar RFC (Lista Negra).csv";               
                break; 
            
            case "Nuevo Cliente Pospago (Quitar Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Nuevo Cliente Pospago (Lista Negra).csv";               
                break;
                
            case "Cambiar Informacion Cliente (Quitar Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Cambiar Informacion Cliente (Lista Negra).csv";               
                break;
                
            case "MNP Port in Pospago (Quitar Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\MNP Port in Pospago (Lista Negra).csv";               
                break; 
            
            case "Venta de Equipo a Cliente Existente (Quitar Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Venta de Equipo a Cliente Existente (Lista Negra).csv";               
                break;
                
            case "Cesion de derechos (Quitar Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Cesion de derechos (Lista Negra).csv";               
                break;
                
            case "Cambiar Informacion de Cuenta (Quitar Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Cambiar Informacion de Cuenta (Lista Negra).csv";               
                break; 
            
            case "Migracion Prepago a Pospago (Quitar Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Migracion Prepago a Pospago (Lista Negra).csv";               
                break;
                
            case "Migracion Prepago a Hibrido (Quitar Lista Negra)":
                ruta = "A:\\DataOnix\\Lista_Negra\\Migracion Prepago a Hibrido (Lista Negra).csv";               
                break;
                
            //Lista Negra Finalizado
            case "Cambiar Información del Suscriptor Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Cambio Info Suscriptor.csv";               
                break;
            
            case "Cambio de SIM (Sin Costo) Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Cambio de SIM (Sin Costo).csv";

            case "Cambio Oferta Primaria Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Cambio Oferta Primaria.csv";               
                break;
              
            case "Agregar Oferta Suplementaria Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Agregar Oferta Suplementaria.csv";               
                break;

            case "Suspension Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Suspension Bidireccional - Hibrido.csv";               
                break;
                
            case "Reactivacion Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Reactivacion Bidireccional - Hibrido.csv";               
                break;  
            
            case "Reporte de Robo y Extravio Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Reporte de Robo y Extravio.csv";               
                break; 

            case "Cancelacion Reporte de Robo y Extravio Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Cancelacion Reporte de Robo y Extravio.csv";               
                break;
                
            case "Agregar Numero Frecuente - Prepago Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Agregar Numero Frecuente - Prepago.csv";               
                break;

            case "Payment-Paying - Hibrido Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Payment-Paying.csv";               
                break;
                        
            case "Migración de Pospago a Híbrido Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Migración de Pospago a Híbrido.csv";               
                break; 
              
            case "Migración de Pospago a Prepago Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Migración de Pospago a Prepago.csv";               
                break; 

            case "Migración de Híbrido a Pospago Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Migración de Híbrido a Pospago.csv";               
                break; 
                
            case "Migración de Híbrido a Prepago Lista Negra":
                ruta = "A:\\DataOnix\\ListaNegraFinalizado\\Migración de Híbrido a Prepago.csv";               
                break;
            //Pymes   
            case "Activacion pymes pospago solo sim cliente PFAE":
                ruta = "A:\\DataOnix\\Pymes\\Activacion pymes pospago solo sim cliente PFAE.csv";               
                break;
                
            case "Activacion pymes pospago solo sim cliente PM":
                ruta = "A:\\DataOnix\\Pymes\\Activacion pymes pospago solo sim cliente PM.csv";               
                break;
                
            case "Activacion pymes hibrido con equipo cliente PFAE":
                ruta = "A:\\DataOnix\\Pymes\\Activacion pymes hibrido con equipo cliente PFAE.csv";               
                break;
                
            case "Activacio pymes hibrido con equipo cliente PM":
                ruta = "A:\\DataOnix\\Pymes\\Activacio pymes hibrido con equipo cliente PM.csv";               
                break;
                
            case "RFC con caracteres durante la venta lo permite (&)":
                ruta = "A:\\DataOnix\\Pymes\\RFC con caracteres durante la venta lo permite (&).csv";               
                break;
                
            case "RFC con caracteres durante la venta lo permite (ñ)":
                ruta = "A:\\DataOnix\\Pymes\\RFC con caracteres durante la venta lo permite (ñ).csv";               
                break;
                
            case "Desabilita boton genera RFC con cliente PM":
                Resultado.setText("El caso no necesita datos");               
                break;
                
            case "Adicion cliente pymes a una cuenta B2C":
                ruta = "A:\\DataOnix\\Pymes\\Adicion cliente pymes a una cuenta B2C.csv";               
                break;
                
            case "Venta nueva pymes con RFC de cliente B2B ya existente":
                ruta = "A:\\DataOnix\\Pymes\\Venta nueva pymes con RFC de cliente B2B ya existente.csv";               
                break;
                
            case "Venta nueva pymes con RFC de cliente PM ya existente":
                ruta = "A:\\DataOnix\\Pymes\\Venta nueva pymes con RFC de cliente PM ya existente.csv";               
                break;
                
            case "Venta nueva pymes con RFC de cliente PFAE ya exitente":
                ruta = "A:\\DataOnix\\Pymes\\Venta nueva pymes con RFC de cliente PFAE ya exitente.csv";               
                break;
                
            case "Agegar RFC a lista negra con formato PM":
                ruta = "A:\\DataOnix\\Pymes\\Agegar RFC a lista negra con formato PM.csv";               
                break;
                
            case "Agegar RFC a lista negra con formato PM caracter especia (&)":
                ruta = "A:\\DataOnix\\Pymes\\Agegar RFC a lista negra con formato PM caracter especia (&).csv";               
                break;
                
            case "Agegar RFC a lista negra con formatoPM letra (ñ)":
                ruta = "A:\\DataOnix\\Pymes\\Agegar RFC a lista negra con formatoPM letra (ñ).csv";               
                break;
                
            case "Quitar RFC De Lista Negra":
                ruta = "A:\\DataOnix\\Pymes\\Quitar RFC De Lista Negra.csv";               
                break;
                
            case "Lista negra error Cambiar informacion de cliente":
                ruta = "A:\\DataOnix\\Pymes\\Lista negra error Cambiar informacion de cliente.csv";               
                break;
                
            case "Lista negra error venta de equipo a cliente existente":
                ruta = "A:\\DataOnix\\Pymes\\Lista negra error venta de equipo a cliente existente.csv";               
                break;
                
            case "Lista negra error cambiar informacion de cuenta":
                ruta = "A:\\DataOnix\\Pymes\\Lista negra error cambiar informacion de cuenta.csv";               
                break;
                
            case "Lista negra error nueva venta cliente pymes":
                ruta = "A:\\DataOnix\\Pymes\\Lista negra error nueva venta cliente pymes.csv";               
                break;
                
            case "Lista negra despues de sacar lista Cambiar informacion de cliente":
                ruta = "A:\\DataOnix\\Pymes\\Lista negra despues de sacar lista Cambiar informacion de cliente.csv";               
                break;
            
            case "Lista negra despues de sacar lista cambiar informacion de cuenta":
                ruta = "A:\\DataOnix\\Pymes\\Lista negra despues de sacar lista cambiar informacion de cuenta.csv";               
                break;
                
            case "Lista negra despues de sacar lista venta de equipo a cliente existente":
                ruta = "A:\\DataOnix\\Pymes\\Lista negra despues de sacar lista venta de equipo a cliente existente.csv";               
                break;
                
            case "Lista negra despues de sacar lista nueva venta cliente pymes":
                ruta = "A:\\DataOnix\\Pymes\\Lista negra despues de sacar lista nueva venta cliente pymes.csv";               
                break;  
                
            case "Exclusividad de oferta pymes cambio de oferta":
                ruta = "A:\\DataOnix\\Pymes\\Exclusividad de oferta pymes cambio de oferta.csv";               
                break; 
            
            case "Exclusividad de oferta pymes migracion de Hibrido a Pospago":
                ruta = "A:\\DataOnix\\Pymes\\Exclusividad de oferta pymes migracion de Hibrido a Pospago.csv";               
                break;
                
            case "Migracion de clinte pymes a prepago (guardar cuenta y nombre cliente)":
                ruta = "A:\\DataOnix\\Pymes\\Migracion de clinte pymes a prepago (guardar cuenta y nombre cliente).csv";               
                break;
                
  ///////////////////Facturaion y Cobranza ////////////////////////                
            case "Consultar-Imprimir factura":
                ruta = "A:\\DataOnix\\Facturacion\\Consultar-Imprimir factura.csv";               
                break;
                
            case "Consultar-Imprimir Nota de Credito":
                ruta = "A:\\DataOnix\\Facturacion\\Consultar-Imprimir factura.csv";               
                break;
                
            case "Cambio de Ciclo de facturacion Pospago":
                ruta = "A:\\DataOnix\\Facturacion\\Cambio de Ciclo de facturacion Pospago.csv";               
                break;
                
            case "Cambiar el metodo de pago (Efectivo a Tarjeta de Credito)":
                ruta = "A:\\DataOnix\\Facturacion\\Cambiar el metodo de pago (Efectivo a Tarjeta de Credito).csv";               
                break;
                
            case "Cambiar Medio de Facturacion":
                ruta = "A:\\DataOnix\\Facturacion\\Cambiar Medio de Facturacion.csv";               
                break; 
                
            case "Cambiar el metodo de pago (Tarjeta de Credito y Departamental a Efectivo)":
                ruta = "A:\\DataOnix\\Facturacion\\Cambiar Medio de Facturacion.csv";               
                break; 
                
            case "Refacturacion":
                ruta = "A:\\DataOnix\\Facturacion\\Refacturacion.csv";               
                break; 
                
            case "Refacturación con Documento Ciclo":
                ruta = "A:\\DataOnix\\Facturacion\\Refacturación con Documento Ciclo.csv";               
                break; 
                
            case "Ordenes de Trabajo en DC por deuda":
                ruta = "A:\\DataOnix\\Facturacion\\Ordenes de Trabajo en DC por deuda.csv";               
                break; 
                
            case "Validacion Dias de Reloj con Ordenes de Trabajo":
                ruta = "A:\\DataOnix\\Facturacion\\Validacion Dias de Reloj con Ordenes de Trabajo.csv";               
                break; 
                
            case "Nota de Credito a Nivel Detalle - Monto Exacto":
                ruta = "A:\\DataOnix\\Facturacion\\Nota de Credito a Nivel Detalle - Monto Exacto.csv";               
                break; 
                
            case "Nota de Credito a Nivel Detalle - Monto Mayor":
                ruta = "A:\\DataOnix\\Facturacion\\Nota de Credito a Nivel Detalle - Monto Mayor.csv";               
                break; 
                
            case "Nota de Credito a Nivel Detalle - Monto Menor":
                ruta = "A:\\DataOnix\\Facturacion\\Nota de Credito a Nivel Detalle - Monto Menor.csv";               
                break;  
                
    ///////////////////////////////////Nuevos Casos CV 54 ////////////////////////////////////////   
            case "Pago de orden con metodo de pago flap":
                ruta = "A:\\DataOnix\\CV54\\Pago de orden con metodo de pago flap.csv";               
                break; 
                
            case "Pago de orden de venta con metodo de pago flap":
                ruta = "A:\\DataOnix\\CV54\\Pago de orden de venta con metodo de pago flap.csv";               
                break; 
            default:
                Resultado.setText("Caso no definido");
         }
        
        (new SystemA()).OpenFile(ruta);
    }//GEN-LAST:event_Aceptar1ActionPerformed

    private void jCheckBox2ActionPerformed(java.awt.event.ActionEvent evt) {//GEN-FIRST:event_jCheckBox2ActionPerformed
        // TODO add your handling code here:
    }//GEN-LAST:event_jCheckBox2ActionPerformed

    
    
    public static void main(String args[]) {
        java.awt.EventQueue.invokeLater(new Runnable() {
            public void run() {
                new Menu().setVisible(true);
            }
        });
    }

    // Variables declaration - do not modify//GEN-BEGIN:variables
    private javax.swing.JButton Aceptar;
    private javax.swing.JButton Aceptar1;
    private javax.swing.JComboBox<String> ComboBox1;
    private javax.swing.JComboBox<String> ComboBox2;
    private javax.swing.JLabel Resultado;
    private javax.swing.JButton jButton1;
    private javax.swing.JButton jButton2;
    private javax.swing.JCheckBox jCheckBox2;
    private javax.swing.JLabel jLabel1;
    private javax.swing.JLabel jLabel2;
    private javax.swing.JLabel jLabel3;
    private javax.swing.JLabel jLabel6;
    private javax.swing.JLabel jLabel7;
    private javax.swing.JLabel jLabel8;
    private javax.swing.JLabel jLabel9;
    private javax.swing.JSeparator jSeparator1;
    // End of variables declaration//GEN-END:variables


}
