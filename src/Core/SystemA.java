/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import onix_selenium.Menu;
import java.awt.AWTException;
import java.awt.Desktop;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedInputStream;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.util.Enumeration;
import java.util.Scanner;
import java.util.logging.Level;
import java.util.logging.Logger;
import java.util.zip.ZipEntry;
import java.util.zip.ZipFile;
import java.util.zip.ZipInputStream;
import javax.imageio.ImageIO;
import javax.swing.JOptionPane;

/**
 *
 * @author LAPTOP-JORGE
 */
public class SystemA {
    
    public void KillProccess(String procc)
    {
        try {
            Runtime.getRuntime().exec("taskkill /F /IM " + procc);
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
    
    public void OpenFile(String filename)
    {
        try {
            Runtime.getRuntime().exec("rundll32 url.dll,FileProtocolHandler " + filename);
        } catch (IOException ex) {
            Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void OpenFolder(String foldername)
    {
        try {
           Desktop.getDesktop().open(new File(foldername));
       } catch (IOException ex) {
           Logger.getLogger(Menu.class.getName()).log(Level.SEVERE, null, ex);
       }
    }
    
    public byte[] ScreenShotScreen() 
    {
        Rectangle rectangleTam = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        Robot robot;
        try {
            robot = new Robot();

            BufferedImage bufferedImage = robot.createScreenCapture(rectangleTam);
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(bufferedImage, "png", baos);
                return baos.toByteArray();
            } catch (IOException ex) {
                Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (AWTException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }   
    
    public String printCommand(String cmd) throws IOException, InterruptedException
    {
        Process proc = Runtime.getRuntime().exec(cmd);

        InputStreamReader isr = new InputStreamReader(proc.getInputStream());
        BufferedReader rdr = new BufferedReader(isr);
        String line, result = "";
        while((line = rdr.readLine()) != null) { 
          //System.out.println(line);
          result = result + line;
        } 

        isr = new InputStreamReader(proc.getErrorStream());
        rdr = new BufferedReader(isr);
        while((line = rdr.readLine()) != null) { 
          //System.out.println(line);
          result = result + line;
        } 
        proc.waitFor(); // Wait for the process to complete
        return result;
    }
    
    public String getTextFromURL(String link) throws MalformedURLException, IOException
    {
        URL url = new URL(link);
        Scanner sc = new Scanner(url.openStream());
        StringBuffer sb = new StringBuffer();
        while(sc.hasNext()) {
         sb.append(sc.next());
        }
        String result = sb.toString();
        //System.out.println(result);
        result = result.replaceAll("<[^>]*>", "");
        //System.out.println("Contents of the web page: "+result);
        return result;
    }
    
    public boolean downloadFilefromURL(String link,String filename)
    {
        try (BufferedInputStream in = new BufferedInputStream(new URL(link).openStream());
                FileOutputStream fileOutputStream = new FileOutputStream(filename)) {
            byte dataBuffer[] = new byte[1024];
            int bytesRead;
            while ((bytesRead = in.read(dataBuffer, 0, 1024)) != -1) {
                fileOutputStream.write(dataBuffer, 0, bytesRead);
            }
        } catch (IOException e) {
            return false;
        }
        return true;
    }
    
    public void ExtractFile(String filepath,String fileoutput) throws Exception {
        ZipInputStream zis = new ZipInputStream(new FileInputStream(filepath));
        while (null != zis.getNextEntry()){
           FileOutputStream fos = new FileOutputStream(fileoutput);
           int leido;
           byte [] buffer = new byte[1024];
           while (0<(leido=zis.read(buffer))){
              fos.write(buffer,0,leido);
           }
           fos.close();
           zis.closeEntry();
        }
    }
    
    public void ShowMessage(String message)
    {
        Thread t = new Thread(new Runnable() {
            public void run() {
                JOptionPane.showMessageDialog(null, message);
            }
        });
        t.start();
    }
}
