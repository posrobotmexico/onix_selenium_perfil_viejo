/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import java.awt.AWTException;
import java.awt.Color;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.IOException;
import java.nio.file.Files;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.List;

import java.util.logging.Level;
import java.util.logging.Logger;
//import javafx.scene.media.Media;
//import javafx.scene.media.MediaPlayer;
import javax.imageio.ImageIO;
import javax.sound.sampled.AudioSystem;
import javax.sound.sampled.Clip;
import org.apache.pdfbox.pdmodel.PDDocument;
import org.apache.pdfbox.pdmodel.PDPage;
import org.apache.pdfbox.pdmodel.PDPageContentStream;
import static org.apache.pdfbox.pdmodel.PDPageContentStream.AppendMode.APPEND;
import org.apache.pdfbox.pdmodel.PageMode;
import org.apache.pdfbox.pdmodel.font.PDType1Font;
import org.apache.pdfbox.pdmodel.graphics.image.PDImageXObject;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageFitWidthDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.destination.PDPageXYZDestination;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDDocumentOutline;
import org.apache.pdfbox.pdmodel.interactive.documentnavigation.outline.PDOutlineItem;

/**
 *
 * @author root
 * 
 * Poner un summary que diga el tiempo total de ejecucion, logo de la empresa, nombre del caso ejecutado, fecha, resultado del reporte (pass, fail), 
 */
public class Report {

    private PDDocument document;
    private String namefile;
    private String namecase;
    private PDPage currentPage;
    private int writeY;
    private long InitialTime;
    private boolean statusOK;
    private int stepNumber;
    private boolean finishTest;
    //private List<String> bookmarks;
    private String bookmarkCurrentPage;
    private String Error;

    public Report(String namefile) {
        this.CreateReport(namefile,"A:\\Reportes\\"+namefile);
    }
    
    public Report(String namefile,String fileoutput) {
        this.CreateReport(namefile,fileoutput);
    }
    public String getNamefile()
    {
        return namefile;
    }

    public Report() {
        throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
    }
    
    private void CreateReport(String namefile,String fileoutput)
    {
        document = new PDDocument();
        this.namecase = namefile;
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH-mm-ss dd-MM-yyyy");  
        LocalDateTime now = LocalDateTime.now();
        if(fileoutput.isEmpty())
            this.namefile = namefile + " - " + dtf.format(now) + ".pdf";
        else
            this.namefile = fileoutput + " - " + dtf.format(now) + ".pdf";
                
        InitialTime =  System.currentTimeMillis();
        writeY = 0;
        statusOK = true;
        stepNumber = 0;
        finishTest = false;
        //bookmarks = new ArrayList<String>();
        bookmarkCurrentPage = "";
        Error = "";
    }

    public byte[] takeScreenShot() 
    {
        Rectangle rectangleTam = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        Robot robot;
        try {
            robot = new Robot();

            BufferedImage bufferedImage = robot.createScreenCapture(rectangleTam);
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(bufferedImage, "png", baos);
                return baos.toByteArray();
            } catch (IOException ex) {
                Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (AWTException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public int ScreenshotElement(String text,byte[] imgByteArray) {
        this.ScreenShot(text,imgByteArray,35);
        return stepNumber;
    }
    
    public int ScreenshotAll(String text) {
        this.ScreenShot(text,this.takeScreenShot(),30);
        return stepNumber;
    }
    
    public void ScreenShot(String text) {
        this.ScreenShot(text,this.takeScreenShot(),30);
    }
    
    public void ScreenShot(String text,byte[] imgByteArray,int Scale) {
        this.writeMessage(text);
        this.writeImage(imgByteArray,Scale);
    }
    
    public void AddImageToReport(String description,byte[] imgByteArray)
    {
        this.ScreenShot(description,imgByteArray,35);
    }
    
    public void writeLine(Color color)
    {
        try {
            this.validateNewPage(10);      
            try (PDPageContentStream contentStream = new PDPageContentStream(document, currentPage, APPEND, false)) {

                contentStream.setStrokingColor(color);
                float startX = currentPage.getCropBox().getLowerLeftX() + 20;
                float endX = currentPage.getCropBox().getUpperRightX() - 20;
                writeY -= 10;
                contentStream.moveTo(startX, writeY);
                contentStream.lineTo(endX, writeY);
                contentStream.stroke();                           
            }
        } catch (IOException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void writeError(String error)
    {
        writeY = 0;
        statusOK = false;
        Error = error;
        writeLine(Color.RED);
        this.writeMessage("Error. La maquina virtual reporto el siguiente error.", 16,Color.RED);
        this.ScreenShot(error,this.takeScreenShot(),30); 
        writeLine(Color.RED);
    }
    
    private void validateNewPage(int value)
    {
        if (writeY - value <= 0) {            
            currentPage = new PDPage();
            this.addPageFoot();
            writeY = (int) currentPage.getMediaBox().getHeight() - 16;
            document.addPage(currentPage);
            /*if(!bookmarkCurrentPage.isBlank())
            {
                System.out.println("Textooo: " + bookmarkCurrentPage);
                bookmarks.add(bookmarkCurrentPage.substring(0, bookmarkCurrentPage.length()-2));
                bookmarkCurrentPage = "";
            }*/
        }
    }
    
    public List<String> getCuttedList(String text,int range)
    {
        List<String> lines = new ArrayList<String>();
        int i = 0;
        while(i < text.length())
        {
            if(i + range >= text.length())
                lines.add(text.substring(i,text.length()));
            else
                lines.add(text.substring(i,i+range));
            i += range;
        }
        return lines;
    }
    
    public void writeMessage(String text)
    {
        this.writeMessage(text,11,Color.BLACK);
    }
    
    private void addCurrentPageBookmark(String text)
    {
        if(text.length()-1 > 20)
            bookmarkCurrentPage = bookmarkCurrentPage + text.substring(0, 20) + " & ";
        else
            bookmarkCurrentPage = bookmarkCurrentPage + text.substring(0, text.length()-1) + " & ";
    }
    
    private String formattingText(String text)
    {
        text = text.replace("\n", "").replace("\r", "");
        //addCurrentPageBookmark(text);
        
        if(!finishTest && statusOK)
        {
            stepNumber += 1;
            text = "(" + stepNumber + ")" + " " + text ;
        }
        
        return text;
    }
    
    public void writeMessage(String text,int lettersize,Color color) {
        
        text = this.formattingText(text);        
        List<String> lines = getCuttedList(text,109); 
        
        try {
            this.validateNewPage(lines.size() * 10);           
            try (PDPageContentStream contentStream = new PDPageContentStream(document, currentPage, APPEND, false)) {

                for (String line: lines)
                {
                    writeY -= lettersize + 4;
                    contentStream.beginText();
                    contentStream.setFont(PDType1Font.HELVETICA, lettersize);
                    contentStream.setNonStrokingColor(color);
                    contentStream.newLineAtOffset(20, writeY);
                    contentStream.showText(line);
                    contentStream.endText();
                    
                }
            }
        } catch (IOException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void addPageFoot() {
        try {        
            try (PDPageContentStream contentStream = new PDPageContentStream(document, currentPage, APPEND, false)) {

                contentStream.beginText();
                contentStream.setFont(PDType1Font.HELVETICA, 9); // PDType1Font.HELVETICA
                contentStream.setNonStrokingColor(Color.BLUE);
                contentStream.newLineAtOffset(20, 15);
                contentStream.showText(namecase + ". Practia. Página " + (document.getNumberOfPages() + 1));
                contentStream.endText();
                    
            }
        } catch (IOException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void writeImage(byte[] imgByteArray,int Scale)
    {
        try {
            PDImageXObject image = PDImageXObject.createFromByteArray(document, imgByteArray, "Report");
            this.validateNewPage(image.getHeight() * Scale / 100 + 10);
            
            try (PDPageContentStream contentStream = new PDPageContentStream(document, currentPage, APPEND, false)) {
                writeY -= image.getHeight() * Scale / 100;
                writeY -= 15;
                contentStream.drawImage(image,20, writeY, image.getWidth() * Scale / 100, image.getHeight() * Scale / 100);
                
            }            
        } catch (IOException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public byte[] FiletoByteArray(String filename) 
    {
        try {
            File file = new File(filename);
            return Files.readAllBytes(file.toPath());
        } catch (IOException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }
    
    public void WriteSummary()
    {
        writeY = 0;
        finishTest = true;
        long totalTime = System.currentTimeMillis() - InitialTime;
        
        DateTimeFormatter dtf = DateTimeFormatter.ofPattern("HH:mm:ss - dd/MM/yyyy");  
        LocalDateTime now = LocalDateTime.now();
        
        this.writeMessage("Summary", 26, Color.BLACK);
        this.writeMessage("Nombre de la prueba: " + namecase);
        this.writeMessage("Hora y fecha de finalización: " + dtf.format(now));
        this.writeMessage("Equipo: "+ System.getenv("COMPUTERNAME"));
        this.writeMessage("Numero de pasos realizados: "+ stepNumber);
        if(statusOK)
        {
            writeLine(Color.GREEN);
            this.writeMessage("Resultado: successful",22,Color.GREEN);
            writeLine(Color.GREEN);
        }
        else
        {
            writeLine(Color.RED);
            this.writeMessage("Resultado: Failed",22,Color.RED);
            this.writeMessage("Error en el paso "+stepNumber+" de la prueba. Error:",11,Color.RED);
            this.writeMessage(Error,11,Color.RED);
            writeLine(Color.RED);
        }
        this.writeMessage("Tiempo total de ejecución: " + Float.toString(totalTime/1000) + " segundos. (" + timeExecution(totalTime/1000) + ")");
        this.writeLine(Color.YELLOW);
        this.writeSteps();
        this.writeImage(this.FiletoByteArray("A:/Perfiles/resources/practiaLogo.png"), 80);
    }
    
    public String timeExecution(Long seconds)
    {
        String res = "";
        if(seconds >= 3600)
        {
            res = res + Float.toString(seconds/3600) + " horas ";
            seconds = seconds - (seconds/3600) * 3600;
        }
        if(seconds >= 60)
        {
            res = res + Float.toString(seconds/60) + " minutos ";
            seconds = seconds - (seconds/60) * 60;
        }
        if(seconds != 0)
            res = res + Float.toString(seconds) + " segundos ";
        return res;
    }

    public void writeSteps()
    {
        PDDocumentOutline documentOutline = new PDDocumentOutline();
        document.getDocumentCatalog().setDocumentOutline(documentOutline);
        PDOutlineItem pagesOutline = new PDOutlineItem();
        pagesOutline.setTitle("Resumen");
        documentOutline.addLast(pagesOutline);

        for(int i = 0; i < document.getNumberOfPages(); i++) {
            PDPageDestination pageDestination = new PDPageFitWidthDestination();
            pageDestination.setPage(document.getPage(i));
            
            
            PDOutlineItem bookmark = new PDOutlineItem();
            
            bookmark.setDestination(pageDestination);
            if(i == document.getNumberOfPages()-1)
                bookmark.setTitle("Resumen");
            else
                bookmark.setTitle("Página " + (i + 1));
            //System.out.println(bookmarks.get(i));
                
            pagesOutline.addLast(bookmark);
        } 
       
        pagesOutline.openNode();
        documentOutline.openNode();
        document.getDocumentCatalog().setPageMode(PageMode.USE_OUTLINES);
        
        
                
        // Obtener los bookmarks
        /*
        PDDocumentOutline root = document.getDocumentCatalog().getDocumentOutline();
        PDOutlineItem item = root.getFirstChild();
        while( item != null )
        {
            System.out.println( "Item:" + item.getTitle() );
            PDOutlineItem child = item.getFirstChild();
            while( child != null )
            {
                System.out.println( "    Child:" + child.getTitle() );
                child = child.getNextSibling();
            }
            item = item.getNextSibling();
        }*/
    }
    
    public void Export() {
        try {
            this.WriteSummary();
            document.save(namefile);
            document.close();
            PlayFinishSound();
        } catch (IOException ex) {
            System.out.println("El reporte no pudo ser generado. razón: ");
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void PlayFinishSound()
    {
        play("A:\\Perfiles\\resources\\finish_report.wav");
    }
    
    public void play(String filename)
    {
        try
        {
            Clip clip = AudioSystem.getClip();
            clip.open(AudioSystem.getAudioInputStream(new File(filename)));
            clip.start();
        }
        catch (Exception exc)
        {
            exc.printStackTrace(System.out);
        }
    }
}
