/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import java.util.logging.Level;
import java.util.logging.Logger;
import javax.swing.JOptionPane;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.edge.EdgeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.firefox.FirefoxOptions;
import org.openqa.selenium.firefox.FirefoxProfile;

/**
 *
 * @author John-117
 */
public class WebDriv 
{
    public WebDriver WebDriver;
    
    //public WebDriver WebDriver;
    // Obtener la lista de procesos
    
    public WebDriv(String Url,String Navegador)
    {
       switch(Navegador)
       {
           case "Chrome":
               System.setProperty("webdriver.chrome.driver", "A:\\Perfiles\\WebDrivers\\Chrome\\chromedriver.exe");
               ChromeOptions options = new ChromeOptions();
               options.addArguments("start-maximized");
               //options.addArguments("--window-size=1920,1080");
               try{
                   WebDriver = new ChromeDriver(options);
               }catch(Exception ex)
               {
                  if(ex.getMessage().contains("Response code 500. Message: session not created: This version of ChromeDriver only supports Chrome version") || ex.getMessage().contains("The driver executable must exist: A:\\Perfiles\\WebDrivers\\Chrome\\chromedriver.exe"))
                       AutoUpdater();
                   else
                       System.out.println(ex); 
               }
               
               WebDriver.manage().window().maximize();
               WebDriver.navigate().to(Url);
               break;
               
           case "Firefox":               
               System.setProperty("webdriver.gecko.driver", "A:\\Perfiles\\WebDrivers\\Firefox\\geckodriver.exe");
               /*FirefoxOptions Optionss = new FirefoxOptions();
               Optionss.setProfile(new FirefoxProfile());
               Optionss.addPreference("privacy.trackingprotection.enabled", false);
               Optionss.addPreference("privacy.trackingprotection.cryptomining.enabled", false);
               Optionss.addPreference("privacy.trackingprotection.fingerprinting.enabled", false);
               Optionss.addPreference("privacy.trackingprotection.pbmode.enabled", false);
               Optionss.addPreference("privacy.trackingprotection.socialtracking.enabled", false);
               Optionss.addPreference("privacy.trackingprotection.origin_telemetry.enabled", false);
               Optionss.addPreference("browser.contentblocking.enabled", false);
               WebDriver = new FirefoxDriver(Optionss);*/
               WebDriver = new FirefoxDriver();
               WebDriver.manage().window().maximize();
               WebDriver.navigate().to(Url); 
               break;
               
           case "Edge":
               System.setProperty("webdriver.edge.driver", "A:\\Perfiles\\WebDrivers\\Edge\\msedgedriver.exe");
               WebDriver = new EdgeDriver();
               WebDriver.manage().window().maximize();
               WebDriver.navigate().to(Url); 
               break;    
             
               
           default:
               System.out.println("Navegador no Soportado");
               System.exit(0);
               break;
       }      
    }
    
    public void CloseWebDriver()//Cierra el navegador del Driver actual
    {
        WebDriver.quit();
    }
    
    public void AssingNewDriver(WebDriver nWebDriver)
    {
        WebDriver=nWebDriver;
        
    }

     public void AutoUpdater()
    {
        try {
            SystemA sistem = new SystemA();
            JOptionPane.showMessageDialog(null,"Se va a descargar el nuevo webDriver, se le informara cuando finalice, cierre este aviso para iniciar la actualización");
            String version = sistem.printCommand("wmic datafile where name=\"C:\\\\Program Files (x86)\\\\Google\\\\Chrome\\\\Application\\\\chrome.exe\" get Version /value");
            version = version.substring(version.indexOf("=")+1, version.indexOf("."));
            //System.out.println("Versión de chrome: "+ version);
            
            String versionOnline = sistem.getTextFromURL("https://chromedriver.storage.googleapis.com/LATEST_RELEASE");
            //System.out.println("Version web del chromedriver: "+ versionOnline);
            
            if(version.contentEquals(versionOnline.substring(0, versionOnline.indexOf("."))))
            {
                if(sistem.downloadFilefromURL("https://chromedriver.storage.googleapis.com/"+ versionOnline +"/chromedriver_win32.zip","A:\\Perfiles\\WebDrivers\\Chrome\\chromedriver.zip"))
                {
                    sistem.ExtractFile("A:\\Perfiles\\WebDrivers\\Chrome\\chromedriver.zip","A:\\Perfiles\\WebDrivers\\Chrome\\chromedriver.exe");
                    JOptionPane.showMessageDialog(null,"Se ha actualizado el webDriver, inicie nuevamente la aplicación");
                    System.exit(0);
                }
            }
            else
                JOptionPane.showMessageDialog(null,"Error desconocido al actualizar el webdriver, la versión online no coincide con la versión de chrome");
        } catch (Exception ex) {
            Logger.getLogger(WebDriv.class.getName()).log(Level.SEVERE, null, ex);
            JOptionPane.showMessageDialog(null,"No se ha podido actualizar el webdriver. "+ex);
        }
    }

    
}
