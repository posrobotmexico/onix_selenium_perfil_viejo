/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Core;

import java.time.Duration;
import java.util.List;
import java.util.concurrent.TimeUnit;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.ui.ExpectedConditions;
import org.openqa.selenium.support.ui.Select;
import org.openqa.selenium.support.ui.WebDriverWait;

/**
 *
 * @author John-117
 */
public class WebAction {
    
    
    public WebElement FindElementByID(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        WebElement Element;
        Element=null;
        try
        {
            Element=drive.findElement(By.id(NameElement));
        }catch(Exception e)
        {            
        }
        return Element;
    }
    
    public WebElement FindElementByXPath(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        WebElement Element;
        Element=null;
        try
        {
            Element=drive.findElement(By.xpath(NameElement));
        }catch(Exception e)
        {
            System.out.print("Error Elemento "+NameElement+" no encontrado Detalle de error: \n"+e);
        }
        return Element;
    }
    
    public WebElement FindElementByLinkText(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        WebElement Element;
        Element = null;
        try
        {
            Element=drive.findElement(By.linkText(NameElement));
        }catch(Exception e)
        {
            System.out.print("Error Elemento "+NameElement+" no encontrado Detalle de error: \n"+e);
        }
        return Element;
    }
    
    public List<WebElement> FindElementsByClassName(WebDriver drive, String NameElement)//Busca el elemento por ID regresa un objeto
    {
        List<WebElement> Element;
        Element=null;
        try
        {
            Element=drive.findElements(By.className(NameElement));
        }catch(Exception e)
        {
            System.out.print("Error Elemento "+NameElement+" no encontrado Detalle de error: \n"+e);
        }
        return Element;
    }
    
    
    public void TypeText(WebElement Element,String Text)//Hace el type text de un elemento 
    {
        try
        {
            //Element.click();
            Element.sendKeys(Text);
            Thread.sleep(500);
        }catch(Exception e)
        {
            System.out.print("Error al hacer TypeText Sobre elemento Detalle de error: \n"+e);
            //System.exit(0);
        }
    }
    
    public void SelenSelect(WebElement Element,String Type,String Value)
    {
        Select selectObj;
        try
        {
            switch(Type)
            {
                case "Value"://Seleccion por Valor
                    selectObj=new Select(Element);
                    selectObj.selectByValue(Value);
                    break;
                    
                case "Index":
                    selectObj=new Select(Element);                    
                    selectObj.selectByIndex(Integer.parseInt(Value));                     
                    break;

                case "Text":
                    selectObj=new Select(Element);                    
                    selectObj.selectByVisibleText(Value);                     
                    break;
                    
                default:
                    break;
            }
        }catch(Exception e)
        {
            System.out.print("Error al escoger el valor del select Detalle de error: \n"+e);
            System.exit(0);
        }
      
    }
    
    
    public void ClickById(WebDriver driver,String NameElement) throws InterruptedException
    {
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            List <WebElement> Button=driver.findElements(By.id(NameElement));
            int count = Button.size();
            if(count==1)
                Button.get(0).click();
            else if(count>1)
            {
                int c=0;
                Boolean ban=false;
                while(c<count)
                {
                    try
                    {
                        Button.get(c).click();
                        ban=true;
                        break;
                    }catch(Exception e)
                    {
                        c++;
                    }
                }
                if(!ban)
                {
                    System.out.print("No se dio click al boton");
                }
            }
            else
            {
                System.out.print("Boton no encontrado");
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
        }      
    }
     
    public void ClickButtonDinamic(WebDriver driver,WebAction WebAction,String TipBus,String NameElement,String TextElement) throws InterruptedException
    {   //Siempre se buscara dar click a span por lo cual el metodo sirve con DIV Teniendo de hijos a SPAN
        
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            List <WebElement> Button=null;
            switch(TipBus)
            {
                case "Id":
                    Button=driver.findElements(By.id(NameElement));
                    break;
                
                case "ClassName":
                    Button=driver.findElements(By.className(NameElement));
                    break;
                case "LinkText":
                    Button = driver.findElements(By.linkText(NameElement));
                    break;
                    
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    System.exit(0);
                    break;
            }
           
            if(Button.isEmpty())
            {
                System.out.println("No se detecto elemento devolvio lista vacia");
            }   
            int count = Button.size();
            Boolean Ban=false;
            for(int i=0;i<count;i++)
            {
                String Tipe=Button.get(i).getTagName();
                if(Tipe.equals("span"))
                {
                    String $TextoBtn = Button.get(i).getText();
                    if($TextoBtn.equals(TextElement ))
                    {
                        Ban=true;
                        Button.get(i).click();
                        break;
                    }              
            
                }
                else if(Tipe.equals("button"))
                {
                    String $TextoBtn = Button.get(i).getText();
                    if($TextoBtn.equals(TextElement ))
                    {
                        Ban=true;
                        Button.get(i).click();
                        break;
                    }              
            
                }
                else
                {
                    List <WebElement> Div=WebAction.GetChildren(Button.get(i));
                    for(int j=0;j<Div.size();j++)
                    {
                        String Val=Button.get(j).getText();
                        if(Val.contentEquals(TextElement))
                        {
                            Button.get(j).click();
                            Ban=true;
                            break;
                        }
                        else
                        {
                           List <WebElement> Div2=WebAction.GetChildren(Button.get(j));                           
                           for(int k=0;k<Div2.size();k++)
                           {
                               String Val2=Button.get(k).getText();
                                if(Div2.isEmpty()==true && Val2.contentEquals(TextElement))
                                 {
                                     Button.get(j).click();
                                     Ban=true;
                                     break;
                                 } 
                           }
                        }
                    }
                        
                    
                }
            }
                
            if(Ban==false)
            {
                 System.out.println("No se detecto el boton con el nombre "+TextElement);
                 throw new NullPointerException( "No se encontro el boton" );
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            throw new NullPointerException( "No se encontro el boton" );
            
        }      
    }
    public List<WebElement> GetChildren(WebElement Element)
    {
        List<WebElement> childrenElements = Element.findElements(By.xpath("*"));
        return childrenElements;
    }

public void ClickButton(WebDriver driver,WebAction SelenElem,String TipBus,String NameElement) throws InterruptedException
    {   //Siempre se buscara dar click a span por lo cual el metodo sirve con DIV Teniendo de hijos a SPAN
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            switch(TipBus)
            {
                case "Id":
                    driver.findElement(By.id(NameElement)).click();
                    break;
                
                case "ClassName":
                    driver.findElement(By.className(NameElement)).click();
                    break;
                    
                case "LinkText":
                    driver.findElement(By.linkText(NameElement)).click();
                    break;
                    
                case "CSS":
                    driver.findElement(By.cssSelector(NameElement)).click();
                    break;
                    
                case "Name":
                driver.findElement(By.name(NameElement)).click();
                break;
                
                case "PartialLinkText":
                driver.findElement(By.partialLinkText(NameElement)).click();
                break;
              
                case "TagName":
                driver.findElement(By.tagName(NameElement)).click();
                break;
                    
                case "Xpath":
                driver.findElement(By.xpath(NameElement)).click();
                break;
                
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    break;
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
        }      
    }
    
    public void CargaAjax(WebDriver drive) throws InterruptedException
    {
        int tiempo = 10 ;// tiempo en minutos maximo de espera, para que no se cicle si pasa algo raro
        Boolean Ban =null;        
        try{
            tiempo = tiempo * 8000;
            WebElement Element = drive.findElement(By.id("bme_monitor"));            
            if(Element!=null)
            {
                while(Element!=null && tiempo > 0)
                {

                    try
                    {
                       Ban=Element.isDisplayed(); 
                    }
                    catch(Exception e)
                    {
                        Ban=false;
                    }

                    if(Ban==false)
                    {
                        break;
                    }
                    else
                    {
                        Thread.sleep(100); // duerme 100 ms para no sobre cargar el procesador
                        tiempo -= 1;
                    }
                }
            }
            else
            {
                    
            }
                
            
        }
        catch (InterruptedException ex) {
            System.out.println("Se supero el tiempo");
            Logger.getLogger(WebAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }



public WebElement FindElement(WebDriver driver,WebAction WebAction,String TipBus,String NameElement) throws InterruptedException
    {   //Buscar elemento
        WebElement Element;
        Element = null;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            switch(TipBus)
            {
                case "Id":
                    Element = driver.findElement(By.id(NameElement));
                    break;
                
                case "ClassName":
                    Element = driver.findElement(By.className(NameElement));
                    break;
                    
                case "LinkText":
                    Element = driver.findElement(By.linkText(NameElement));
                    break;
                    
                case "CCS":
                    Element = driver.findElement(By.cssSelector(NameElement));
                    break;
                    
                case "Name":
                    Element = driver.findElement(By.name(NameElement));
                    break;
                
                case "PartialLinkText":
                    Element = driver.findElement(By.partialLinkText(NameElement));
                    break;
              
                case "TagName":
                    Element = driver.findElement(By.tagName(NameElement));
                    break;                    
                    
                case "Xpath":
                    Element = driver.findElement(By.xpath(NameElement));
                    break;
                
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    break;
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            
        }
        return Element;
    }
public String GetText(WebDriver driver,String TipBus,String NameElement) throws InterruptedException
    {   //Obtiene text del elemento
        String Element;
        Element = null;
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            switch(TipBus)
            {
                case "Id":
                    Element = driver.findElement(By.id(NameElement)).getText();
                    break;
                
                case "ClassName":
                    Element = driver.findElement(By.className(NameElement)).getText();
                    break;
                    
                case "LinkText":
                    Element = driver.findElement(By.linkText(NameElement)).getText();
                    break;
                    
                case "CCS":
                    Element = driver.findElement(By.cssSelector(NameElement)).getText();
                    break;
                    
                case "Name":
                    Element = driver.findElement(By.name(NameElement)).getText();
                    break;
                
                case "PartialLinkText":
                    Element = driver.findElement(By.partialLinkText(NameElement)).getText();
                    break;
              
                case "TagName":
                    Element = driver.findElement(By.tagName(NameElement)).getText();
                    break;
                    
                    
                case "Xpath":
                    Element = driver.findElement(By.xpath(NameElement)).getText();
                    break;
                
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    System.exit(0);
                    break;
            }
        }
        catch(Exception e)
        {

        }
        return Element;
    } 
    
    public void focus(WebDriver Driver,WebElement elemento)
    {
        ((JavascriptExecutor) Driver).executeScript("arguments[0].scrollIntoView(true); window.scrollBy(0, -window.innerHeight / 4);",elemento);
        new Actions(Driver).moveToElement(elemento).perform(); 
    }
    
    public void loadElement(WebDriver Driver,WebElement elemento)
    {
        WebDriverWait wait = new WebDriverWait(Driver,60); // 1 minuto de espera de carga
        wait.until(ExpectedConditions.elementToBeClickable(elemento));
    }
    
    public void clickJs(WebDriver Driver,WebElement elemento)
    {
        JavascriptExecutor js = (JavascriptExecutor)Driver; 
        js.executeScript("arguments[0].click();", elemento);
    }
    
      public void WaitFor(WebDriver Driver,WebElement elemento)
    {
        WebDriverWait wait = new WebDriverWait(Driver, 60); // 1 minuto de espera de carga
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("about_me")));
        wait.until(ExpectedConditions.visibilityOf(elemento));
    }
    
    public void WaitForLoad(WebDriver Driver,By by,int timeinseconds)
    {
        WebDriverWait wait = new WebDriverWait(Driver, timeinseconds); 
        wait.until(ExpectedConditions.presenceOfElementLocated(by));
    }
      public void WaitForMulti(WebDriver Driver,List<WebElement>  elemento)
    {
        WebDriverWait wait = new WebDriverWait(Driver,60); // 1 minuto de espera de carga
        //wait.until(ExpectedConditions.visibilityOfElementLocated(By.id("about_me")));
        wait.until(ExpectedConditions.visibilityOfAllElements(elemento));
    }
        public void WaitForElem(WebDriver Driver, String Elem)
    {
        
        int tiempo = 10 ;// tiempo en minutos maximo de espera, para que no se cicle si pasa algo raro
        Boolean Ban =null, Bandera = null;
        WebElement Element = null;
        try{
            tiempo = tiempo * 8000;
            while(Element!=null && tiempo > 0)
                {

                    try
                    {
                       Element = Driver.findElement(By.id(Elem));   
                       Ban=Element.isDisplayed(); 
                    }
                    catch(Exception e)
                    {
                        Ban=false;
                    }

                    if(Ban==true)
                    {
                        break;
                    }
                    else
                    {
                        Thread.sleep(100); // duerme 100 ms para no sobre cargar el procesador
                        tiempo -= 1;
                    }
                }
                      
            if(Element!=null)
            {
                while(Element!=null && tiempo > 0)
                {

                    try
                    {
                       Ban=Element.isDisplayed();
                       if(Ban.equals(true))
                       {
                           Bandera = true;
                       }
                       
                    }
                    catch(Exception e)
                    {
                        Ban=false;
                    }

                    if(Bandera==true)
                    {
                        break;
                    }
                    else
                    {
                        Thread.sleep(100); // duerme 100 ms para no sobre cargar el procesador
                        tiempo -= 1;
                    }
                }
            }
            else
            {
                    
            }
            
            }
        catch (InterruptedException ex) {
            System.out.println("Se supero el tiempo");
            Logger.getLogger(WebAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
        
            public void WaitForElemName(WebDriver Driver, String Elem)
    {
        
        int tiempo = 10 ;// tiempo en minutos maximo de espera, para que no se cicle si pasa algo raro
        Boolean Ban =null, Bandera = null;
        WebElement Element = null;
        try{
            tiempo = tiempo * 8000;
            while(Element!=null && tiempo > 0)
                {

                    try
                    {
                       Element = Driver.findElement(By.name(Elem));   
                       Ban=Element.isDisplayed(); 
                    }
                    catch(Exception e)
                    {
                        Ban=false;
                    }

                    if(Ban==true)
                    {
                        break;
                    }
                    else
                    {
                        Thread.sleep(100); // duerme 100 ms para no sobre cargar el procesador
                        tiempo -= 1;
                    }
                }
                      
            if(Element!=null)
            {
                while(Element!=null && tiempo > 0)
                {

                    try
                    {
                       Ban=Element.isDisplayed();
                       if(Ban.equals(true))
                       {
                           Bandera = true;
                       }
                       
                    }
                    catch(Exception e)
                    {
                        Ban=false;
                    }

                    if(Bandera==true)
                    {
                        break;
                    }
                    else
                    {
                        Thread.sleep(100); // duerme 100 ms para no sobre cargar el procesador
                        tiempo -= 1;
                    }
                }
            }
            else
            {
                    
            }
            
            }
        catch (InterruptedException ex) {
            System.out.println("Se supero el tiempo");
            Logger.getLogger(WebAction.class.getName()).log(Level.SEVERE, null, ex);
        }
        
    }
      
    public WebElement FindMultiElem(WebDriver driver,WebAction WebAction,String TipBus,String NameElement,String TextElement) throws InterruptedException
    {   //Siempre se buscara dar click a span por lo cual el metodo sirve con DIV Teniendo de hijos a SPAN
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement Elem=null;
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            List <WebElement> Button=null;
            
            switch(TipBus)
            {
                case "Id":
                    Button=driver.findElements(By.id(NameElement));
                    break;
                
                case "ClassName":
                    Button=driver.findElements(By.className(NameElement));
                    break;
                case "LinkText":
                    Button = driver.findElements(By.linkText(NameElement));
                    break;
                    
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    System.exit(0);
                    break;
            }
           
            if(Button.isEmpty())
            {
                System.out.println("No se detecto elemento devolvio lista vacia");
                System.exit(0);
            }   
            int count = Button.size();
            Boolean Ban=false;
            for(int i=0;i<count;i++)
            {
                String Tipe=Button.get(i).getTagName();
                if(Tipe.equals("span"))
                {
                    String $TextoBtn = Button.get(i).getText();
                    if($TextoBtn.equals(TextElement ))
                    {
                        Ban=true;
                        Elem = Button.get(i);
                        break;
                    }              
            
                }
                else
                {
                    List <WebElement> Div=WebAction.GetChildren(Button.get(i));
                    for(int j=0;j<Div.size();j++)
                    {
                        String Val=Button.get(j).getText();
                        if(Val.contentEquals(TextElement))
                        {
                            Elem = Button.get(j);
                            Ban=true;
                            break;
                        }
                        else
                        {
                           List <WebElement> Div2=WebAction.GetChildren(Button.get(j));                           
                           for(int k=0;k<Div2.size();k++)
                           {
                               String Val2=Button.get(k).getText();
                                if(Div2.isEmpty()==true && Val2.contentEquals(TextElement))
                                 {
                                     Elem = Button.get(j);
                                     Ban=true;
                                     break;
                                 } 
                           }
                        }
                    }
                        
                    
                }
            }
                
            if(Ban==false)
            {
                 System.out.println("No se detecto el boton con el nombre "+TextElement);
                 
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            
        }  
        
        return Elem;
    }
    
    
    
    public List<WebElement> FindElements(WebDriver driver,String TipBus,String NameElement) throws InterruptedException
    {
        List<WebElement> Element;
        Element=null;
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);

            switch(TipBus)
            {
                case "Id":
                    Element = driver.findElements(By.id(NameElement));
                    break;
                
                case "ClassName":
                    Element = driver.findElements(By.className(NameElement));
                    break;
                    
                case "LinkText":
                    Element = driver.findElements(By.linkText(NameElement));
                    break;
                    
                case "CCS":
                    Element = driver.findElements(By.cssSelector(NameElement));
                    break;
                    
                case "Name":
                    Element = driver.findElements(By.name(NameElement));
                    break;
                
                case "PartialLinkText":
                    Element = driver.findElements(By.partialLinkText(NameElement));
                    break;
              
                case "TagName":
                    Element = driver.findElements(By.tagName(NameElement));
                    break;                    
                    
                case "Xpath":
                    Element = driver.findElements(By.xpath(NameElement));
                    break;
                
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    System.exit(0);
                    break;
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            
        }
        return Element;
    
    }
    
    public WebElement FindMultiElemFB(WebDriver driver,WebAction WebAction,String TipBus,String NameElement,String TextElement) throws InterruptedException
    {   //Siempre se buscara dar click a span por lo cual el metodo sirve con DIV Teniendo de hijos a SPAN
        driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        WebElement Elem=null;
        try
        {
            driver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
            List <WebElement> Button=null;
            
            switch(TipBus)
            {
                case "Id":
                    Button=driver.findElements(By.id(NameElement));
                    break;
                
                case "ClassName":
                    Button=driver.findElements(By.className(NameElement));
                    break;
                case "LinkText":
                    Button = driver.findElements(By.linkText(NameElement));
                    break;
                    
                default:
                    System.out.println("No esta definido el tipo de busqueda");
                    System.exit(0);
                    break;
            }
           
            if(Button.isEmpty())
            {
                System.out.println("No se detecto elemento devolvio lista vacia");
                System.exit(0);
            }   
            int count = Button.size();
            Boolean Ban=false;
            for(int i=0;i<count;i++)
            {
                String Tipe=Button.get(i).getTagName();
                if(Tipe.equals("span"))
                {
                    String $TextoBtn = Button.get(i).getText();
                    if($TextoBtn.equals(TextElement ))
                    {
                        Ban=true;
                        Elem = Button.get(i);
                        break;
                    }              
            
                }
                else
                {
                    List <WebElement> Div=WebAction.GetChildren(Button.get(i));
                    for(int j=0;j<Div.size();j++)
                    {
                        String Val=Button.get(j).getText();
                        if(Val.contentEquals(TextElement))
                        {
                            Elem = Button.get(j);
                            Ban=true;
                            break;
                        }
                        else
                        {
                           List <WebElement> Div2=WebAction.GetChildren(Button.get(j));                           
                           for(int k=0;k<Div2.size();k++)
                           {
                               String Val2=Button.get(k).getText();
                                if(Div2.isEmpty()==true && Val2.contentEquals(TextElement))
                                 {
                                     Elem = Button.get(k);
                                     Ban=true;
                                     break;
                                 }
                                else
                                {
                                   List <WebElement> Div3=WebAction.GetChildren(Button.get(k));                           
                                   for(int l=0;l<Div3.size();l++)
                                   {
                                       String Val3=Button.get(l).getText();
                                        if(Div3.isEmpty()==true && Val3.contentEquals(TextElement))
                                         {
                                             Elem = Button.get(l);
                                             Ban=true;
                                             break;
                                         }
                                        else
                                        {
                                           List <WebElement> Div4=WebAction.GetChildren(Button.get(l));                           
                                           for(int x=0;x<Div4.size();x++)
                                           {
                                               String Val4=Button.get(l).getText();
                                                if(Div4.isEmpty()==true && Val4.contentEquals(TextElement))
                                                 {
                                                     Elem = Button.get(x);
                                                     Ban=true;
                                                     break;
                                                 }
                                           }
                                        }
                                   }
                                }
                           }
                        }
                    }
                        
                    
                }
            }
                
            if(Ban==false)
            {
                 System.out.println("No se detecto el boton con el nombre "+TextElement);
                 
            }
        }
        catch(Exception e)
        {
            System.out.print("Error al dar Click al boton: \n"+e);
            
        }  
        
        return Elem;
    }
    
    
    public void EsperaElem(WebDriver driver,String Elemento) throws InterruptedException
    {
        int tiempo = 3 ;// tiempo en minutos maximo de espera, para que no se cicle si pasa algo raro
        Boolean Ban;        
        try{
            tiempo = tiempo * 8000;
            WebElement Element = driver.findElement(By.id(Elemento));            
            if(Element!=null)
            {
                while(Element!=null && tiempo > 0)
                {

                    try 
                    {
                       Ban=Element.isDisplayed(); 
                    }
                    catch(Exception e) 
                    {
                        Ban=true; 
                    }

                    if(Ban==true)
                    {
                        break;
                    }
                    else
                    {
                        Thread.sleep(100); // duerme 100 ms para no sobre cargar el procesador
                        tiempo -= 1;
                        Element = driver.findElement(By.id(Elemento)); 
                    }
                }
            }
            else
            {
                    
            }
                
            
        }
        catch (InterruptedException ex) {
            System.out.println("Se supero el tiempo");
            Logger.getLogger(WebAction.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
    
    public void Over(WebDriver driver, String ID)
    {
        Actions action = new Actions(driver);
        WebElement element = driver.findElement(By.id(ID));
        action.moveToElement(element).perform();
        
    }
    
    public void DobleClick(WebDriver driver, WebElement Elem)
    {
        Actions action = new Actions(driver);
        action.doubleClick(Elem).perform();        
    }
    
}

