
package Operaciones;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.OutputStreamWriter;
import java.nio.charset.Charset;
import java.util.ArrayList;
import java.util.List;
import java.util.logging.Level;
import java.util.logging.Logger;


public class CargaVariables {
    
    public static void EscribeCSV(int a,String text,String Ruta, String Archivo) 
    {
        FileWriter csvWriter;
        try {
            String toWrite = "";
            for(int i = 0; i < a-1; i++)
            {
                toWrite = toWrite + ",";
            }
            toWrite = toWrite + text;
            csvWriter = new FileWriter("A:\\DataOnix\\"+Ruta+"\\"+Archivo+".csv");
            csvWriter.append(toWrite);
            
            csvWriter.flush();
            csvWriter.close();
        
        } catch (IOException ex) {
            Logger.getLogger(CargaVariables.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print("No fue posible leer el archivo CSV");
        }
    }
    
    public static String LeerCSV(int a,String Ruta, String Archivo) 
    {   
        String User = null;
        try
        {
        // Abro el .csv en buffer de lectura
            BufferedReader bufferLectura = new BufferedReader(new InputStreamReader(new FileInputStream("A:\\DataOnix\\"+Ruta+"\\"+Archivo+".csv"), "ISO-8859-1"));
            //BufferedReader bufferLectura = new BufferedReader(new FileReader("A:\\DataOnix\\"+Ruta+"\\"+Archivo+".csv"));

            // Leo una línea del archivo
            String linea = bufferLectura.readLine();

            while (linea != null) 
            {
                    // Separa la línea leída con el separador definido previamente
                    String[] campos = linea.split(String.valueOf(","));
                    User = campos[a];

                    // Vuelvo a leer del fichero
                    linea = bufferLectura.readLine();
            }

            // CIerro el buffer de lectura
            if (bufferLectura != null) 
            {
                    bufferLectura.close();
            }
        }
        catch(Exception e)
        {
            
        }
        return User;
    }
    
    
    
    public static String LeerCSV2(int a,String Ruta, String Archivo) 
    {   
        String User = null;
        try
        {
        // Abro el .csv en buffer de lectura
            BufferedReader bufferLectura = new BufferedReader(new InputStreamReader(new FileInputStream("A:\\DataOnix\\"+Ruta+"\\"+Archivo+".csv"), "UTF-8"));
            //BufferedReader bufferLectura = new BufferedReader(new FileReader("A:\\DataOnix\\"+Ruta+"\\"+Archivo+".csv"));

            // Leo una línea del archivo
            String linea = bufferLectura.readLine();

            while (linea != null) 
            {
                    // Separa la línea leída con el separador definido previamente
                    String[] campos = linea.split(String.valueOf(","));
                    User = campos[a];

                    // Vuelvo a leer del fichero
                    linea = bufferLectura.readLine();
            }

            // CIerro el buffer de lectura
            if (bufferLectura != null) 
            {
                    bufferLectura.close();
            }
        }
        catch(Exception e)
        {
            
        }
        return User;
    }
    
    public static ArrayList LeerCSV3(String Ruta, String Archivo) throws FileNotFoundException, IOException
    {   
        String line = null;
        ArrayList<String[]> lista = new ArrayList<>();
        //ArrayList lista = new ArrayList();
        String[] linea=null;
        try
        {
        // Abro el .csv en buffer de lectura
		BufferedReader bufferLectura = new BufferedReader(new FileReader("A:\\DataUPC\\"+Ruta+"\\"+Archivo+".csv"));
		
		while ((line = bufferLectura.readLine()) != null) 
                {
                    //line = bufferLectura.readLine();
                    linea = line.split(String.valueOf(","));
                    lista.add(linea);
		}
		
		// CIerro el buffer de lectura
		if (bufferLectura != null) 
                {
			bufferLectura.close();
		}
        }
        catch(Exception e)
        {
            
        }
        return lista;
    }
    
     public static void EscribeTxt(ArrayList Tabla, String Ruta, String Archivo,Boolean Temporal) throws IOException 
    {
        //FileWriter csvWriter;
        OutputStreamWriter csvWriter;
        if(Temporal==true)
        {
           csvWriter = new OutputStreamWriter(new FileOutputStream("A:\\DataOnix\\"+Ruta+"\\"+Archivo+"_Temp"+".txt"),Charset.forName("ISO-8859-1"));
           //OutputStreamWriter osw = new OutputStreamWriter("A:\\DataUPC\\"+Ruta+"\\"+Archivo+".csv", Charset.forName("ISO-8859-1"));
           //csvWriter = new FileWriter("A:\\DataUPC\\"+Ruta+"\\"+Archivo+"_Temp"+".csv" , "ISO-8859-1"); 
        }
        else
        {
           csvWriter = new OutputStreamWriter(new FileOutputStream("A:\\DataOnix\\"+Ruta+"\\"+Archivo+".txt"),Charset.forName("ISO-8859-1"));
           //csvWriter = new FileWriter("A:\\DataUPC\\"+Ruta+"\\"+Archivo+".csv"); 
        }        
        try {
            //String toWrite = "";
            int Tamano = Tabla.size();
            for(int i = 0; i < Tamano; i++)
            {
                csvWriter.append((String)Tabla.get(i)); 
            }          
            csvWriter.flush();
            csvWriter.close();
        
        } catch (IOException ex) {
            Logger.getLogger(CargaVariables.class.getName()).log(Level.SEVERE, null, ex);
            System.out.print("No fue posible escribir el archivo TXT");
        }
    }
    
    
}
    



