
package Operaciones;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import onix_selenium.Menu;
import java.awt.AWTException;
import java.awt.Rectangle;
import java.awt.Robot;
import java.awt.Toolkit;
import java.awt.image.BufferedImage;
import java.io.BufferedReader;
import java.io.ByteArrayOutputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.InputStreamReader;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Date;
import java.util.List;
import java.util.concurrent.TimeUnit;
import static java.util.concurrent.TimeUnit.SECONDS;
import java.util.logging.Level;
import java.util.logging.Logger;
import javax.imageio.ImageIO;
import onix_selenium.Menu;
import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.ui.FluentWait;
import org.openqa.selenium.support.ui.Wait;

/**
 *
 * @author John-117
 */
public class OperacionesGenerales {

    public static void Login(WebDriv Driver,WebAction SelenElem) throws InterruptedException, IOException
    {
        CargaVariables Leer = new CargaVariables();
        String Password = null;
        String User = null;
        //Rutas
        User     = CargaVariables.LeerCSV(0,"ConfigOnix","Users");
        Password = CargaVariables.LeerCSV(1,"ConfigOnix","Users");
        //Lenguaje
        WebElement SelenLanguage;
        SelenLanguage=SelenElem.FindElementByID(Driver.WebDriver,"loginad");
        SelenElem.SelenSelect(SelenLanguage,"Value","Local");
        Driver.WebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);
        //Usuario
        WebElement Usuario;//Se crea un elemento de tipo WebElement
        Usuario=SelenElem.FindElementByID(Driver.WebDriver,"username");
        SelenElem.TypeText(Usuario,User);
        //Password
        WebElement Pass;
        Pass=SelenElem.FindElementByID(Driver.WebDriver,"password");
        SelenElem.TypeText(Pass,Password);
        
        SelenElem.ClickButton(Driver.WebDriver,SelenElem,"Id","submitBtn");
        Driver.WebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(10, SECONDS);
               
    }
    
    public void IframeActual(WebDriv Driver)
    {
        WebDriver nFrame = null;
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);               
        
    }
    
    public static void SwitchIframe(WebDriv Driver, String Frame)
    {
        WebDriver nFrame = null;
        
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
                
        nFrame = Driver.WebDriver.switchTo().frame(Frame); 
        if(nFrame.equals(null))
        {
            throw new NullPointerException( "No se encontro el Iframe" );
        }
        else
        {
            Driver.AssingNewDriver(nFrame);
        }
                
        
    }
       
        
        public static void ValidaSesion(WebDriv Driver,WebAction SelenElem) throws InterruptedException
    {
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(10, SECONDS);
        Driver.WebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            WebElement Btn;
            Btn=SelenElem.FindElementByID(Driver.WebDriver,"usm_continue");
            if(Btn!=null)
            {
                Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);
                SelenElem.ClickButton(Driver.WebDriver,SelenElem,"Id","usm_continue");
            }
        }
        catch(Exception e)
        {
            
        }  
        
    }
        
        public static void CierraBoletin(WebDriv Driver,WebAction SelenElem) throws InterruptedException
    {
        List <WebElement> Div,Tbody, Tr, Td, Div2,Img;
        //WebDriver nFrame = Driver.WebDriver.switchTo().parentFrame();
        //Driver.AssingNewDriver(nFrame);
        WebAction WA = new WebAction();
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(10, SECONDS);
        Driver.WebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        try
        {
            WebElement Btn;
            Btn=SelenElem.FindElementByID(Driver.WebDriver,"msgBoxDIV" );
            if(Btn!=null)
            {
                Div=   WA.GetChildren(Btn);
                Tbody= WA.GetChildren(Div.get(0));
                Tr=    WA.GetChildren(Tbody.get(0));
                Td=    WA.GetChildren(Tr.get(0));
                Div2=  WA.GetChildren(Td.get(0));
                Img=   WA.GetChildren(Div2.get(0));
                Img.get(1).click();                
               
            }
        }
        catch(Exception e)
        {
            
        }  
        
    }

     public void MapaSitio(WebDriver driver)
    {
        driver.manage().timeouts().pageLoadTimeout(100, SECONDS);
        Wait wait = new FluentWait(driver);
        WebElement MS=driver.findElement(By.id("sitemap"));
        MS.click();
    }
     
     public static void NavegaTab(WebDriv Driver,WebDriver driver,WebAction WebAction, String IdTab,String NameTab, Boolean IsTop)//Navega Pestaña y selecciona frame
     {
         //IdTab = ul
         WebDriver nFrame;
         if(IsTop==true)
         {
            nFrame=null;
            //nFrame = driver.switchTo().parentFrame();
            nFrame = Driver.WebDriver.switchTo().defaultContent();
            Driver.AssingNewDriver(nFrame);
            driver.manage().timeouts().pageLoadTimeout(100, SECONDS);//espera carga de pagina
         }
        WebElement MS=driver.findElement(By.id(IdTab));
        List<WebElement> childrenElements=WebAction.GetChildren(MS);
         
       //int count = childrenElements.size();//nos devuleve el tamaño del array
       
       for(int i=0;i<childrenElements.size(); i++) 
       {
           WebElement childrenElement = childrenElements.get(i);
           String name=childrenElement.getText();
           if(NameTab.contentEquals(name))
           {
               //Seleccion de frame
               childrenElement.click();
               String id=childrenElement.getAttribute("Id");
               String iFrame=id.replace("_head","_iframe");
               nFrame = driver.switchTo().frame(iFrame);               
               Driver.AssingNewDriver(nFrame);
               if(IsTop==true)
               {
                  String Parent=iFrame;
                  System.out.println(Parent);
               }
               break;
           }
           int t=i;//temporal para suma
           t++;
           if(t>=childrenElements.size())//Seleccion de ultima pestaña al estilo chilenos time
           {
               childrenElement.click();
               //String Cadena = childrenElement.getAccessibleName();
               String Cadena = childrenElement.getTagName();
               String id=childrenElement.getAttribute("name");
               String iFrame=id.replace("_head","_iframe");
               nFrame = driver.switchTo().frame(iFrame);
               Driver.AssingNewDriver(nFrame);
           }
       }
      
     }
     
     
     public void NavegaMapaSitio(WebDriv Drive,WebAction WebAction,String Menu1,String Menu2,String Menu3,Report reporte) throws InterruptedException
     {
         WebElement Lista;
         
         Drive.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);//Carga de pagina
         WebAction.ClickById(Drive.WebDriver,"sitemap");
         Thread.sleep(8000);//Carga de pagina         
         String Parent = OperacionesGenerales.NavegaTab360(Drive,Drive.WebDriver, WebAction, "tabpage_items", "Mapa del Sitio",true);
         reporte.AddImageToReport("Navegamos a la opcion: "+Menu3, OperacionesGenerales.ScreenShotElement(Drive));
         Lista=WebAction.FindElementByID(Drive.WebDriver,"catalog");
         List<WebElement> childrenElements=WebAction.GetChildren(Lista);
         childrenElements=WebAction.GetChildren(childrenElements.get(0));

         for(int i=0;i<childrenElements.size(); i++) 
         {
            WebElement childrenElement = childrenElements.get(i);
            String name=childrenElement.getText();
            if(Menu1.contentEquals(name))
            {
                childrenElement.click();
                break;
            }
         }
         childrenElements=WebAction.FindElementsByClassName(Drive.WebDriver,"crm_sitemap_category");
         int count = childrenElements.size();
         Boolean visible;
         Boolean enable;
         
         Boolean stop1=false;
         Boolean stop2=false;
         for(int i=0;i<count;i++)
         {
            visible=childrenElements.get(i).isDisplayed();
            enable=childrenElements.get(i).isEnabled();
            if(visible&&enable)
            {
               List <WebElement> hijo=WebAction.GetChildren(childrenElements.get(i));
               String name=hijo.get(0).getText();
               if(Menu2.contentEquals(name))
               {
                    stop1=true;
                    List <WebElement> nieto=WebAction.GetChildren(hijo.get(1));
                    for(int j=0;j<nieto.size();j++)
                    {
                        name=nieto.get(j).getText();
                        if(Menu3.contentEquals(name))
                        {
                            stop2=true;
                            List <WebElement> bisnieto=WebAction.GetChildren(nieto.get(j));
                            bisnieto.get(0).click();
                            break;
                        }
                    }
               }
               if(stop1&&stop2)
                   break;
            }
            
         }
         if(!stop1&&!stop2)
         {
             System.out.println("Error Seleccionando menu de Mapa de Sitio, no fue detectado");
             System.exit(0);
         }
         Thread.sleep(2000);
     }
     
     public static String Busqueda360(WebDriv Driver,WebAction SelenElem,String TipBusq,String DN,String Codigo) throws InterruptedException
     {
         //Variables
         String Txt,Txt2,Txt3;
         List <WebElement> Div2=null, Div3=null, Div4, Div5,Div6,ClickChil,ZizeTabla;
         WebAction WAC = new WebAction();
         OperacionesGenerales OPG = new OperacionesGenerales();         
         WebElement Tabla=null, Obj, Click;
         
         Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);//Carga de pagina
         SelenElem.ClickById(Driver.WebDriver,"newcustomer");
         Thread.sleep(8000);//Carga de pagina
         //OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Búsqueda de cliente",true);
         String Parent = OPG.NavegaTab360(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Búsqueda de cliente",true);
         
         if(TipBusq.equals("DN"))
         {
            WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"serviceNo_input_value");//Busqueda de cuadro de texto
            SelenElem.TypeText(Busqueda, DN);//Se ingresa el DN 
         }
         else if(TipBusq.equals("RFC"))
         {
             WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"rfc_input_value");
             SelenElem.TypeText(Busqueda, DN);//Se ingresa el RFC 
         }
         //WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"serviceNo_input_value");//Busqueda de cuadro de texto
         //SelenElem.TypeText(Busqueda, DN);//Se ingresa el DN
         
         //Se busca el boton "Buscar"
         WebElement Div = SelenElem.FindElementByID(Driver.WebDriver, "searchConditions");
         Div2= WAC.GetChildren(Div);
         Div3= WAC.GetChildren(Div2.get(0));
         Div4= WAC.GetChildren(Div3.get(1));
         Div5= WAC.GetChildren(Div4.get(3));
         Div6= WAC.GetChildren(Div5.get(0));
         Div6= WAC.GetChildren(Div6.get(0));
         Div6= WAC.GetChildren(Div6.get(0));
         Div6.get(1).click();
         Thread.sleep(2000);
         SelenElem.CargaAjax(Driver.WebDriver);
         
         //Se busca la cuenta, si no se encuentra se selecciona la primera 
         Tabla = SelenElem.FindElementByID(Driver.WebDriver, "contentList_databody");
         if (Tabla != null && Codigo.contentEquals("CODIGO"))
         {
            Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_9");
            ClickChil = SelenElem.GetChildren(Click);
            ClickChil.get(0).click();
         }
         else
         {
             
         
            Tabla = SelenElem.FindElementByID(Driver.WebDriver, "contentList_databody");
            if (Tabla!=null && Codigo!="")
            {             
                ZizeTabla = SelenElem.GetChildren(Tabla);
                int count = ZizeTabla.size();
                Boolean Ban=false;
                for(int i=0;i<count;i++)
                {
                    Obj = SelenElem.FindElementByID(Driver.WebDriver, "contentList_"+i+"_6");
                    Txt=Obj.getText();
                    if(Txt.contentEquals(Codigo))
                     {
                         Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_"+i+"_9");
                         ClickChil = SelenElem.GetChildren(Click);
                         ClickChil.get(0).click();
                         Ban=true;
                         break;            

                      }
                }
            }
            else if(Tabla!=null)
            {
                Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_9");
                ClickChil = SelenElem.GetChildren(Click);
                ClickChil.get(0).click();
            }
         }
         
        return Parent;
         
     } 
     
     public static String BusquedaCliente(WebDriv Driver,WebAction SelenElem,String TipBusq,String Busq , String Codigo) throws InterruptedException
     {
         //Variables
         WebElement Tabla=null, Obj, Click;
         String Txt,Txt2,Txt3;
         List <WebElement> Div2=null, Div3=null, Div4, Div5,Div6, ZizeTabla, ClickChil;
         WebAction WAC = new WebAction();
         OperacionesGenerales OPG = new OperacionesGenerales();         
         
         Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);//Carga de pagina
         //SelenElem.ClickById(Driver.WebDriver,"newcustomer");
         String Parent = OPG.NavegaTab360(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Búsqueda de cliente",true);
         //Thread.sleep(3000);
         SelenElem.WaitForElem(Driver.WebDriver, "serviceNo_input_value");
         if(TipBusq.equals("DN"))
         {
            WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"serviceNo_input_value");//Busqueda de cuadro de texto
            SelenElem.TypeText(Busqueda, Busq);//Se ingresa el DN 
         }
         else if(TipBusq.equals("RFC"))
         {
             WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"rfc_input_value");
             SelenElem.TypeText(Busqueda, Busq);//Se ingresa el RFC 
         }
                  
         //Se busca el boton "Buscar"
         WebElement Div = SelenElem.FindElementByID(Driver.WebDriver, "searchConditions");
         Div2= WAC.GetChildren(Div);
         Div3= WAC.GetChildren(Div2.get(0));
         Div4= WAC.GetChildren(Div3.get(1));
         Div5= WAC.GetChildren(Div4.get(3));
         Div6= WAC.GetChildren(Div5.get(0));
         Div6= WAC.GetChildren(Div6.get(0));
         Div6= WAC.GetChildren(Div6.get(0));
         Div6.get(1).click();
         Thread.sleep(1000);
         SelenElem.CargaAjax(Driver.WebDriver);
         Thread.sleep(2000);
         
         //Se busca la cuenta, si no se encuentra se selecciona la primera 
         Tabla = SelenElem.FindElementByID(Driver.WebDriver, "contentList_databody");
         if(Tabla!=null && Codigo.contentEquals("CODIGO"))
         {
            Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_9");
            ClickChil = SelenElem.GetChildren(Click);
            ClickChil.get(0).click();
            SelenElem.CargaAjax(Driver.WebDriver);
         }
         else
         {
             
         
            Tabla = SelenElem.FindElementByID(Driver.WebDriver, "contentList_databody");
            if (Tabla!=null && Codigo!="")
            {             
                ZizeTabla = SelenElem.GetChildren(Tabla);
                int count = ZizeTabla.size();
                Boolean Ban=false;
                for(int i=0;i<count;i++)
                {
                    Obj = SelenElem.FindElementByID(Driver.WebDriver, "contentList_"+i+"_6");
                    Txt=Obj.getText();
                    if(Txt.contentEquals(Codigo))
                     {
                         Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_"+i+"_9");
                         ClickChil = SelenElem.GetChildren(Click);
                         ClickChil.get(0).click();
                         SelenElem.CargaAjax(Driver.WebDriver);
                         Ban=true;
                         break;            

                      }
                }
            }
            else if(Tabla!=null)
            {
                Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_9");
                ClickChil = SelenElem.GetChildren(Click);
                ClickChil.get(0).click();
                SelenElem.CargaAjax(Driver.WebDriver);
            }
         }
        return Parent;
         
     } 
     
      public static String BusquedaCuenta(WebDriv Driver,WebAction SelenElem,String TipBusq,String Busq , String Codigo) throws InterruptedException
     {
         //Variables
         WebElement Tabla=null, Obj, Click;
         String Txt,Txt2,Txt3;
         List <WebElement> Div2=null, Div3=null, Div4, Div5,Div6, ZizeTabla, ClickChil;
         WebAction WAC = new WebAction();
         OperacionesGenerales OPG = new OperacionesGenerales();         
         
         Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);//Carga de pagina
         //SelenElem.ClickById(Driver.WebDriver,"newcustomer");
         String Parent = OPG.NavegaTab360(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Búsqueda de cliente",true);
         //Thread.sleep(3000);
         SelenElem.EsperaElem(Driver.WebDriver, "serviceNo_input_value");
         if(TipBusq.equals("DN"))
         {
            WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"serviceNo_input_value");//Busqueda de cuadro de texto
            SelenElem.TypeText(Busqueda, Busq);//Se ingresa el DN 
         }
         else if(TipBusq.equals("RFC"))
         {
             WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"rfc_input_value");
             SelenElem.TypeText(Busqueda, Busq);//Se ingresa el RFC 
         }
                  
         //Se busca el boton "Buscar"
         WebElement Div = SelenElem.FindElementByID(Driver.WebDriver, "searchConditions");
         Div2= WAC.GetChildren(Div);
         Div3= WAC.GetChildren(Div2.get(0));
         Div4= WAC.GetChildren(Div3.get(1));
         Div5= WAC.GetChildren(Div4.get(3));
         Div6= WAC.GetChildren(Div5.get(0));
         Div6= WAC.GetChildren(Div6.get(0));
         Div6= WAC.GetChildren(Div6.get(0));
         Div6.get(1).click();
         Thread.sleep(1000);
         SelenElem.CargaAjax(Driver.WebDriver);
         Thread.sleep(2000);
         
         //Se busca el pop
         OperacionesGenerales.BuscaPOP(Driver);
         //Se busca la cuenta, si no se encuentra se selecciona la primera 
         Tabla = SelenElem.FindElementByID(Driver.WebDriver, "accountList_databody");
         if(Tabla!=null && Codigo.contentEquals("CODIGO"))
         {
            Click = SelenElem.FindElementByID(Driver.WebDriver, "accountList_0_0");
            ClickChil = SelenElem.GetChildren(Click);
            ClickChil.get(0).click();
            //SelenElem.CargaAjax(Driver.WebDriver);
         }
         else
         {
             
         
            Tabla = SelenElem.FindElementByID(Driver.WebDriver, "accountList_databody");
            if (Tabla!=null && Codigo!="")
            {             
                ZizeTabla = SelenElem.GetChildren(Tabla);
                int count = ZizeTabla.size();
                Boolean Ban=false;
                for(int i=0;i<count;i++)
                {
                    Obj = SelenElem.FindElementByID(Driver.WebDriver, "accountList_"+i+"_1");
                    Txt=Obj.getText();
                    if(Txt.contentEquals(Codigo))
                     {
                         Click = SelenElem.FindElementByID(Driver.WebDriver, "accountList_"+i+"_0");
                         ClickChil = SelenElem.GetChildren(Click);
                         ClickChil.get(0).click();
                         //SelenElem.CargaAjax(Driver.WebDriver);
                         Ban=true;
                         break;            

                      }
                }
            }
            else if(Tabla!=null)
            {
                Click = SelenElem.FindElementByID(Driver.WebDriver, "accountList_0_0");
                ClickChil = SelenElem.GetChildren(Click);
                ClickChil.get(0).click();
                SelenElem.CargaAjax(Driver.WebDriver);
            }
         }
        return Parent;
         
     } 
     public static String BusquedaClienteVentas(WebDriv Driver,WebAction SelenElem,String TipBusq,String Busq , String Codigo, String Tab) throws InterruptedException
     {
         //Variables
         WebElement Tabla=null, Obj, Click;
         String Txt,Txt2,Txt3;
         List <WebElement> Div2=null, Div3=null, Div4, Div5,Div6, ZizeTabla, ClickChil;
         WebAction WAC = new WebAction();
         OperacionesGenerales OPG = new OperacionesGenerales();         
         
         Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);//Carga de pagina
         String Parent = OPG.NavegaTab360(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
         //Thread.sleep(3000);
         SelenElem.EsperaElem(Driver.WebDriver, "serviceNo_input_value");
         if(TipBusq.equals("DN"))
         {
            WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"serviceNo_input_value");//Busqueda de cuadro de texto
            SelenElem.TypeText(Busqueda, Busq);//Se ingresa el DN 
         }
         else if(TipBusq.equals("RFC"))
         {
             WebElement Busqueda=SelenElem.FindElementByID(Driver.WebDriver,"rfc_input_value");
             SelenElem.TypeText(Busqueda, Busq);//Se ingresa el RFC 
         }
                  
         //Se busca el boton "Buscar"
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar");
         Thread.sleep(1000);
         SelenElem.CargaAjax(Driver.WebDriver);
         Thread.sleep(2000);
         
         //Se busca la cuenta, si no se encuentra se selecciona la primera 
         Tabla = SelenElem.FindElementByID(Driver.WebDriver, "contentList_databody");
         if(Tabla!=null && Codigo.contentEquals("CODIGO"))
         {
            Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_9");
            ClickChil = SelenElem.GetChildren(Click);
            ClickChil.get(0).click();
            SelenElem.CargaAjax(Driver.WebDriver);
         }
         else
         {
             
         
            Tabla = SelenElem.FindElementByID(Driver.WebDriver, "contentList_databody");
            if (Tabla!=null && Codigo!="")
            {             
                ZizeTabla = SelenElem.GetChildren(Tabla);
                int count = ZizeTabla.size();
                Boolean Ban=false;
                for(int i=0;i<count;i++)
                {
                    Obj = SelenElem.FindElementByID(Driver.WebDriver, "contentList_"+i+"_6");
                    Txt=Obj.getText();
                    if(Txt.contentEquals(Codigo))
                     {
                         Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_"+i+"_9");
                         ClickChil = SelenElem.GetChildren(Click);
                         ClickChil.get(0).click();
                         SelenElem.CargaAjax(Driver.WebDriver);
                         Ban=true;
                         break;            

                      }
                }
            }
            else if(Tabla!=null)
            {
                Click = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_9");
                ClickChil = SelenElem.GetChildren(Click);
                ClickChil.get(0).click();
                SelenElem.CargaAjax(Driver.WebDriver);
            }
         }
        return Parent;
         
     } 
   
    
    public void clickInChildrenAnyLevel(WebDriv Driver,String grandfather,String target)
    {
        if(grandfather == null)
            return;
        
        WebElement t = null;
        try
        {
            t = Driver.WebDriver.findElement(By.id(grandfather));
        }
        catch(Exception ex)
        {
            t = Driver.WebDriver.findElement(By.className(grandfather));
        }
        
        List<WebElement> ele = t.findElements(By.xpath("./child::*"));
        for(WebElement el : ele){
            String type = "id";
            if(el.getAttribute("id").equals(""))
                type = "class";
            
            System.out.println("Atributo encontrado: "+el.getAttribute(type));
            if(el.getAttribute(type).contains(target))
                el.click();
            else
                clickInChildrenAnyLevel(Driver,el.getAttribute(type),target);
        }
    }
    public void CierraUltimaTab(WebDriv Driver)
    {
        WebAction WB = new WebAction();
        int i,Hijos;
        WebElement t,Tab;
        List<WebElement> ele,chil1,chil2,chil3,chil4;
        
        t = Driver.WebDriver.findElement(By.id("tabpage_items"));
        ele = t.findElements(By.xpath("./child::*"));
        Hijos = ele.size();
        Hijos = Hijos - 1;
        //for(i=1;i<Hijos;i++)
        //{
            ele.get(Hijos).click();
            Tab = ele.get(Hijos);
            chil1= WB.GetChildren(Tab);
            chil2= WB.GetChildren(chil1.get(1));
            chil3= WB.GetChildren(chil2.get(0));
            chil3.get(1).click();
            //chil4= WB.GetChildren(chil3.get(0));
            //chil4.get(0).click();
            
       // }
    }
   public void CierraPestanas(WebDriv Driver)
    {
        WebAction WB = new WebAction();
        int i,Hijos;
        WebElement t,Tab;
        List<WebElement> ele,chil1,chil2,chil3,chil4;
        
        t = Driver.WebDriver.findElement(By.id("tabpage_items"));
        ele = t.findElements(By.xpath("./child::*"));
        Hijos = ele.size();
        for(i=1;i<Hijos;i++)
        {
            ele.get(i).click();
            Tab = ele.get(i);
            chil1= WB.GetChildren(Tab);
            chil2= WB.GetChildren(chil1.get(1));
            chil3= WB.GetChildren(chil2.get(0));
            chil3.get(1).click();
            //chil4= WB.GetChildren(chil3.get(0));
            //chil4.get(0).click();
            
        }
    }
   
    public void CierraPestanas2(WebDriv Driver) // Works fine! :3
    {
        WebElement t = Driver.WebDriver.findElement(By.id("tabpage_items"));
        List<WebElement> ele = t.findElements(By.xpath("./child::*"));
        
        for(WebElement el : ele)
        {
            if((el.getAttribute("id").contains("AID") && el.getAttribute("id").contains("head")) || (el.getAttribute("id").contains("tabPage") && el.getAttribute("id").contains("head")))
            {
                el.click();
                List<WebElement> cerrar = Driver.WebDriver.findElements(By.className("bc_tabitem_close"));
                for(WebElement i : cerrar)
                {
                    if(i.getAttribute("id").contains("AID") || i.getAttribute("id").contains("tabPage"))
                    {
                        i.click();
                        break;
                    }
                }
            }
        }
    }
    
    
    public static void CierraNavegadores(String Navegador)
    {
        try {
            Process pro = Runtime.getRuntime().exec("tasklist");
            BufferedReader reader = new BufferedReader(new InputStreamReader(pro.getInputStream()));
            String line;
            
            while ((line = reader.readLine()) != null) {
                if (line.startsWith("chrome") && Navegador.contentEquals("Chrome"))
                {
                    Runtime.getRuntime().exec("taskkill /F /IM chrome.exe");
                    Runtime.getRuntime().exec("taskkill /F /IM chromedriver.exe");                    
                    
                }
                else if(line.startsWith("msedge") && Navegador.contentEquals("Edge")) 
                {
                    Runtime.getRuntime().exec("taskkill /F /IM msedge.exe");
                    
                }
                else if(line.startsWith("firefox") && Navegador.contentEquals("Firefox")) 
                {
                    Runtime.getRuntime().exec("taskkill /F /IM geckodriver.exe");
                    Runtime.getRuntime().exec("taskkill /F /IM firefox.exe");
                    
                }
            }

        } catch (IOException e) {
            e.printStackTrace();
        }
        
    }
    
    public static byte[] ScreenShotElement(WebDriv Driver)
    {
        //Driver.WebDriver.manage().window().maximize(); 
        return ((TakesScreenshot) Driver.WebDriver).getScreenshotAs(OutputType.BYTES);
    }
    
     public static byte[] ScreenShotElement2(WebElement Elem)
    {
        //Driver.WebDriver.manage().window().maximize(); 
        return ((TakesScreenshot) Elem).getScreenshotAs(OutputType.BYTES);
    }
    
    public byte[] ScreenShotAll() 
    {
        //driver.switchTo().parentFrame();
        Rectangle rectangleTam = new Rectangle(Toolkit.getDefaultToolkit().getScreenSize());
        Robot robot;
        try {
            robot = new Robot();

            BufferedImage bufferedImage = robot.createScreenCapture(rectangleTam);
            try (ByteArrayOutputStream baos = new ByteArrayOutputStream()) {
                ImageIO.write(bufferedImage, "png", baos);
                return baos.toByteArray();
            } catch (IOException ex) {
                Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
            }
        } catch (AWTException ex) {
            Logger.getLogger(Report.class.getName()).log(Level.SEVERE, null, ex);
        }
        return null;
    }  
    
     public static String  NavegaTab360(WebDriv Driver,WebDriver driver,WebAction WebAction, String IdTab,String NameTab, Boolean IsTop)//Navega Pestaña y selecciona frame
     {
         String Parent ="";
         WebDriver nFrame;
         if(IsTop==true)
         {
            nFrame=null;
            nFrame = driver.switchTo().parentFrame();
            Driver.AssingNewDriver(nFrame);
            driver.manage().timeouts().pageLoadTimeout(100, SECONDS);//espera carga de pagina
         }
        WebElement MS=driver.findElement(By.id(IdTab));
        List<WebElement> childrenElements=WebAction.GetChildren(MS);
         
       //int count = childrenElements.size();//nos devuleve el tamaño del array
       
       for(int i=0;i<childrenElements.size(); i++) 
       {
           WebElement childrenElement = childrenElements.get(i);
           String name=childrenElement.getText();
           if(NameTab.contentEquals(name))
           {
               //Seleccion de frame
               childrenElement.click();
               String id=childrenElement.getAttribute("Id");
               String iFrame=id.replace("_head","_iframe");
               //System.out.println(iFrame);
               nFrame = driver.switchTo().frame(iFrame);               
               Driver.AssingNewDriver(nFrame);
               if(IsTop==true)
               {
                  Parent=iFrame;                  
                  System.out.println(Parent);
               }
               break;
           }
           int t=i;//temporal para suma
           t++;
           if(t>=childrenElements.size())//Seleccion de ultima pestaña al estilo chilenos time
           {
               String id=childrenElement.getAttribute("Id");
               String iFrame=id.replace("_head","_iframe");
               nFrame = driver.switchTo().frame(iFrame);
               Driver.AssingNewDriver(nFrame);
           }
       }
        return Parent;
      
     }
    
    
    public static Boolean ValidaImpPago1(WebDriver Driver,WebAction SelenElem)
    {
        List<WebElement> Div, Elem;
        WebElement  Cash;
        String text;
        Boolean Ban=null;
        Cash = SelenElem.FindElementByID(Driver, "cash");
        Div  = SelenElem.GetChildren(Cash);
        Elem = SelenElem.GetChildren(Div.get(2));
        text = Elem.get(0).getText();
        
        if(text.equals("$0.00"))
            Ban=false;
        else
            Ban=true;
        
        return Ban;
    
    }
    //
    public static Boolean ValidaImpPago(WebDriver Driver,WebAction SelenElem) throws InterruptedException
    {
        List<WebElement> Div, Elem;
        WebElement  Cash;
        String text;
        Boolean Ban=null;
        Thread.sleep(3000); 
        Cash = SelenElem.FindElementByID(Driver, "bill");
        Div  = SelenElem.GetChildren(Cash);
        Elem = SelenElem.GetChildren(Div.get(2));
        text = Elem.get(0).getText();
        
        if(text.equals("$0.00"))
            Ban=false;
        else
            Ban=true;
        
        return Ban;
    
    }
    
    public void EnviarOrden(WebDriv Driver,WebAction SelenElem, String ParentPri,String IDTab, String NameTab,Report reporte) throws InterruptedException
    {
        Thread.sleep(1000);
        reporte.AddImageToReport("Se revisa la orden", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,false);
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver));             
    }
    
        public static void EnviarOrden2(WebDriv Driver,WebAction SelenElem,String IDTab, String NameTab,Report reporte) throws InterruptedException
    {
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,true);
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver));             
    }
    
    public static void EnviarOrdenContraDigital(WebDriv Driver,WebAction SelenElem, String ParentPri,String IDTab, String NameTab,Report reporte) throws InterruptedException
    {
        Thread.sleep(2000);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "button_econtract");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se acepta el contrato Digital", OperacionesGenerales.ScreenShotElement(Driver)); 
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        //popwin_close
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,false);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        
        nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,false);
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver));       
        
           
    }
    public static void EnviarOrdenContraDigital2(WebDriv Driver,WebAction SelenElem,String IDTab, String NameTab,Report reporte) throws InterruptedException
    {
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "button_econtract");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se acepta el contrato Digital", OperacionesGenerales.ScreenShotElement(Driver)); 
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        //WebDriver nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        //Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,true);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        
        //nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        //Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,true);
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver));       
        
           
    }
    public static void EnviarOrdenContraDigitalVentanaWeb(WebDriv Driver,WebAction SelenElem, String ParentPri,String IDTab, String NameTab,Report reporte) throws InterruptedException
    {
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "button_econtract");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se acepta el contrato Digital", OperacionesGenerales.ScreenShotElement(Driver)); 
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "popwin_close");
        
        //popwin_close
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,false);
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        
        nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,false);
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver));       
        
           
    }
    
    public static void EnviarOrdenConFactura(WebDriv Driver,WebAction SelenElem, String ParentPri,String IDTab, String NameTab,Report reporte) throws InterruptedException
    {
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        reporte.AddImageToReport("Se factura", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        Thread.sleep(4000);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        OperacionesGenerales.NavegaPop(Driver);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Usar RFC");
        SelenElem.CargaAjax(Driver.WebDriver);
        //Detalle
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        WebElement RFC = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_5");
        List<WebElement> Children = SelenElem.GetChildren(RFC);
        Children.get(0).click();
        Thread.sleep(2000);
        OperacionesGenerales.SwitchIframe(Driver, ParentPri);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,false);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver));       
        
           
    }
    
    public static void EnviarOrdenConFactura2(WebDriv Driver,WebAction SelenElem,String IDTab, String NameTab,Report reporte) throws InterruptedException
    {
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        reporte.AddImageToReport("Se factura", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        Thread.sleep(4000);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        OperacionesGenerales.NavegaPop(Driver);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Usar RFC");
        
       // OperacionesGenerales.SwitchIframe(Driver, ParentPri);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,true);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver));       
     
    }
    
    public static void EnviarOrdenConFacturaDN(WebDriv Driver,WebAction SelenElem,String IDTab, String NameTab,Report reporte,String DN) throws InterruptedException
    {
        WebElement  InputDN,TD;
        List<WebElement> Chil;
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        reporte.AddImageToReport("Se factura", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        Thread.sleep(4000);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        OperacionesGenerales.NavegaPop(Driver);
        InputDN = SelenElem.FindElementByID(Driver.WebDriver, "serviceNo_input_value");
        SelenElem.TypeText(InputDN, DN);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        TD = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_9");
        Chil = SelenElem.GetChildren(TD);
        Chil.get(0).click(); 
        Thread.sleep(4000);
        
       // OperacionesGenerales.SwitchIframe(Driver, ParentPri);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,true);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver));       
        
           
    }
    
    public static void EnviarOrdenConFacturayContrato(WebDriv Driver,WebAction SelenElem, String ParentPri,String IDTab, String NameTab,Report reporte) throws InterruptedException
    {
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "button_econtract");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se acepta el contrato Digital", OperacionesGenerales.ScreenShotElement(Driver)); 
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,false);
        SelenElem.ClickById(Driver.WebDriver, "subfee_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "button_forward");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver);
        //se regresa al principal para el popwin
        nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        OperacionesGenerales.NavegaPop(Driver);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Usar RFC");
        SelenElem.CargaAjax(Driver.WebDriver);
        //Detalle
        //reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        //Thread.sleep(1000);
        WebElement RFC = SelenElem.FindElementByID(Driver.WebDriver, "contentList_0_5");
        List<WebElement> Children = SelenElem.GetChildren(RFC);
        Children.get(0).click();
        Thread.sleep(2000);
        OperacionesGenerales.SwitchIframe(Driver, ParentPri);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, IDTab, NameTab,false);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("Se envia la orden", OperacionesGenerales.ScreenShotElement(Driver)); 
        
    }
    
    public static String BuscaFolio(WebDriv Driver,WebAction SelenElem) throws InterruptedException
    {
        //se debe de enfocar con navega tab 
                
        String  Orden=null, name=null;
        
        WebAction WB =new WebAction();
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);//Carga de pagina
        Thread.sleep(2000);
        SelenElem.EsperaElem(Driver.WebDriver, "navigateSucDesc");
        List<WebElement> Div2 = WB.FindElementsByClassName(Driver.WebDriver, "orderNolabel"); //bme_layout_table, 
        if(Div2.equals(null))
        {
            SelenElem.WaitForMulti(Driver.WebDriver, Div2);
            Div2 = WB.FindElementsByClassName(Driver.WebDriver, "orderNolabel"); //bme_layout_table
        }
         
      
       if (Div2!=null)
       {
           for(int i=0;i<Div2.size(); i++) 
            {
                WebElement childrenElement = Div2.get(i);
                name=childrenElement.getText();
                if(name.contains("La orden ha sido guardada con el"))
                {
                    String[] Cadena = name.split(" ");
                    int j =Cadena.length-1;
                    Orden = Cadena[j];
                    break;
                    
                }
            }
       }
      
        
        return Orden;
        
    }
    
    public static void BuscaPOP(WebDriv Driver)
    {
        int c=0;
        Boolean Ban=false;
        WebDriver nFrame,nFrame2;
        
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
                
        while(c<8)
        {
            try
            {
                
                nFrame2 = Driver.WebDriver.switchTo().frame("popwin"+c); 
                Driver.AssingNewDriver(nFrame2);
                Ban=true;
                break;
            }catch(Exception ex)
            {
                c++;
            }
        }
    }
    
    public static void BuscaPOP2(WebDriv Driver)
    {
        int c=0;
        Boolean Ban=false;
        WebDriver nFrame,nFrame2;
        
        while(c<25)
        {
            try
            {
                nFrame2 = Driver.WebDriver.switchTo().frame("popwin"+c); 
                Driver.AssingNewDriver(nFrame2);
                Ban=true;
                break;
            }catch(Exception ex)
            {
                c++;
            }
        }
    }
    
    public static void NavegaPop(WebDriv Driver)
    {
        WebDriver nFrame, nFrameP;
        
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
                
        try
        {
            nFrameP = Driver.WebDriver.switchTo().frame("popwin0"); 
            Driver.AssingNewDriver(nFrameP);
        }catch(Exception ex)
        {
            try
            {
                nFrameP = Driver.WebDriver.switchTo().frame("popwin1"); 
                Driver.AssingNewDriver(nFrameP);
            }catch(Exception ex2)
            {
                OperacionesGenerales.BuscaPOP(Driver);
            }
            
        }
        
    }
     
    public void NavegaIframe(WebDriv Driver,String NameIframe)
    {
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(NameIframe); 
        Driver.AssingNewDriver(nFrame);
    }
    public static void NavegaIframeDefault(WebDriv Driver,String NameIframe)
    {
        WebDriver nFrame1 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame1);
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(NameIframe); 
        Driver.AssingNewDriver(nFrame);
    }
    
    
    public void PasaBuro(WebDriv Driver,WebAction SelenElem,Report reporte) throws InterruptedException
    {
        List <WebElement> InputCH;
        WebElement Ex=null;
        
        InputCH = SelenElem.FindElements(Driver.WebDriver,"Name", "#BMEAttr.currentValidatorAction.paramMap.authorization");
        InputCH.get(1).click();
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "ownerofCard_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "creditofHouse_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "creditofCar_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Buro de credito ", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "confirmBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(2000);
        WebDriver nFrame = null;
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        Ex = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        if(Ex!=null)
        {
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar"); //popwin_btn_group
            OperacionesGenerales.BuscaPOP(Driver);
            SelenElem.ClickById(Driver.WebDriver, "continue");
            //SelenElem.CargaAjax(Driver.WebDriver);
        }
        Thread.sleep(4000);
        
        
    }
    public void BurodeCredito(WebDriv Driver,WebAction SelenElem,Report reporte) throws InterruptedException
    {
        List <WebElement> InputCH;
        WebElement Ex=null;
        //Se debe llamar un navega tab antes o estar en el frame
        SelenElem.ClickById(Driver.WebDriver, "creditCheck");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        OperacionesGenerales.BuscaPOP(Driver);
        
        InputCH = SelenElem.FindElements(Driver.WebDriver,"Name", "#BMEAttr.currentValidatorAction.paramMap.authorization");
        InputCH.get(1).click();
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "ownerofCard_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "creditofHouse_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "creditofCar_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "confirmBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        
        Thread.sleep(2000);
        WebDriver nFrame = null;
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        Ex = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        if(Ex!=null)
        {
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar"); //popwin_btn_group
            OperacionesGenerales.BuscaPOP(Driver);
            SelenElem.ClickById(Driver.WebDriver, "continue");
            //SelenElem.CargaAjax(Driver.WebDriver);
        }
        Thread.sleep(4000);
        
        
    }
    public void BurodeCreditoPymes(WebDriv Driver,WebAction WB,Report reporte) throws InterruptedException
    {
        List <WebElement> InputCH;
        WebElement Ex=null,Tarjeta,CCV,Carro,Casa;
        //Se debe llamar un navega tab antes o estar en el frame
        WB.ClickById(Driver.WebDriver, "creditCheck");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        OperacionesGenerales.BuscaPOP(Driver);
        
        InputCH = WB.FindElements(Driver.WebDriver,"Name", "#BMEAttr.currentValidatorAction.paramMap.authorization");
        InputCH.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        WB.ClickById(Driver.WebDriver, "ownerofCard_input_0");
        WB.CargaAjax(Driver.WebDriver);
        
        Tarjeta = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.currentValidatorAction.paramMap.bankNameCC");
        WB.SelenSelect(Tarjeta, "Value", "195");
        CCV = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.currentValidatorAction.paramMap.last4ofCreditCard");
        WB.TypeText(CCV, "1234");  
        
        WB.ClickById(Driver.WebDriver, "creditofHouse_input_0");
        WB.CargaAjax(Driver.WebDriver);
        Casa = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.currentValidatorAction.paramMap.bankNameHM");
        WB.SelenSelect(Casa, "Value", "194");
        
        WB.ClickById(Driver.WebDriver, "creditofCar_input_0");
        WB.CargaAjax(Driver.WebDriver);
        Carro = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.currentValidatorAction.paramMap.bankNameCL");
        WB.SelenSelect(Carro, "Value", "197");
        
        WB.ClickById(Driver.WebDriver, "confirmBtn");
        WB.CargaAjax(Driver.WebDriver);
        
        /*
        Thread.sleep(2000);
        WebDriver nFrame = null;
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        Ex = WB.FindMultiElem(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        if(Ex!=null)
        {
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar"); //popwin_btn_group
            OperacionesGenerales.BuscaPOP(Driver);
            WB.ClickById(Driver.WebDriver, "continue");
            //SelenElem.CargaAjax(Driver.WebDriver);
        }*/
        Thread.sleep(4000);
        
        
    }
    
    public void LlenaDatosClienteNuevo(WebDriv Driver,WebAction SelenElem,Report reporte) throws InterruptedException
    {
        WebDriver nFrame2,nFrame;
        String Nombre,APP,APM,FECHA,CORREO,NUMCONTACTO,CP,CALLE,NUM,RFC,nameFrame;
        WebElement APPW,Fiscal, APMW, Fecha,ID,NombreW,Pasaporte,Genero,Titulo,Num,Corr,CPW,CalleW,NUMW,RFCW,AvisoPriv,RFCUsado;
        
        //Se debe llamar un navega tab antes de la funcion
        Nombre        = CargaVariables.LeerCSV(0, "ConfigOnix", "CliNuevo");
        APP           = CargaVariables.LeerCSV(1, "ConfigOnix", "CliNuevo");
        APM           = CargaVariables.LeerCSV(2, "ConfigOnix", "CliNuevo");
        FECHA         = CargaVariables.LeerCSV(3, "ConfigOnix", "CliNuevo");
        CORREO        = CargaVariables.LeerCSV(4, "ConfigOnix", "CliNuevo");
        NUMCONTACTO   = CargaVariables.LeerCSV(5, "ConfigOnix", "CliNuevo");
        CP            = CargaVariables.LeerCSV(6, "ConfigOnix", "CliNuevo");
        CALLE         = CargaVariables.LeerCSV(7, "ConfigOnix", "CliNuevo");
        NUM           = CargaVariables.LeerCSV(8, "ConfigOnix", "CliNuevo");
        RFC           = CargaVariables.LeerCSV(9, "ConfigOnix", "CliNuevo");

        
        Pasaporte = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500017_input_select");
        SelenElem.SelenSelect(Pasaporte, "Value", "2");
        ID = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.idNumber");
        SelenElem.TypeText(ID, "123456788");
        NombreW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name1");
        SelenElem.TypeText(NombreW, Nombre);
        APPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name2");
        SelenElem.TypeText(APPW, APP);
        APMW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name3");
        SelenElem.TypeText(APMW,APM);
        Genero = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500023_input_select");
        SelenElem.SelenSelect(Genero, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Titulo = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500024_input_select");
        SelenElem.SelenSelect(Titulo, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500025_input_value");
        Fecha = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500025_input_value");
        SelenElem.TypeText(Fecha,FECHA);  
        //RFCW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500019_input_value");
        //SelenElem.TypeText(RFCW,RFC); 
        APPW.click();
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500200"); //Click para generar el RFC
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        /*Se valida que el RFC no este en uso 
        nFrame = Driver.WebDriver;
        nFrame2 = Driver.WebDriver.switchTo().defaultContent();        
        RFCUsado = SelenElem.FindElementByID(nFrame2, "winmsg0");
        if(RFCUsado != null)
        {
            reporte.AddImageToReport("EL RFC esta en uso", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "EL RFC esta en uso" );
        }
        nFrame = Driver.WebDriver.switchTo().frame(nFrame); */
                
        
        Num = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500082_input_value");
        SelenElem.TypeText(Num,NUMCONTACTO);  
        Corr = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500027_input_value");
        SelenElem.TypeText(Corr,CORREO);
       // Fiscal = SelenElem.FindElementByID(Driver.WebDriver, "fiscalClassification_input_select");
       // SelenElem.SelenSelect(Fiscal, "Value", "611");   
               
       // AvisoPriv = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
       // AvisoPriv.click();
        
        //Domicilio
       // CPW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500008_input");
        CPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.indCustAddress.homeAddress.postCode");
        SelenElem.TypeText(CPW,CP); 
        SelenElem.ClickById(Driver.WebDriver, "field_500001_500007_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        CalleW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500005_input_value");
        SelenElem.TypeText(CalleW,CALLE); 
        NUMW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500006_input_value");
        SelenElem.TypeText(NUMW,NUM); 
        //Acuerdo
        //SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        
        
        reporte.AddImageToReport("Se llenan los campos", OperacionesGenerales.ScreenShotElement(Driver));
       // SelenElem.ClickById(Driver.WebDriver, "saveBtn");
        //SelenElem.CargaAjax(Driver.WebDriver);
        
    }
    
    public void LlenaDatosCliente(WebDriv Driver,WebAction SelenElem,Report reporte) throws InterruptedException
    {
        WebDriver nFrame2,nFrame;
        String Nombre,APP,APM,FECHA,CORREO,NUMCONTACTO,CP,CALLE,NUM,RFC,nameFrame;
        WebElement APPW,Fiscal, APMW, Fecha,ID,NombreW,Pasaporte,Genero,Titulo,Num,Corr,CPW,CalleW,NUMW,RFCW,AvisoPriv,RFCUsado;
        
        //Se debe llamar un navega tab antes de la funcion
        Nombre        = CargaVariables.LeerCSV(0, "ConfigOnix", "CliNuevo");
        APP           = CargaVariables.LeerCSV(1, "ConfigOnix", "CliNuevo");
        APM           = CargaVariables.LeerCSV(2, "ConfigOnix", "CliNuevo");
        FECHA         = CargaVariables.LeerCSV(3, "ConfigOnix", "CliNuevo");
        CORREO        = CargaVariables.LeerCSV(4, "ConfigOnix", "CliNuevo");
        NUMCONTACTO   = CargaVariables.LeerCSV(5, "ConfigOnix", "CliNuevo");
        CP            = CargaVariables.LeerCSV(6, "ConfigOnix", "CliNuevo");
        CALLE         = CargaVariables.LeerCSV(7, "ConfigOnix", "CliNuevo");
        NUM           = CargaVariables.LeerCSV(8, "ConfigOnix", "CliNuevo");
        RFC           = CargaVariables.LeerCSV(9, "ConfigOnix", "CliNuevo");

        
        Pasaporte = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500017_input_select");
        SelenElem.SelenSelect(Pasaporte, "Value", "2");
        ID = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.idNumber");
        SelenElem.TypeText(ID, "123456788");
        
        Genero = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500023_input_select");
        SelenElem.SelenSelect(Genero, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Titulo = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500024_input_select");
        SelenElem.SelenSelect(Titulo, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500025_input_value");
        Fecha = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500025_input_value");
        SelenElem.TypeText(Fecha,FECHA);  
        
        Num = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500082_input_value");
        SelenElem.TypeText(Num,NUMCONTACTO);  
        Corr = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500027_input_value");
        SelenElem.TypeText(Corr,CORREO);
       // Fiscal = SelenElem.FindElementByID(Driver.WebDriver, "fiscalClassification_input_select");
       // SelenElem.SelenSelect(Fiscal, "Value", "611");   
               
       // AvisoPriv = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
       // AvisoPriv.click();
        
        //Domicilio
        CPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.indCustAddress.homeAddress.postCode");
        SelenElem.TypeText(CPW,CP); 
        SelenElem.ClickById(Driver.WebDriver, "field_500001_500007_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        CalleW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500005_input_value");
        SelenElem.TypeText(CalleW,CALLE); 
        NUMW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500006_input_value");
        SelenElem.TypeText(NUMW,NUM); 
   
        reporte.AddImageToReport("Se llenan los campos", OperacionesGenerales.ScreenShotElement(Driver));
        
    }
    
    public void LlenaDatosClienteNuevoPymes(WebDriv Driver,WebAction SelenElem,Report reporte) throws InterruptedException
    {
        String Nombre,APP,APM,FECHA,CORREO,NUMCONTACTO,CP,CALLE,NUM,RFC;
        WebElement APPW,Fiscal, APMW, Fecha,ID,NombreW,Pasaporte,Genero,Titulo,Num,Corr,CPW,CalleW,NUMW,RFCW,TipoPersona;
        
        //Se debe llamar un navega tab antes de la funcion
        Nombre        = CargaVariables.LeerCSV(0, "ConfigOnix", "CliNuevo");
        APP           = CargaVariables.LeerCSV(1, "ConfigOnix", "CliNuevo");
        APM           = CargaVariables.LeerCSV(2, "ConfigOnix", "CliNuevo");
        FECHA         = CargaVariables.LeerCSV(3, "ConfigOnix", "CliNuevo");
        CORREO        = CargaVariables.LeerCSV(4, "ConfigOnix", "CliNuevo");
        NUMCONTACTO   = CargaVariables.LeerCSV(5, "ConfigOnix", "CliNuevo");
        CP            = CargaVariables.LeerCSV(6, "ConfigOnix", "CliNuevo");
        CALLE         = CargaVariables.LeerCSV(7, "ConfigOnix", "CliNuevo");
        NUM           = CargaVariables.LeerCSV(8, "ConfigOnix", "CliNuevo");
        RFC           = CargaVariables.LeerCSV(9, "ConfigOnix", "CliNuevo");

        
        Pasaporte = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500017_input_select");
        SelenElem.SelenSelect(Pasaporte, "Value", "2");
        ID = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.idNumber");
        SelenElem.TypeText(ID, "123456788");
        NombreW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name1");
        SelenElem.TypeText(NombreW, Nombre);
        APPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name2");
        SelenElem.TypeText(APPW, APP);
        APMW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name3");
        SelenElem.TypeText(APMW,APM);
        Genero = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500023_input_select");
        SelenElem.SelenSelect(Genero, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Titulo = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500024_input_select");
        SelenElem.SelenSelect(Titulo, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500025_input_value");
        Fecha = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500025_input_value");
        SelenElem.TypeText(Fecha,FECHA);  
        //RFCW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500019_input_value");
        //SelenElem.TypeText(RFCW,RFC); 
        //TipoPersona = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500119_input_select");
        //SelenElem.SelenSelect(TipoPersona, "Value", TipPer);
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500200"); //Click para generar el RFC
        
        Thread.sleep(1000);
        Num = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500082_input_value");
        SelenElem.TypeText(Num,NUMCONTACTO);  
        Corr = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500027_input_value");
        SelenElem.TypeText(Corr,CORREO);        
        Fiscal = SelenElem.FindElementByID(Driver.WebDriver, "fiscalClassification_input_select");
        SelenElem.SelenSelect(Fiscal, "Value", "616");
        

        
        //Domicilio
       // CPW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500008_input");
        CPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.indCustAddress.homeAddress.postCode");
        SelenElem.TypeText(CPW,CP); 
        SelenElem.ClickById(Driver.WebDriver, "field_500001_500007_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        CalleW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500005_input_value");
        SelenElem.TypeText(CalleW,CALLE); 
        NUMW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500006_input_value");
        SelenElem.TypeText(NUMW,NUM); 
        //Acuerdo
        SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        
        
        reporte.AddImageToReport("Se llenan los campos necesarios", OperacionesGenerales.ScreenShotElement(Driver));
        
    }
    
    public void LlenaDatosClienteNuevoPymes2(WebDriv Driver,WebAction SelenElem,Report reporte,String TipPer) throws InterruptedException
    {
        String Nombre,APP,APM,FECHA,CORREO,NUMCONTACTO,CP,CALLE,NUM,RFC;
        WebElement APPW,Fiscal, APMW, Fecha,ID,NombreW,Pasaporte,Genero,Titulo,Num,Corr,CPW,CalleW,NUMW,RFCW,TipoPersona;
        
        //Se debe llamar un navega tab antes de la funcion
        Nombre        = CargaVariables.LeerCSV(0, "ConfigOnix", "CliNuevo");
        APP           = CargaVariables.LeerCSV(1, "ConfigOnix", "CliNuevo");
        APM           = CargaVariables.LeerCSV(2, "ConfigOnix", "CliNuevo");
        FECHA         = CargaVariables.LeerCSV(3, "ConfigOnix", "CliNuevo");
        CORREO        = CargaVariables.LeerCSV(4, "ConfigOnix", "CliNuevo");
        NUMCONTACTO   = CargaVariables.LeerCSV(5, "ConfigOnix", "CliNuevo");
        CP            = CargaVariables.LeerCSV(6, "ConfigOnix", "CliNuevo");
        CALLE         = CargaVariables.LeerCSV(7, "ConfigOnix", "CliNuevo");
        NUM           = CargaVariables.LeerCSV(8, "ConfigOnix", "CliNuevo");
        RFC           = CargaVariables.LeerCSV(9, "ConfigOnix", "CliNuevo");

        
        Pasaporte = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500017_input_select");
        SelenElem.SelenSelect(Pasaporte, "Value", "2");
        ID = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.idNumber");
        SelenElem.TypeText(ID, "123456788");
        NombreW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name1");
        SelenElem.TypeText(NombreW, Nombre);
        APPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name2");
        SelenElem.TypeText(APPW, APP);
        APMW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name3");
        SelenElem.TypeText(APMW,APM);
        Genero = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500023_input_select");
        SelenElem.SelenSelect(Genero, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Titulo = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500024_input_select");
        SelenElem.SelenSelect(Titulo, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500025_input_value");
        Fecha = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500025_input_value");
        SelenElem.TypeText(Fecha,FECHA);  
        //RFCW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500019_input_value");
        //SelenElem.TypeText(RFCW,RFC); 
        TipoPersona = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500119_input_select");
        SelenElem.SelenSelect(TipoPersona, "Value", TipPer);
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500200"); //Click para generar el RFC
        
        Thread.sleep(1000);
        Num = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500082_input_value");
        SelenElem.TypeText(Num,NUMCONTACTO);  
        Corr = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500027_input_value");
        SelenElem.TypeText(Corr,CORREO);        
        Fiscal = SelenElem.FindElementByID(Driver.WebDriver, "fiscalClassification_input_select");
        SelenElem.SelenSelect(Fiscal, "Value", "616");
        

        
        //Domicilio
       // CPW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500008_input");
        CPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.indCustAddress.homeAddress.postCode");
        SelenElem.TypeText(CPW,CP); 
        SelenElem.ClickById(Driver.WebDriver, "field_500001_500007_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        CalleW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500005_input_value");
        SelenElem.TypeText(CalleW,CALLE); 
        NUMW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500006_input_value");
        SelenElem.TypeText(NUMW,NUM); 
        //Acuerdo
        SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        
        
        reporte.AddImageToReport("Se llenan los campos necesarios", OperacionesGenerales.ScreenShotElement(Driver));
        
    }
    
    public void LlenaDatosCliNuevVenta(WebDriv Driver,WebAction SelenElem,Report reporte) throws InterruptedException
    {
        String Nombre,APP,APM,FECHA,CORREO,NUMCONTACTO,CP,CALLE,NUM,RFC;
        WebElement APPW,Fiscal, APMW, Fecha,ID,NombreW,Pasaporte,Genero,Titulo,Num,Corr,CPW,CalleW,NUMW,RFCW,CFDI;
        
        //Se debe llamar un navega tab antes de la funcion
        Nombre        = CargaVariables.LeerCSV(0, "ConfigOnix", "CliNuevo");
        APP           = CargaVariables.LeerCSV(1, "ConfigOnix", "CliNuevo");
        APM           = CargaVariables.LeerCSV(2, "ConfigOnix", "CliNuevo");
        FECHA         = CargaVariables.LeerCSV(3, "ConfigOnix", "CliNuevo");
        CORREO        = CargaVariables.LeerCSV(4, "ConfigOnix", "CliNuevo");
        NUMCONTACTO   = CargaVariables.LeerCSV(5, "ConfigOnix", "CliNuevo");
        CP            = CargaVariables.LeerCSV(6, "ConfigOnix", "CliNuevo");
        CALLE         = CargaVariables.LeerCSV(7, "ConfigOnix", "CliNuevo");
        NUM           = CargaVariables.LeerCSV(8, "ConfigOnix", "CliNuevo");
        RFC           = CargaVariables.LeerCSV(9, "ConfigOnix", "CliNuevo");

                                                                                
        NombreW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.name1");
        SelenElem.TypeText(NombreW, Nombre);
        APPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.name2");
        SelenElem.TypeText(APPW, APP);
        APMW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.name3");
        SelenElem.TypeText(APMW,APM);
        Fecha = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.extAttrs.birthDate");
        SelenElem.TypeText(Fecha,FECHA); 
        SelenElem.ClickById(Driver.WebDriver, "btn_generateRfc"); //Click para generar el RFC
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "acctDataProtectionFlag_input_0"); //Proteccion de datos
        
        //Domicilio
        CPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.contactInfo.postCode");
        SelenElem.TypeText(CPW,CP); 
        CalleW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.contactInfo.address5");
        CalleW.click();
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.TypeText(CalleW,CALLE);
        NUMW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.contactInfo.address6");
        SelenElem.TypeText(NUMW,NUM);
        CFDI = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.extAttrs.acctUsoCfdi");
        SelenElem.SelenSelect(CFDI, "Value", "G03");
        
        reporte.AddImageToReport("Se llenan los campos", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Confirmar");
        SelenElem.CargaAjax(Driver.WebDriver);
        
    }
    
    public void LlenaDatosCliNuevVentaPOP(WebDriv Driver,WebAction SelenElem,Report reporte) throws InterruptedException
    {
        String Nombre,APP,APM,FECHA,CORREO,NUMCONTACTO,CP,CALLE,NUM,RFC;
        WebElement APPW,Fiscal, APMW, Fecha,ID,NombreW,Pasaporte,Genero,Titulo,Num,Corr,CPW,CalleW,NUMW,RFCW,TipoPersona;
        
        //Se debe llamar un navega tab antes de la funcion
        Nombre        = CargaVariables.LeerCSV(0, "ConfigOnix", "CliNuevo");
        APP           = CargaVariables.LeerCSV(1, "ConfigOnix", "CliNuevo");
        APM           = CargaVariables.LeerCSV(2, "ConfigOnix", "CliNuevo");
        FECHA         = CargaVariables.LeerCSV(3, "ConfigOnix", "CliNuevo");
        CORREO        = CargaVariables.LeerCSV(4, "ConfigOnix", "CliNuevo");
        NUMCONTACTO   = CargaVariables.LeerCSV(5, "ConfigOnix", "CliNuevo");
        CP            = CargaVariables.LeerCSV(6, "ConfigOnix", "CliNuevo");
        CALLE         = CargaVariables.LeerCSV(7, "ConfigOnix", "CliNuevo");
        NUM           = CargaVariables.LeerCSV(8, "ConfigOnix", "CliNuevo");
        RFC           = CargaVariables.LeerCSV(9, "ConfigOnix", "CliNuevo");

        
        Pasaporte = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500017_input_select");
        SelenElem.SelenSelect(Pasaporte, "Value", "2");
        ID = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.idNumber");
        SelenElem.TypeText(ID, "123456788");
        NombreW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name1");
        SelenElem.TypeText(NombreW, Nombre);
        APPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name2");
        SelenElem.TypeText(APPW, APP);
        APMW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name3");
        SelenElem.TypeText(APMW,APM);
        Genero = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500023_input_select");
        SelenElem.SelenSelect(Genero, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Titulo = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500024_input_select");
        SelenElem.SelenSelect(Titulo, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500025_input_value");
        Fecha = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500025_input_value");
        SelenElem.TypeText(Fecha,FECHA);  
        
        Num = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500082_input_value");
        SelenElem.TypeText(Num,NUMCONTACTO);  
        Corr = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500027_input_value");
        SelenElem.TypeText(Corr,CORREO);
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500200"); //Click para generar el RFC        
        Thread.sleep(1000);
        /*TipoPersona = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500119_input_select");
        SelenElem.SelenSelect(TipoPersona, "Value", "PFAE");
        SelenElem.CargaAjax(Driver.WebDriver);
        Fiscal = SelenElem.FindElementByID(Driver.WebDriver, "fiscalClassification_input_select");
        SelenElem.SelenSelect(Fiscal, "Value", "000");*/
        

        
        //Domicilio
       // CPW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500008_input");
        CPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.indCustAddress.homeAddress.postCode");
        SelenElem.TypeText(CPW,CP); 
        SelenElem.ClickById(Driver.WebDriver, "field_500001_500007_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        CalleW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500005_input_value");
        SelenElem.TypeText(CalleW,CALLE); 
        NUMW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500006_input_value");
        SelenElem.TypeText(NUMW,NUM); 
        //Acuerdo
        //SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        reporte.AddImageToReport("Se llenan los campos necesarios", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "saveBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        
    }
    public void LlenaDatosClienteNuevoConRFC(WebDriv Driver,WebAction SelenElem,Report reporte, Boolean RegFiscal) throws InterruptedException
    {
        String Nombre,APP,APM,FECHA,CORREO,NUMCONTACTO,CP,CALLE,NUM,RFC;
        WebElement APPW,Fiscal, APMW, Fecha,ID,NombreW,Pasaporte,Genero,Titulo,Num,Corr,CPW,CalleW,NUMW,RFCW,TipoPersona,RegimenFiscal;
        
        //Se debe llamar un navega tab antes de la funcion
        Nombre        = CargaVariables.LeerCSV(0, "ConfigOnix", "CliNuevo");
        APP           = CargaVariables.LeerCSV(1, "ConfigOnix", "CliNuevo");
        APM           = CargaVariables.LeerCSV(2, "ConfigOnix", "CliNuevo");
        FECHA         = CargaVariables.LeerCSV(3, "ConfigOnix", "CliNuevo");
        CORREO        = CargaVariables.LeerCSV(4, "ConfigOnix", "CliNuevo");
        NUMCONTACTO   = CargaVariables.LeerCSV(5, "ConfigOnix", "CliNuevo");
        CP            = CargaVariables.LeerCSV(6, "ConfigOnix", "CliNuevo");
        CALLE         = CargaVariables.LeerCSV(7, "ConfigOnix", "CliNuevo");
        NUM           = CargaVariables.LeerCSV(8, "ConfigOnix", "CliNuevo");
        RFC           = CargaVariables.LeerCSV(9, "ConfigOnix", "CliNuevo");

        
        Pasaporte = SelenElem.FindElementByID(Driver.WebDriver, "field_22_121_input_select");
        SelenElem.SelenSelect(Pasaporte, "Value", "2");
        ID = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramActualCustBaseInfo.custBriefInfo.idNumber");
        SelenElem.TypeText(ID, "123456788");
        NombreW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramActualCustBaseInfo.custBriefInfo.name1");
        SelenElem.TypeText(NombreW, Nombre);
        APPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramActualCustBaseInfo.custBriefInfo.name2");
        SelenElem.TypeText(APPW, APP);
        APMW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramActualCustBaseInfo.custBriefInfo.name3");
        SelenElem.TypeText(APMW,APM);
        Genero = SelenElem.FindElementByID(Driver.WebDriver, "field_22_127_input_select");
        SelenElem.SelenSelect(Genero, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Titulo = SelenElem.FindElementByID(Driver.WebDriver, "field_22_128_input_select");
        SelenElem.SelenSelect(Titulo, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        //SelenElem.ClickById(Driver.WebDriver, "field_500003_500025_input_value");
        Fecha = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_22_129_input_value");
        SelenElem.TypeText(Fecha,FECHA); 
        RFCW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_22_500089_input_value");
        SelenElem.TypeText(RFCW,RFC);
        if (RegFiscal == true)
        {
            RegimenFiscal = SelenElem.FindElementByID(Driver.WebDriver, "fiscalClassification_input_select");
            SelenElem.SelenSelect(RegimenFiscal, "Value", "616");
        }
        //SelenElem.ClickById(Driver.WebDriver, "field_500003_500200"); //Click para generar el RFC
        
        Thread.sleep(1000);
        Num = SelenElem.FindElementByID(Driver.WebDriver, "field_22_500090_input_value");
        SelenElem.TypeText(Num,NUMCONTACTO);  
        Corr = SelenElem.FindElementByID(Driver.WebDriver, "field_22_500092_input_value");
        SelenElem.TypeText(Corr,CORREO);
        //TipoPersona = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500119_input_select");
        //SelenElem.SelenSelect(TipoPersona, "Value", "PFAE");
        //SelenElem.CargaAjax(Driver.WebDriver);
        

        
        //Domicilio
       // CPW = SelenElem.FindElementByID(Driver.WebDriver, "field_500001_500008_input");
        CPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.actualCustAddress.homeAddress.postCode");
        SelenElem.TypeText(CPW,CP); 
        SelenElem.ClickById(Driver.WebDriver, "field_500008_500007_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        CalleW = SelenElem.FindElementByID(Driver.WebDriver, "field_500008_500005_input_value");
        SelenElem.TypeText(CalleW,CALLE); 
        NUMW = SelenElem.FindElementByID(Driver.WebDriver, "field_500008_500006_input_value");
        SelenElem.TypeText(NUMW,NUM); 
        //Acuerdo
        //SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        
        
        reporte.AddImageToReport("Se llenan los campos necesarios", OperacionesGenerales.ScreenShotElement(Driver));
        
    }
    
    public void ValidaRFC(WebDriv Driver,WebAction SelenElem,Report reporte)
    {
        WebDriver nFrame = null, nFrame2;
        WebElement RFCUsado;
        //Se valida que el RFC no este en uso 
        nFrame = Driver.WebDriver;
        nFrame2 = Driver.WebDriver.switchTo().defaultContent();        
        RFCUsado = SelenElem.FindElementByID(nFrame2, "winmsg0");
        if(RFCUsado != null)
        {
            reporte.AddImageToReport("RFC NO VALIDO", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "RFC NO VALIDO" );
        }
        //Se deve enviar un navegaTb para enfocar el iframe        
    }    
    public void ValidaRFCPymes(WebDriv Driver,WebAction SelenElem,Report reporte)
    {
        WebDriver nFrame = null, nFrame2;
        WebElement RFCUsado;
        //Se valida que el RFC no este en uso 
        nFrame = Driver.WebDriver;
        nFrame2 = Driver.WebDriver.switchTo().defaultContent();        
        RFCUsado = SelenElem.FindElementByID(nFrame2, "winmsg0");
        if(RFCUsado != null)
        {
            reporte.AddImageToReport("RFC EN USO", OperacionesGenerales.ScreenShotElement(Driver));
        }
        else
        {
            reporte.AddImageToReport("EL RFC NO ESTA EN USO", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "RFC NO VALIDO" );
        }
        //Se deve enviar un navegaTb para enfocar el iframe  despues        
    }
    public void ValidaICC(WebDriv Driver,WebAction SelenElem,Report reporte)
    {
        WebDriver nFrame = null, nFrame2;
        WebElement RFCUsado;
        //Se valida que el RFC no este en uso 
        nFrame = Driver.WebDriver;
        nFrame2 = Driver.WebDriver.switchTo().defaultContent();        
        RFCUsado = SelenElem.FindElementByID(nFrame2, "winmsg0");
        if(RFCUsado != null)
        {
            reporte.AddImageToReport("EL recurso no es valido", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "EL recurso no es valido" );
        }
        //Se deve enviar un navegaTb para enfocar el iframe
        
    }
    public Boolean ValidaPipe(WebDriv Driver,WebAction SelenElem,Report reporte,String Elem) throws InterruptedException
    {
        WebElement Input1;
        List <WebElement> Val;
        String Txt;
        Boolean Bandera=false;
        
        Input1 = SelenElem.FindElementByID(Driver.WebDriver, Elem);
        SelenElem.TypeText(Input1, "|");
        Val = SelenElem.FindElementsByClassName(Driver.WebDriver, "bf_tips_content");
        Txt = Val.get(0).getText();
        if(Txt.equals("No está permitido registrar el carácter \"|\"(pipe)."))
        {
            Bandera=true;
        }
        reporte.AddImageToReport("Se valida el Valor PIPE", OperacionesGenerales.ScreenShotElement(Driver));
        
        return Bandera;
        
    }
    
    
    public void ValidaBuroReno(WebDriv Driver,WebAction SelenElem,String ParentPri) throws InterruptedException
    {
        List <WebElement> Input1;
        WebElement Ex;
        
        Input1 = SelenElem.FindElements(Driver.WebDriver, "Name","#BMEAttr.currentValidatorAction.paramMap.authorization");
        Input1.get(1).click();
        SelenElem.ClickById(Driver.WebDriver, "ownerofCard_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "creditofHouse_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "creditofCar_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "buttonGroupValidate");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(2000);
        WebDriver nFrame = null;
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        Ex = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        if(Ex!=null)
        {
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar"); //popwin_btn_group
            OperacionesGenerales.SwitchIframe(Driver, ParentPri);
            OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Renovar Contrato",false);  
            SelenElem.ClickById(Driver.WebDriver, "continue");
            //SelenElem.CargaAjax(Driver.WebDriver);
        }
        Thread.sleep(4000); 
   
    }
    
    public void EquipoReno(WebDriv Driver,WebAction SelenElem,Report reporte, String Tipo, String Equipo,String Plan, String IMEI) throws InterruptedException
    {
        WebElement Input,Input2,Meses,Div,Div2,DivTabla,DivTabla2,RadioBtn,BtnCont,Pay;
        List <WebElement> Chil,Chil2,Chil3,Chil4,Chil5;
        String Precio,TxtBusq="",TxtDiv,TxtPlan;
        Boolean Bandera=false;
        
        /*catalogTypeCell
        if(Tipo.contentEquals("POSPAGO"))
        {
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_link", "SIM Pospago con Dispositivo Móvil de Crédito");
            SelenElem.CargaAjax(Driver.WebDriver);
        }
        else if(Tipo.contentEquals("HIBRIDO"))
        {
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_link", "SIM Híbrida con Dispositivo Móvil de Crédito");
            SelenElem.CargaAjax(Driver.WebDriver);
        }
        */
        DivTabla = SelenElem.FindElementByID(Driver.WebDriver, "catalogTypeCell");
        Chil2 = SelenElem.GetChildren(DivTabla);
        if(DivTabla.equals(null))
        {
            throw new NullPointerException( "No es posible buscar el equipo" );
        }
        else
        {
            if(Tipo.contentEquals("HIBRIDO"))
                TxtBusq = "SIM Híbrida con Dispositivo Móvil de Crédito";
            else if(Tipo.contentEquals("POSPAGO"))
                TxtBusq = "SIM Pospago con Dispositivo Móvil de Crédito";
            
            for(int i=0;i<=Chil2.size();i++)
            {
                TxtDiv = Chil2.get(i).getText();
                if(TxtDiv.equals(TxtBusq))
                {
                    Chil2.get(i).click();
                    SelenElem.CargaAjax(Driver.WebDriver);
                    break;
                }
            }
        }
        Input = SelenElem.FindElementByID(Driver.WebDriver, "queryOffer_value");
        SelenElem.TypeText(Input, Equipo);
        SelenElem.ClickById(Driver.WebDriver, "queryOffer_search");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "offerdelivery_0_4");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        //imeiId_input_value
        Input2 = SelenElem.FindElementByID(Driver.WebDriver, "imeiId_input_value");
        SelenElem.TypeText(Input2, IMEI);
        Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "quantity_input");
        SelenElem.CargaAjax(Driver.WebDriver);
        Meses = SelenElem.FindElementByID(Driver.WebDriver, "creditInstallmentTypeId_input_select");
        SelenElem.SelenSelect(Meses, "Value", "24");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        Pay = SelenElem.FindElementByID(Driver.WebDriver, "creditPayTypeId_input_select");
        SelenElem.SelenSelect(Pay, "Value", "2");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        //Se busca el plan 
        Thread.sleep(3000);
        DivTabla2 = SelenElem.FindElementByID(Driver.WebDriver, "primaryOfferingList_databody");
        Chil3 = SelenElem.GetChildren(DivTabla2);
        if(DivTabla.equals(null))
        {
            throw new NullPointerException( "No es posible buscar el plan" );
        }
        else
        {
            
            for(int j=0;j<=Chil3.size();j++)
            {
                Div2 = SelenElem.FindElementByID(Driver.WebDriver, "primaryOfferingList_"+j+"_1");
                TxtPlan = Div2.getText();
                if(TxtPlan.equals(Plan))
                {
                    SelenElem.focus(Driver.WebDriver, Div2);
                    RadioBtn = SelenElem.FindElementByID(Driver.WebDriver, "primaryOfferingList_"+j+"_0");
                    Chil4 = SelenElem.GetChildren(RadioBtn);
                    reporte.AddImageToReport("Se elige el Plan", OperacionesGenerales.ScreenShotElement2(RadioBtn));
                    Chil4.get(0).click();
                    Thread.sleep(3000);
                    reporte.AddImageToReport("Se elige el Plan", OperacionesGenerales.ScreenShotElement(Driver));
                    BtnCont = SelenElem.FindElementByID(Driver.WebDriver, "primaryOfferingList_"+j+"_6");
                    Chil5 = SelenElem.GetChildren(BtnCont);
                    Chil5.get(0).click();                    
                    SelenElem.CargaAjax(Driver.WebDriver);
                    break;
                }
            }
        }
        //primaryOfferingList_databody
        
        
    }
    
    public void ValidaPrecioHandset(WebDriv Driver,WebAction SelenElem,Report reporte, String Tipo, String Equipo) throws InterruptedException
    {
        WebElement Input,Meses,Div,Td;
        List <WebElement> Chil,Chil2;
        String Precio;
        Boolean Bandera=false;
        
        Tipo = Tipo.toUpperCase();
        try {
            if(Tipo.contentEquals("POSPAGO"))
            {
                SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_link", "SIM Pospago con Dispositivo Móvil de Crédito");
                SelenElem.CargaAjax(Driver.WebDriver);
            }
            else if(Tipo.contentEquals("HIBRIDO"))
            {
                SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_link", "SIM Híbrida con Dispositivo Móvil de Crédito");
                SelenElem.CargaAjax(Driver.WebDriver);
            }
            
        } catch (Exception e) {
            SelenElem.CargaAjax(Driver.WebDriver);
        }
        
        
        Input = SelenElem.FindElementByID(Driver.WebDriver, "queryOffer_value");
        SelenElem.TypeText(Input, Equipo);
        SelenElem.ClickById(Driver.WebDriver, "queryOffer_search");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "offerdelivery_0_4");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        Meses = SelenElem.FindElementByID(Driver.WebDriver, "creditInstallmentTypeId_input_select");
        SelenElem.SelenSelect(Meses, "Value", "24");
        SelenElem.CargaAjax(Driver.WebDriver);
        Div = SelenElem.FindElementByID(Driver.WebDriver, "priceRange");
        Chil = SelenElem.GetChildren(Div);
        Precio = Chil.get(0).getText();
        reporte.AddImageToReport("Se Valida el Precio "+Precio, OperacionesGenerales.ScreenShotElement(Driver));
        
    }
    
    public void ValidaPrecioHandsetVenta(WebDriv Driver,WebAction SelenElem,Report reporte, String Tipo, String Equipo) throws InterruptedException
    {
        WebElement Input,Meses,Div,Td,Cont;
        List <WebElement> Chil,Chil2,Chil3;
        String Precio;
        Boolean Bandera=false;
        
        
        if(Tipo.contentEquals("POSPAGO"))
        {
            Td = SelenElem.FindElementByID(Driver.WebDriver, "catalogTypeCell");
            Chil2 = SelenElem.GetChildren(Td);
            Chil2.get(32).click();
            SelenElem.CargaAjax(Driver.WebDriver);
            
        }
        else if(Tipo.contentEquals("HIBRIDO"))
        {
            Td = SelenElem.FindElementByID(Driver.WebDriver, "catalogTypeCell");
            Chil2 = SelenElem.GetChildren(Td);
            Chil2.get(36).click();
            SelenElem.CargaAjax(Driver.WebDriver);
        }
        Thread.sleep(1000);
        Input = SelenElem.FindElementByID(Driver.WebDriver, "queryOffer_value");
        SelenElem.TypeText(Input, Equipo);
        SelenElem.ClickById(Driver.WebDriver, "queryOffer_search");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        Cont = SelenElem.FindElementByID(Driver.WebDriver, "offerdelivery_0_4");
        Chil3 = SelenElem.GetChildren(Cont);
        Chil3.get(0).click();
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        Meses = SelenElem.FindElementByID(Driver.WebDriver, "creditInstallmentTypeId_input_select");
        SelenElem.SelenSelect(Meses, "Value", "24");
        SelenElem.CargaAjax(Driver.WebDriver);
        Div = SelenElem.FindElementByID(Driver.WebDriver, "priceRange");
        Chil = SelenElem.GetChildren(Div);
        Precio = Chil.get(0).getText();
        reporte.AddImageToReport("Se Valida el Precio "+Precio, OperacionesGenerales.ScreenShotElement(Driver));
        
    }
    
    public static int GetPosicion (String Relacion)
    {
        int Pos=10;
        
        if(Relacion.equals("Mandatory"))
            Pos=0;
        else if(Relacion.equals("Optional"))
            Pos=1;
        else if(Relacion.equals("Default"))
            Pos=2;
        
        return Pos;
    }
    
     public String GeneraTxtBatch(String DN, String Tipo) throws IOException, FileNotFoundException
    {
        
        ArrayList Resultados = new ArrayList();
        String NameTxt = null;
    
        
        Date todayDate = new Date();
        //Usando el método SimpleDateFormat, recuperamos solo el día
        //SimpleDateFormat dia = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat dia = new SimpleDateFormat("ddMMyyyyHHmm");
        //almacenamos el día en la variable string fDía
        String fDia = dia.format(todayDate);
            
        NameTxt = Tipo+"CargaOferSup"+fDia;
        Resultados.add(DN);

        CargaVariables.EscribeTxt(Resultados, "Suspencion\\Archivos Temporales", NameTxt,false);
        
        return NameTxt;

    }
      public String GeneraTxt(String Contenido, String Tipo, String Ruta, String Name) throws IOException, FileNotFoundException
    {
        
        ArrayList Resultados = new ArrayList();
        String NameTxt = null;
    
        
        Date todayDate = new Date();
        //Usando el método SimpleDateFormat, recuperamos solo el día
        //SimpleDateFormat dia = new SimpleDateFormat("dd/MM/yyyy HH:mm:ss");
        SimpleDateFormat dia = new SimpleDateFormat("ddMMyyyyHHmm");
        //almacenamos el día en la variable string fDía
        String fDia = dia.format(todayDate);
            
        NameTxt = Tipo+Name+fDia;
        Resultados.add(Contenido);
        //CargaOferSup

        CargaVariables.EscribeTxt(Resultados, Ruta, NameTxt,false);
        
        return NameTxt;

    }
    
     public void SelectBarraLateral(WebDriver driver,WebAction WA, String Nombre) throws InterruptedException
    {
        String GetText;
        int i,Hijos;
        List <WebElement> Div2;
        Div2 = WA.FindElements(driver, "ClassName", "bc_toolbaritem");
        
        Hijos = Div2.size();
        for(i=0;i<Hijos;i++)
        {
            GetText = Div2.get(i).getText();
            if(GetText.contentEquals(Nombre))
            {
                Div2.get(i).click();
                WA.CargaAjax(driver);
                break;
            }
            
        }
    }
     
     
     public void CambiaVentana(WebDriver driver)
     {
         String windowHandle = driver.getWindowHandle();

        //Get the list of window handles
 
        ArrayList<String> tabs = new ArrayList<String> (driver.getWindowHandles());
        System.out.println(tabs.size());
        driver.switchTo().window(tabs.get(1));

     }
     
     public void BuscaOfertaPrimaria(WebDriv Driver,WebAction WA,Report reporte, String Texto, String ID) throws InterruptedException
     {
        WebElement Tbody,Elem;
        List <WebElement> Chil,Chil2,Chil3;
        String Text;
        Boolean Bandera=false;
        int Vueltas,i;
        Thread.sleep(3000);
        Tbody = WA.FindElementByID(Driver.WebDriver, ID);
        Chil = WA.GetChildren(Tbody);
        Vueltas = Chil.size();
        for(i=0;i<Vueltas;i++)
        {
            Elem = WA.FindElementByID(Driver.WebDriver, "migrateOfferList_"+i+"_0");
            Text = Elem.getText().toUpperCase();
            Texto = Texto.toUpperCase();
            if(Text.equals(Texto))
            {
                Chil2 = WA.GetChildren(Elem);
                Chil2.get(1).click();
                reporte.AddImageToReport("Se Elige la oferta", OperacionesGenerales.ScreenShotElement(Driver));
                Bandera = true;
                break;
            }
            
        }
        if(Bandera!=true)
            throw new NullPointerException( "Oferta no encontrada" );
         
     }
     
     public static void VisualiaElem(WebDriv Driver,WebElement Element)
     {
         JavascriptExecutor js = (JavascriptExecutor) Driver.WebDriver;
         js.executeScript("arguments[0].scrollIntoView();", Element);
     }
     
     
     public void PagaOrden(WebDriv Driver,WebAction WB,Report reporte,String ParentPri,String IDTab,String NameTab) throws InterruptedException
     {
        WebElement SelenSelect,Input;
        
        WB.ClickById(Driver.WebDriver, "otherLinkPayment"); 
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "btnPayment"); 
        WB.CargaAjax(Driver.WebDriver);
        SelenSelect = WB.FindElement(Driver.WebDriver, WB, "Name", "busiPaymentMethodListOnixCtz[0].paymentMethod");
        WB.SelenSelect(SelenSelect, "Value", "1001");
        WB.ClickById(Driver.WebDriver, "attrId_0");
        Input = WB.FindElementByID(Driver.WebDriver, "remark");
        WB.TypeText(Input, "Pago con Robot");
        reporte.AddImageToReport("Se procede al pago", OperacionesGenerales.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Pago");
        WB.CargaAjax(Driver.WebDriver);

        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, WB, IDTab, NameTab,false);
        
     }
     public void PagaOrdenFlap(WebDriv Driver,WebAction WB,Report reporte,String ParentPri,String IDTab,String NameTab) throws InterruptedException
     {
        WebElement SelenSelect,Input,Mensaje,User,Pass,InpName,InpCC,InpMEsex,InpCCV;
        OperacionesGenerales OPG = new OperacionesGenerales();
        String TextMensaje,Nombre,NumTarj,MesExp,AnoExp,CCV;
                
        WB.ClickById(Driver.WebDriver, "otherLinkPayment"); 
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "btnPayment"); 
        WB.CargaAjax(Driver.WebDriver);
        SelenSelect = WB.FindElement(Driver.WebDriver, WB, "Name", "busiPaymentMethodListOnixCtz[0].paymentMethod");
        WB.SelenSelect(SelenSelect, "Value", "6001");
        WB.CargaAjax(Driver.WebDriver);
        WB.ClickById(Driver.WebDriver, "attrId_0");
        Input = WB.FindElementByID(Driver.WebDriver, "remark");
        WB.TypeText(Input, "Pago con Robot");
        reporte.AddImageToReport("Se procede al pago", OperacionesGenerales.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Pago");
        //WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);

        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(5000);
        try {
            OPG.NavegaIframeDefault(Driver, "popwin0");
            Mensaje = WB.FindElementByID(Driver.WebDriver, "editor_value");
            TextMensaje = Mensaje.getText();
            if (TextMensaje.equals("No se abre la caja registradora.")) 
            {
                OPG.NavegaIframeDefault(Driver, "tabPage_12100102002_iframe");
                reporte.AddImageToReport("La caja registradora esta cerrada", OPG.ScreenShotElement(Driver));
                throw new NullPointerException( "" );
            }
            else if(TextMensaje.equals("La caja registradora ha expirado.")) 
            {
                OPG.NavegaIframeDefault(Driver, "tabPage_12100102002_iframe");
                reporte.AddImageToReport("La caja registradora esta cerrada", OPG.ScreenShotElement(Driver));
                throw new NullPointerException( "" );
            }
            
        } catch (Exception e) {
        }
        Thread.sleep(8000);
        //OPG.NavegaPop(Driver); 
        User = WB.FindElementByID(Driver.WebDriver, "username");
        Pass = WB.FindElementByID(Driver.WebDriver, "password");
        WB.TypeText(User, "onix");
        WB.TypeText(Pass, "flap2021");
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //Ingreasr = WB.FindElement(Driver.WebDriver, WB, "Xpath", "/html/body/div/form/div/table/tbody/tr[4]/td/input[2]");
        WB.ClickButton(Driver.WebDriver, WB, "Xpath", "/html/body/div/form/div/table/tbody/tr[4]/td/input[2]");
        Thread.sleep(8000);
        
        //Se cargan los datos de la tarjeta 
        Nombre     = CargaVariables.LeerCSV(0, "CV54", "TarjetaCredito");
        NumTarj    = CargaVariables.LeerCSV(1, "CV54", "TarjetaCredito");
        MesExp     = CargaVariables.LeerCSV(2, "CV54", "TarjetaCredito");
        AnoExp     = CargaVariables.LeerCSV(3, "CV54", "TarjetaCredito");
        CCV        = CargaVariables.LeerCSV(4, "CV54", "TarjetaCredito");
        
        
        InpName = WB.FindElementByID(Driver.WebDriver, "name");
        WB.TypeText(InpName, Nombre);
        InpCC = WB.FindElementByID(Driver.WebDriver, "cc");
        WB.TypeText(InpCC, NumTarj);
        InpMEsex = WB.FindElementByID(Driver.WebDriver, "mesexp");
        WB.SelenSelect(InpMEsex, "Value", MesExp);
        InpMEsex = WB.FindElementByID(Driver.WebDriver, "anyoexp");
        WB.SelenSelect(InpMEsex, "Value", AnoExp);
        InpCCV = WB.FindElementByID(Driver.WebDriver, "cvv");
        WB.TypeText(InpCCV, CCV);

        reporte.AddImageToReport("", OPG.ScreenShotAll());
        WB.ClickButton(Driver.WebDriver, WB, "Xpath", "/html/body/div/form/div/table/tbody/tr[9]/td/input[3]");
        Thread.sleep(8000);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //email name
        //custdn name
        
     }
     
     public void MenuVentas(WebDriv Driver,WebAction WB,Report reporte,String Tipo,String Busq) throws InterruptedException
     {
        WebElement Input,Input2,Meses,Div,Div2,DivTabla;
        List <WebElement> Chil,Chil2,Chil3,Td,Tr,BtnSig,DivTablaBusq;
        String TxtDiv,TxtDiv2;
        Boolean Bandera=false;

        DivTabla = WB.FindElementByID(Driver.WebDriver, "catalogTypeCell");
        Chil2 = WB.GetChildren(DivTabla);
        if(DivTabla.equals(null))
        {
            throw new NullPointerException( "No es posible buscar el equipo" );
        }
        else
        {            
            for(int i=0;i<=Chil2.size();i++)
            {
                TxtDiv = Chil2.get(i).getText();
                if(TxtDiv.equals(Tipo))
                {
                    Chil2.get(i).click();
                    WB.CargaAjax(Driver.WebDriver);
                    break;
                }
            }
        }
        Input = WB.FindElementByID(Driver.WebDriver, "queryOffer_value");
        WB.TypeText(Input, Busq);
        WB.ClickById(Driver.WebDriver, "queryOffer_search");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        
        //Se busca en las opciones de busqueda
        //bc_grid_databody 
        DivTablaBusq = WB.FindElements(Driver.WebDriver, "ClassName", "bc_grid_databody");
        //DivTablaBusq = WB.FindElementByID(Driver.WebDriver, "bc_grid_databody");
        Chil3 = WB.GetChildren(DivTablaBusq.get(0));
        if(DivTablaBusq.equals(null))
        {
            throw new NullPointerException( "No es posible hacer la busqueda" );
        }
        else
        { 
            //Tr = WB.GetChildren(Chil3.get(0));
            
            for(int j=0;j<=Chil3.size();j++)
            {
                Tr = WB.GetChildren(Chil3.get(j));
                TxtDiv2 = Tr.get(0).getText();
                if(TxtDiv2.equals(Busq))
                {
                    //Se da click a siguiente
                    //Tr = WB.GetChildren(Chil3.get(j));
                    BtnSig = WB.GetChildren(Tr.get(3));
                    BtnSig.get(0).click();
                    WB.CargaAjax(Driver.WebDriver);
                    break;
                }
            }
            
        }
     }
     
     public void MenuVentasHandset(WebDriv Driver,WebAction WB,Report reporte,String Tipo,String Busq) throws InterruptedException
     {
        WebElement Input,Input2,Meses,Div,Div2,DivTabla;
        List <WebElement> Chil,Chil2,Chil3,Td,Tr,BtnSig,DivTablaBusq;
        String TxtDiv,TxtDiv2;
        Boolean Bandera=false;

        DivTabla = WB.FindElementByID(Driver.WebDriver, "catalogTypeCell");
        Chil2 = WB.GetChildren(DivTabla);
        if(DivTabla.equals(null))
        {
            throw new NullPointerException( "No es posible buscar el equipo" );
        }
        else
        {            
            for(int i=0;i<=Chil2.size();i++)
            {
                TxtDiv = Chil2.get(i).getText();
                if(TxtDiv.equals(Tipo))
                {
                    Chil2.get(i).click();
                    WB.CargaAjax(Driver.WebDriver);
                    break;
                }
            }
        }
        Input = WB.FindElementByID(Driver.WebDriver, "queryOffer_value");
        WB.TypeText(Input, Busq);
        WB.ClickById(Driver.WebDriver, "queryOffer_search");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        
        //Se busca en las opciones de busqueda
        //bc_grid_databody 
        DivTablaBusq = WB.FindElements(Driver.WebDriver, "ClassName", "bc_grid_databody");
        //DivTablaBusq = WB.FindElementByID(Driver.WebDriver, "bc_grid_databody");
        Chil3 = WB.GetChildren(DivTablaBusq.get(0));
        if(DivTablaBusq.equals(null))
        {
            throw new NullPointerException( "No es posible hacer la busqueda" );
        }
        else
        { 
            //Tr = WB.GetChildren(Chil3.get(0));
            
            for(int j=0;j<=Chil3.size();j++)
            {
                Tr = WB.GetChildren(Chil3.get(j));
                TxtDiv2 = Tr.get(1).getText();
                if(TxtDiv2.equals(Busq))
                {
                    //Se da click a siguiente
                    //Tr = WB.GetChildren(Chil3.get(j));
                    BtnSig = WB.GetChildren(Tr.get(4));
                    BtnSig.get(0).click();
                    WB.CargaAjax(Driver.WebDriver);
                    break;
                }
            }
            
        }
     }
     
     public void VentaSim(WebDriv Driver,WebAction WB,Report reporte,String Tipo) throws InterruptedException
     {
        List <WebElement> Tbody;
         
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_link", "SIM Híbrida con Dispositivo Móvil de Crédito");
        WB.CargaAjax(Driver.WebDriver);
        Tbody = WB.FindElements(Driver.WebDriver, "ClassName", "bc_grid_databody");
          
     }
     
     public void CarritoCompras(WebDriv Driver,WebAction WB,Report reporte) throws InterruptedException
    {
       WebDriver nFrame3;
               
        WB.Over(Driver.WebDriver, "div3");
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "a10");
        Thread.sleep(1000);
        reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        
        //se regresa al principal para el popwin
        nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        
        
    }
     
     public void SeleccionaDNPool(WebDriv Driver,WebAction WB,Report reporte) throws InterruptedException
     {
        WebElement Estado,Ciudad,Nir;
         
        WB.ClickById(Driver.WebDriver, "btnSelectDN");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        OperacionesGenerales.NavegaPop(Driver);
        Estado = WB.FindElementByID(Driver.WebDriver, "searcharea_input_select");
        WB.SelenSelect(Estado, "Value", "DF");
        WB.CargaAjax(Driver.WebDriver);
        Ciudad = WB.FindElementByID(Driver.WebDriver, "searchctiy_input_select");
        WB.SelenSelect(Ciudad, "Value", "Distrito Federal");
        WB.CargaAjax(Driver.WebDriver);
        Nir = WB.FindElementByID(Driver.WebDriver, "searchnir_input_select");
        WB.SelenSelect(Nir, "Value", "55");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se asigna el DN", OperacionesGenerales.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
         try {
             OperacionesGenerales.NavegaIframeDefault(Driver, "popwin2");
             Thread.sleep(1000);
             reporte.AddImageToReport("ERROR Primer intento", OperacionesGenerales.ScreenShotElement(Driver));
             WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Confirmar");
             //WB.CargaAjax(Driver.WebDriver);
             Thread.sleep(1000);
             OperacionesGenerales.NavegaPop(Driver);
             WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
             WB.CargaAjax(Driver.WebDriver);
             //
             Thread.sleep(1000);
             OperacionesGenerales.NavegaIframeDefault(Driver, "popwin3");
             reporte.AddImageToReport("ERROR Segundo intento", OperacionesGenerales.ScreenShotElement(Driver));
             WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Confirmar");
             //WB.CargaAjax(Driver.WebDriver);
             OperacionesGenerales.NavegaPop(Driver);
             WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
             WB.CargaAjax(Driver.WebDriver);
         } catch (Exception e) {
         }

        
     }
     
     public void SeleccionaDNPool2(WebDriv Driver,WebAction WB,Report reporte) throws InterruptedException
     {
        WebElement Estado,Ciudad,Nir;
        String Pop;
         
        WB.ClickById(Driver.WebDriver, "btnSelectDN");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        OperacionesGenerales.NavegaPop(Driver);
        Estado = WB.FindElementByID(Driver.WebDriver, "searcharea_input_select");
        WB.SelenSelect(Estado, "Value", "DF");
        WB.CargaAjax(Driver.WebDriver);
        Ciudad = WB.FindElementByID(Driver.WebDriver, "searchctiy_input_select");
        WB.SelenSelect(Ciudad, "Value", "Distrito Federal");
        WB.CargaAjax(Driver.WebDriver);
        Nir = WB.FindElementByID(Driver.WebDriver, "searchnir_input_select");
        WB.SelenSelect(Nir, "Value", "55");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se asigna el DN", OperacionesGenerales.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        Pop = "popwin2";
         try {
             
             try
             {
                OperacionesGenerales.NavegaIframeDefault(Driver, Pop);
                Thread.sleep(1000);
                reporte.AddImageToReport("ERROR Primer intento", OperacionesGenerales.ScreenShotElement(Driver));
                WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Confirmar");
                Pop = "popwin3";
                 
             } catch (Exception e)
             {
                 WebDriver nFrame1 = Driver.WebDriver.switchTo().defaultContent();
                 Driver.AssingNewDriver(nFrame1);
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");  
             }
             //WB.CargaAjax(Driver.WebDriver);
             OperacionesGenerales.NavegaPop(Driver);
             WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
             WB.CargaAjax(Driver.WebDriver);
             //
             try
             {
                OperacionesGenerales.NavegaIframeDefault(Driver, Pop);
                Thread.sleep(1000);
                reporte.AddImageToReport("ERROR Segundo intento", OperacionesGenerales.ScreenShotElement(Driver));
                WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Confirmar");
                 
             } catch (Exception e)
             {
                 WebDriver nFrame1 = Driver.WebDriver.switchTo().defaultContent();
                 Driver.AssingNewDriver(nFrame1);
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");  
             }
             OperacionesGenerales.NavegaPop(Driver);
             //WB.CargaAjax(Driver.WebDriver);
             WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
             WB.CargaAjax(Driver.WebDriver);
         } catch (Exception e) {
         }
        
        
        
        
     }
             
    public void CreaCuentaVentas(WebDriv Driver,WebAction WB,Report reporte,String DNFact, String Tab) throws InterruptedException
    {
        WebElement CrearCuenta,SelenSelect,Input,Direc,BanPop=null;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Div1;
                
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        CrearCuenta = WB.FindElementByID(Driver.WebDriver, "acctList_header");
        Div1 = WB.GetChildren(CrearCuenta);
        Div1.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        WB.ClickById(Driver.WebDriver, "sameWithCustFlag_0");
        WB.CargaAjax(Driver.WebDriver);
        SelenSelect = WB.FindElementByID(Driver.WebDriver, "acctUsoCfdi_input_select");
        WB.SelenSelect(SelenSelect, "Value", "G03");
        Input = WB.FindElementByID(Driver.WebDriver, "field_1000_1119_input_value");
        WB.TypeText(Input, DNFact);
        WB.ClickById(Driver.WebDriver, "field_1000_500507_input_value");
        WB.CargaAjax(Driver.WebDriver);
        
        Direc = WB.FindElementByID(Driver.WebDriver, "sameWithAcctFlag_0");
        OPG.VisualiaElem(Driver, Direc);
        WB.ClickById(Driver.WebDriver, "sameWithAcctFlag_0");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        //WB.ClickById(Driver.WebDriver, "set_1000_titlebar");
        reporte.AddImageToReport("Datos de la cuenta nueva", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Confirmar");
        WB.CargaAjax(Driver.WebDriver);
        
        //Se valida el RFC que no coincide
        OPG.IframeActual(Driver);
        BanPop = WB.FindElementByID(Driver.WebDriver, "popwin_close");
        if(BanPop !=null)
        {
            reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
            WB.CargaAjax(Driver.WebDriver);
        }
       // OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", Tab,true);
    }
    
     public void CreaCuentaVentasCredit(WebDriv Driver,WebAction WB,Report reporte,String DNFact, String Tab, String NameTopTab) throws InterruptedException
    {
        WebElement CrearCuenta,SelenSelect,Input,Direc,BanPop=null,TarjetaSelect,NombreV,NumTarjV,AnoExpV,MesExpV,CCVV;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Div1;
        String Nombre,NumTarj,AnoExp,MesExp,CCV;
                
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        CrearCuenta = WB.FindElementByID(Driver.WebDriver, "acctList_header");
        Div1 = WB.GetChildren(CrearCuenta);
        Div1.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        WB.ClickById(Driver.WebDriver, "sameWithCustFlag_0");
        WB.CargaAjax(Driver.WebDriver);
        SelenSelect = WB.FindElementByID(Driver.WebDriver, "acctUsoCfdi_input_select");
        WB.SelenSelect(SelenSelect, "Value", "G03");
        Input = WB.FindElementByID(Driver.WebDriver, "field_1000_1119_input_value");
        WB.TypeText(Input, DNFact);
        WB.ClickById(Driver.WebDriver, "field_1000_500507_input_value");
        WB.CargaAjax(Driver.WebDriver);
        
        Direc = WB.FindElementByID(Driver.WebDriver, "sameWithAcctFlag_0");
        OPG.VisualiaElem(Driver, Direc);
        WB.ClickById(Driver.WebDriver, "sameWithAcctFlag_0");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        //WB.ClickById(Driver.WebDriver, "set_1000_titlebar");
        //-------------
        TarjetaSelect = WB.FindElementByID(Driver.WebDriver, "field_1001_1019_input_select");
        WB.SelenSelect(TarjetaSelect, "Value", "DDCC");
        WB.CargaAjax(Driver.WebDriver);
        WB.ClickById(Driver.WebDriver, "registerToken_0");
        WB.CargaAjax(Driver.WebDriver);
        OperacionesGenerales.NavegaPop(Driver);
        Thread.sleep(2000);
        WB.WaitForElemName(Driver.WebDriver, "cardholder_name");
        
        //Se cargan los datos de la tarjeta 
        Nombre     = CargaVariables.LeerCSV(0, "Ventas", "TarjetaCredito");
        NumTarj    = CargaVariables.LeerCSV(1, "Ventas", "TarjetaCredito");
        MesExp     = CargaVariables.LeerCSV(2, "Ventas", "TarjetaCredito");
        AnoExp     = CargaVariables.LeerCSV(3, "Ventas", "TarjetaCredito");
        CCV        = CargaVariables.LeerCSV(4, "Ventas", "TarjetaCredito");
        
        NombreV = WB.FindElement(Driver.WebDriver, WB, "Name", "cardholder_name");
        WB.TypeText(NombreV, Nombre);
        NumTarjV = WB.FindElement(Driver.WebDriver, WB, "Name", "cc_number");
        WB.TypeText(NumTarjV, NumTarj);
        MesExpV = WB.FindElement(Driver.WebDriver, WB, "Name", "cc_month");
        WB.TypeText(MesExpV, MesExp);
        AnoExpV = WB.FindElement(Driver.WebDriver, WB, "Name", "cc_year");
        WB.TypeText(AnoExpV, AnoExp);
        CCVV = WB.FindElement(Driver.WebDriver, WB, "Name", "cc_cvv");
        WB.TypeText(CCVV, CCV);
        
       // NombreV.click();
        //NumTarjV.click();
       // MesExpV.click();
       // AnoExpV.click();
       /// CCVV.click();
       // AnoExpV.click();
        Direc = WB.FindElementByID(Driver.WebDriver, "btn_submit");
        OPG.VisualiaElem(Driver, Direc);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "btn_submit");
        try {
            Thread.sleep(5000);
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "btn", "Enviar Respuesta");
        } catch (Exception e) {
            reporte.AddImageToReport("Error", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "" );
        }
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", NameTopTab,true);
        reporte.AddImageToReport("Datos de la cuenta nueva", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Confirmar");
        WB.CargaAjax(Driver.WebDriver);

        //Se valida el RFC que no coincide
        OPG.IframeActual(Driver);
        BanPop = WB.FindElementByID(Driver.WebDriver, "popwin_close");
        if(BanPop !=null)
        {
            reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
            WB.CargaAjax(Driver.WebDriver);
        }
       // OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", Tab,true);
    }
    
     public void SeleccionaCuentas(WebDriv Driver,WebAction WB,String Segmento) throws InterruptedException
    {
        WebElement Tabla,Div,Td;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Tbody,Trs,TdElem;
        String ID,ID2,ID3,SegmentoB,IDBus;
        int Size,i;
        Boolean Input=false;
        
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");
        Tabla = Tbody.get(2);
        ID = Tabla.getAttribute("id");
        ID2 = ID.replace("databody", "0"); //"AID_1407129_0"
        if(ID.contentEquals(ID2))
        {
            throw new NullPointerException( "Error en el proceso el ID de la tabla no cambio" );
        }
        else
        {
            Trs = WB.GetChildren(Tabla);
            Size = Trs.size();
            for(i=0;i<Size;i++)
            {
                IDBus = ID2.replace("_0", "_"+i+"_"); 
                Div = WB.FindElementByID(Driver.WebDriver, IDBus+"2");
                SegmentoB = Div.getText();
                if(SegmentoB.contentEquals(Segmento))
                {
                    ID3 = (IDBus+"0");
                    Td = WB.FindElementByID(Driver.WebDriver, ID3);
                    TdElem = WB.GetChildren(Td);
                    Input = TdElem.get(0).isSelected();
                    if(Input == false)
                    {
                        TdElem.get(0).click();
                        WB.CargaAjax(Driver.WebDriver);
                        break;
                    }
                    else
                        break;
                    
                }
            }
            
        }
    }
    
    public void SeleccionaCuentaPri(WebDriv Driver,WebAction WB)
    {
        //Siempre selecciona la pospago 
        WebElement Tabla,Div,Td;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Tbody,Trs,TdElem;
        String ID,ID2,ID3,SegmentoB,IDBus;
        int Size,i;
        Boolean Ban=false;
        
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");
        Tabla = Tbody.get(3);
        ID = Tabla.getAttribute("id");
        ID2 = ID.replace("databody", "0"); //"AID_1407129_0"
        if(ID.contentEquals(ID2))
        {
            throw new NullPointerException( "Error en el proceso el ID de la tabla no cambio" );
        }
        else
        {
            Trs = WB.GetChildren(Tabla);
            Size = Trs.size();
            for(i=0;i<Size;i++)
            {
                IDBus = ID2.replace("_0", "_"+i+"_"); 
                Div = WB.FindElementByID(Driver.WebDriver, IDBus+"1");//
                SegmentoB = Div.getText();
                if(SegmentoB.contentEquals("Pospago"))
                {
                    ID3 = (IDBus+"4");
                    Td = WB.FindElementByID(Driver.WebDriver, ID3);
                    TdElem = WB.GetChildren(Td);
                    TdElem.get(0).click();
                    Ban = true;
                    break;
                }
            }
            if(Ban==false)
            {
                throw new NullPointerException( "No se encuentra la cuenta Pospago" );
            }
            
        }
    }
     
    public void SeleccionaPlanVenta(Report reporte, WebDriv Driver, WebAction WB, String Plan) throws InterruptedException
    {
        WebElement Tabla,Div,DivContinuar,DivInput;
        List <WebElement> ChilCont,Trs,Input;
        String ID,ID2,ID3,PlanDiv,IDBus;
        int Size,i;
        Boolean Ban=false;
        
        Tabla = WB.FindElementByID(Driver.WebDriver, "primaryOfferingList_databody");
        Trs = WB.GetChildren(Tabla);
        Size = Trs.size();
        for(i=0;i<Size;i++)
        { 
            Div = WB.FindElementByID(Driver.WebDriver, "primaryOfferingList_"+i+"_1");//
            PlanDiv = Div.getText();
            if(PlanDiv.contentEquals(Plan))
            {
                DivInput = WB.FindElementByID(Driver.WebDriver, "primaryOfferingList_"+i+"_0");
                Input = WB.GetChildren(DivInput);
                Input.get(0).click();
                WB.CargaAjax(Driver.WebDriver);
                Thread.sleep(2000);
                DivContinuar = WB.FindElementByID(Driver.WebDriver, "primaryOfferingList_"+i+"_6");
                ChilCont = WB.GetChildren(DivContinuar);
                ChilCont.get(0).click();
                WB.CargaAjax(Driver.WebDriver);
                Thread.sleep(3000);
                Ban = true;
                break;
            }
        }
        if(Ban==false)
        {
            reporte.AddImageToReport("No se encuentra el Plan", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "No se encuentra el Plan " );
        }
        
        
        
    }
    
    public void SeleccionaEquipo(WebDriv Driver, WebAction WB, Report reporte,String ParentPri,String Equipo) throws InterruptedException
    {
        WebElement Tabla,Div,DivContinuar,DivInput;
        List <WebElement> ChilCont,Trs,Input;
        String ID,ID2,ID3,PlanDiv,IDBus;
        int Size,i;
        Boolean Ban=false;
        
        Tabla = WB.FindElementByID(Driver.WebDriver, "resourceListId_databody");
        Trs = WB.GetChildren(Tabla);
        Size = Trs.size();
        for(i=0;i<Size;i++)
        { 
            Div = WB.FindElementByID(Driver.WebDriver, "resourceListId_"+i+"_4");//
            PlanDiv = Div.getText();
            if(PlanDiv.contentEquals(Equipo))
            {
                DivInput = WB.FindElementByID(Driver.WebDriver, "resourceListId_"+i+"_0");
                Input = WB.GetChildren(DivInput);
                if(Input.get(0).isSelected())
                    reporte.AddImageToReport("Se selecciona el Equipo a Liquidar", OperacionesGenerales.ScreenShotElement(Driver));
                else
                {
                   Input.get(0).click();
                    WB.CargaAjax(Driver.WebDriver); 
                }
                WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
                Driver.AssingNewDriver(nFrame);
                Driver.WebDriver.switchTo().frame(ParentPri);
                OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Suscriptor",false); //zBusinessAccept_Subscriber_iframe
                WB.ClickById(Driver.WebDriver, "zSubscriberInfo_SubContract_title");
                Thread.sleep(1000);
                OperacionesGenerales.NavegaTab(Driver,Driver.WebDriver, WB, "BusinessAccept_items", "Histórico De Equipos",false);
                Thread.sleep(1000);
                reporte.AddImageToReport("Se selecciona el Equipo a Liquidar", OperacionesGenerales.ScreenShotElement(Driver));
                WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Saldar");
                Thread.sleep(5000);
                Ban = true;
                break;
            }
        }
        if(Ban==false)
        {
            reporte.AddImageToReport("No se encuentra el Equipo a Liquidar", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "No se encuentra el Equipo " );
        }
    }
    
    public void AgregaTarjeta(WebDriv Driver, WebAction WB, Report reporte) throws InterruptedException
    {
        WebElement TarjetaSelect,NombreV,NumTarjV,MesExpV,AnoExpV,CCVV,Direc;
        String Nombre,NumTarj,MesExp,AnoExp,CCV;
         
        TarjetaSelect = WB.FindElementByID(Driver.WebDriver, "field_507001_507081_input_select");
        WB.SelenSelect(TarjetaSelect, "Value", "DDCC");
        WB.CargaAjax(Driver.WebDriver);
        WB.ClickById(Driver.WebDriver, "registerToken_0");
        WB.CargaAjax(Driver.WebDriver);
        OperacionesGenerales.NavegaPop(Driver);
        Thread.sleep(2000);
        WB.WaitForElemName(Driver.WebDriver, "cardholder_name");
        
        //Se cargan los datos de la tarjeta 
        Nombre     = CargaVariables.LeerCSV(0, "Facturacion", "TarjetaCredito");
        NumTarj    = CargaVariables.LeerCSV(1, "Facturacion", "TarjetaCredito");
        MesExp     = CargaVariables.LeerCSV(2, "Facturacion", "TarjetaCredito");
        AnoExp     = CargaVariables.LeerCSV(3, "Facturacion", "TarjetaCredito");
        CCV        = CargaVariables.LeerCSV(4, "Facturacion", "TarjetaCredito");
        
        NombreV = WB.FindElement(Driver.WebDriver, WB, "Name", "cardholder_name");
        WB.TypeText(NombreV, Nombre);
        NumTarjV = WB.FindElement(Driver.WebDriver, WB, "Name", "cc_number");
        WB.TypeText(NumTarjV, NumTarj);
        MesExpV = WB.FindElement(Driver.WebDriver, WB, "Name", "cc_month");
        WB.TypeText(MesExpV, MesExp);
        AnoExpV = WB.FindElement(Driver.WebDriver, WB, "Name", "cc_year");
        WB.TypeText(AnoExpV, AnoExp);
        CCVV = WB.FindElement(Driver.WebDriver, WB, "Name", "cc_cvv");
        WB.TypeText(CCVV, CCV);
        
       // NombreV.click();
        //NumTarjV.click();
       // MesExpV.click();
       // AnoExpV.click();
       /// CCVV.click();
       // AnoExpV.click();
        Direc = WB.FindElementByID(Driver.WebDriver, "btn_submit");
        OperacionesGenerales.VisualiaElem(Driver, Direc);
        //reporte.AddImageToReport("", OperacionesGenerales.ScreenShotElement(Driver));
        Thread.sleep(1000);
        WB.ClickById(Driver.WebDriver, "btn_submit");
        Thread.sleep(8000);
        
    }
    
}
