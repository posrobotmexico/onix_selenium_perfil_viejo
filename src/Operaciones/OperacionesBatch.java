
package Operaciones;

import java.util.List;
import org.openqa.selenium.WebElement;
import Core.WebAction;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileWriter;
import org.openqa.selenium.WebDriver;


public class OperacionesBatch {
    WebAction WA = new WebAction();
    
    public void SelectBarraLateral(WebDriver driver, String Nombre) throws InterruptedException
    {
        String GetText;
        int i,Hijos;
        List <WebElement> Div2;
        Div2 = WA.FindElements(driver, "ClassName", "bc_toolbaritem");
        
        Hijos = Div2.size();
        for(i=0;i<Hijos;i++)
        {
            GetText = Div2.get(i).getText();
            if(GetText.contentEquals(Nombre))
            {
                Div2.get(i).click();
                WA.CargaAjax(driver);
                break;
            }
            
        }
    }
    
    public String CreaArchivo(String CodCu)
    {
        String NomArchivo="",Contenido,Ruta;
        int RetVal,TarjetaCod;
        RetVal = (int)(Math.random()*100000+1);
        NomArchivo = "CambioTarjeta"+RetVal;
        TarjetaCod = (int)(Math.random()*(100000+999999+1)+6);
        
        try {
                       
            //Contenido =CodCu+"|"+TarjetaCod+"0000000000";
            Contenido =CodCu+"|"+"4178499199164080";
            Ruta = "A:/DataOnix/Batch/Archivos/"+NomArchivo+".txt";
            FileWriter file = new FileWriter(Ruta);
            file.write(Contenido);
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return NomArchivo+".txt";
        
    }
    
    public String CreaArchivoCategory(String CodCu, String NewCategoria)
    {
        String NomArchivo="",Contenido,Ruta;
        int RetVal;
        RetVal = (int)(Math.random()*100000+1);
        NomArchivo = "CambioCategoria"+RetVal;
        
        try {                       
            Contenido =CodCu+"|"+NewCategoria;
            Ruta = "A:/DataOnix/Batch/Archivos/"+NomArchivo+".txt";
            FileWriter file = new FileWriter(Ruta);
            file.write(Contenido);
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return NomArchivo+".txt";
        
    }
    
    public String CreaArchivoReno(String SKU,String PrecioNuevo,String Meses,String CodigoOferta)
    {
        String NomArchivo="",Contenido,Ruta;
        int RetVal;
        RetVal = (int)(Math.random()*100000+1);
        NomArchivo = "CambioPrecioHandset"+RetVal;
        
        try {                       
            Contenido =SKU+"|"+"R"+"|"+PrecioNuevo+"|"+Meses+"|"+CodigoOferta;
            Ruta = "A:/DataOnix/Batch/Archivos/"+NomArchivo+".txt";
            FileWriter file = new FileWriter(Ruta);
            file.write(Contenido);
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return NomArchivo+".txt";
        
    }
   public String CreaArchivoVenta(String SKU,String PrecioNuevo,String Meses,String CodigoOferta)
    {
        String NomArchivo="",Contenido,Ruta;
        int RetVal;
        RetVal = (int)(Math.random()*100000+1);
        NomArchivo = "CambioPrecioHandsetVenta"+RetVal;
        
        try {                       
            Contenido =SKU+"|"+"N"+"|"+PrecioNuevo+"|"+Meses+"|"+CodigoOferta;
            Ruta = "A:/DataOnix/Batch/Archivos/"+NomArchivo+".txt";
            FileWriter file = new FileWriter(Ruta);
            file.write(Contenido);
            file.close();
        } catch (Exception e) {
            e.printStackTrace();
        }
        
        return NomArchivo+".txt";
        
    }
}
