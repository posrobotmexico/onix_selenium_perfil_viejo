/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Operaciones;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import java.util.List;
import org.openqa.selenium.WebElement;
import static org.openqa.selenium.remote.http.Contents.string;

/**
 *
 * @author PRACTIA
 */
public class OperacionesMigraReno {
    
    public void AvisoSaldo(WebDriv Driver,WebAction WB,Report reporte) throws InterruptedException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem=null;
        
        //SE PASA LA ALARMA DEL SALDO QUE SE PERDERA    
        Elem = WB.FindElementByID(Driver.WebDriver, "reserveInfo");
        if(Elem != null)
        {
           reporte.AddImageToReport("Alerta de Saldo", OPG.ScreenShotElement(Driver)); 
           WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Continuar");
           WB.CargaAjax(Driver.WebDriver);
        }
    }
    
    public void ValidaDatosCliente(WebDriv Driver,WebAction WB,Report reporte) throws InterruptedException
    {
        WebElement SelenSelect;
        OperacionesGenerales OPG = new OperacionesGenerales();
                
        WB.EsperaElem(Driver.WebDriver, "generalCustInfo_titlebar");
        SelenSelect = WB.FindElementByID(Driver.WebDriver, "fiscalClassification_input_select");
        WB.SelenSelect(SelenSelect, "Value", "616");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "creditCheck");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se pasa la consulta de credito", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        WB.CargaAjax(Driver.WebDriver);
    }
    public void ValidaDatosCliente2(WebDriv Driver,WebAction WB,Report reporte) throws InterruptedException
    {
        WebElement SelenSelect;
        OperacionesGenerales OPG = new OperacionesGenerales();
                
        WB.EsperaElem(Driver.WebDriver, "generalCustInfo_titlebar");
        SelenSelect = WB.FindElementByID(Driver.WebDriver, "fiscalClassification_input_select");
        WB.SelenSelect(SelenSelect, "Value", "616");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.BurodeCredito(Driver, WB, reporte);
        //WB.ClickById(Driver.WebDriver, "creditCheck");
        //WB.CargaAjax(Driver.WebDriver);
        
    }
    public void EligePlan(WebDriv Driver,WebAction WB,Report reporte,String Plan) throws InterruptedException
    {
        WebElement Input;
        OperacionesGenerales OPG = new OperacionesGenerales();
    
        WB.EsperaElem(Driver.WebDriver, "queryOffer_value");
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Input = WB.FindElementByID(Driver.WebDriver, "queryOffer_value");
        WB.TypeText(Input, Plan);
        WB.ClickById(Driver.WebDriver, "queryOffer_search");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se muestra el Plan Buscado", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "primary__0");
        reporte.AddImageToReport("Se selecciona el plan", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        //WB.CargaAjax(Driver.WebDriver);
    }
    
    public void CreaCuenta(WebDriv Driver,WebAction WB,Report reporte,String Plan,String DNFact,String ParentPri, String Tab) throws InterruptedException
    {
        WebElement CrearCuenta,SelenSelect,Input,Direc,BanPop=null;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Div1;
                
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        CrearCuenta = WB.FindElementByID(Driver.WebDriver, "acctList_header");
        Div1 = WB.GetChildren(CrearCuenta);
        Div1.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        WB.ClickById(Driver.WebDriver, "sameWithCustFlag_0");
        WB.CargaAjax(Driver.WebDriver);
        SelenSelect = WB.FindElementByID(Driver.WebDriver, "acctUsoCfdi_input_select");
        WB.SelenSelect(SelenSelect, "Value", "G03");
        Input = WB.FindElementByID(Driver.WebDriver, "field_1000_1119_input_value");
        WB.TypeText(Input, DNFact);
        WB.ClickById(Driver.WebDriver, "field_1000_500507_input_value");
        WB.CargaAjax(Driver.WebDriver);
        
        Direc = WB.FindElementByID(Driver.WebDriver, "sameWithAcctFlag_0");
        OPG.VisualiaElem(Driver, Direc);
        WB.ClickById(Driver.WebDriver, "sameWithAcctFlag_0");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        //WB.ClickById(Driver.WebDriver, "set_1000_titlebar");
        reporte.AddImageToReport("Datos de la cuenta nueva", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Confirmar");
        WB.CargaAjax(Driver.WebDriver);
        
        //Se valida el RFC que no coincide
        OPG.IframeActual(Driver);
        BanPop = WB.FindElementByID(Driver.WebDriver, "popwin_close");
        if(BanPop !=null)
        {
            reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
            WB.CargaAjax(Driver.WebDriver);
        }
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", Tab,false);
    }
    
    public void SeleccionaCuentas(WebDriv Driver,WebAction WB,String Segmento) throws InterruptedException
    {
        WebElement Tabla,Div,Td;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Tbody,Trs,TdElem;
        String ID,ID2,ID3,SegmentoB,IDBus;
        int Size,i;
        Boolean Input=false;
        
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");
        Tabla = Tbody.get(1);
        ID = Tabla.getAttribute("id");
        ID2 = ID.replace("databody", "0"); //"AID_1407129_0"
        if(ID.contentEquals(ID2))
        {
            throw new NullPointerException( "Error en el proceso el ID de la tabla no cambio" );
        }
        else
        {
            Trs = WB.GetChildren(Tabla);
            Size = Trs.size();
            for(i=0;i<Size;i++)
            {
                IDBus = ID2.replace("_0", "_"+i+"_"); 
                Div = WB.FindElementByID(Driver.WebDriver, IDBus+"2");
                SegmentoB = Div.getText();
                if(SegmentoB.contentEquals(Segmento))
                {
                    ID3 = (IDBus+"0");
                    Td = WB.FindElementByID(Driver.WebDriver, ID3);
                    TdElem = WB.GetChildren(Td);
                    Input = TdElem.get(0).isSelected();
                    if(Input == false)
                    {
                        TdElem.get(0).click();
                        WB.CargaAjax(Driver.WebDriver);
                        break;
                    }
                    else
                        break;
                    
                }
            }
            
        }
    }
    
    public void SeleccionaCuentaPri(WebDriv Driver,WebAction WB)
    {
        //Siempre selecciona la pospago 
        WebElement Tabla,Div,Td;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Tbody,Trs,TdElem;
        String ID,ID2,ID3,SegmentoB,IDBus;
        int Size,i;
        Boolean Ban=false;
        
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");
        Tabla = Tbody.get(2);
        ID = Tabla.getAttribute("id");
        ID2 = ID.replace("databody", "0"); //"AID_1407129_0"
        if(ID.contentEquals(ID2))
        {
            throw new NullPointerException( "Error en el proceso el ID de la tabla no cambio" );
        }
        else
        {
            Trs = WB.GetChildren(Tabla);
            Size = Trs.size();
            for(i=0;i<Size;i++)
            {
                IDBus = ID2.replace("_0", "_"+i+"_"); 
                Div = WB.FindElementByID(Driver.WebDriver, IDBus+"2");//
                SegmentoB = Div.getText();
                if(SegmentoB.contentEquals("Pospago"))
                {
                    ID3 = (IDBus+"4");
                    Td = WB.FindElementByID(Driver.WebDriver, ID3);
                    TdElem = WB.GetChildren(Td);
                    TdElem.get(0).click();
                    Ban = true;
                    break;
                }
            }
            if(Ban==false)
            {
                throw new NullPointerException( "No se encuentra la cuenta Pospago" );
            }
            
        }
    }
    public void SeleccionaCuentaPriPosCont(WebDriv Driver,WebAction WB)
    {
        //Siempre selecciona la pospago 
        WebElement Tabla,Div,Td;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Tbody,Trs,TdElem;
        String ID,ID2,ID3,SegmentoB,IDBus;
        int Size,i;
        Boolean Ban=false;
        
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");
        Tabla = Tbody.get(2);
        ID = Tabla.getAttribute("id");
        ID2 = ID.replace("databody", "0"); //"AID_1407129_0"
        if(ID.contentEquals(ID2))
        {
            throw new NullPointerException( "Error en el proceso el ID de la tabla no cambio" );
        }
        else
        {
            Trs = WB.GetChildren(Tabla);
            Size = Trs.size();
            for(i=0;i<Size;i++)
            {
                IDBus = ID2.replace("_0", "_"+i+"_"); 
                Div = WB.FindElementByID(Driver.WebDriver, IDBus+"1");//
                SegmentoB = Div.getText();
                if(SegmentoB.contentEquals("Pospago"))
                {
                    ID3 = (IDBus+"4");
                    Td = WB.FindElementByID(Driver.WebDriver, ID3);
                    TdElem = WB.GetChildren(Td);
                    TdElem.get(0).click();
                    Ban = true;
                    break;
                }
            }
            if(Ban==false)
            {
                throw new NullPointerException( "No se encuentra la cuenta Pospago" );
            }
            
        }
    }
    
    public void SeleccionaCuentaPriPrePos(WebDriv Driver,WebAction WB)
    {
        //Siempre selecciona la pospago 
        WebElement Tabla,Div,Td;
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Tbody,Trs,TdElem;
        String ID,ID2,ID3,SegmentoB,IDBus,TxtTabla;
        int Size,i;
        Boolean Ban=false;
        
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");//bc_grid_databody
        Tabla = Tbody.get(1);
        ID = Tabla.getAttribute("id");
        TxtTabla = Tabla.getText();
        ID2 = ID.replace("databody", "0"); //"AID_1407129_0"
        if(ID.contentEquals(ID2))
        {
            throw new NullPointerException( "Error en el proceso el ID de la tabla no cambio" );
        }
        else
        {
            Trs = WB.GetChildren(Tabla);
            Size = Trs.size();
            for(i=0;i<Size;i++)
            {
                IDBus = ID2.replace("_0", "_"+i+"_"); //AID_351217_0_2
                Div = WB.FindElementByID(Driver.WebDriver, IDBus+"2");
                SegmentoB = Div.getText();
                if(SegmentoB.contentEquals("Pospago"))
                {
                    ID3 = (IDBus+"0");
                    Td = WB.FindElementByID(Driver.WebDriver, ID3);
                    TdElem = WB.GetChildren(Td);
                    TdElem.get(0).click();
                    Ban = true;
                    break;
                }
            }
            if(Ban==false)
            {
                throw new NullPointerException( "No se encuentra la cuenta Pospago" );
            }
            
        }
    }
    
}
