/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import Operaciones.OperacionesMigraReno;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class Ventas {
    
    public static void SoloSIMFact(WebDriv Driver,WebAction SelenElem,Report reporte,String SIM) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig;
        WebElement ICC,InputDN;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas","Venta Prepago",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        Thread.sleep(2000);
        SelenElem.WaitForElem(Driver.WebDriver, "div3");
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.MenuVentas(Driver, SelenElem, reporte, "Solo SIM", "MOVISTAR SIM");
        Thread.sleep(2000);        
        ICC = SelenElem.FindElementByID(Driver.WebDriver, "iccid__input_value");
        SelenElem.TypeText(ICC, SIM);
        SelenElem.ClickById(Driver.WebDriver, "password__input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        //Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "accountInfo_title");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        //Se pasa a los detalles de la cuenta 
        /*DivTablaBusq = SelenElem.FindElements(Driver.WebDriver, "ClassName", "bc_grid_databody");
        Chil3 = SelenElem.GetChildren(DivTablaBusq.get(2));
        Tr = SelenElem.GetChildren(Chil3.get(0));
        BtnSig = SelenElem.GetChildren(Tr.get(5));
        BtnSig.get(0).click();
        OPG.LlenaDatosCliNuevVenta(Driver, SelenElem, reporte);*/
        
        SelenElem.ClickById(Driver.WebDriver, "addShopCartBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        //Se registra el usuaro en la ventana de factura 
        OPG.NavegaPop(Driver);
        InputDN = SelenElem.FindElementByID(Driver.WebDriver, "serviceNo_input_value");
        SelenElem.TypeText(InputDN, "123456789");
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Registrar");
        Thread.sleep(2000);
        
        OPG.LlenaDatosClienteNuevo(Driver, SelenElem, reporte);
         SelenElem.ClickById(Driver.WebDriver, "saveBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        OPG.CarritoCompras(Driver, SelenElem, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        OPG.EnviarOrdenConFactura2(Driver,SelenElem,"tabpage_items","Venta Prepago", reporte);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
       
        
    }
    
     public static void SoloSIMFactCliEx(WebDriv Driver,WebAction SelenElem,Report reporte,String DN, String SIM) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig;
        WebElement ICC,InputDN;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas","Venta Prepago",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        Thread.sleep(2000);
        SelenElem.WaitForElem(Driver.WebDriver, "div3");
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        OPG.MenuVentas(Driver, SelenElem, reporte, "Solo SIM", "MOVISTAR SIM");
        Thread.sleep(2000);        
        ICC = SelenElem.FindElementByID(Driver.WebDriver, "iccid__input_value");
        SelenElem.TypeText(ICC, SIM);
        SelenElem.ClickById(Driver.WebDriver, "password__input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        //Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "accountInfo_title");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Thread.sleep(3000);  
        SelenElem.ClickById(Driver.WebDriver, "addShopCartBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        //Se busca el DN en la ventana de factura 
        OPG.NavegaPop(Driver);
        InputDN = SelenElem.FindElementByID(Driver.WebDriver, "serviceNo_input_value");
        SelenElem.TypeText(InputDN, DN);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar");
        SelenElem.CargaAjax(Driver.WebDriver);        
        Thread.sleep(2000);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        OPG.CarritoCompras(Driver, SelenElem, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        OPG.EnviarOrdenConFacturaDN(Driver,SelenElem,"tabpage_items","Venta Prepago", reporte,DN);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));

    }
     
    public static void SoloSIMPosp(WebDriv Driver,WebAction WB,Report reporte,String SIM) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig;
        WebElement ICC,InputDN;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta Pospago",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        WB.WaitForElem(Driver.WebDriver, "serviceNo_input_value");
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos Cliente Nuevo", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        OPG.LlenaDatosClienteNuevo(Driver, WB, reporte);
        OPG.ValidaRFC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(6000); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.MenuVentas(Driver, WB, reporte, "Sólo SIM pospago", "Datos Ilimitados Plus");
        Thread.sleep(2000);                         
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Ingrsamos la ICC", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaDNPool(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        OPG.CreaCuentaVentas(Driver, WB, reporte, "5500110000", "Venta Pospago");
        //OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta Pospago", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
       
        
    }
    
    public static void SoloSIMHibri(WebDriv Driver,WebAction WB,Report reporte,String SIM) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig;
        WebElement ICC,InputDN;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta Pospago",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        WB.WaitForElem(Driver.WebDriver, "serviceNo_input_value");
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos Cliente Nuevo", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        OPG.LlenaDatosClienteNuevo(Driver, WB, reporte);
        OPG.ValidaRFC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(6000); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.MenuVentas(Driver, WB, reporte, "Sólo SIM Híbrido", "Plan Datos Ilimitados Control.");
        Thread.sleep(2000);                         
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Ingrsamos la ICC", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaDNPool(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        OPG.CreaCuentaVentas(Driver, WB, reporte, "5500110000", "Venta Pospago");
        OPG.SeleccionaCuentas(Driver, WB, "Prepago");
        OPG.SeleccionaCuentas(Driver, WB, "Pospago");
        OPG.SeleccionaCuentaPri(Driver, WB);
        //OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta Pospago", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
               
    }
    
    public static void VentaSimconHandset(WebDriv Driver,WebAction SelenElem,Report reporte,String SIM,String Equipo, String IMEI) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig,ChilCont;
        WebElement ICC,InputDN,InputIMEI,DivContinuar;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas","Venta Prepago",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        Thread.sleep(2000);
        SelenElem.WaitForElem(Driver.WebDriver, "div3");
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.MenuVentasHandset(Driver, SelenElem, reporte, "Equipo con Sim", Equipo);
        Thread.sleep(2000);    
        InputIMEI = SelenElem.FindElementByID(Driver.WebDriver, "imeiId_input_value");
        SelenElem.TypeText(InputIMEI, IMEI);
        reporte.AddImageToReport("Se ingresa el IMEI", OPG.ScreenShotElement(Driver));
        DivContinuar = SelenElem.FindElementByID(Driver.WebDriver, "primaryOfferingList_0_4");
        ChilCont = SelenElem.GetChildren(DivContinuar);
        ChilCont.get(0).click();
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000); 
        DivContinuar = SelenElem.FindElementByID(Driver.WebDriver, "primaryOfferingList_0_4");
        ChilCont = SelenElem.GetChildren(DivContinuar);
        ChilCont.get(0).click();
        
        ICC = SelenElem.FindElementByID(Driver.WebDriver, "iccid__input_value");
        SelenElem.TypeText(ICC, SIM);
        SelenElem.ClickById(Driver.WebDriver, "password__input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        //Thread.sleep(1000);
        SelenElem.ClickById(Driver.WebDriver, "accountInfo_title");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        SelenElem.ClickById(Driver.WebDriver, "addShopCartBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        //Se registra el usuaro en la ventana de factura 
        OPG.NavegaPop(Driver);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Registrar");
        Thread.sleep(2000);
        
        OPG.LlenaDatosCliNuevVentaPOP(Driver, SelenElem, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        OPG.CarritoCompras(Driver, SelenElem, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Prepago",true);
        //SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        OPG.EnviarOrdenConFactura2(Driver,SelenElem,"tabpage_items","Venta Prepago", reporte);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
       
        
    }

    public static void AdicionLineaPosp(WebDriv Driver,WebAction WB,Report reporte,String DN,String SIM) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig,ChilCont;
        WebElement ICC,InputDN,InputIMEI,DivContinuar;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta Pospago",reporte);
        String ParentPri = OperacionesGenerales.BusquedaClienteVentas(Driver,WB,"DN",DN,"","Venta Pospago");
        //OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Pospago",true);
        Thread.sleep(2000);
        WB.WaitForElem(Driver.WebDriver, "field_500003_500020_input_value");
        WB.ClickButton(Driver.WebDriver, WB, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        reporte.AddImageToReport("Aceptamos el aviso de privacidad", OPG.ScreenShotElement(Driver));
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        //OPG.MenuVentasHandset(Driver, SelenElem, reporte, "Equipo con Sim", Equipo);
        Thread.sleep(2000);  
        OPG.MenuVentas(Driver, WB, reporte, "Sólo SIM pospago", "Plan Datos Ilimitados.");
        Thread.sleep(2000);                         
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Ingrsamos la ICC", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaDNPool(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        //OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta Pospago", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        
    }
    public static void Liquidacion(WebDriv Driver,WebAction WB,Report reporte,String DN,String Codigo,String Equipo) throws InterruptedException, IOException
    {
        WebElement div,radioBtn,Input,Razon,textarea, Error;
        String text,Value="",Folio;
        List<WebElement> childrenElements;
        Boolean Apagar;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        //Driver.WebDriver.findElement(By.id("firstname-placeholder")).sendKeys(Keys.F5);
        //Driver.WebDriver.navigate().refresh();
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Suscriptor",false); //zBusinessAccept_Subscriber_iframe
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "BusinessAccept_items", "Histórico De Equipos",false);
        Thread.sleep(5000);
        reporte.AddImageToReport("Se muestran el historico de equipos", OPG.ScreenShotElement(Driver));
        //Liquidación de financiamiento
        //Driver.WebDriver.switchTo().frame("zSubscriberInfo_SubResource_iframe"); 
        OPG.SeleccionaEquipo( Driver, WB, reporte,ParentPri,Equipo);
        
        WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        Driver.WebDriver.switchTo().frame(ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Liquidación de financiamiento",false);
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, WB);
        
        
        OPG.EnviarOrden(Driver,WB,ParentPri,"custTabPanel_items", "Liquidación de financiamiento", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        if(Apagar.equals(true))
        {
            try {
                OPG.PagaOrden(Driver, WB, reporte, ParentPri, "custTabPanel_items", "Liquidación de financiamiento");
            } catch (Exception e) {
            }
        }
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
        
        
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        OPG.CierraPestanas(Driver);
        ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Suscriptor",false); //zBusinessAccept_Subscriber_iframe
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "BusinessAccept_items", "Histórico De Equipos",false);
        Thread.sleep(5000);
        reporte.AddImageToReport("Se muestran el historico de equipos", OPG.ScreenShotElement(Driver));
                   
        
        
    }
    
    public static void VentaCrediPosp(WebDriv Driver,WebAction WB,Report reporte,String SIM,String Plan,String Equipo,String IMEI, String MESES) throws InterruptedException, IOException
    {
        WebElement DivContinuar,radioBtn,Input,InputIMEI,mesesinput,Codigo, Error,ICC;
        String text,Value="",Folio;
        List<WebElement> ChilCont;
        Boolean Apagar;  
        OperacionesGenerales OPG = new OperacionesGenerales();

        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta Pospago",reporte); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Nuevo Cliente", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));  
        
        OPG.LlenaDatosClienteNuevo(Driver, WB, reporte);
        OPG.ValidaRFC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(6000); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));  
        OPG.MenuVentasHandset(Driver, WB, reporte, "SIM Pospago con Dispositivo Móvil de Crédito", Equipo);
        Thread.sleep(2000);    
        InputIMEI = WB.FindElementByID(Driver.WebDriver, "imeiId_input_value");
        WB.TypeText(InputIMEI, IMEI);
        WB.ClickById(Driver.WebDriver, "primaryOfferingList_0_3");
        WB.CargaAjax(Driver.WebDriver);
        OPG.ValidaICC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta por Distribuidor",true);
        //meses
        mesesinput = WB.FindElementByID(Driver.WebDriver, "creditInstallmentTypeId_input_select");
        WB.SelenSelect(mesesinput, "Value", MESES);
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se ingresa el IMEI", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaPlanVenta(reporte,Driver,WB, Plan);
        Thread.sleep(4000);
        OPG.CreaCuentaVentasCredit(Driver, WB, reporte, "5500110000", "Venta Pospago","Venta Pospago");
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Ingrsamos la ICC", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaDNPool2(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        
        //OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta Pospago", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
    
    }
    
    public static void VentaCrediHibr(WebDriv Driver,WebAction WB,Report reporte,String SIM,String Plan,String Equipo,String IMEI, String MESES) throws InterruptedException, IOException
    {
        WebElement DivContinuar,radioBtn,Input,InputIMEI,mesesinput,Codigo, Error,ICC;
        String text,Value="",Folio;
        List<WebElement> ChilCont;
        Boolean Apagar;  
        OperacionesGenerales OPG = new OperacionesGenerales();

        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta Pospago",reporte); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Nuevo Cliente", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));  
        
        OPG.LlenaDatosClienteNuevo(Driver, WB, reporte);
        OPG.ValidaRFC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(6000); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));  
        OPG.MenuVentasHandset(Driver, WB, reporte, "SIM Híbrida con Dispositivo Móvil de Crédito", Equipo);
        Thread.sleep(2000);    
        InputIMEI = WB.FindElementByID(Driver.WebDriver, "imeiId_input_value");
        WB.TypeText(InputIMEI, IMEI);
        WB.ClickById(Driver.WebDriver, "primaryOfferingList_0_3");
        WB.CargaAjax(Driver.WebDriver);
        //meses
        mesesinput = WB.FindElementByID(Driver.WebDriver, "creditInstallmentTypeId_input_select");
        WB.SelenSelect(mesesinput, "Value", MESES);
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se ingresa el IMEI", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaPlanVenta(reporte,Driver,WB, Plan);
        Thread.sleep(4000);
        OPG.CreaCuentaVentasCredit(Driver, WB, reporte, "5500110000", "Venta Pospago","Venta Pospago");
        OPG.SeleccionaCuentas(Driver, WB, "Prepago");
        OPG.SeleccionaCuentas(Driver, WB, "Pospago");
        OPG.SeleccionaCuentaPri(Driver, WB);
        
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        OPG.ValidaICC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Ingrsamos la ICC", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaDNPool2(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        
        //OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta Pospago", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
    
    }
    
     public static void SoloSIMESP(WebDriv Driver,WebAction WB,Report reporte,String SIM,String Plan,String Operador) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig;
        WebElement ICC,InputDN,InputOpe;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta por Distribuidor",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta por Distribuidor",true);
        Thread.sleep(2000);
        WB.WaitForElem(Driver.WebDriver, "dealerOperator_input_value");
        InputOpe = WB.FindElementByID(Driver.WebDriver,"dealerOperator_input_value");
        WB.TypeText(InputOpe, Operador);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.LlenaDatosClienteNuevo(Driver, WB, reporte);
        OPG.ValidaRFC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta por Distribuidor",true);
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta por Distribuidor",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        Thread.sleep(2000); 
        
        OPG.MenuVentas(Driver, WB, reporte, "Sólo SIM pospago", Plan);
        Thread.sleep(2000);   
        OPG.CreaCuentaVentas(Driver, WB, reporte, "5500110000", "Venta por Distribuidor");
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        OPG.ValidaICC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta por Distribuidor",true);
        //Thread.sleep(1000);
        OPG.SeleccionaDNPool2(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta por Distribuidor",true);
        
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta por Distribuidor", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
    
    }
     
}
