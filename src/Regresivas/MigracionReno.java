/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import Operaciones.OperacionesMigraReno;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class MigracionReno {
    
    public static void MigraPrepHibri(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String DNFact) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Migracion PRE-CTRL",reporte);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion PRE-CTRL",false);
        
        OPMR.AvisoSaldo(Driver,WB, reporte);
        OPMR.ValidaDatosCliente2(Driver, WB, reporte);
        OPG.NavegaIframeDefault(Driver,ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion PRE-CTRL",false);
        reporte.AddImageToReport("Se pasa la consulta de credito", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        WB.CargaAjax(Driver.WebDriver);
        
        OPMR.EligePlan(Driver, WB, reporte, Plan);
        Thread.sleep(2000);
        OPMR.CreaCuenta(Driver, WB, reporte, Plan, DNFact, ParentPri,"Migracion PRE-CTRL");
        OPMR.SeleccionaCuentas(Driver, WB, "Prepago");
        OPMR.SeleccionaCuentas(Driver, WB, "Pospago");
        OPMR.SeleccionaCuentaPriPosCont(Driver, WB);
        Thread.sleep(2000);
        reporte.AddImageToReport("Se eligen las Cuentas", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Migracion PRE-CTRL", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        //if(Apagar.equals(true))
            //OPG.PagaOrden();
        
    }
    
    public static void MigraHibriPosp(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String DNFact) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Migracion CTRL-POS",reporte);
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion CTRL-POS",false);
        
        OPMR.AvisoSaldo(Driver,WB, reporte);
        reporte.AddImageToReport("Datos de la cuenta", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        SelenElem.CargaAjax(Driver.WebDriver);
        OPMR.EligePlan(Driver, WB, reporte, Plan);
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        Thread.sleep(3000);
        //OPMR.SeleccionaCuentaPri(Driver, WB);
        
        reporte.AddImageToReport("Se elige la Cuenta", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Migracion CTRL-POS", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        //if(Apagar.equals(true))
            //OPG.PagaOrden();
        
    }
    
     public static void MigraPrepPosp(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String DNFact) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame,nFrame2;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2,TxtTabla;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Migracion PRE-POS",reporte);
//        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion PRE-POS",false);
        
        OPMR.AvisoSaldo(Driver,WB, reporte);
        reporte.AddImageToReport("Datos de la cuenta", OPG.ScreenShotElement(Driver));
        OPMR.ValidaDatosCliente(Driver, WB, reporte);
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        OPG.BurodeCredito(Driver, SelenElem, reporte);
        Thread.sleep(1000);
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        nFrame2 = Driver.WebDriver.switchTo().frame(ParentPri);
        Driver.AssingNewDriver(nFrame);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion PRE-POS",false);
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        SelenElem.CargaAjax(Driver.WebDriver);
        OPMR.EligePlan(Driver, WB, reporte, Plan);
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        Thread.sleep(1000);
        //Validamos que tenga cuentas
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");//bc_grid_databody
        Tabla = Tbody.get(1);
        TxtTabla = Tabla.getText();
        if(TxtTabla.equals("No hay registros"))
        {
            OPMR.CreaCuenta(Driver, WB, reporte, Plan, DNFact, ParentPri, "Migracion PRE-POS");
        }
        else
        {
           OPMR.SeleccionaCuentaPriPrePos(Driver, WB); 
        }
        
        
        reporte.AddImageToReport("Se elige la Cuenta", OPG.ScreenShotElement(Driver));
        Thread.sleep(1000);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        OPG.EnviarOrdenConFacturayContrato(Driver,SelenElem,ParentPri,"custTabPanel_items","Migracion PRE-POS", reporte);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        //if(Apagar.equals(true))
            //OPG.PagaOrden();
        
    }
     
     public static void MigraPospHibr(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String DNFact) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2,TxtTabla;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Migracion POS-CTRL",reporte);
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion POS-CTRL",false);
        
        OPMR.AvisoSaldo(Driver,WB, reporte);
        reporte.AddImageToReport("Datos de la cuenta", OPG.ScreenShotElement(Driver));
        //OPMR.ValidaDatosCliente(Driver, WB, reporte);
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        SelenElem.CargaAjax(Driver.WebDriver);
        OPMR.EligePlan(Driver, WB, reporte, Plan);
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        Thread.sleep(1000);
        //Validamos que tenga cuentas
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");//bc_grid_databody
        Tabla = Tbody.get(1);
        TxtTabla = Tabla.getText();
        if(TxtTabla.equals("No hay registros"))
        {
            OPMR.CreaCuenta(Driver, WB, reporte, Plan, DNFact, ParentPri, "Migracion POS-CTRL");
        }
        else
        {
           OPMR.SeleccionaCuentas(Driver, WB, "Prepago");
           OPMR.SeleccionaCuentas(Driver, WB, "Pospago");
           Thread.sleep(1000);
           OPMR.SeleccionaCuentaPriPosCont(Driver, WB);
        }
        
        
        reporte.AddImageToReport("Se elige la Cuenta", OPG.ScreenShotElement(Driver));
        Thread.sleep(1000);
        WB.ClickById(Driver.WebDriver, "newGsmSub_titlebar");
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        //Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        //OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Migracion POS-CTRL", reporte);
        OPG.EnviarOrdenConFacturayContrato(Driver,SelenElem,ParentPri,"custTabPanel_items","Migracion POS-CTRL", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        //if(Apagar.equals(true))
            //OPG.PagaOrden();
        
    }
     
     public static void MigraHibriPrep(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String DNFact) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2,TxtTabla;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Migracion CTRL-PRE",reporte);
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion CTRL-PRE",false);
        
        OPMR.AvisoSaldo(Driver,WB, reporte);
        reporte.AddImageToReport("Datos de la cuenta", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        SelenElem.CargaAjax(Driver.WebDriver);
        OPMR.EligePlan(Driver, WB, reporte, Plan);
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        Thread.sleep(1000);
        //Validamos que tenga cuentas
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");//bc_grid_databody
        Tabla = Tbody.get(1);
        TxtTabla = Tabla.getText();
        if(TxtTabla.equals("No hay registros"))
        {
            OPMR.CreaCuenta(Driver, WB, reporte, Plan, DNFact, ParentPri, "Migracion POS-CTRL");
        }
        else
        {
           OPMR.SeleccionaCuentas(Driver, WB, "Prepago");
        }
        
        reporte.AddImageToReport("Se elige la Cuenta", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Migracion CTRL-PRE", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        //if(Apagar.equals(true))
            //OPG.PagaOrden();
        
    }
     
     public static void MigraPosPrep(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String DNFact) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2,TxtTabla;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Migracion POS-PRE",reporte);
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion POS-PRE",false);
        
        OPMR.AvisoSaldo(Driver,WB, reporte);
        reporte.AddImageToReport("Datos de la cuenta", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        SelenElem.CargaAjax(Driver.WebDriver);
        OPMR.EligePlan(Driver, WB, reporte, Plan);
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        Thread.sleep(1000);
        //Validamos que tenga cuentas
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");//bc_grid_databody
        Tabla = Tbody.get(1);
        TxtTabla = Tabla.getText();
        if(TxtTabla.equals("No hay registros"))
        {
            OPMR.CreaCuenta(Driver, WB, reporte, Plan, DNFact, ParentPri, "Migracion POS-PRE");
        }
        else
        {
           OPMR.SeleccionaCuentas(Driver, WB, "Prepago");
        }
        
        reporte.AddImageToReport("Se elige la Cuenta", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Migracion POS-PRE", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        //reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        //if(Apagar.equals(true))
            //OPG.PagaOrden();
        
    }
     
     
      public static void RenoUpgrade(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String Equipo,String IMEI, String TIPORE) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2,TxtTabla;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Renovar Contrato",reporte);
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Renovar Contrato",false);
        OPG.PasaBuro(Driver, SelenElem, reporte);
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        nFrame = Driver.WebDriver.switchTo().frame(ParentPri);
        Driver.AssingNewDriver(nFrame);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Renovar Contrato",false);
        //OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Renovar Contrato",false);
        OPG.EquipoReno(Driver, SelenElem, reporte, TIPORE, Equipo,Plan,IMEI);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Thread.sleep(1000);
        WB.ClickById(Driver.WebDriver, "nextButtonid");
        WB.CargaAjax(Driver.WebDriver);
        OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Renovar Contrato", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
    }
      
    public static void RenoUpgradePosp(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String Equipo,String IMEI, String TIPORE) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2,TxtTabla;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Renovar Contrato",reporte);
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Renovar Contrato",false);
        OPG.PasaBuro(Driver, SelenElem, reporte);
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        nFrame = Driver.WebDriver.switchTo().frame(ParentPri);
        Driver.AssingNewDriver(nFrame);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Renovar Contrato",false);
        OPG.EquipoReno(Driver, SelenElem, reporte, TIPORE, Equipo,Plan,IMEI);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "nextButtonid");
        WB.CargaAjax(Driver.WebDriver);
        OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Renovar Contrato", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
    }
}
