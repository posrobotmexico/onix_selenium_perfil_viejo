/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class ListaNegra {
    
     public static void Agregar(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,InputComen,InputBusqRFC,Tbody;
        String Txt;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Lista de administración","Gestionar Lista Negra",reporte);        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        //WebDriver nFrame1 = Driver.WebDriver.switchTo().frame("tabPage_121001006003_iframe");               
        //Driver.AssingNewDriver(nFrame1);
        WebDriver nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        WebDriver nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        
        WB.WaitForElem(Driver.WebDriver, "add");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        WB.ClickById(Driver.WebDriver, "add");
        WB.CargaAjax(Driver.WebDriver);
        OPG.NavegaPop(Driver);
        InputRFC = WB.FindElementByID(Driver.WebDriver, "textfield_id_0_input_value");
        WB.TypeText(InputRFC, RFC);
        InputComen = WB.FindElement(Driver.WebDriver, WB, "Name", "logListItemValue.remark");
        WB.TypeText(InputComen, "Ingresado por robot");
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver)); 
        WB.ClickById(Driver.WebDriver, "addSaveButton_1");
        WB.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        //nFrame1 = Driver.WebDriver.switchTo().frame("tabPage_121001006003_iframe");               
        //Driver.AssingNewDriver(nFrame1);
        nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //Validacion
        InputBusqRFC = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        WB.TypeText(InputBusqRFC, RFC);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        
        Tbody = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_databody");
        Txt = Tbody.getText();
        if(Txt.equals("No hay registros"))
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );
        }
        else
            reporte.AddImageToReport("RFC en lista negra", OPG.ScreenShotElement(Driver));         
     }
     
     public static void NewClientPosp(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta Pospago",reporte);      
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);        
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        InputRFC = WB.FindElementByID(Driver.WebDriver, "field_500003_500019_input_value");
        WB.TypeText(InputRFC, RFC);
        WB.ClickById(Driver.WebDriver, "field_500003_500020_input_value");
        WB.CargaAjax(Driver.WebDriver);
        
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "popwin_top");
        if(POPError != null)
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
     }     
             
    public static void CambiarInfoCli(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cambiar Información de Cliente",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cliente",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        Txt = POPError.getText();
        if(POPError != null && Txt.equals("El cliente está en la lista negra."))
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
     }
    
    public static void PortaListaNegra(WebDriv Driver,WebAction WB,Report reporte,String RFC, String DNPorta) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError,InputDNPorta,Pass1,Pass2,Tipo,Aviso;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Portabilidad","Portabilidad Postpago",reporte);      
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Portabilidad Postpago",true);
        InputDNPorta = WB.FindElementByID(Driver.WebDriver, "msisdn_input_value");
        WB.TypeText(InputDNPorta,DNPorta);
        Pass1 = WB.FindElementByID(Driver.WebDriver, "pinpassword_input_value");
        Pass1.click();
        WB.CargaAjax(Driver.WebDriver);
        WB.TypeText(Pass1,"1234");
        Pass2 = WB.FindElementByID(Driver.WebDriver, "pinconfirm_input_value");
        WB.TypeText(Pass2,"1234");
        Tipo = WB.FindElement(Driver.WebDriver, WB, "Name", "portInCurrentRatingCategory");
        WB.SelenSelect(Tipo, "Value", "1");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);

        OPG.NavegaPop(Driver);
        InputRFC = WB.FindElementByID(Driver.WebDriver, "rfc_input_value");
        WB.TypeText(InputRFC, RFC);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Portabilidad Postpago",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Aviso = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        Aviso.click();
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Portabilidad Postpago",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        POPError = WB.FindElementByID(Driver.WebDriver, "popwin_title");
        Txt = POPError.getText();
        if(POPError != null && Txt.equals("Error"))
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
     }
    
    public static void VentaListaNegra(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Recursos de Negocio","Venta de Equipo a Cliente Existente",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Venta de Equipo a Cliente Existente",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        Txt = POPError.getText();
        if(POPError != null && Txt.equals("El cliente está en la lista negra."))
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
     }
    public static void CesionListaNegra(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cesión de Derechos",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cesión de Derechos",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        Txt = POPError.getText();
        if(POPError != null && Txt.equals("El cliente está en la lista negra."))
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
     }
    
    public static void CambiarInfoCuenta(WebDriv Driver,WebAction WB,Report reporte,String RFC,String Cuenta) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement IndiceCuenta,POPError,Tbody,Div;
        String Txt,Txtcuenta;
        List <WebElement> CHilTBody,Click;
        int Zize;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cambiar Informacion de Cuenta",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCuenta(Driver,WB,"RFC",RFC,Cuenta);
        /*
         try {
             Thread.sleep(1000);
             OPG.NavegaPop(Driver);
             Tbody = WB.FindElementByID(Driver.WebDriver, "accountList_databody");
             CHilTBody = WB.GetChildren(Tbody);
             Zize = CHilTBody.size();
             for(int i=0;i<Zize;i++)
             {
                 IndiceCuenta = WB.FindElementByID(Driver.WebDriver, "accountList_0_1");
                 Txtcuenta = IndiceCuenta.getText();
                 if(Txtcuenta.equals(Cuenta))
                 {
                     reporte.AddImageToReport("Seleccionamos la cuenta", OPG.ScreenShotElement(Driver));
                     Div = WB.FindElementByID(Driver.WebDriver, "accountList_0_0");
                     Click = WB.GetChildren(Div);
                     Click.get(0).click();
                     WB.CargaAjax(Driver.WebDriver);
                     break;
                 }
             }
         } catch (Exception e) {
             
         }*/
        WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);                       
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame(ParentPri);
        Driver.AssingNewDriver(nFrame3); 
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(6000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Informacion de Cuenta",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.WaitForElem(Driver.WebDriver, "editor_value");
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        Txt = POPError.getText();
        if(POPError != null && Txt.equals("El cliente está en la lista negra."))
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
     }
    
    public static void  MigraListaNegra(WebDriv Driver,WebAction WB,Report reporte,String RFC,String Tipo)throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente",Tipo,reporte);
//        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items",Tipo,false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        Txt = POPError.getText();
        if(POPError != null && Txt.equals("El cliente está en la lista negra."))
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
    }
    
    public static void Eliminar(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,DivRFC,Div;
        String TxtRFC;
        List <WebElement> ChilDiv;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Lista de administración","Gestionar Lista Negra",reporte);        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        WebDriver nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        WebDriver nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        
        WB.WaitForElem(Driver.WebDriver, "add");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        WB.TypeText(InputRFC, RFC);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Busca el RFC", OPG.ScreenShotElement(Driver));
        
         try {
             DivRFC = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_1");
             TxtRFC = DivRFC.getText();
             if (TxtRFC.equals(RFC))
             {
                 Div = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_0");
                 ChilDiv = WB.GetChildren(Div);
                 ChilDiv.get(0).click();
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Informacion del RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_toolbaritem", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 OPG.NavegaPop(Driver);
                 reporte.AddImageToReport("Se elimina el RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 //
                 
                 OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
                 nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
                 Driver.AssingNewDriver(nFrame2);
                 nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
                 Driver.AssingNewDriver(nFrame3);
                 nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
                 Driver.AssingNewDriver(nFrame4);
                 InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
                 InputRFC.clear();
                 WB.TypeText(InputRFC, RFC);
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Se valida que el RFC no este en lista negra", OPG.ScreenShotElement(Driver));
             }
         } catch (Exception e) {
             reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );                
         }                             
              
     }
    
    ////////////////Lista Negra (Quitar Lista Negra) //////////////////////////////
    
    public static void NewClientPosp2(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        List <WebElement> DivPOP;
        String TxtPop;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta Pospago",reporte);      
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);        
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        InputRFC = WB.FindElementByID(Driver.WebDriver, "field_500003_500019_input_value");
        WB.TypeText(InputRFC, RFC);
        WB.ClickById(Driver.WebDriver, "field_500003_500020_input_value");
        WB.CargaAjax(Driver.WebDriver);
                
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "popwin_top");        
        if(POPError != null)
        {
            DivPOP = WB.FindElementsByClassName(Driver.WebDriver, "popwin_content");
            TxtPop = DivPOP.get(0).getText();
            if(TxtPop.equals("RFC has been used by another customer."))
                reporte.AddImageToReport("RFC No esta en Lista Negra", OPG.ScreenShotElement(Driver));
            else
            {
                reporte.AddImageToReport("Prueba Fallida", OPG.ScreenShotElement(Driver));
                throw new NullPointerException( "No se encontro el RFC en lista Negra" );
            }
        }
        else
        {
            reporte.AddImageToReport("RFC No esta en Lista Negra", OPG.ScreenShotElement(Driver));
                        
        }
     }  
    
     public static void CambiarInfoCli2(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cambiar Información de Cliente",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cliente",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        if(POPError != null )
        {
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "No se encontro el RFC en lista Negra" ); 
        }
        else
        {
            reporte.AddImageToReport("RFC No esta en Lista Negra", OPG.ScreenShotElement(Driver)); 
                       
        }
     }
     
     public static void PortaListaNegra2(WebDriv Driver,WebAction WB,Report reporte,String RFC, String DNPorta) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError,InputDNPorta,Pass1,Pass2,Tipo,Aviso;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Portabilidad","Portabilidad Postpago",reporte);      
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Portabilidad Postpago",true);
        InputDNPorta = WB.FindElementByID(Driver.WebDriver, "msisdn_input_value");
        WB.TypeText(InputDNPorta,DNPorta);
        Pass1 = WB.FindElementByID(Driver.WebDriver, "pinpassword_input_value");
        Pass1.click();
        WB.CargaAjax(Driver.WebDriver);
        WB.TypeText(Pass1,"1234");
        Pass2 = WB.FindElementByID(Driver.WebDriver, "pinconfirm_input_value");
        WB.TypeText(Pass2,"1234");
        Tipo = WB.FindElement(Driver.WebDriver, WB, "Name", "portInCurrentRatingCategory");
        WB.SelenSelect(Tipo, "Value", "1");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);

        OPG.NavegaPop(Driver);
        InputRFC = WB.FindElementByID(Driver.WebDriver, "rfc_input_value");
        WB.TypeText(InputRFC, RFC);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Portabilidad Postpago",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Aviso = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        Aviso.click();
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Portabilidad Postpago",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        POPError = WB.FindElementByID(Driver.WebDriver, "popwin_title");
        if(POPError != null )
        {
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );
        }
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
                        
        }
        
     }
     
     public static void VentaListaNegra2(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Recursos de Negocio","Venta de Equipo a Cliente Existente",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Venta de Equipo a Cliente Existente",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        if(POPError != null )
        {
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "No se encontro el RFC en lista Negra" ); 
        }
        else
        {
            reporte.AddImageToReport("RFC No esta en Lista Negra", OPG.ScreenShotElement(Driver)); 
                       
        }
     }
     
      public static void CesionListaNegra2(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cesión de Derechos",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cesión de Derechos",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        if(POPError != null )
        {
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "No se encontro el RFC en lista Negra" ); 
        }
        else
        {
            reporte.AddImageToReport("RFC No esta en Lista Negra", OPG.ScreenShotElement(Driver)); 
                       
        }
     }
    
    public static void CambiarInfoCuenta2(WebDriv Driver,WebAction WB,Report reporte,String RFC,String Cuenta) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement IndiceCuenta,POPError,Tbody,Div;
        String Txt,Txtcuenta;
        List <WebElement> CHilTBody,Click;
        int Zize;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cambiar Informacion de Cuenta",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        //try
         try {
             Thread.sleep(1000);
             OPG.NavegaPop(Driver);
             Tbody = WB.FindElementByID(Driver.WebDriver, "accountList_databody");
             CHilTBody = WB.GetChildren(Tbody);
             Zize = CHilTBody.size();
             for(int i=0;i<Zize;i++)
             {
                 IndiceCuenta = WB.FindElementByID(Driver.WebDriver, "accountList_0_1");
                 Txtcuenta = IndiceCuenta.getText();
                 if(Txtcuenta.equals(Cuenta))
                 {
                     reporte.AddImageToReport("Seleccionamos la cuenta", OPG.ScreenShotElement(Driver));
                     Div = WB.FindElementByID(Driver.WebDriver, "accountList_0_0");
                     Click = WB.GetChildren(Div);
                     Click.get(0).click();
                     WB.CargaAjax(Driver.WebDriver);
                     break;
                 }
             }
         } catch (Exception e) {
             
         }
        WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);                       
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame(ParentPri);
        Driver.AssingNewDriver(nFrame3); 
        Thread.sleep(3000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Informacion de Cuenta",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        if(POPError != null )
        {
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "No se encontro el RFC en lista Negra" ); 
        }
        else
        {
            reporte.AddImageToReport("RFC No esta en Lista Negra", OPG.ScreenShotElement(Driver)); 
                       
        }
     }
    
    public static void  MigraListaNegra2(WebDriv Driver,WebAction WB,Report reporte,String RFC,String Tipo)throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente",Tipo,reporte);
//        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items",Tipo,false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        if(POPError != null )
        {
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "No se encontro el RFC en lista Negra" ); 
        }
        else
        {
            reporte.AddImageToReport("RFC No esta en Lista Negra", OPG.ScreenShotElement(Driver)); 
                       
        }
    }
    
    
    
}
