
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRIDE ICUE 004
 */
public class CV54 {

    public static void PagoFlap(WebDriv Driver,WebAction WB,Report reporte,String Orden) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InpuOrder,SelenSelect,Input,User,Pass,Mensaje,Ingreasr,InpName,InpCC,InpMEsex,InpAnio,InpCCV,Email,InpDN;
        String Folio=null,TextMensaje,CCV,AnoExp,MesExp,NumTarj,Nombre;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Comercio de Negocio","Pagos",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Pagos",true);
        Thread.sleep(5000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        InpuOrder = WB.FindElementByID(Driver.WebDriver, "orderNoId_input_value");
        WB.TypeText(InpuOrder, Orden);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se muestra la orden a pagar", OPG.ScreenShotElement(Driver));
        try 
        {
            WB.ClickById(Driver.WebDriver, "attrId_0");
        } catch (Exception e)
        {
            reporte.AddImageToReport("No existe orden a pagar", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "" );
        }
        reporte.AddImageToReport("Se selecciona la orden a pagar", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));

        
        WB.ClickById(Driver.WebDriver, "btnPayment"); 
        WB.CargaAjax(Driver.WebDriver);
        SelenSelect = WB.FindElement(Driver.WebDriver, WB, "Name", "busiPaymentMethodListOnixCtz[0].paymentMethod");
        WB.SelenSelect(SelenSelect, "Value", "6001");
        WB.CargaAjax(Driver.WebDriver);
        WB.ClickById(Driver.WebDriver, "attrId_0");
        Input = WB.FindElementByID(Driver.WebDriver, "remark");
        WB.TypeText(Input, "Pago con Robot");
        reporte.AddImageToReport("Se procede al pago", OperacionesGenerales.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Pago");
        //WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);

        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(5000);
        try {
            OPG.NavegaIframeDefault(Driver, "popwin0");
            Mensaje = WB.FindElementByID(Driver.WebDriver, "editor_value");
            TextMensaje = Mensaje.getText();
            if (TextMensaje.equals("No se abre la caja registradora.")) 
            {
                OPG.NavegaIframeDefault(Driver, "tabPage_12100102002_iframe");
                reporte.AddImageToReport("La caja registradora esta cerrada", OPG.ScreenShotElement(Driver));
                throw new NullPointerException( "" );
            }
            else if(TextMensaje.equals("La caja registradora ha expirado.")) 
            {
                OPG.NavegaIframeDefault(Driver, "tabPage_12100102002_iframe");
                reporte.AddImageToReport("La caja registradora esta cerrada", OPG.ScreenShotElement(Driver));
                throw new NullPointerException( "" );
            }
            
        } catch (Exception e) {
        }
        Thread.sleep(8000);
        //OPG.NavegaPop(Driver);
        User = WB.FindElementByID(Driver.WebDriver, "username");
        Pass = WB.FindElementByID(Driver.WebDriver, "password");
        WB.TypeText(User, "onix");
        WB.TypeText(Pass, "flap2021");
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //Ingreasr = WB.FindElement(Driver.WebDriver, WB, "Xpath", "/html/body/div/form/div/table/tbody/tr[4]/td/input[2]");
        WB.ClickButton(Driver.WebDriver, WB, "Xpath", "/html/body/div/form/div/table/tbody/tr[4]/td/input[2]");
        Thread.sleep(8000);
        
        //Se cargan los datos de la tarjeta 
        Nombre     = CargaVariables.LeerCSV(0, "CV54", "TarjetaCredito");
        NumTarj    = CargaVariables.LeerCSV(1, "CV54", "TarjetaCredito");
        MesExp     = CargaVariables.LeerCSV(2, "CV54", "TarjetaCredito");
        AnoExp     = CargaVariables.LeerCSV(3, "CV54", "TarjetaCredito");
        CCV        = CargaVariables.LeerCSV(4, "CV54", "TarjetaCredito");
        
        
        InpName = WB.FindElementByID(Driver.WebDriver, "name");
        WB.TypeText(InpName, Nombre);
        InpCC = WB.FindElementByID(Driver.WebDriver, "cc");
        WB.TypeText(InpCC, NumTarj);
        InpMEsex = WB.FindElementByID(Driver.WebDriver, "mesexp");
        WB.SelenSelect(InpMEsex, "Value", MesExp);
        InpMEsex = WB.FindElementByID(Driver.WebDriver, "anyoexp");
        WB.SelenSelect(InpMEsex, "Value", AnoExp);
        InpCCV = WB.FindElementByID(Driver.WebDriver, "cvv");
        WB.TypeText(InpCCV, CCV);

        reporte.AddImageToReport("", OPG.ScreenShotAll());
        WB.ClickButton(Driver.WebDriver, WB, "Xpath", "/html/body/div/form/div/table/tbody/tr[9]/td/input[3]");
        Thread.sleep(8000);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //email name
        //custdn name
        
    }
    
    public static void PagoVentaFlap(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod,String Equipo, String IMEI) throws InterruptedException, IOException
    {
        WebElement InputEquipo,DivEquipo,DivIMEI,DIvadd;
        String Value="",Folio,User,Password;
        Boolean Apagar;
        List <WebElement> clickEquipo,clickAdd;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Recursos de Negocio","Venta de Equipo a Cliente Existente",reporte);
        Thread.sleep(5000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"DN",DN,Cod);
        Thread.sleep(8000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Venta de Equipo a Cliente Existente",false);
        
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver)); 
        InputEquipo = WB.FindElementByID(Driver.WebDriver, "queryOffer_value");
        WB.TypeText(InputEquipo, Equipo);
        WB.ClickById(Driver.WebDriver, "queryOffer_search");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Busqueda de equipo", OPG.ScreenShotElement(Driver)); 
        DivEquipo = WB.FindElementByID(Driver.WebDriver, "productlist_0_4");
        clickEquipo = WB.GetChildren(DivEquipo);
        clickEquipo.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaPop(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        DivIMEI = WB.FindElementByID(Driver.WebDriver, "imeiId_input_value");
        WB.TypeText(DivIMEI, IMEI);
        WB.ClickById(Driver.WebDriver, "primaryOfferingList_head");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        DIvadd = WB.FindElementByID(Driver.WebDriver, "primaryOfferingList_0_5");
        clickAdd = WB.GetChildren(DIvadd);
        clickAdd.get(0).click();
        
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(5000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Venta de Equipo a Cliente Existente",false);

        Thread.sleep(3000);
        OPG.CarritoCompras(Driver, WB, reporte);
        OPG.SwitchIframe(Driver, ParentPri);
        Thread.sleep(5000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Venta de Equipo a Cliente Existente",false);
        OPG.EnviarOrdenConFactura(Driver,WB,ParentPri,"custTabPanel_items", "Venta de Equipo a Cliente Existente", reporte);
        Thread.sleep(2000);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Folio: " +Folio, OPG.ScreenShotElement(Driver)); 
        Thread.sleep(9000);
        OPG.PagaOrdenFlap(Driver, WB, reporte, ParentPri, "custTabPanel_items", "Liquidación de financiamiento");
    }
}
