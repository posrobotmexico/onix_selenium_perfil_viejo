
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesBatch;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class Batch {
    
   
   public static void BatchPrecioVenta(WebDriv Driver,WebAction SelenElem, Report reporte, String Equipo, String SKU, String Meses, String CodigoOferta, String Tipo, String PrecioNuevo, String DN) throws InterruptedException, IOException
    {                                  
        OperacionesGenerales OPG = new OperacionesGenerales();
        OperacionesBatch OPB = new OperacionesBatch();
        WebElement Elem,Ruta;
        List <WebElement> Chil1,Chil2;
        String NomArchivo,CodCuenta,LevelAct;        
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        Thread.sleep(3000);
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas","Venta Pospago",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Pospago",true);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Nuevo Cliente");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        //Se LLenan los datos para el RFC
        OPG.LlenaDatosClienteNuevo(Driver, SelenElem, reporte);
        OPG.BurodeCredito(Driver, SelenElem, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Venta Pospago",true);
        SelenElem.ClickById(Driver.WebDriver, "nextBtn");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
      
        
        OPG.ValidaPrecioHandsetVenta(Driver, SelenElem, reporte, Tipo, Equipo);
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
        Thread.sleep(1000); 
        
        //Se carga el archivo
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Operaciones Masivas","Gestionar operación por lote",reporte);
        Thread.sleep(3000); 
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Gestionar operación por lote",true);
        OPG.NavegaIframe(Driver, "adminBatFrame");
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "batOperList_2_3");
        Chil1 = SelenElem.GetChildren(Elem);
        Chil2 = SelenElem.GetChildren(Chil1.get(0));
        Chil2.get(0).click();
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Gestionar operación por lote",true);
        OPG.NavegaIframe(Driver, "adminBatFrame");
        //SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //SKU|Business Type|New Price|Period|Offer_Code
        NomArchivo = OPB.CreaArchivoVenta(SKU,PrecioNuevo,Meses,CodigoOferta);
        //batOperList_2_3
        Ruta= SelenElem.FindElementByID(Driver.WebDriver, "uploadfile_file");
        SelenElem.TypeText(Ruta,"A:\\DataOnix\\Batch\\Archivos\\"+NomArchivo);
        reporte.AddImageToReport("Se Carga El Archivo "+NomArchivo, OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "submitbtn");
        //SelenElem.CargaAjax(Driver.WebDriver); 
        reporte.AddImageToReport("", OPG.ScreenShotAll());
    }
            
   public static void BatchPrecioReno(WebDriv Driver,WebAction SelenElem, Report reporte, String Equipo, String SKU, String Meses, String CodigoOferta, String Tipo, String PrecioNuevo, String DN) throws InterruptedException, IOException
    {                                  
        OperacionesGenerales OPG = new OperacionesGenerales();
        OperacionesBatch OPB = new OperacionesBatch();
        WebElement Elem,Ruta;
        List <WebElement> Chil1,Chil2;
        String NomArchivo,CodCuenta,LevelAct;        
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Renovar Contrato",reporte);
        String ParentPri = OPG.BusquedaCliente(Driver, SelenElem, "DN", DN, "");
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Renovar Contrato",false);  
        OPG.ValidaBuroReno(Driver, SelenElem,ParentPri);
        Thread.sleep(3000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Renovar Contrato",false);
        //SelenElem.ClickById(Driver.WebDriver, "queryOffer_search");
        OPG.ValidaPrecioHandset(Driver, SelenElem, reporte, Tipo, Equipo);
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
        Thread.sleep(1000); 
        
        //Se carga el archivo
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Operaciones Masivas","Gestionar operación por lote",reporte);
        Thread.sleep(3000); 
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Gestionar operación por lote",true);
        OPG.NavegaIframe(Driver, "adminBatFrame");
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "batOperList_2_3");
        Chil1 = SelenElem.GetChildren(Elem);
        Chil2 = SelenElem.GetChildren(Chil1.get(0));
        Chil2.get(0).click();
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Gestionar operación por lote",true);
        OPG.NavegaIframe(Driver, "adminBatFrame");
        //SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //SKU|Business Type|New Price|Period|Offer_Code
        NomArchivo = OPB.CreaArchivoReno(SKU,PrecioNuevo,Meses,CodigoOferta);
        
        Ruta= SelenElem.FindElementByID(Driver.WebDriver, "uploadfile_file");
        SelenElem.TypeText(Ruta,"A:\\DataOnix\\Batch\\Archivos\\"+NomArchivo);
        reporte.AddImageToReport("Se Carga El Archivo "+NomArchivo, OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "submitbtn");
        //SelenElem.CargaAjax(Driver.WebDriver); 
        reporte.AddImageToReport("", OPG.ScreenShotAll());
    }
    
   public static void BatchCostumerCategory(WebDriv Driver,WebAction SelenElem, Report reporte, String DN, String Codigo, String NewCategoria) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        OperacionesBatch OPB = new OperacionesBatch();
        WebElement Elem,Ruta;
        List <WebElement> RadioBTN;
        String NomArchivo,CodCuenta,LevelAct;        
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver)); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cliente",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custAccept_items", "Información de Cliente",false);
        
        Elem= SelenElem.FindElementByID(Driver.WebDriver,"defaultAccountCode_input");
        CodCuenta = Elem.getText();
        reporte.AddImageToReport("Se obtiene el codigo de cuenta "+CodCuenta, OPG.ScreenShotElement2(Elem));
        Elem= SelenElem.FindElementByID(Driver.WebDriver,"custLevel_input");
        LevelAct = Elem.getText();
        reporte.AddImageToReport("Se obtiene el codigo de cuenta "+LevelAct, OPG.ScreenShotElement2(Elem));
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
        Thread.sleep(1000); 
        
        //Se carga el archivo
        OPG.NavegaMapaSitio(Driver,SelenElem,"Gestión de Batch","Batch de Negocio","Carga de archivos Residenciales",reporte);
        Thread.sleep(4000); 
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPB.SelectBarraLateral(Driver.WebDriver, "Cambio de categoria de cliente por lote");
        SelenElem.ClickById(Driver.WebDriver, "batchNewBusiness");
        OPG.NavegaPop(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //SelenElem.ClickById(Driver.WebDriver, "uploadfile_select");
        NomArchivo = OPB.CreaArchivoCategory(CodCuenta,NewCategoria);
        
        Ruta= SelenElem.FindElementByID(Driver.WebDriver, "uploadfile_file");
        SelenElem.TypeText(Ruta,"A:\\DataOnix\\Batch\\Archivos\\"+NomArchivo);
        reporte.AddImageToReport("Se Carga el archivo "+NomArchivo, OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "submitBtn");
        SelenElem.CargaAjax(Driver.WebDriver);        
        Thread.sleep(8000); 
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Búsqueda");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
    }
    
   public static void BatchPymentMethod(WebDriv Driver,WebAction SelenElem, Report reporte, String DN, String Codigo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        OperacionesBatch OPB = new OperacionesBatch();
        WebElement Elem,Ruta;
        List <WebElement> RadioBTN;
        String NomArchivo,CodCuenta;        
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver)); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cliente",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custAccept_items", "Información de Cliente",false);
        
        Elem= SelenElem.FindElementByID(Driver.WebDriver,"defaultAccountCode_input");
        CodCuenta = Elem.getText();
        reporte.AddImageToReport("Se obtiene el codigo de cuenta"+CodCuenta, OPG.ScreenShotElement2(Elem));
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
        Thread.sleep(1000); 
        
        //Se carga el archivo
        OPG.NavegaMapaSitio(Driver,SelenElem,"Gestión de Batch","Batch de Negocio","Carga de archivos Residenciales",reporte);
        Thread.sleep(4000); 
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPB.SelectBarraLateral(Driver.WebDriver, "Actualización del método de pago por lotes/Actualización del Número de tarjeta departamental por lotes");
        SelenElem.ClickById(Driver.WebDriver, "batchNewBusiness");
        OPG.NavegaPop(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //SelenElem.ClickById(Driver.WebDriver, "uploadfile_select");
        NomArchivo = OPB.CreaArchivo(CodCuenta);
        
        Ruta= SelenElem.FindElementByID(Driver.WebDriver, "uploadfile_file");
        SelenElem.TypeText(Ruta,"A:\\DataOnix\\Batch\\Archivos\\"+NomArchivo);
        RadioBTN = SelenElem.FindElements(Driver.WebDriver, "Name", "newPaymentMethod");
        RadioBTN.get(1).click();
        reporte.AddImageToReport("Se Carga el archivo "+NomArchivo, OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "submitBtn");
        SelenElem.CargaAjax(Driver.WebDriver);        
        Thread.sleep(8000); 
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Búsqueda");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
    }
    
    
}
