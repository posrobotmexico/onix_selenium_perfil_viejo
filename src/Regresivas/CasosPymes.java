/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import Operaciones.OperacionesMigraReno;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class CasosPymes {
    
    public static void ValidaRFC(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
    {
        WebElement TipPersona,RFCInput;
        String Value="";
        OperacionesGenerales OPG = new OperacionesGenerales();

        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta PYMES",reporte); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        reporte.AddImageToReport("Nuevo Cliente", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));  
        
        
        TipPersona = WB.FindElementByID(Driver.WebDriver, "field_500003_500119_input_select");
        WB.SelenSelect(TipPersona, "Value", "PM");
        WB.CargaAjax(Driver.WebDriver);
        RFCInput = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.paramCustBaseInfo.custExtPerson.extAttrs.custRfc");
        WB.TypeText(RFCInput,RFC);
        WB.ClickById(Driver.WebDriver,"field_500003_500085_input_value");
        WB.CargaAjax(Driver.WebDriver);
        //validacion error 
        RFCInput = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.paramCustBaseInfo.custExtPerson.extAttrs.custRfc");
        RFCInput.click();
        Value = RFCInput.getAttribute("class");
        if(Value.contains("error"))
        {
            reporte.AddImageToReport("RFC NO VALIDO", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "RFC NO VALIDO" );
        }
        OPG.ValidaRFC(Driver, WB, reporte);
        reporte.AddImageToReport("Se valida el RFC con caracter especial" + RFC, OPG.ScreenShotElement(Driver));
    }
    
    public static void ValidaBtnRFC(WebDriv Driver,WebAction WB,Report reporte) throws InterruptedException, IOException
    {
        WebElement TipPersona,TipPersona2,RFCBtn;
        OperacionesGenerales OPG = new OperacionesGenerales();
        String StatusBTN ="";
        boolean  BTN,BTN2,BTN3;

        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta PYMES",reporte); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        reporte.AddImageToReport("Nuevo Cliente", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        WB.EsperaElem(Driver.WebDriver, "field_500003_500019_input_value");
        reporte.AddImageToReport("Se valida que este habilitado el boton de Generar RFC", OPG.ScreenShotElement(Driver));  
        
        RFCBtn = WB.FindElementByID(Driver.WebDriver, "field_500003_500200");
        StatusBTN = RFCBtn.getAttribute("disabled");
        reporte.AddImageToReport("", OPG.ScreenShotElement2(RFCBtn));
        
        if (StatusBTN == null) 
        {
            TipPersona = WB.FindElementByID(Driver.WebDriver, "field_500003_500119_input_select");
            WB.SelenSelect(TipPersona, "Value", "PM");
            WB.CargaAjax(Driver.WebDriver);
            
            RFCBtn = WB.FindElementByID(Driver.WebDriver, "field_500003_500200");
            StatusBTN = RFCBtn.getAttribute("disabled");
            
            if (StatusBTN.equals("true"))
            {
               reporte.AddImageToReport("Se valida que el boton Generar RFC se deshabilite ", OPG.ScreenShotElement(Driver)); 
               reporte.AddImageToReport("", OPG.ScreenShotElement2(RFCBtn)); 
            }
            else
            {
                reporte.AddImageToReport("RFC NO VALIDO", OperacionesGenerales.ScreenShotElement(Driver));
                throw new NullPointerException( "RFC NO VALIDO" );
            }
            
        }
        else
        {
            reporte.AddImageToReport("RFC NO VALIDO", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "RFC NO VALIDO" );
        }   
    }
    
    public static void AdicionCuent(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
    {
        WebElement Tabla,RFCInput;
        String Value="";
        OperacionesGenerales OPG = new OperacionesGenerales();

        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta PYMES",reporte); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        WB.EsperaElem(Driver.WebDriver, "rfc_input_value");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        RFCInput = WB.FindElementByID(Driver.WebDriver, "rfc_input_value");
        WB.TypeText(RFCInput, RFC);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        
        Tabla = WB.FindElementByID(Driver.WebDriver, "contentList_databody");
        if (Tabla != null) 
        {
            Value = Tabla.getText();
            if(Value.equals("No hay registros"))
            {
                reporte.AddImageToReport("Validamos que no se puede agregar un nuevo cliente", OperacionesGenerales.ScreenShotElement(Driver));

            }
            else
            {
                reporte.AddImageToReport("RFC invalido" + RFC, OPG.ScreenShotElement(Driver));
                throw new NullPointerException( "RFC NO VALIDO" );
            }
            
        }
        else
        {
            reporte.AddImageToReport("RFC invalido" + RFC, OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "RFC NO VALIDO" );
        }
        
    }
     
    public static void VentaCliExi(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
    {
        WebElement Tabla,RFCInput,TipPersona;
        String Value="";
        OperacionesGenerales OPG = new OperacionesGenerales();

        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta PYMES",reporte); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        reporte.AddImageToReport("Nuevo Cliente", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 

        RFCInput = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.paramCustBaseInfo.custExtPerson.extAttrs.custRfc");
        WB.TypeText(RFCInput,RFC);
        reporte.AddImageToReport("Se ingresa el RFC", OPG.ScreenShotElement(Driver));
        reporte.AddImageToReport("", OPG.ScreenShotElement2(RFCInput));
        WB.ClickById(Driver.WebDriver,"field_500003_500200");
        WB.CargaAjax(Driver.WebDriver);
        //validacion error 
        OPG.ValidaRFCPymes(Driver, WB, reporte);
        
    }
    
    public static void AgregarRFC(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,InputComen,InputBusqRFC,Tbody;
        String Txt;
        String Folio=null;
        int Tamano;
        
        Tamano = RFC.length();
        if(Tamano != 12)
        {
            reporte.AddImageToReport("El RFC no es Pymes, no cuenta con 12 caracteres", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "El RFC no es PM" );
        }
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Lista de administración","Gestionar Lista Negra",reporte);        
        Thread.sleep(6000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        //WebDriver nFrame1 = Driver.WebDriver.switchTo().frame("tabPage_121001006003_iframe");               
        //Driver.AssingNewDriver(nFrame1);
        WebDriver nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        WebDriver nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        
        WB.WaitForElem(Driver.WebDriver, "add");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        WB.ClickById(Driver.WebDriver, "add");
        WB.CargaAjax(Driver.WebDriver);
        OPG.NavegaPop(Driver);
        InputRFC = WB.FindElementByID(Driver.WebDriver, "textfield_id_0_input_value");
        WB.TypeText(InputRFC, RFC);
        InputComen = WB.FindElement(Driver.WebDriver, WB, "Name", "logListItemValue.remark");
        WB.TypeText(InputComen, "Ingresado por robot");
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver)); 
        WB.ClickById(Driver.WebDriver, "addSaveButton_1");
        WB.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        //nFrame1 = Driver.WebDriver.switchTo().frame("tabPage_121001006003_iframe");               
        //Driver.AssingNewDriver(nFrame1);
        nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //Validacion
        InputBusqRFC = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        WB.TypeText(InputBusqRFC, RFC);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        
        Tbody = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_databody");
        Txt = Tbody.getText();
        if(Txt.equals("No hay registros"))
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );
        }
        else
            reporte.AddImageToReport("RFC en lista negra", OPG.ScreenShotElement(Driver));         
     }
     
    public static void AgregarRFCSim1(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException 
     {
         OperacionesGenerales OPG = new OperacionesGenerales();
         if (RFC.contains("&"))
         {
             CasosPymes.AgregarRFC(Driver, WB, reporte, RFC);
         }
         else
         {
            reporte.AddImageToReport("El RFC no cuenta con el caracter: &", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "El RFC no cuenta con el caracter: &" );
         }
     }
     
    public static void AgregarRFCSim2(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException 
     {
         OperacionesGenerales OPG = new OperacionesGenerales();
         RFC = RFC.toUpperCase();
         if (RFC.contains("Ñ"))
         {
             CasosPymes.AgregarRFC(Driver, WB, reporte, RFC);
         }
         else
         {
            reporte.AddImageToReport("El RFC no cuenta con el caracter: Ñ", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "El RFC no cuenta con el caracter: Ñ" );
         }
     }
    
    public static void CambiarInfoCli(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
        String Txt;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cambiar Información de Cliente",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,"");
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cliente",false);        
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "editor_value");
        Txt = POPError.getText();
        if(POPError != null && Txt.equals("El cliente está en la lista negra."))
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
     }
    
    public static void VentaNewClientPosp(WebDriv Driver,WebAction WB,Report reporte,String RFC) throws InterruptedException, IOException
     {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,POPError;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta PYMES",reporte);      
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);        
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        InputRFC = WB.FindElementByID(Driver.WebDriver, "field_500003_500019_input_value");
        WB.TypeText(InputRFC, RFC);
        WB.ClickById(Driver.WebDriver, "field_500003_500020_input_value");
        WB.CargaAjax(Driver.WebDriver);
        
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        POPError = WB.FindElementByID(Driver.WebDriver, "popwin_top");
        if(POPError != null)
            reporte.AddImageToReport("RFC en Lista Negra", OPG.ScreenShotElement(Driver));
        else
        {
            reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );            
        }
     }
    
    public static void SoloSIMPosp(WebDriv Driver,WebAction WB,Report reporte,String SIM,String Oferta,String TipPer) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig;
        WebElement ICC,InputDN;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta PYMES",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        WB.WaitForElem(Driver.WebDriver, "serviceNo_input_value");
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos Cliente Nuevo", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        OPG.LlenaDatosClienteNuevoPymes2(Driver, WB, reporte,TipPer);
        if(TipPer.equals("PM"))
            OPG.ValidaRFC(Driver, WB, reporte);
        
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
                OPG.BurodeCreditoPymes(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(6000); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.MenuVentas(Driver, WB, reporte, "Sólo SIM pospago", Oferta);
        Thread.sleep(2000);                         
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Ingrsamos la ICC", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaDNPool(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        OPG.CreaCuentaVentas(Driver, WB, reporte, "5500110000", "Venta PYMES");
        //OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta PYMES", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
 
    }
    
    public static void VentaSimconHandset(WebDriv Driver,WebAction WB,Report reporte,String SIM,String Oferta,String Equipo, String IMEI,String MESES,String TipPer) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> DivTablaBusq,Chil3,Tr,BtnSig,ChilCont;
        WebElement ICC,InputDN,InputIMEI,DivContinuar,mesesinput;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta PYMES",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        WB.WaitForElem(Driver.WebDriver, "serviceNo_input_value");
        Thread.sleep(1000);
        reporte.AddImageToReport("Seleccionamos Cliente Nuevo", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Nuevo Cliente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        OPG.LlenaDatosClienteNuevoPymes2(Driver, WB, reporte,TipPer);
        if(TipPer.equals("PM"))
            OPG.ValidaRFC(Driver, WB, reporte);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        
        OPG.BurodeCreditoPymes(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(6000); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.MenuVentasHandset(Driver, WB, reporte, "SIM Híbrida con Dispositivo Móvil de Crédito", Equipo);
        Thread.sleep(2000); 
       // Oferta   
        InputIMEI = WB.FindElementByID(Driver.WebDriver, "imeiId_input_value");
        WB.TypeText(InputIMEI, IMEI);
        WB.ClickById(Driver.WebDriver, "primaryOfferingList_0_3");
        WB.CargaAjax(Driver.WebDriver);
        OPG.ValidaICC(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        //meses
        mesesinput = WB.FindElementByID(Driver.WebDriver, "creditInstallmentTypeId_input_select");
        WB.SelenSelect(mesesinput, "Value", MESES);
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se ingresa el IMEI", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaPlanVenta(reporte,Driver,WB, Oferta);
        Thread.sleep(4000);
        OPG.CreaCuentaVentasCredit(Driver, WB, reporte, "5500110000", "Venta Pospago","Venta Pospago");
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Ingrsamos la ICC", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaDNPool2(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        
        //OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta Pospago", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
    }
    
      public static void CambiarInfoClien(WebDriv Driver,WebAction WB,Report reporte,String RFC,String Codigo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,DivRFC,Div,CP,CALLE,NUMEX;
        String TxtRFC, Folio;;
        List <WebElement> ChilDiv;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Lista de administración","Gestionar Lista Negra",reporte);        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        Thread.sleep(2000);
        WebDriver nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        WebDriver nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        
        WB.WaitForElem(Driver.WebDriver, "add");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        WB.TypeText(InputRFC, RFC);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Busca el RFC", OPG.ScreenShotElement(Driver));
        
         try {
             DivRFC = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_1");
             TxtRFC = DivRFC.getText();
             if (TxtRFC.equals(RFC))
             {
                 Div = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_0");
                 ChilDiv = WB.GetChildren(Div);
                 ChilDiv.get(0).click();
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Informacion del RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_toolbaritem", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 OPG.NavegaPop(Driver);
                 reporte.AddImageToReport("Se elimina el RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 //
                 
                 OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
                 nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
                 Driver.AssingNewDriver(nFrame2);
                 nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
                 Driver.AssingNewDriver(nFrame3);
                 nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
                 Driver.AssingNewDriver(nFrame4);
                 InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
                 InputRFC.clear();
                 WB.TypeText(InputRFC, RFC);
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Se valida que el RFC no este en lista negra", OPG.ScreenShotElement(Driver));
             }
         } catch (Exception e) {
             reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );                
         }
         
        WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        OPG.CierraPestanas(Driver);
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cambiar Información de Cliente",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"RFC",RFC,Codigo);
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cliente",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        NUMEX = WB.FindElementByID(Driver.WebDriver, "field_1007_500506_input_value");
        WB.TypeText(NUMEX,"22");
        
        
        //OPG.LlenaDatosClienteNuevoConRFC(Driver, WB, reporte, false);
        
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        OPG.EnviarOrden(Driver, WB, ParentPri, "custTabPanel_items", "Cambiar Información de Cliente", reporte);
        Folio = OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver)); 
                
    }
     public static void CambiarInfoCuent(WebDriv Driver,WebAction WB,Report reporte,String RFC,String Codigo) throws InterruptedException, IOException
    {
        
       OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputRFC,DivRFC,Div,CALLE,NUMEX,Elem,Input,CP,FECHA,TITULO, BTNpop=null,Selen;
        Boolean Apagar=false;
        String TxtRFC, Folio;;
        List <WebElement> ChilDiv;
              
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB); 
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Lista de administración","Gestionar Lista Negra",reporte);        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        Thread.sleep(2000);
        WebDriver nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        WebDriver nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        
        WB.WaitForElem(Driver.WebDriver, "add");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        WB.TypeText(InputRFC, RFC);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Busca el RFC", OPG.ScreenShotElement(Driver));
        
         try {
             DivRFC = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_1");
             TxtRFC = DivRFC.getText();
             if (TxtRFC.equals(RFC))
             {
                 Div = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_0");
                 ChilDiv = WB.GetChildren(Div);
                 ChilDiv.get(0).click();
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Informacion del RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_toolbaritem", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 OPG.NavegaPop(Driver);
                 reporte.AddImageToReport("Se elimina el RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 //
                 
                 OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
                 nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
                 Driver.AssingNewDriver(nFrame2);
                 nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
                 Driver.AssingNewDriver(nFrame3);
                 nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
                 Driver.AssingNewDriver(nFrame4);
                 InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
                 InputRFC.clear();
                 WB.TypeText(InputRFC, RFC);
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Se valida que el RFC no este en lista negra", OPG.ScreenShotElement(Driver));
             }
         } catch (Exception e) {
             reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );                
         }
         
        WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        OPG.CierraPestanas(Driver); 
        
        
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"RFC",RFC,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cuenta",false);
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
         
        WB.ClickById(Driver.WebDriver, "acctInformation_titlebar");
        WB.ClickById(Driver.WebDriver, "address_titlebar");
        
        
        WB.ClickById(Driver.WebDriver, "chgAcctBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cuenta",false);
       
        //Direccion de entrega de factura
        NUMEX = WB.FindElementByID(Driver.WebDriver, "field_500010_500506_input_value");
        WB.TypeText(NUMEX,"22");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        
        
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        OPG.IframeActual(Driver);
        BTNpop = WB.FindMultiElem(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        if(BTNpop!=null)
        {
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
            WB.CargaAjax(Driver.WebDriver);
        }
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, WB);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,WB,ParentPri,"custTabPanel_items","Cambiar Información de Cuenta", reporte);
            Folio= OPG.BuscaFolio(Driver,WB);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
      }
     
     public static void VentaConHandsetCliExist(WebDriv Driver,WebAction WB,Report reporte,String RFC,String Codigo,String Equipo, String IMEI) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> ChilCarr,ChilDiv;
        WebElement ICC,InputDN,InputRFC,CarritoAd,DivRFC,Div;
        String Folio=null,ParentPri,TxtRFC;
        WebDriver nFrame;
        
              
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB); 
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Lista de administración","Gestionar Lista Negra",reporte);        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        Thread.sleep(2000);
        WebDriver nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        WebDriver nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        
        WB.WaitForElem(Driver.WebDriver, "add");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        WB.TypeText(InputRFC, RFC);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Busca el RFC", OPG.ScreenShotElement(Driver));
        
         try {
             DivRFC = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_1");
             TxtRFC = DivRFC.getText();
             if (TxtRFC.equals(RFC))
             {
                 Div = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_0");
                 ChilDiv = WB.GetChildren(Div);
                 ChilDiv.get(0).click();
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Informacion del RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_toolbaritem", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 OPG.NavegaPop(Driver);
                 reporte.AddImageToReport("Se elimina el RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 //
                 
                 OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
                 nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
                 Driver.AssingNewDriver(nFrame2);
                 nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
                 Driver.AssingNewDriver(nFrame3);
                 nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
                 Driver.AssingNewDriver(nFrame4);
                 InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
                 InputRFC.clear();
                 WB.TypeText(InputRFC, RFC);
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Se valida que el RFC no este en lista negra", OPG.ScreenShotElement(Driver));
             }
         } catch (Exception e) {
             reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );                
         }
         
        WebDriver nFrame1 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame1);
        OPG.CierraPestanas(Driver); 
        
        
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Recursos de Negocio","Venta de Equipo a Cliente Existente",reporte);
        ParentPri = OPG.BusquedaCliente(Driver,WB,"RFC",RFC,Codigo);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Venta de Equipo a Cliente Existente",false);       
        OPG.MenuVentasHandset(Driver, WB, reporte, "Equipo", Equipo);
        Thread.sleep(2000);  
        
        //Se busca el pop 
        OPG.NavegaPop(Driver);
        ICC = WB.FindElementByID(Driver.WebDriver, "imeiId_input_value");
        WB.TypeText(ICC, IMEI);
        WB.ClickById(Driver.WebDriver, "quantity_input_value");
        WB.CargaAjax(Driver.WebDriver);
        CarritoAd = WB.FindElementByID(Driver.WebDriver, "primaryOfferingList_0_5");
        ChilCarr = WB.GetChildren(CarritoAd);
        ChilCarr.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        //Se hacepta la ventana 
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        Thread.sleep(5000);
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Venta de Equipo a Cliente Existente",false);
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        OPG.CarritoCompras(Driver, WB, reporte);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Venta de Equipo a Cliente Existente",false);
        Thread.sleep(3000);
        OPG.EnviarOrden(Driver, WB, ParentPri,"custTabPanel_items", "Venta de Equipo a Cliente Existente", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
    } 
     
     public static void NuevaVentaCliExist(WebDriv Driver,WebAction WB,Report reporte,String RFC,String Codigo,String SIM,String Oferta) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> ChilCarr,ChilDiv;
        WebElement ICC,InputDN,InputRFC,CarritoAd,DivRFC,Div;
        String Folio=null,ParentPri,TxtRFC;
        WebDriver nFrame;
        
              
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB); 
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Lista de administración","Gestionar Lista Negra",reporte);        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
        Thread.sleep(2000);
        WebDriver nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
        Driver.AssingNewDriver(nFrame2);
        WebDriver nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
        Driver.AssingNewDriver(nFrame3);
        WebDriver nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
        Driver.AssingNewDriver(nFrame4);
        
        WB.WaitForElem(Driver.WebDriver, "add");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        WB.TypeText(InputRFC, RFC);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Busca el RFC", OPG.ScreenShotElement(Driver));
        
         try {
             DivRFC = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_1");
             TxtRFC = DivRFC.getText();
             if (TxtRFC.equals(RFC))
             {
                 Div = WB.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_0");
                 ChilDiv = WB.GetChildren(Div);
                 ChilDiv.get(0).click();
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Informacion del RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_toolbaritem", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 OPG.NavegaPop(Driver);
                 reporte.AddImageToReport("Se elimina el RFC", OPG.ScreenShotElement(Driver));
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Eliminar");
                 WB.CargaAjax(Driver.WebDriver);
                 //
                 
                 OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Gestionar Lista Negra",true);
                 nFrame2 = Driver.WebDriver.switchTo().frame("listTypeId_0_iframe");               
                 Driver.AssingNewDriver(nFrame2);
                 nFrame3 = Driver.WebDriver.switchTo().frame("subtypeTab_1_iframe");               
                 Driver.AssingNewDriver(nFrame3);
                 nFrame4 = Driver.WebDriver.switchTo().frame("fromItemId_iframe");               
                 Driver.AssingNewDriver(nFrame4);
                 InputRFC = WB.FindElement(Driver.WebDriver,WB,"Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
                 InputRFC.clear();
                 WB.TypeText(InputRFC, RFC);
                 WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
                 WB.CargaAjax(Driver.WebDriver);
                 reporte.AddImageToReport("Se valida que el RFC no este en lista negra", OPG.ScreenShotElement(Driver));
             }
         } catch (Exception e) {
             reporte.AddImageToReport("El RFC no se muestra en lista negra", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro el RFC en lista Negra" );                
         }
         
        WebDriver nFrame1 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame1);
        OPG.CierraPestanas(Driver); 
         
        
        
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Punto de Ventas","Venta PYMES",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        WB.WaitForElem(Driver.WebDriver, "serviceNo_input_value");
        Thread.sleep(1000);
        reporte.AddImageToReport("Se busca al cliente", OPG.ScreenShotElement(Driver));
        ParentPri = OPG.BusquedaClienteVentas(Driver,WB,"RFC",RFC,Codigo,"Venta PYMES");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        //Aviso de Priv
        WB.ClickButton(Driver.WebDriver, WB, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.dynAttributes.noticeOfPrivacy");
        OPG.BurodeCredito(Driver, WB, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(6000); 
        
        OPG.MenuVentas(Driver, WB, reporte, "Sólo SIM pospago", Oferta);
        Thread.sleep(2000);                         
        ICC = WB.FindElementByID(Driver.WebDriver, "iccid__input_value");
        WB.TypeText(ICC, SIM);
        WB.ClickById(Driver.WebDriver, "password__input_value");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Ingrsamos la ICC", OPG.ScreenShotElement(Driver));
        OPG.SeleccionaDNPool(Driver, WB, reporte);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta PYMES",true);
        //OPG.CreaCuentaVentas(Driver, WB, reporte, "5500110000", "Venta PYMES");
        //OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Venta Pospago",true);
        reporte.AddImageToReport("Agreamos al carrito de compras", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Agregar a Carro de Compras");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Detalle de Compra", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "paynow");
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(3000);
        OPG.EnviarOrdenContraDigital2(Driver,WB,"tabpage_items","Venta PYMES", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        
    }
     
     public static void CambioOferPrim(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Input,Eletab,Tabla,BtnSig,BtnPagoAdelantado;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio,TxtBusq;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Cambiar Oferta",reporte);
        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        WB.EsperaElem(Driver.WebDriver, "zSubscriberInfo_SubsOffering_title");
         Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra la informacion del cliente", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver,"zSubscriberInfo_SubsOffering_title");
         Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra la informacion del Plan", OPG.ScreenShotElement(Driver));
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        reporte.AddImageToReport("Se muestra la oferta Pymes", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_toolbaritem", "Seleccionar Oferta Primaria");
        WB.CargaAjax(Driver.WebDriver);
        OPG.NavegaPop(Driver);
        WB.EsperaElem(Driver.WebDriver, "migrateOfferList_databody");
        //Sebusca la oferta 
        OPG.BuscaOfertaPrimaria(Driver, WB, reporte,Plan, "migrateOfferList_databody");
        Thread.sleep(2000);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        WB.CargaAjax(Driver.WebDriver);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        SelenElem.CargaAjax(Driver.WebDriver);
        BtnSig = WB.FindElementByID(Driver.WebDriver, "nextButtonid");
        OPG.VisualiaElem(Driver, BtnSig);
        WB.ClickById(Driver.WebDriver, "nextButtonid");         
        Thread.sleep(2000);
        BtnPagoAdelantado = WB.FindElementByID(Driver.WebDriver, "confirmBtn");
        if(BtnPagoAdelantado == null)
        {
        }
        else
          BtnPagoAdelantado.click();
            
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Oferta", reporte);
            //OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Oferta", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }        

    }
     
      public static void MigraPymPrep(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan,String DNFact) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        Operaciones.OperacionesMigraReno OPMR = new OperacionesMigraReno();
        WebDriver nFrame;
        WebElement Elem=null,SelenSelect,Input,CrearCuenta,BanPop=null,Direc,Tabla;
        List <WebElement> Div1, Div2, Div4, Div5,Tbody;
        Boolean Apagar=false;
        String Folio,ID,ID2,TxtTabla;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Migracion POS-PRE",reporte);
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        //Se busca la cuenta hibrida en el pop "No siempre aparece"
        
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Migracion POS-PRE",false);
        
        OPMR.AvisoSaldo(Driver,WB, reporte);
        reporte.AddImageToReport("Datos de la cuenta", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "generalCustInfo_titlebar");
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        SelenElem.CargaAjax(Driver.WebDriver);
        OPMR.EligePlan(Driver, WB, reporte, Plan);
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "accountInfo_head");
        Thread.sleep(1000);
        //Validamos que tenga cuentas
        Tbody = WB.FindElementsByClassName(Driver.WebDriver, "bc_grid_databody");//bc_grid_databody
        Tabla = Tbody.get(1);
        TxtTabla = Tabla.getText();
        if(TxtTabla.equals("No hay registros"))
        {
            OPMR.CreaCuenta(Driver, WB, reporte, Plan, DNFact, ParentPri, "Migracion POS-PRE");
        }
        else
        {
           OPMR.SeleccionaCuentas(Driver, WB, "Prepago");
        }
        
        reporte.AddImageToReport("Se elige la Cuenta", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Migracion POS-PRE", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        //reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        //if(Apagar.equals(true))
            //OPG.PagaOrden();
        
    }
}
