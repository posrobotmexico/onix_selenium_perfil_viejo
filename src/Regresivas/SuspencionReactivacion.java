/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import java.util.concurrent.TimeUnit;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class SuspencionReactivacion {
    
    public static void SuspencionDiaEspesifico(WebDriv Driver,WebAction SelenElem,Report reporte, String DN,String Codigo, String Fecha, Boolean Dia) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Elem2,Input;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio, ParentPri =null;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Suspensión Unidireccional/Bidireccional",reporte);
        Thread.sleep(3000);
        try {
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        } catch (Exception e) {
            Thread.sleep(8000);
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        }
        
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        Driver.WebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(3000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suspensión Unidireccional/Bidireccional",false);
        WB.EsperaElem(Driver.WebDriver, "stopReason_input_select");
        Elem = WB.FindElement(Driver.WebDriver, WB, "Name", "stopType");
        WB.SelenSelect(Elem, "Value", "RT003_3");
        Elem2 = WB.FindElement(Driver.WebDriver, WB, "Name", "stopReason");
        WB.SelenSelect(Elem2, "Value", "SO001");
        reporte.AddImageToReport("Se configura la suspencion", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Thread.sleep(3000);
        if(Dia == true)
        {
            WB.ClickById(Driver.WebDriver, "mode_specifytime_0");
            WB.CargaAjax(Driver.WebDriver);
            Input = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEModel.feeInfo.tempFeeDataValue.specifyEffDate");
            Input.clear();
            WB.TypeText(Input, Fecha);
            reporte.AddImageToReport("Se asigna el dia a suspender", OPG.ScreenShotElement(Driver));            
        }       
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Suspensión Unidireccional/Bidireccional", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
    }
    
     public static void Reactivacion(WebDriv Driver,WebAction SelenElem,Report reporte, String DN,String Codigo, String Fecha, Boolean Dia) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Elem2,Input;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio, ParentPri =null;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Reactivación de Línea",reporte);
        Thread.sleep(3000);
        try {
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        } catch (Exception e) {
            Thread.sleep(8000);
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        }
        
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        Driver.WebDriver.manage().timeouts().implicitlyWait(10, TimeUnit.SECONDS);
        Thread.sleep(3000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Reactivación de Línea",false);
        //----------------------------------------
        WB.EsperaElem(Driver.WebDriver, "resumeReason_input_select");
        Elem = WB.FindElement(Driver.WebDriver, WB, "Name", "resumeType");
        WB.SelenSelect(Elem, "Value", "RT003_4");
        Elem2 = WB.FindElement(Driver.WebDriver, WB, "Name", "resumeReason");
        WB.SelenSelect(Elem2, "Value", "RO001");
        reporte.AddImageToReport("Se configura la reactivacion", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Thread.sleep(3000);
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Reactivación de Línea", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
    }
     
     
    public static void  BarringPospago(WebDriv Driver,WebAction SelenElem,Report reporte, String DN) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Ruta,Elem1,Elem2,Select;
        String Nametxt;
        List <WebElement> Chil1,Chil2;
        
        Nametxt = OPG.GeneraTxtBatch(DN,"Barring");
        
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Gestión de Batch","Batch de Negocio","Carga de archivos Residenciales",reporte);
        Thread.sleep(4000); 
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.SelectBarraLateral(Driver.WebDriver,SelenElem, "Suspender y Bloquear Batch");
        SelenElem.ClickById(Driver.WebDriver, "batchNewBusiness");
        Thread.sleep(3000);
        OPG.NavegaPop(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //SelenElem.ClickById(Driver.WebDriver, "uploadfile_select");
        //NomArchivo = OPB.CreaArchivoCategory(CodCuenta,NewCategoria);
        Thread.sleep(3000);
        Ruta= SelenElem.FindElementByID(Driver.WebDriver, "uploadfile_file");
        SelenElem.TypeText(Ruta,"A:\\DataOnix\\Suspencion\\Archivos Temporales\\"+Nametxt+".txt");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Carga el archivo: "+Nametxt, OPG.ScreenShotElement(Driver));
        
        Chil1 = WB.FindElements(Driver.WebDriver, "Name", "subscriberType");
        Chil1.get(1).click();
        WB.CargaAjax(Driver.WebDriver);
        Chil1 = WB.FindElements(Driver.WebDriver, "Name", "operateType");
        Chil1.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        Select = WB.FindElementByID(Driver.WebDriver, "suspendBarReasonCode_input_select");
        WB.SelenSelect(Select, "Value", "BC001");
        WB.ClickById(Driver.WebDriver, "runAtOnce1_input_0");
        reporte.AddImageToReport("Se Envia lel archivo", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "submitBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(7000);
        OPG.IframeActual(Driver);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        WB.ClickById(Driver.WebDriver, "includehistory_0");
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Búsqueda");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
         
        
}
     
    
    public static void  UnbarringPospago(WebDriv Driver,WebAction SelenElem,Report reporte, String DN) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Ruta,Elem1,Elem2,Select;
        String Nametxt;
        List <WebElement> Chil1,Chil2;
        
        Nametxt = OPG.GeneraTxtBatch(DN,"Unbarring");
        
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Gestión de Batch","Batch de Negocio","Carga de archivos Residenciales",reporte);
        Thread.sleep(4000); 
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.SelectBarraLateral(Driver.WebDriver,SelenElem, "Suspender y Bloquear Batch");
        SelenElem.ClickById(Driver.WebDriver, "batchNewBusiness");
        Thread.sleep(3000);
        OPG.NavegaPop(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //SelenElem.ClickById(Driver.WebDriver, "uploadfile_select");
        //NomArchivo = OPB.CreaArchivoCategory(CodCuenta,NewCategoria);
        Thread.sleep(3000);
        Ruta= SelenElem.FindElementByID(Driver.WebDriver, "uploadfile_file");
        SelenElem.TypeText(Ruta,"A:\\DataOnix\\Suspencion\\Archivos Temporales\\"+Nametxt+".txt");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Carga el archivo: "+Nametxt, OPG.ScreenShotElement(Driver));
        
        Chil1 = WB.FindElements(Driver.WebDriver, "Name", "subscriberType");
        Chil1.get(1).click();
        WB.CargaAjax(Driver.WebDriver);
        Chil1 = WB.FindElements(Driver.WebDriver, "Name", "operateType");
        Chil1.get(1).click();
        WB.CargaAjax(Driver.WebDriver);
        Select = WB.FindElementByID(Driver.WebDriver, "suspendBarReasonCode_input_select");
        WB.SelenSelect(Select, "Value", "UC001");
        WB.ClickById(Driver.WebDriver, "runAtOnce1_input_0");
        reporte.AddImageToReport("Se Envia lel archivo", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "submitBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(7000);
        OPG.IframeActual(Driver);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        WB.ClickById(Driver.WebDriver, "includehistory_0");
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Búsqueda");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
         
        
}
    
}
