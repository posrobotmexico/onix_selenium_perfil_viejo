/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import Core.WebAction;
import java.awt.AWTException;
import java.io.IOException;
import java.util.List;
import java.awt.Robot;
import java.awt.event.KeyEvent;
import java.util.concurrent.TimeUnit;
import static java.util.concurrent.TimeUnit.SECONDS;
import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;

/**
 *
 * @author PRACTIABOTPC06
 */
public class AtencionAlCliente {
    
    
    public static void CambioDeSimSinCosto(WebDriv Driver,WebAction SelenElem,String DN,String CodigoC,String ICC,String Causa,Report reporte) throws InterruptedException, IOException
    {
        WebElement div,radioBtn,Input,Razon,textarea,Codigo, Error;
        String text,Value="",Folio;
        List<WebElement> childrenElements;
        Boolean Apagar;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        //Driver.WebDriver.findElement(By.id("firstname-placeholder")).sendKeys(Keys.F5);
        //Driver.WebDriver.navigate().refresh();
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,CodigoC);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "BusinessAccept_items", "Manejo de SIM",false);
        Thread.sleep(5000);
        div = SelenElem.FindElementByID(Driver.WebDriver, "resources_0_4");
        text=div.getText();
        if(text.equals("Activada"))
        {
           radioBtn = SelenElem.FindElementByID(Driver.WebDriver, "resources_0_0");
           childrenElements = SelenElem.GetChildren(radioBtn);
           childrenElements.get(0).click();
           SelenElem.CargaAjax(Driver.WebDriver);
           //WB.ClickById(Driver.WebDriver, "simInfomation_titlebar");
           
        }
        else
        {
            reporte.AddImageToReport("SIM Desactivada", OPG.ScreenShotElement(Driver)); 
            System.out.println("SIM no disponible");
            throw new NullPointerException( "Error en el proceso" );
        }
        reporte.AddImageToReport("Selecciona la SIM a Cambiar", OPG.ScreenShotElement(Driver));
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Facturación",false);
        Thread.sleep(3000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        Thread.sleep(1000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "BusinessAccept_items", "Manejo de SIM",false);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Cambio de Tarjeta SIM");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
               
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false); 
        Input = WB.FindElementByID(Driver.WebDriver, "newsimno_input_value");              
        Razon=SelenElem.FindElementByID(Driver.WebDriver,"busiReason_input_select");
        if(Razon!=null)
        {
            if(Causa.equals("Dano o Bloqueo"))
                     Value = "07";
            else if (Causa.equals("Robo"))
                Value = "ROE";
        
            WB.SelenSelect(Razon,"Value",Value);
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(2000);
        }
        WB.TypeText(Input, ICC); 
        textarea = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "chgSimContentInfo.remark");
        textarea.click();
        WB.CargaAjax(Driver.WebDriver);
        WB.TypeText(textarea, "Prueba Automatizada");
        reporte.AddImageToReport("Se llenan los campos", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "sendBtn");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se envia el codigo", OPG.ScreenShotElement(Driver));
        
        WebDriver nFrame2 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame2);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "popwin_btn_group", "Aceptar");
        
        //Regresamos al iframe del tab
        OPG.SwitchIframe(Driver, ParentPri);
    
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        Codigo = SelenElem.FindElementByID(Driver.WebDriver, "verificationCode_input_value");
        WB.TypeText(Codigo, "12345");
        reporte.AddImageToReport("Se valida el codigo", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "sendBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        WB.ClickById(Driver.WebDriver, "validateBtn");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "popwin_btn_group", "Aceptar");
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        WB.ClickById(Driver.WebDriver, "nextBt");
        //Driver.AssingNewDriver(nFrame3);
        Thread.sleep(3000);
        WB.CargaAjax(Driver.WebDriver);
        nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        Error = WB.FindElementByID(Driver.WebDriver, "popwin_top");
        if(Error== null)
        {
            reporte.AddImageToReport("Se Valida la orden", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "Error en el proceso" );
        }
        Thread.sleep(1000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambio de Tarjeta SIM", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
        
                    
    }
    
    public static void CambioDeSimConCosto(WebDriv Driver,WebAction SelenElem,String DN,String CodigoC,String ICC,String Causa,Report reporte) throws InterruptedException, IOException
    {
        WebElement div,radioBtn,Input,Razon,textarea,Codigo, Error;
        String text,Value="",Folio;
        List<WebElement> childrenElements;
        Boolean Apagar;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        //Driver.WebDriver.findElement(By.id("firstname-placeholder")).sendKeys(Keys.F5);
        //Driver.WebDriver.navigate().refresh();
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,CodigoC);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "BusinessAccept_items", "Manejo de SIM",false);
        Thread.sleep(5000);
        SelenElem.EsperaElem(Driver.WebDriver, "resources_0_4");
        div = SelenElem.FindElementByID(Driver.WebDriver, "resources_0_4");
        text=div.getText();
        if(text.equals("Activada"))
        {
           radioBtn = SelenElem.FindElementByID(Driver.WebDriver, "resources_0_0");
           childrenElements = SelenElem.GetChildren(radioBtn);
           childrenElements.get(0).click();
           SelenElem.CargaAjax(Driver.WebDriver);
           //WB.ClickById(Driver.WebDriver, "simInfomation_titlebar");
           
        }
        else
        {
            reporte.AddImageToReport("SIM Desactivada", OPG.ScreenShotElement(Driver)); 
            System.out.println("SIM no disponible");
            throw new NullPointerException( "Error en el proceso" );
        }
        reporte.AddImageToReport("Selecciona la SIM a Cambiar", OPG.ScreenShotElement(Driver));
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Facturación",false);
        Thread.sleep(3000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        Thread.sleep(1000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "BusinessAccept_items", "Manejo de SIM",false);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Cambio de Tarjeta SIM");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
               
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false); 
        Input = WB.FindElementByID(Driver.WebDriver, "newsimno_input_value");              
        Razon=SelenElem.FindElementByID(Driver.WebDriver,"busiReason_input_select");
        if(Razon!=null)
        {
            if(Causa.equals("Dano o Bloqueo"))
                     Value = "07";
            else if (Causa.equals("Robo"))
                Value = "ROE";
        
            WB.SelenSelect(Razon,"Value",Value);
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(2000);
        }
        WB.TypeText(Input, ICC); 
        textarea = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "chgSimContentInfo.remark");
        textarea.click();
        WB.CargaAjax(Driver.WebDriver);
        WB.TypeText(textarea, "Prueba Automatizada");
        reporte.AddImageToReport("Se llenan los campos", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "sendBtn");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se envia el codigo", OPG.ScreenShotElement(Driver));
        
        WebDriver nFrame2 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame2);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "popwin_btn_group", "Aceptar");
        
        //Regresamos al iframe del tab
        OPG.SwitchIframe(Driver, ParentPri);
    
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        Codigo = SelenElem.FindElementByID(Driver.WebDriver, "verificationCode_input_value");
        WB.TypeText(Codigo, "12345");
        reporte.AddImageToReport("Se valida el codigo", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "sendBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        WB.ClickById(Driver.WebDriver, "validateBtn");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "popwin_btn_group", "Aceptar");
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        WB.ClickById(Driver.WebDriver, "nextBt");
        //Driver.AssingNewDriver(nFrame3);
        Thread.sleep(3000);
        WB.CargaAjax(Driver.WebDriver);
        nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        Error = WB.FindElementByID(Driver.WebDriver, "popwin_top");
        if(Error== null)
        {
            reporte.AddImageToReport("Se Valida la orden", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "Error en el proceso" );
        }
        Thread.sleep(1000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrdenConFactura(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambio de Tarjeta SIM", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden no tiene Costo" );
                
        }
        
        
                    
    }
    
    public static void CambioDeSimSinCostoPospago(WebDriv Driver,WebAction SelenElem,String DN,String CodigoC,String ICC,String Causa,Report reporte) throws InterruptedException, IOException
    {
        WebElement div,radioBtn,Input,Razon,textarea,Codigo, Error;
        String text,Value="",Folio;
        List<WebElement> childrenElements;
        Boolean Apagar;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        //Driver.WebDriver.findElement(By.id("firstname-placeholder")).sendKeys(Keys.F5);
        //Driver.WebDriver.navigate().refresh();
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,CodigoC);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "BusinessAccept_items", "Manejo de SIM",false);
        Thread.sleep(5000);
        div = SelenElem.FindElementByID(Driver.WebDriver, "resources_0_4");
        text=div.getText();
        if(text.equals("Activada"))
        {
           radioBtn = SelenElem.FindElementByID(Driver.WebDriver, "resources_0_0");
           childrenElements = SelenElem.GetChildren(radioBtn);
           childrenElements.get(0).click();
           SelenElem.CargaAjax(Driver.WebDriver);
           //WB.ClickById(Driver.WebDriver, "simInfomation_titlebar");
           
        }
        else
        {
            reporte.AddImageToReport("SIM Desactivada", OPG.ScreenShotElement(Driver)); 
            System.out.println("SIM no disponible");
            throw new NullPointerException( "Error en el proceso" );
        }
        reporte.AddImageToReport("Selecciona la SIM a Cambiar", OPG.ScreenShotElement(Driver));
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Facturación",false);
        Thread.sleep(3000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        Thread.sleep(1000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "BusinessAccept_items", "Manejo de SIM",false);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Cambio de Tarjeta SIM");
//////////////////////////////////////////////////////////////////////////////////////////////////////////////////
               
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false); 
        Input = WB.FindElementByID(Driver.WebDriver, "newsimno_input_value");              
        Razon=SelenElem.FindElementByID(Driver.WebDriver,"busiReason_input_select");
        if(Razon!=null)
        {
            if(Causa.equals("Dano o Bloqueo"))
                     Value = "07";
            else if (Causa.equals("Robo"))
                Value = "ROE";
        
            WB.SelenSelect(Razon,"Value",Value);
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(2000);
        }
        WB.TypeText(Input, ICC); 
        textarea = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "chgSimContentInfo.remark");
        textarea.click();
        WB.CargaAjax(Driver.WebDriver);
        WB.TypeText(textarea, "Prueba Automatizada");
        reporte.AddImageToReport("Se llenan los campos", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "sendBtn");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se envia el codigo", OPG.ScreenShotElement(Driver));
        
        WebDriver nFrame2 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame2);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "popwin_btn_group", "Aceptar");
        
        //Regresamos al iframe del tab
        OPG.SwitchIframe(Driver, ParentPri);
    
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        Codigo = SelenElem.FindElementByID(Driver.WebDriver, "verificationCode_input_value");
        WB.TypeText(Codigo, "12345");
        reporte.AddImageToReport("Se valida el codigo", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        WB.ClickById(Driver.WebDriver, "sendBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        WB.ClickById(Driver.WebDriver, "validateBtn");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        //se regresa al principal para el popwin
        WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "popwin_btn_group", "Aceptar");
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        WB.ClickById(Driver.WebDriver, "nextBt");
        //Driver.AssingNewDriver(nFrame3);
        Thread.sleep(3000);
        WB.CargaAjax(Driver.WebDriver);
        nFrame3 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame3);
        Error = WB.FindElementByID(Driver.WebDriver, "popwin_top");
        if(Error== null)
        {
            reporte.AddImageToReport("Se Valida la orden", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "Error en el proceso" );
        }
        Thread.sleep(1000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambio de Tarjeta SIM",false);
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrdenConFactura(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambio de Tarjeta SIM", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
        
                    
    }
    
    public static void AgregarNumeroFrecuente(WebDriv Driver,WebAction SelenElem,String DN,String Codigo,String SMS02,Report reporte) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Input,Eletab;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Administración de Servicios","Manejo de Números Gratis/Frecuentes",reporte);
        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Manejo de Números Gratis/Frecuentes",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Administrar Números Frecuentes (FN)",false);
        
        Elem = WB.FindElementByID(Driver.WebDriver, "ffnProductList_0_0_1");
        Div2= WB.GetChildren(Elem);
        Div3= WB.GetChildren(Div2.get(0));
        Div4= WB.GetChildren(Div3.get(0));
        Div5= WB.GetChildren(Div4.get(0));
        Div6= WB.GetChildren(Div5.get(0));
        Div6= WB.GetChildren(Div6.get(0));
        Div6.get(0).click();
        Thread.sleep(2000);
        SelenElem.CargaAjax(Driver.WebDriver);       
                        
        Input = WB.FindElement(Driver.WebDriver, SelenElem, "Name", "ffnOfferList[0].ffnProductList[0].groupFFNList[0].ffnumberListValue.familyNo");
        WB.TypeText(Input, SMS02);
        reporte.AddImageToReport("Se agrega el DN", OPG.ScreenShotElement(Driver));
         
        WB.ClickById(Driver.WebDriver, "groupFFNList_0_0_0_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Eletab = SelenElem.FindElementByID(Driver.WebDriver, "ffnOfferList_0_0");
        Div5 = SelenElem.GetChildren(Eletab);
        Div6 = SelenElem.GetChildren(Div5.get(0));
        Div6.get(0).click();
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver);        
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items", "Manejo de Números Gratis/Frecuentes", reporte);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Administrar Números Frecuentes (FN)",false);
            
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }        

    }
    
    public static void AgregarNumeroFrecuenteCosto(WebDriv Driver,WebAction SelenElem,String DN,String Codigo,String SMS02,Report reporte) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Input,Eletab;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        Thread.sleep(2000);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Administración de Servicios","Manejo de Números Gratis/Frecuentes",reporte);
        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Manejo de Números Gratis/Frecuentes",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Administrar Números Frecuentes (FN)",false);
        
        Elem = WB.FindElementByID(Driver.WebDriver, "ffnProductList_0_2_1");
        Div2= WB.GetChildren(Elem);
        Div3= WB.GetChildren(Div2.get(0));
        Div4= WB.GetChildren(Div3.get(0));
        Div5= WB.GetChildren(Div4.get(0));
        Div6= WB.GetChildren(Div5.get(0));
        Div6= WB.GetChildren(Div6.get(0));
        Div6.get(0).click();
        Thread.sleep(2000);
        SelenElem.CargaAjax(Driver.WebDriver);       
                        
        Input = WB.FindElement(Driver.WebDriver, SelenElem, "Name", "ffnOfferList[0].ffnProductList[2].groupFFNList[0].ffnumberListValue.familyNo");
        WB.TypeText(Input, SMS02);
        reporte.AddImageToReport("Se agrega el DN", OPG.ScreenShotElement(Driver));
        
        WB.ClickById(Driver.WebDriver, "ffnProductList_0_0_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Eletab = SelenElem.FindElementByID(Driver.WebDriver, "ffnOfferList_0_0");
        Div5 = SelenElem.GetChildren(Eletab);
        Div6 = SelenElem.GetChildren(Div5.get(0));
        Div6.get(0).click();
        Thread.sleep(1000);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver);        
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(true))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items", "Manejo de Números Gratis/Frecuentes", reporte);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Administrar Números Frecuentes (FN)",false);
            
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
        }
        else
        {
            throw new NullPointerException( "La orden no tiene Costo" );
                
        }        

    }
    public static void EliminarNumeroFrecuente(WebDriv Driver,WebAction SelenElem,String DN, String Codigo,Report reporte) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Eletab;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Administración de Servicios","Manejo de Números Gratis/Frecuentes",reporte);
        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Manejo de Números Gratis/Frecuentes",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Administrar Números Frecuentes (FN)",false);
        /////
        Elem = WB.FindElementByID(Driver.WebDriver, "groupFFNList_0_0_0_4");
        Div2= WB.GetChildren(Elem);
        Div3= WB.GetChildren(Div2.get(0));
        Div4= WB.GetChildren(Div3.get(0));
        Div4.get(0).click();
        Thread.sleep(2000);
        SelenElem.CargaAjax(Driver.WebDriver);  
        reporte.AddImageToReport("Se Elimina el DN", OPG.ScreenShotElement(Driver));
        
        Eletab = SelenElem.FindElementByID(Driver.WebDriver, "ffnOfferList_0_0");
        Div5 = SelenElem.GetChildren(Eletab);
        Div6 = SelenElem.GetChildren(Div5.get(0));
        Div6.get(0).click();
        
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Siguiente");
        SelenElem.CargaAjax(Driver.WebDriver);        
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        
        //Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        //if(Apagar.equals(false))
        //{
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items", "Manejo de Números Gratis/Frecuentes", reporte);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Administrar Números Frecuentes (FN)",false);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
        /*}
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }      */  

    }
    
    public static void RecargaCAC(WebDriv Driver,WebAction SelenElem, Report reporte, String DN,String Codigo, String Monto) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Input;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Recargas y Ajustes","Recargas",reporte);
        Thread.sleep(4000);        
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Recargas",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Input= SelenElem.FindElementByID(Driver.WebDriver, "msisdn_input_value");
        SelenElem.TypeText(Input, DN);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar");
        SelenElem.CargaAjax(Driver.WebDriver);        
        
        Elem= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "amout");
        SelenElem.SelenSelect(Elem, "Value", Monto);  
        reporte.AddImageToReport("Se ingresan datos de la recarga", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_ui_ele", "Enviar");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        WebDriver nFrame2 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame2);
        Thread.sleep(2000);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Recargas",false);
        
        Elem= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "busiPaymentMethodListOnixCtz[0].rechargeMethod");
        SelenElem.SelenSelect(Elem, "Text", "Efectivo");
        SelenElem.ClickById(Driver.WebDriver, "attrId_0");
        reporte.AddImageToReport("Se selecciona la forma de pago", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Pago");
        
        nFrame2 = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame2);
        WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Recargas",false);
        reporte.AddImageToReport("Se envia la recarga", OPG.ScreenShotElement(Driver));
        
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        if(Folio!=null)
        {      
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver)); 


            //Se valida el registro de la recarga 
            nFrame2 = Driver.WebDriver.switchTo().defaultContent();
            Driver.AssingNewDriver(nFrame2);
            String ParentPri = OPG.Busqueda360(Driver,SelenElem,"DN",DN,"");        
            reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));

            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Órdenes de Cliente",false);
            Thread.sleep(2000);
            reporte.AddImageToReport("Se muestra la orden", OPG.ScreenShotElement(Driver)); 
        }
        else
        {
            throw new NullPointerException( "Error" );
        }
        
    }
    
    public static void IncrementoDN(WebDriv Driver,WebAction SelenElem, Report reporte, String DN,String TipAjuste, String Monto, String Comentario, String TipRegresiva) throws InterruptedException, IOException
    {
        
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebDriver nFrame;
        WebElement Elem,Input,Tabla,BTN;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Recargas y Ajustes","Ajuste",reporte);        
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Ajuste",true);
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);
        SelenElem.EsperaElem(Driver.WebDriver, "msisdn_input_value");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Input= SelenElem.FindElementByID(Driver.WebDriver, "msisdn_input_value");
        SelenElem.TypeText(Input, DN);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar"); //Se busca el DN
        SelenElem.CargaAjax(Driver.WebDriver);       
        
        if(TipRegresiva.contentEquals("Incremento"))
        {
            Elem= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "adjustDirection");//Se llenan las opciones 
            SelenElem.SelenSelect(Elem, "Value", "I");
            SelenElem.CargaAjax(Driver.WebDriver); 
            Elem= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "adjustType");//Se llenan las opciones 
            SelenElem.SelenSelect(Elem, "Value", TipAjuste);
            SelenElem.CargaAjax(Driver.WebDriver); 
            Input= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "adjustAmount_disp");
            SelenElem.TypeText(Input, Monto);
            Input= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "remark");
            SelenElem.TypeText(Input, Comentario);
            reporte.AddImageToReport("Se llenan los campos y se envia la orden", OPG.ScreenShotElement(Driver));
        }
        else if(TipRegresiva.contentEquals("Decremento"))
        {
            Elem= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "adjustDirection");//Se llenan las opciones 
            SelenElem.SelenSelect(Elem, "Value", "D");
            SelenElem.CargaAjax(Driver.WebDriver); 
            Elem= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "adjustType");//Se llenan las opciones 
            SelenElem.SelenSelect(Elem, "Value", TipAjuste);
            SelenElem.CargaAjax(Driver.WebDriver); 
            Input= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "adjustAmount_disp");
            SelenElem.TypeText(Input, Monto);
            Input= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "remark");
            SelenElem.TypeText(Input, Comentario);
            reporte.AddImageToReport("Se llenan los campos", OPG.ScreenShotElement(Driver));
            
        }
        
        
        SelenElem.ClickById(Driver.WebDriver, "submitButton");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        
        nFrame = Driver.WebDriver.switchTo().defaultContent();//Cambiamos al padre
        Driver.AssingNewDriver(nFrame);        
        //WB.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        reporte.AddImageToReport("Se envia la orden", OPG.ScreenShotElement(Driver));
        BTN = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        SelenElem.clickJs(Driver.WebDriver, BTN);
        SelenElem.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Ajuste",false);
        Thread.sleep(4000); //para onix, que pase la orden
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar"); //Se busca el DN
        SelenElem.CargaAjax(Driver.WebDriver);
        Tabla= SelenElem.FindElementByID(Driver.WebDriver, "balanceResultList_body");
        reporte.AddImageToReport("Se muestra el Saldo", OPG.ScreenShotElement2(Tabla));
        
    
    }
    
    
    public static void CambioDN(WebDriv Driver,WebAction SelenElem, Report reporte, String DN,String Codigo, String Comentario) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebDriver nFrame;
        WebElement Elem,Input,Tabla,BTN,NewDN;
        List <WebElement> DivDN;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Cambiar numero de DN",reporte);        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar numero de DN",false);
        
        SelenElem.ClickById(Driver.WebDriver, "selectBtn");
        Thread.sleep(1000);
        SelenElem.CargaAjax(Driver.WebDriver); 
        reporte.AddImageToReport("Se busca el nuevo DN", OPG.ScreenShotElement(Driver));        
        
        //Se llenan los campos para la busqueda del DN
        OPG.NavegaPop(Driver);
        Elem= SelenElem.FindElementByID(Driver.WebDriver, "searcharea_input_select");//Se llenan las opciones 
        SelenElem.SelenSelect(Elem, "Value", "DF");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Elem= SelenElem.FindElementByID(Driver.WebDriver, "searchctiy_input_select");//Se llenan las opciones 
        SelenElem.SelenSelect(Elem, "Value", "Distrito Federal");
        SelenElem.CargaAjax(Driver.WebDriver);
        Elem= SelenElem.FindElementByID(Driver.WebDriver, "searchnir_input_select");//Se llenan las opciones 
        SelenElem.SelenSelect(Elem, "Value", "55");
        SelenElem.CargaAjax(Driver.WebDriver);               
        reporte.AddImageToReport("Se busca el nuevo DN", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Id", "searchButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //Se selecciona el nuevo DN
        Actions actions = new Actions(Driver.WebDriver);
        NewDN = SelenElem.FindElementByID(Driver.WebDriver, "msisdnInfoList1_0_0");
        DivDN = SelenElem.GetChildren(NewDN);
        actions.doubleClick(DivDN.get(0)).perform();
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar numero de DN",false);
        Input = SelenElem.FindElementByID(Driver.WebDriver, "remark_input_value");
        SelenElem.TypeText(Input, Comentario);
        
        reporte.AddImageToReport("Se completan los datos", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "nextBtn");
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar numero de DN",false);
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar numero de DN", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
    
    }
        
    public static void ComplementoEmail(WebDriv Driver,WebAction SelenElem, Report reporte, String No_Orden) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebDriver nFrame;
        WebElement Elem,Correo,Tabla,BTN,NewDN;
        List <WebElement> Div,Div2;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Comercio de Negocio","Consulta Complemento de pago",reporte);        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Consulta Complemento de pago",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        
        Elem= SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "conds.orderNo");//Se llenan las opciones 
        SelenElem.TypeText(Elem, No_Orden);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se muestra el resultado de la busqueda", OPG.ScreenShotElement(Driver)); 
        
        Correo= SelenElem.FindElementByID(Driver.WebDriver, "resultArea_0_14");//Se llenan las opciones 
        if(Correo!=null)
        {
            Div = SelenElem.GetChildren(Correo);
            Div2 = SelenElem.GetChildren(Div.get(0));
            Div2.get(0).click();
            SelenElem.CargaAjax(Driver.WebDriver);
            reporte.AddImageToReport("Se envia el correo", OPG.ScreenShotElement(Driver)); 
            
            OPG.NavegaPop(Driver);
            SelenElem.ClickById(Driver.WebDriver, "sendBtn");
            reporte.AddImageToReport("El correo fue enviado", OPG.ScreenShotElement(Driver));
        }
        else
        {
            reporte.AddImageToReport("No se encontro nunguna orden", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "Intente con otro numero de orden" );
        }     
           
    }
    
    public static void MovRefresh(WebDriv Driver,WebAction SelenElem, Report reporte,String DN,String Codigo,String IMEI_Act,String IMEI_New,String Equipo,String Plan) throws InterruptedException, IOException
    {
        
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebDriver nFrame;
        WebElement Input,Imeii,Tabla,BTN,NewDN;
        List <WebElement> Div,Div2,Input1;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Movistar Refresh",reporte); 
        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Movistar Refresh",false);
        
        //Buro
        Input1 = SelenElem.FindElements(Driver.WebDriver, "Name","#BMEAttr.currentValidatorAction.paramMap.authorization");
        Input1.get(1).click();
        SelenElem.ClickById(Driver.WebDriver, "ownerofCard_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "creditofHouse_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "creditofCar_input_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        SelenElem.ClickById(Driver.WebDriver, "confirmBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000); 
        
        Input = SelenElem.FindElementByID(Driver.WebDriver, "folio_input_value");
        SelenElem.TypeText(Input, "123456");
        Imeii = SelenElem.FindElementByID(Driver.WebDriver, "folio_input_value");
        SelenElem.TypeText(Imeii, IMEI_New);
        SelenElem.ClickById(Driver.WebDriver, "checkHandsetBoxId_input_0");
        
        
        
                
    }

        public static void Payment(WebDriv Driver,WebAction SelenElem, Report reporte,String DN) throws InterruptedException, IOException
    {
        
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebDriver nFrame;
        WebElement Elem,Correo,Tabla,BTN,NewDN;
        List <WebElement> Div,Div2;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Gestión de Pago","Pago",reporte);        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Pago",true);
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Thread.sleep(2000);
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "msisdn_condition_input_value");
        SelenElem.TypeText(Elem, DN);
        reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "queryUserInfoButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        nFrame = Driver.WebDriver.switchTo().frame("popwin0"); 
        Driver.AssingNewDriver(nFrame);
        SelenElem.ClickById(Driver.WebDriver, "acctSelectShowList_0_0");
        reporte.AddImageToReport("Seleccionamos la cuenta", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "selectAcctButton");
        //SelenElem.ClickById(Driver.WebDriver, "selectSubsButton");
        //SelenElem.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Pago",true);
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "paymentMethodList[0].paymentMethodId");
        SelenElem.SelenSelect(Elem, "Value", "1001");
        reporte.AddImageToReport("Se procede al Pago", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "submitButton");
        
        //OPG.SwitchIframe(Driver,"popwin1");
        nFrame = Driver.WebDriver.switchTo().frame("popwin1");  
        Driver.AssingNewDriver(nFrame);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "button_1492087964974_32626");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        OPG.IframeActual(Driver);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Pago",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Imprimir");
        Thread.sleep(10000);
        reporte.AddImageToReport("Se muestra el recibo", OPG.ScreenShotAll());

                
    }
        
    public static void PagoDeposito(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Importe) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,DivCod,Div;
        String CodCli;
        WebDriver nFrame;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Gestión de Depósito","Pago de Depósito",reporte);        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Pago de Depósito",true);
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Thread.sleep(2000);
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "msisdn_condition_input_value");
        SelenElem.TypeText(Elem, DN);
        reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "queryUserInfoButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        nFrame = Driver.WebDriver.switchTo().frame("popwin0"); 
        Driver.AssingNewDriver(nFrame);
        SelenElem.ClickById(Driver.WebDriver, "acctSelectShowList_0_0");
        reporte.AddImageToReport("Seleccionamos la cuenta", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "selectAcctButton");
        Thread.sleep(3000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Pago de Depósito",true);
        DivCod = SelenElem.FindElementByID(Driver.WebDriver, "field_1492087745054_74666_acctCode_input");
        CodCli = DivCod.getText();
        reporte.AddImageToReport("Obtenemos el Codigo de la cuenta"+CodCli, OPG.ScreenShotElement(Driver));
        
        Div = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "paymentMethodList[0].displayPaymentAmt");
        Div.clear();
        SelenElem.TypeText(Div, Importe);

        
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "reasonCategroy");
        SelenElem.SelenSelect(Elem, "Value", "Deposit Payment");
        reporte.AddImageToReport("Se procede al Deposito", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "submitButton");
        Thread.sleep(2000);
        //OPG.SwitchIframe(Driver,"popwin1");
        nFrame = Driver.WebDriver.switchTo().frame("popwin1");  
        Driver.AssingNewDriver(nFrame);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "confirmSubmitBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        OPG.IframeActual(Driver);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Pago de Depósito",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "popwin_close");
        
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Consultar","Registros de transacciones de los clientes",reporte);        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Registros de transacciones de los clientes",true);
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        
        Thread.sleep(2000);
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "acctCodeCondition_input_value");
        SelenElem.TypeText(Elem, CodCli);
        SelenElem.ClickById(Driver.WebDriver, "queryUserInfoButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
                
    }
    
    public static void LiberacionDeposito(WebDriv Driver, WebAction SelenElem, Report reporte, String DN) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,DivCod,Div,BTN;
        String CodCli;
        WebDriver nFrame;
        List<WebElement> Child,Img2,Img;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Gestión de Depósito","Procesamiento del depósito",reporte);        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Procesamiento del depósito",true);
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Thread.sleep(2000);
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "msisdn_condition_input_value");
        SelenElem.TypeText(Elem, DN);
        reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "queryUserInfoButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        nFrame = Driver.WebDriver.switchTo().frame("popwin0"); 
        Driver.AssingNewDriver(nFrame);
        SelenElem.ClickById(Driver.WebDriver, "acctSelectShowList_0_0");
        reporte.AddImageToReport("Seleccionamos la cuenta", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "selectAcctButton");
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Procesamiento del depósito",true);
        DivCod = SelenElem.FindElementByID(Driver.WebDriver, "field_1492087745054_74666_acctCode_input");
        CodCli = DivCod.getText();
        reporte.AddImageToReport("Obtenemos el Codigo de la cuenta "+CodCli, OPG.ScreenShotElement(Driver));
        
        Div = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "depositIOList_0_8");
        Child = SelenElem.GetChildren(Div);
        Img = SelenElem.GetChildren(Child.get(0));
        Img2 = SelenElem.GetChildren(Img.get(0));
        Thread.sleep(2000);
        Img2.get(3).click();
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Id", "radio_1492087878150_86394_1");
         Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "reasonCategroy");
        SelenElem.SelenSelect(Elem, "Text", "DepositReq");        
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "reasonCode");
        SelenElem.SelenSelect(Elem, "Value", "DEL38");
        reporte.AddImageToReport("Se ingresa la información para la Liberación del Depósito", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "submitButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        BTN = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem,"ClassName", "bc_btn", "Sí");
        if(BTN.equals(null))
        {
            throw new NullPointerException( "Fallo" );
        }
        
        reporte.AddImageToReport("Se confirma la operacion", OPG.ScreenShotElement(Driver));
        Thread.sleep(1000);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Sí");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se confirma la operacion", OPG.ScreenShotElement(Driver));
        OPG.IframeActual(Driver);
        Thread.sleep(1000);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "OK");
        
        
        
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Consultar","Registros de transacciones de los clientes",reporte);        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Registros de transacciones de los clientes",true);
        Driver.WebDriver.manage().timeouts().pageLoadTimeout(100, SECONDS);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        
        Thread.sleep(2000);
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "acctCodeCondition_input_value");
        SelenElem.TypeText(Elem, CodCli);
        SelenElem.ClickById(Driver.WebDriver, "queryUserInfoButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
    }
    
    public static void  AbrirCaja(WebDriv Driver,WebAction SelenElem,Report reporte, String User,String Contra,String Monto) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,BTN;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Módulo Cajero","Abrir Caja",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Abrir Caja",true);
        Thread.sleep(3000);    
        
        SelenElem.WaitForElem(Driver.WebDriver, "commitCashRegisterButton");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "inputCurrency_value");
        Elem.clear();
        SelenElem.TypeText(Elem, Monto);
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","cashRegIO.superOperNo");
        SelenElem.TypeText(Elem, User);
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","cashRegIO.superOperPassword");
        SelenElem.TypeText(Elem, Contra);
        reporte.AddImageToReport("Se llenan los datos", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "commitCashRegisterButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        reporte.AddImageToReport("Se confirma", OPG.ScreenShotElement(Driver));
        //OPG.IframeActual(Driver);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Sí");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        reporte.AddImageToReport("Se confirma", OPG.ScreenShotElement(Driver));
        BTN = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "OK");
        Thread.sleep(1000);
        if(BTN.equals(null))
        {
            reporte.AddImageToReport("Fallo la apertura de caja", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "Intente nuevamente" );
        }
        Div2 = SelenElem.FindElements(Driver.WebDriver, "ClassName", "popwin_error");
        if(Div2.equals(null))
        {            
        }
        else
        {
            //reporte.AddImageToReport("Fallo la apertura de caja", OPG.ScreenShotElement(Driver));
            //throw new NullPointerException( "Intente nuevamente" );
        }
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "OK");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        
    }
    
    public static void  CierreCaja(WebDriv Driver,WebAction SelenElem,Report reporte, String User,String Contra) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,Input,Cash,BTN;
        List <WebElement> Div2, Div3, Div4, Div5,Div6,Div7,Div8,Div9;
        Boolean Apagar;
        String Monto,V;
        int Total;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Módulo Cajero","Cerrar Caja",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Cerrar Caja",true);
        Thread.sleep(2000);
        
        
        SelenElem.WaitForElem(Driver.WebDriver, "cashRegPrintButton");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","cashRegIO.superOperNo");
        SelenElem.TypeText(Elem, User);
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","cashRegIO.superOperPassword");
        SelenElem.TypeText(Elem, Contra);
        reporte.AddImageToReport("Se llenan los datos", OPG.ScreenShotElement(Driver));
        
        //se desglosa la caja        
        SelenElem.ClickById(Driver.WebDriver, "cashRegDenoButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        OPG.BuscaPOP2(Driver);
        Cash = SelenElem.FindElementByID(Driver.WebDriver, "basicInfoField_PIASTER_PCS");
        if(Cash==null)
        {
            reporte.AddImageToReport("No se encontro el desgloce de la cuenta", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "Intente nuevamente" );
        }
        else
        {
            //se llega al monto
            Div2 = SelenElem.GetChildren(Cash);
            Div3 = SelenElem.GetChildren(Div2.get(0));
            Div4 = SelenElem.GetChildren(Div3.get(1));
            Div5 = SelenElem.GetChildren(Div4.get(2));
            Div6 = SelenElem.GetChildren(Div5.get(0));
            Div7 = SelenElem.GetChildren(Div6.get(0));
            Div8 = SelenElem.GetChildren(Div7.get(2));
            Monto = Div8.get(0).getText();
            
            int indice = 3 > Monto.length() ? 0 : Monto.length() - 3;
            V = Monto.substring(0, indice);
            V = V.replaceAll(",", "");
            Input = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "piasterOfPCS.coinsList[3].valueTotalAmt");
            SelenElem.TypeText(Input, V);
            reporte.AddImageToReport("Se cubre el monto en Caja", OPG.ScreenShotElement(Driver)); 
            SelenElem.ClickById(Driver.WebDriver, "submit");
            SelenElem.CargaAjax(Driver.WebDriver);
            
        }
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Cerrar Caja",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "OK");
        SelenElem.ClickById(Driver.WebDriver, "cashRegPrintButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Sí");
        SelenElem.CargaAjax(Driver.WebDriver);
        //ESC
        Thread.sleep(15000);
        //OPG.IframeActual(Driver);
        Actions action = new Actions(Driver.WebDriver);
	//action.sendKeys(Keys.ESCAPE);
        //Driver.WebDriver.switchTo().window("parent");
        Robot robot = new Robot();
        robot.keyPress(KeyEvent.VK_ESCAPE);
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "popwin_close");
        SelenElem.CargaAjax(Driver.WebDriver);        
        //Se cierra la caja
        SelenElem.ClickById(Driver.WebDriver, "cashRegCloseButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        BTN = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "OK");
        if(BTN.equals(null))
        {
            reporte.AddImageToReport("Fallo la apertura de caja", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "Intente nuevamente" );
        }
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "OK");
        
        
    }
    
    public static void AgregaListaRoja(WebDriv Driver,WebAction SelenElem,Report reporte, String RFC) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,Input,Select,Table,Div;
        List <WebElement> Chil, Div3, Div4, Div5,Div6,Div7,Div8,Div2,Div9;
        String CodCli,Res;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        //Se obtiene el codigo de Cliente
        OperacionesGenerales.Busqueda360(Driver,SelenElem,"RFC",RFC,"");
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cliente",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custAccept_items", "Información de Cliente",false);
        Div = SelenElem.FindElementByID(Driver.WebDriver, "generalCustInfo_content");
        Div2 = SelenElem.GetChildren(Div);
        Div3 = SelenElem.GetChildren(Div2.get(0));
        Div4 = SelenElem.GetChildren(Div3.get(0));
        Div5 = SelenElem.GetChildren(Div4.get(1));
        Div6 = SelenElem.GetChildren(Div5.get(8));
        Div7 = SelenElem.GetChildren(Div6.get(0));
        Div8 = SelenElem.GetChildren(Div7.get(0));
        Div9 = SelenElem.GetChildren(Div8.get(2));
        CodCli = Div9.get(0).getText();
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
        Thread.sleep(2000);
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Lista de administración","Gestionar Lista Roja",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Gestionar Lista Roja",true);
        OPG.NavegaIframe(Driver,"listTypeId_0_iframe");
        OPG.NavegaIframe(Driver,"subtypeTab_2_iframe");
        OPG.NavegaIframe(Driver,"fromItemId_iframe");        
        
        Select = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "conditionValue.status");
        SelenElem.SelenSelect(Select, "Value", "1");
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        SelenElem.TypeText(Elem, RFC);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se busca el RFC para confirmar que no este en lista Roja", OPG.ScreenShotElement(Driver));
        Table = SelenElem.FindElementByID(Driver.WebDriver, "blacklistInfoResult_databody");
        Chil = SelenElem.GetChildren(Table);
        Res = Chil.get(0).getText();
        if(Res.equals("No hay registros"))
        {
            SelenElem.ClickById(Driver.WebDriver, "add");
            Thread.sleep(2000);
            OPG.BuscaPOP(Driver);
            Input = SelenElem.FindElementByID(Driver.WebDriver, "textfield_id_0_input_value");
            SelenElem.TypeText(Input, RFC);
            Input = SelenElem.FindElementByID(Driver.WebDriver, "textfield_id_1_input_value");
            SelenElem.TypeText(Input, CodCli);
            reporte.AddImageToReport("Se agrega el RFC a lista roja", OPG.ScreenShotElement(Driver));
            SelenElem.ClickById(Driver.WebDriver, "addSaveButton_2");
            SelenElem.CargaAjax(Driver.WebDriver);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Gestionar Lista Roja",true);
            SelenElem.CargaAjax(Driver.WebDriver);
            reporte.AddImageToReport("Se agrega el RFC a lista roja", OPG.ScreenShotElement(Driver));
        }
        else
        {
            reporte.AddImageToReport("El RFC ya se encuentra en Lista Roja", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "Intente nuevamente" );
        }
    }
    
    public static void EliminarListaRoja(WebDriv Driver,WebAction SelenElem,Report reporte, String RFC) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,Input,Select,Table,Div,Elem2;
        List <WebElement> Chil, Div3, Div4, Div5,Div6,Div7,Div8,Div2,Div9,Checkbox;
        String CodCli,Res;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        //Se obtiene el codigo de Cliente
        OperacionesGenerales.Busqueda360(Driver,SelenElem,"RFC",RFC,"");
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cliente",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custAccept_items", "Información de Cliente",false);
        Div = SelenElem.FindElementByID(Driver.WebDriver, "generalCustInfo_content");
        Div2 = SelenElem.GetChildren(Div);
        Div3 = SelenElem.GetChildren(Div2.get(0));
        Div4 = SelenElem.GetChildren(Div3.get(0));
        Div5 = SelenElem.GetChildren(Div4.get(1));
        Div6 = SelenElem.GetChildren(Div5.get(8));
        Div7 = SelenElem.GetChildren(Div6.get(0));
        Div8 = SelenElem.GetChildren(Div7.get(0));
        Div9 = SelenElem.GetChildren(Div8.get(2));
        CodCli = Div9.get(0).getText();
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
        Thread.sleep(2000);
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Lista de administración","Gestionar Lista Roja",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Gestionar Lista Roja",true);
        OPG.NavegaIframe(Driver,"listTypeId_0_iframe");
        OPG.NavegaIframe(Driver,"subtypeTab_2_iframe");
        OPG.NavegaIframe(Driver,"fromItemId_iframe");
        
        
        Select = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "conditionValue.status");
        SelenElem.SelenSelect(Select, "Value", "1");
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEModel.mgrlistBigValue.queryItemAttrValues[0].attrValue");
        SelenElem.TypeText(Elem, RFC);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Buscar");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se busca el RFC para confirmar que no este en lista Roja", OPG.ScreenShotElement(Driver));
        Table = SelenElem.FindElementByID(Driver.WebDriver, "blacklistInfoResult_databody");
        Chil = SelenElem.GetChildren(Table);
        Res = Chil.get(0).getText();
        if(Res.equals("No hay registros"))
        {
            
            reporte.AddImageToReport("El RFC no se encuentra en Lista Roja", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "" );
        }
        else
        {
            Elem2 = SelenElem.FindElementByID(Driver.WebDriver, "blacklistInfoResult_0_0");
            Checkbox = SelenElem.GetChildren(Elem2);
            Checkbox.get(0).click();
            SelenElem.CargaAjax(Driver.WebDriver);
            reporte.AddImageToReport("Se elige el RFC", OPG.ScreenShotElement(Driver));
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_toolbaritem", "Eliminar");
            SelenElem.CargaAjax(Driver.WebDriver);
            Thread.sleep(2000);
            OPG.BuscaPOP(Driver);
            
            Input = SelenElem.FindElementByID(Driver.WebDriver, "deleteReasonCode_input_select");
            SelenElem.SelenSelect(Input, "Value", "1");
            reporte.AddImageToReport("Se elimina el RFC a lista roja", OPG.ScreenShotElement(Driver));
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Eliminar");
            SelenElem.CargaAjax(Driver.WebDriver);
            Thread.sleep(2000);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Gestionar Lista Roja",true);
            SelenElem.CargaAjax(Driver.WebDriver);
            reporte.AddImageToReport("Se elimino el RFC de lista roja", OPG.ScreenShotElement(Driver));
        }
    }
    
    public static void ConsultaInfoCliente(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Div;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        //Se obtiene el codigo de Cliente
        OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cliente",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custAccept_items", "Información de Cliente",false);
        Div = SelenElem.FindElementByID(Driver.WebDriver, "generalCustInfo_content");
        reporte.AddImageToReport("Se muestra la informacion del Cliente", OPG.ScreenShotElement2(Div)); 
    }
    
    public static void ConsultaInfoCuenta(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Div;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        //Se obtiene el codigo de Cliente
        OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cuenta",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Información de Cuenta",false);
        Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra la informacion de la Cuenta", OPG.ScreenShotElement(Driver));
        Div = SelenElem.FindElementByID(Driver.WebDriver, "acctInformation");
        reporte.AddImageToReport("Se muestra la informacion de la Cuenta", OPG.ScreenShotElement2(Div)); 
        Div = SelenElem.FindElementByID(Driver.WebDriver, "otherAddress");
        reporte.AddImageToReport("Se muestra la informacion de la Cuenta", OPG.ScreenShotElement2(Div)); 
        reporte.AddImageToReport("Se muestra la informacion de la Cuenta", OPG.ScreenShotElement(Driver));         
    }
    
    public static void ConsultaInfoSuscriptor(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Div;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        //Se obtiene el codigo de Cliente
        OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra la Información de Suscriptor", OPG.ScreenShotElement(Driver));
        
    }
    
    public static void ConsultaSaldo(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Div;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        //Se obtiene el codigo de Cliente
        OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Facturación",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Saldo",false);
        Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra el saldo de la Cuenta", OPG.ScreenShotElement(Driver));        
    }
    
     public static void CambiarInfoCuenta(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Div,Elem;
        String Text,ParentPri;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        //Se obtiene el codigo de Cliente
        ParentPri =OPG.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cuenta",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Información de Cuenta",false);
        Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra la informacion de la Cuenta", OPG.ScreenShotElement(Driver));
        Elem = SelenElem.FindElementByID(Driver.WebDriver, "chgAcctBtn");
        SelenElem.clickJs(Driver.WebDriver, Elem);
        Thread.sleep(3000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Div = SelenElem.FindElementByID(Driver.WebDriver, "editor_value");
        //Text = SelenElem.GetText(Driver.WebDriver, "Id", "editor_value");
        if(Div!=null)
        {
            reporte.AddImageToReport("Ordenes Pendientes", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "EL DN NO CUENTA CON ORDENES PENDIENTES" );
        }
        
              
    }
    
    
            
    public static void ObtenOrden(WebDriv Driver,WebAction SelenElem,String DN,Report reporte) throws InterruptedException, IOException
    {
        WebElement div,radioBtn,Input,Razon,textarea,Codigo, Error;
        String text,Value="",Folio;
        List<WebElement> childrenElements;
        Boolean Apagar;
    
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        //Driver.WebDriver.findElement(By.id("firstname-placeholder")).sendKeys(Keys.F5);
        //Driver.WebDriver.navigate().refresh();
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,"");
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        Thread.sleep(2000);
        OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Información de Cuenta", reporte);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver)); 
                       
    }
    
    public static void  IncrementoBalanceCaja(WebDriv Driver,WebAction SelenElem,Report reporte, String User,String Contra,String Monto) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,Input,Cash;
        List <WebElement> Div2, Div3, Div4, Div5,Div6,Div7,Div8,Div9;
        Boolean Apagar;
        String V;
        int Total;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Módulo Cajero","Ajustar Saldo de Caja",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Ajustar Saldo de Caja",true);
        
        SelenElem.ClickButton(Driver.WebDriver, SelenElem, "Name", "transMethodSelected");
        SelenElem.CargaAjax(Driver.WebDriver);
        Input = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "cashRegisterDetailIOList[0].displayAmount");
        SelenElem.TypeText(Input, Monto+".00");
        reporte.AddImageToReport("Se llenan los datos", OPG.ScreenShotElement(Driver));
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","cashRegIO.superOperNo");
        SelenElem.TypeText(Elem, User);
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","cashRegIO.superOperPassword");
        SelenElem.TypeText(Elem, Contra);
        reporte.AddImageToReport("Se llenan los datos", OPG.ScreenShotElement(Driver));
        
        //SE envia
        SelenElem.ClickById(Driver.WebDriver, "commitCashRegisterButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "OK"); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
       
    }
    
    public static void  DecrementoBalanceCaja(WebDriv Driver,WebAction SelenElem,Report reporte, String User,String Contra,String Monto) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,Input,Cash;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Módulo Cajero","Ajustar Saldo de Caja",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Ajustar Saldo de Caja",true);
        
        SelenElem.ClickById(Driver.WebDriver, "column_1492087615552_88687_1");
        SelenElem.CargaAjax(Driver.WebDriver);
        Input = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "cashRegisterDetailIOList[1].displayAmount");
        SelenElem.TypeText(Input, Monto+".00");
        reporte.AddImageToReport("Se llenan los datos", OPG.ScreenShotElement(Driver));
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","cashRegIO.superOperNo");
        SelenElem.TypeText(Elem, User);
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","cashRegIO.superOperPassword");
        SelenElem.TypeText(Elem, Contra);
        reporte.AddImageToReport("Se llenan los datos", OPG.ScreenShotElement(Driver));
        
        //SE envia
        SelenElem.ClickById(Driver.WebDriver, "commitCashRegisterButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "OK"); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
       
    }
    
    public static void  ReporteDiario(WebDriv Driver,WebAction SelenElem,Report reporte, String Fecha) throws InterruptedException, IOException, AWTException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,Input;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Cuentas por Cobrar","Módulo Cajero","Reporte Diario",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Reporte Diario",true);
        Thread.sleep(2000);
        Input = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "closeDate_input_value");
        SelenElem.TypeText(Input, Fecha);
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","society");
        SelenElem.SelenSelect(Elem, "Value","1");
        Elem = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name","currencyId");
        SelenElem.SelenSelect(Elem, "Value","1103");
        reporte.AddImageToReport("Se ingresan los valores de busqueda", OPG.ScreenShotElement(Driver));
        
        SelenElem.ClickById(Driver.WebDriver, "queryBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se muestran los resultados de la busqueda", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        SelenElem.ClickById(Driver.WebDriver, "printButton");
        SelenElem.CargaAjax(Driver.WebDriver);
        //OPG.IframeActual(Driver);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Sí");
        Thread.sleep(2000);
        
        OPG.CambiaVentana(Driver.WebDriver);
        Driver.WebDriver.manage().window().maximize();
        reporte.AddImageToReport("Se muestra el Reporte", OPG.ScreenShotElement(Driver));
        Driver.WebDriver.findElement(By.cssSelector("body")).sendKeys(Keys.CONTROL, Keys.END);
        Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra el Reporte", OPG.ScreenShotElement(Driver));
       
            
        
        
       
    }
    
    
    


}
