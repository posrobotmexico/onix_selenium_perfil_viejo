/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class ConsolidacionCesiondeDerechos {
    
    public static void ConsolidacionPosp(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod,String Cuenta, String Comentario) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement CodNuevCuenta,DivComentario;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Consolidacion y Desconsolidacion de Cuenta",reporte);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"DN",DN,Cod);
        Thread.sleep(1000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Consolidacion y Desconsolidacion de Cuenta",false);
        reporte.AddImageToReport("Se procede a busca la cuenta", OPG.ScreenShotElement(Driver));
        CodNuevCuenta = WB.FindElement(Driver.WebDriver, WB, "Name", "newAcctCode");
        WB.SelenSelect(CodNuevCuenta, "Value", Cuenta);
        DivComentario = WB.FindElement(Driver.WebDriver, WB, "Name", "remarks");
        WB.TypeText(DivComentario, Comentario);
        reporte.AddImageToReport("Se Selecciona la cuenta", OPG.ScreenShotElement(Driver));
        
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        try {
            //se regresa al principal para el popwin
            WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
            Driver.AssingNewDriver(nFrame3);
            reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(1000);
        } catch (Exception e) {
        }
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Consolidacion y Desconsolidacion de Cuenta",false);
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.EnviarOrden(Driver,WB,ParentPri,"custTabPanel_items","Consolidacion y Desconsolidacion de Cuenta", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
    }
    
    public static void DesonsolidacionHib(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod, String Comentario,String Ciclo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Divnum,DivComentario,SelecCiclo;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Consolidacion y Desconsolidacion de Cuenta",reporte);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"DN",DN,Cod);
        Thread.sleep(1000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Consolidacion y Desconsolidacion de Cuenta",false);
        DivComentario = WB.FindElement(Driver.WebDriver, WB, "Name", "remarks");
        WB.TypeText(DivComentario, Comentario);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        WB.ClickById(Driver.WebDriver, "newBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        //Clicktabs
        WB.ClickById(Driver.WebDriver, "set_1001_titlebar");
        WB.ClickById(Driver.WebDriver, "billMedium_titlebar");
 
        OPG.LlenaDatosCliNuevVenta(Driver, WB, reporte);
        Divnum = WB.FindElementByID(Driver.WebDriver, "field_1000_1119_input_value");
        WB.TypeText(Divnum, "1234567899");
        SelecCiclo = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.tempAcctData.paramAccount.acctInfo.billCycleType");
        WB.SelenSelect(SelecCiclo, "Text", Ciclo);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        WB.ClickById(Driver.WebDriver, "set_1000_titlebar");
        WB.ClickById(Driver.WebDriver, "sameWithAcctFlag_0");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));        
        
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        try {
            //se regresa al principal para el popwin
            WebDriver nFrame3 = Driver.WebDriver.switchTo().defaultContent();
            Driver.AssingNewDriver(nFrame3);
            reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(1000);
        } catch (Exception e) {
        }
        WebDriver nFrame = Driver.WebDriver.switchTo().frame(ParentPri);               
        Driver.AssingNewDriver(nFrame);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Consolidacion y Desconsolidacion de Cuenta",false);
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.EnviarOrden(Driver,WB,ParentPri,"custTabPanel_items","Consolidacion y Desconsolidacion de Cuenta", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
    }
    
    public static void CesionDerechos(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod, String DNCesion,String Ciclo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Click,Tabla,SelecDN,InputDN;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cesión de Derechos",reporte);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"DN",DN,Cod);
        Thread.sleep(1000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cesión de Derechos",false);
        Thread.sleep(3000);
        WB.ClickById(Driver.WebDriver, "searchCustCheckbox_0");
        OPG.NavegaPop(Driver);
        SelecDN = WB.FindElementByID(Driver.WebDriver, "searchBy_input_select");
        WB.SelenSelect(SelecDN, "Value", "byServiceNumber");
        InputDN = WB.FindElementByID(Driver.WebDriver, "searchFor_input_value");
        WB.TypeText(InputDN, DNCesion);
        reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        try {
            Tabla = WB.FindElementByID(Driver.WebDriver, "custsDatagrid_databody");
            Click = WB.FindElementByID(Driver.WebDriver, "custsDatagrid_1_0");
            WB.DobleClick(Driver.WebDriver, Click);
            WB.CargaAjax(Driver.WebDriver);
        } catch (Exception e) {
        }
        Thread.sleep(3000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cesión de Derechos",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "submitCustInfo");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        //Se pasa el buro
        OPG.PasaBuro(Driver, WB, reporte); 
        //Error buro onix
        WebDriver nFrame = null;
        nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        try {
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar"); //popwin_btn_group 
            OPG.SwitchIframe(Driver, ParentPri);
            OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cesión de Derechos",false);
            WB.ClickById(Driver.WebDriver, "continue");
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(2000);
        } catch (Exception e) {
            
        }
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cesión de Derechos",false);   
        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.EnviarOrdenContraDigitalVentanaWeb(Driver,WB,ParentPri,"custTabPanel_items","Cesión de Derechos", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
    }
    
}
