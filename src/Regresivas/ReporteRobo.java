/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class ReporteRobo {
    
    public static void  ReportePrepago(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem1,Elem2,Select;
        String Nametxt;
        String Folio, ParentPri =null;
        List <WebElement> Chil1,Chil2;
        Boolean Apagar;
               
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Reporte de Robo y Extravío",reporte);
        Thread.sleep(3000);
        try {
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        } catch (Exception e) {
            Thread.sleep(4000);
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        }

        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Reporte de Robo y Extravío",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Elem1 = WB.FindElement(Driver.WebDriver, WB, "Name", "operateType");
        WB.SelenSelect(Elem1, "Value", "RT002_5");
        WB.CargaAjax(Driver.WebDriver);
        Elem2 = WB.FindElement(Driver.WebDriver, WB, "Name", "lostReason");
        WB.SelenSelect(Elem2, "Value", "LT002");
        reporte.AddImageToReport("Se configura el reporte de robo", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Reporte de Robo y Extravío", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
            OPG.SwitchIframe(Driver, ParentPri);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Órdenes de Cliente",false);
            Thread.sleep(5000);
            WB.CargaAjax(Driver.WebDriver);
            reporte.AddImageToReport("Se valida el estatus de la orden", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
        
    }
    
     public static void  CancelarReportePrepago(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem1,Elem2,Select;
        String Nametxt;
        String Folio, ParentPri =null;
        List <WebElement> Chil1,Chil2;
        Boolean Apagar;
               
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Reporte de Robo y Extravío",reporte);
        Thread.sleep(3000);
        try {
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        } catch (Exception e) {
            Thread.sleep(4000);
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        }

        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Reporte de Robo y Extravío",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Elem1 = WB.FindElement(Driver.WebDriver, WB, "Name", "operateType");
        WB.SelenSelect(Elem1, "Value", "RT002_6");
        WB.CargaAjax(Driver.WebDriver);
        Elem2 = WB.FindElement(Driver.WebDriver, WB, "Name", "lostReason");
        WB.SelenSelect(Elem2, "Value", "CL001");
        reporte.AddImageToReport("Se configura el reporte de robo", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Reporte de Robo y Extravío", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
            OPG.SwitchIframe(Driver, ParentPri);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Órdenes de Cliente",false);
            Thread.sleep(5000);
            WB.CargaAjax(Driver.WebDriver);
            reporte.AddImageToReport("Se valida el estatus de la orden", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
        
    }
     
    public static void  ReporteHibrido(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem1,Elem2,Select;
        String Nametxt;
        String Folio, ParentPri =null;
        List <WebElement> Chil1,Chil2;
        Boolean Apagar;
               
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Reporte de Robo y Extravío",reporte);
        Thread.sleep(3000);
        try {
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        } catch (Exception e) {
            Thread.sleep(4000);
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        }

        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Reporte de Robo y Extravío",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Elem1 = WB.FindElement(Driver.WebDriver, WB, "Name", "operateType");
        WB.SelenSelect(Elem1, "Value", "RT002_5");
        WB.CargaAjax(Driver.WebDriver);
        Elem2 = WB.FindElement(Driver.WebDriver, WB, "Name", "lostReason");
        WB.SelenSelect(Elem2, "Value", "LT001");
        reporte.AddImageToReport("Se configura el reporte de robo", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Reporte de Robo y Extravío", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
            OPG.SwitchIframe(Driver, ParentPri);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Órdenes de Cliente",false);
            Thread.sleep(5000);
            WB.CargaAjax(Driver.WebDriver);
            reporte.AddImageToReport("Se valida el estatus de la orden", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
        
    }
    
    public static void  ReporteIMEI(WebDriv Driver,WebAction SelenElem,Report reporte, String DN, String Codigo, String IMEI) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem1,Elem2,Select,InputIMEI;
        String Nametxt;
        String Folio, ParentPri =null;
        List <WebElement> Chil1,Chil2;
        Boolean Apagar;
               
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Reporte de Robo y Extravío",reporte);
        Thread.sleep(3000);
        try {
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        } catch (Exception e) {
            Thread.sleep(4000);
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        }

        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Reporte de Robo y Extravío",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Elem1 = WB.FindElement(Driver.WebDriver, WB, "Name", "operateType");
        WB.SelenSelect(Elem1, "Value", "RT002_5");
        WB.CargaAjax(Driver.WebDriver);
        Elem2 = WB.FindElement(Driver.WebDriver, WB, "Name", "lostReason");
        WB.SelenSelect(Elem2, "Value", "LT002");
        //IMEI
        WB.ClickById(Driver.WebDriver,"blockimei_input_0");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se configura el reporte de robo con IMEI", OPG.ScreenShotElement(Driver));
        OPG.IframeActual(Driver);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "si");
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Reporte de Robo y Extravío",false);
        InputIMEI = WB.FindElementByID(Driver.WebDriver, "handsetimei_input_value");
        WB.TypeText(InputIMEI, IMEI);
        reporte.AddImageToReport("Se configura el reporte de robo", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Reporte de Robo y Extravío", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
            Thread.sleep(2000);
            OPG.SwitchIframe(Driver, ParentPri);
            OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Órdenes de Cliente",false);
            Thread.sleep(5000);
            WB.CargaAjax(Driver.WebDriver);
            reporte.AddImageToReport("Se valida el estatus de la orden", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
        
    }
    
    
    
}
