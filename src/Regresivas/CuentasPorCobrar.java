
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;


public class CuentasPorCobrar {
    
     public static void  ReportePrepago(WebDriv Driver,WebAction SelenElem,Report reporte, String DN) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Ruta,Elem1,Elem2,Select;
        String Nametxt;
        String Folio, ParentPri =null;
        List <WebElement> Chil1,Chil2;
               
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Reporte de Robo y Extravío",reporte);
        Thread.sleep(3000);
        try {
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,"");
        } catch (Exception e) {
            Thread.sleep(4000);
            ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,"");
        }
        
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Reporte de Robo y Extravío",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
    }
    
    
    
    
    
}
