/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.util.ArrayList;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 * @author PRACTIA
 */
public class AdministracionOferta {
    
    public static void CambioOferPrimPrepago(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Plan) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Input,Eletab,Tabla,BtnSig,BtnPagoAdelantado;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio,TxtBusq;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Cambiar Oferta",reporte);
        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Suscriptor",false);
        WB.EsperaElem(Driver.WebDriver, "zSubscriberInfo_SubsOffering_title");
         Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra la informacion del cliente", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver,"zSubscriberInfo_SubsOffering_title");
         Thread.sleep(2000);
        reporte.AddImageToReport("Se muestra la informacion del Plan", OPG.ScreenShotElement(Driver));
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        reporte.AddImageToReport("Se muestra la oferta", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_toolbaritem", "Seleccionar Oferta Primaria");
        WB.CargaAjax(Driver.WebDriver);
        OPG.NavegaPop(Driver);
        Thread.sleep(3000);
        WB.EsperaElem(Driver.WebDriver, "migrateOfferList_databody");
        //Sebusca la oferta 
        OPG.BuscaOfertaPrimaria(Driver, WB, reporte,Plan, "migrateOfferList_databody");
        Thread.sleep(4000);
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        WB.CargaAjax(Driver.WebDriver);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        SelenElem.CargaAjax(Driver.WebDriver);
        BtnSig = WB.FindElementByID(Driver.WebDriver, "nextButtonid");
        OPG.VisualiaElem(Driver, BtnSig);
        WB.ClickById(Driver.WebDriver, "nextButtonid");         
        Thread.sleep(6000);
        BtnPagoAdelantado = WB.FindElementByID(Driver.WebDriver, "confirmBtn");
        if(BtnPagoAdelantado == null)
        {
        }
        else
          BtnPagoAdelantado.click();
            
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Oferta", reporte);
            //OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Oferta", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
            if(Folio==null)
            {
                throw new NullPointerException( "" );
            }
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }        

    }
    
    public static void AgregarOferSupl(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Oferta) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Input,Eletab,Tabla,BTNNext;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio,TxtBusq;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Cambiar Oferta",reporte);
        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        reporte.AddImageToReport("Se muestra la Informacion", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "moreofferInd");
        WB.CargaAjax(Driver.WebDriver);
        OPG.NavegaPop(Driver);
        WB.EsperaElem(Driver.WebDriver, "searchbox_value");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Input = WB.FindElementByID(Driver.WebDriver, "searchbox_value");
        WB.TypeText(Input, Oferta);
        WB.ClickById(Driver.WebDriver, "searchbox_search");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se busca la oferta", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "independVasOfferSelector_0");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Seleccionamos la oferta", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "okBtn");
        Thread.sleep(4000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        
        BTNNext = WB.FindElementByID(Driver.WebDriver, "nextButtonid");
        WB.focus(Driver.WebDriver, BTNNext);
        //reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement2(BTNNext));
        WB.ClickById(Driver.WebDriver, "nextButtonid");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        //reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Oferta", reporte);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        //reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
              
    }
    
    public static void AgregarOferSuplPos(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Oferta) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Input,Eletab,Tabla,BTNNext;
        List <WebElement> Div2, Div3, Div4, Div5,Div6;
        Boolean Apagar;
        String Folio,TxtBusq;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Cambiar Oferta",reporte);
        
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        reporte.AddImageToReport("Se muestra la Informacion", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "moreofferInd");
        WB.CargaAjax(Driver.WebDriver);
        OPG.NavegaPop(Driver);
        WB.EsperaElem(Driver.WebDriver, "searchbox_value");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Input = WB.FindElementByID(Driver.WebDriver, "searchbox_value");
        WB.TypeText(Input, Oferta);
        WB.ClickById(Driver.WebDriver, "searchbox_search");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se busca la oferta", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "independVasOfferSelector_0");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Seleccionamos la oferta", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "okBtn");
        Thread.sleep(4000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        
        BTNNext = WB.FindElementByID(Driver.WebDriver, "nextButtonid");
        WB.focus(Driver.WebDriver, BTNNext);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement2(BTNNext));
        WB.ClickById(Driver.WebDriver, "nextButtonid");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Oferta", reporte);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        //reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        //reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
              
    }
    
    
    public static void EliminarOferHibrido(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Codigo,String Oferta) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebAction WB = new WebAction();
        WebDriver nFrame;
        WebElement Elem,Input,Eletab,Tabla,Texto,Div,Captura;
        List <WebElement> Ofer, Div3, Div4, Div5,Div6;
        Boolean Bandera=false;
        String Folio,TxtBusq;
        int Size,i;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Cambiar Oferta",reporte);
        
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,Codigo);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        reporte.AddImageToReport("Se muestra la Informacion", OPG.ScreenShotElement(Driver));
        Tabla = WB.FindElementByID(Driver.WebDriver, "subVasOfferList_databody");
        Ofer = WB.GetChildren(Tabla);
        Size = Ofer.size();
        for(i=0;i<=Size;i++)
        {
         Texto = WB.FindElementByID(Driver.WebDriver, "subVasOfferList_"+i+"_0");
         TxtBusq = Texto.getText();
         if(TxtBusq.equals(Oferta))
         {
             Captura = WB.FindElementByID(Driver.WebDriver, "subVasOfferList_"+i);
             reporte.AddImageToReport("se elimina la oferta", OPG.ScreenShotElement2(Captura));
             Div = WB.FindElementByID(Driver.WebDriver, "subVasOfferList_"+i+"_8");
             Div3 = WB.GetChildren(Div);
             Div3.get(3).click();
             WB.CargaAjax(Driver.WebDriver);
             reporte.AddImageToReport("se elimina la oferta", OPG.ScreenShotElement(Driver));
             Captura = WB.FindElementByID(Driver.WebDriver, "subVasOfferList_"+i);
             WB.focus(Driver.WebDriver, Captura);
             //reporte.AddImageToReport("se elimina la oferta", OPG.ScreenShotElement2(Captura));
             Bandera = true;
             break;
         }
        }
        if(Bandera == false)
        {
            reporte.AddImageToReport("Oferta no Encontrada", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "Oferta no Encontrada" );
        }
        
        WB.ClickById(Driver.WebDriver, "nextButtonid");
        SelenElem.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        OPG.EnviarOrdenContraDigital(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Oferta", reporte);
        Folio= OPG.BuscaFolio(Driver,SelenElem);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));            
              
    }
    
    public static void Cambia_Oferta_Sup(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Segmento, String Plan) throws InterruptedException, IOException, FileNotFoundException, Exception
    {
        
        String Codigo,Oferta,CODIGO_OFER,ID_OFERTA,Edo="",Ope="";
        WebElement TablaOfer,InputBusq,BTNSiguiente,Ruta;
        String TxtTabla,Folio,Nametxt;
        int Valores,NumPlanes,NumIDs;
        List<WebElement> Chil,Chil2;
        ArrayList Tabla = new ArrayList();
        ArrayList Resultados = new ArrayList();
        Boolean BanPrim = false, Apagar;
        WebAction WB = new WebAction();
        OperacionesGenerales OPG = new OperacionesGenerales();
    
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Gestión de Batch","Batch de Negocio","Carga de archivos Residenciales",reporte);
        Thread.sleep(4000); 
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.SelectBarraLateral(Driver.WebDriver,SelenElem, "Cambiar Oferta Complementaria por Lote");
        SelenElem.ClickById(Driver.WebDriver, "batchNewBusiness");
        Thread.sleep(3000);
        OPG.NavegaPop(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //SelenElem.ClickById(Driver.WebDriver, "uploadfile_select");
        //NomArchivo = OPB.CreaArchivoCategory(CodCuenta,NewCategoria);
        Thread.sleep(3000);
        Nametxt = OPG.GeneraTxt(DN,Segmento,"AdminOfer\\ArchivosBatch","CargaOferSup");
        Ruta= SelenElem.FindElementByID(Driver.WebDriver, "uploadfile_file");
        SelenElem.TypeText(Ruta,"A:\\DataOnix\\AdminOfer\\ArchivosBatch\\"+Nametxt+".txt");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Carga el archivo: "+Nametxt, OPG.ScreenShotElement(Driver));
        Segmento = Segmento.toUpperCase();
        if(Segmento.equals("HIBRIDO"))
        {
            WB.ClickById(Driver.WebDriver, "hybrid_container");
            WB.CargaAjax(Driver.WebDriver);
            //WB.ClickById(Driver.WebDriver, "hybrid_0");
        }
        else if(Segmento.equals("POSPAGO"))
        {
            WB.ClickById(Driver.WebDriver, "postpaid_container");
            WB.CargaAjax(Driver.WebDriver);
            //WB.ClickById(Driver.WebDriver, "postpaid_0");
        }
        else if(Segmento.equals("PREPAGO"))
        {
            WB.ClickById(Driver.WebDriver, "prepaid_container");
            WB.CargaAjax(Driver.WebDriver);
            //WB.ClickById(Driver.WebDriver, "prepaid_0");
        }
        Thread.sleep(3000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //Se busca la oferta
        WB.ClickById(Driver.WebDriver, "addSupOfferBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        //OPG.NavegaPop(Driver);
        Thread.sleep(2000);
        OPG.SwitchIframe(Driver, "popwin1");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Thread.sleep(7000);
        InputBusq = WB.FindElementByID(Driver.WebDriver,"searchbox_value");
        WB.TypeText(InputBusq, Plan);
        Thread.sleep(3000);
        WB.ClickById(Driver.WebDriver, "searchbox_search");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se busca la oferta", OPG.ScreenShotElement(Driver));
        try {
            Thread.sleep(3000);
            WB.ClickButton(Driver.WebDriver, WB, "Name", "independVasOfferSelector");
            SelenElem.CargaAjax(Driver.WebDriver);
        } catch (Exception e) {
            throw new NullPointerException( "No se encontro la oferta" );
        }
        reporte.AddImageToReport("Se Selecciona la oferta", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
        //SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        //Regresamos
        OPG.NavegaPop(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        //
        
        SelenElem.ClickById(Driver.WebDriver, "submitbtn");
        SelenElem.CargaAjax(Driver.WebDriver);        
        Thread.sleep(8000); 
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Búsqueda");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));

        
    }
    
    public static void Desc_Oferta_Pri(WebDriv Driver,WebAction SelenElem,Report reporte,String DN,String Segmento, String IdOfer, String Monto) throws InterruptedException, IOException, FileNotFoundException, Exception
    {
        
        String Codigo,Oferta,CODIGO_OFER,ID_OFERTA,Edo="",Ope="";
        WebElement TablaOfer,InputBusq,BTNSiguiente,Ruta,Tabla,Texto;
        String TxtTabla,Folio,Nametxt,TxtBusq;
        int Valores,NumPlanes,NumIDs;
        List<WebElement> Ofer,Chil,Chil2;
        Boolean Bandera = false, Apagar;
        int Size,i;
        WebAction WB = new WebAction();
        OperacionesGenerales OPG = new OperacionesGenerales();
    
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        
        //Se Valida que no tenga oferta con costo
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Servicios del Cliente","Cambiar Oferta",reporte);
        
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,SelenElem,"DN",DN,"");
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Oferta",false);
        reporte.AddImageToReport("Se muestra la Informacion", OPG.ScreenShotElement(Driver));
        Tabla = WB.FindElementByID(Driver.WebDriver, "subVasOfferList_databody");
        Ofer = WB.GetChildren(Tabla);
        Size = Ofer.size();
        for(i=0;i<Size;i++)
        {
         Texto = WB.FindElementByID(Driver.WebDriver, "subVasOfferList_"+i+"_6");
         TxtBusq = Texto.getText();
         if(TxtBusq.equals("$0.00"))
         {
         }
         else
         {
             reporte.AddImageToReport("Oferta con costo encontrada", OPG.ScreenShotElement(Driver));
             Bandera = true;
             break;
         }
        }
        if(Bandera.equals(false))
        {
            reporte.AddImageToReport("Oferta con costo no Encontrada", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "Oferta no Encontrada" );
        }
        OPG.IframeActual(Driver);
        OPG.CierraPestanas(Driver);
          
        OPG.NavegaMapaSitio(Driver,SelenElem,"Gestión de Batch","Batch de Negocio","Carga de archivos Residenciales",reporte);
        Thread.sleep(4000); 
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.SelectBarraLateral(Driver.WebDriver,SelenElem, "Cambio de oferta suplementaria por lotes");
        SelenElem.ClickById(Driver.WebDriver, "batchNewBusiness");
        Thread.sleep(3000);
        OPG.NavegaPop(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //SelenElem.ClickById(Driver.WebDriver, "uploadfile_select");
        //NomArchivo = OPB.CreaArchivoCategory(CodCuenta,NewCategoria);
        Thread.sleep(3000);
        Nametxt = OPG.GeneraTxt(DN+","+IdOfer+","+Monto,Segmento,"AdminOfer\\ArchivosBatch","DescOferSup");
        Ruta= SelenElem.FindElementByID(Driver.WebDriver, "uploadfile_file");
        SelenElem.TypeText(Ruta,"A:\\DataOnix\\AdminOfer\\ArchivosBatch\\"+Nametxt+".txt");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Carga el archivo: "+Nametxt, OPG.ScreenShotElement(Driver));
        Segmento = Segmento.toUpperCase();
        if(Segmento.equals("HIBRIDO"))
        {
            WB.ClickById(Driver.WebDriver, "hybrid_0");
            WB.CargaAjax(Driver.WebDriver);
            //WB.ClickById(Driver.WebDriver, "hybrid_0");
        }
        else if(Segmento.equals("POSPAGO"))
        {
            WB.ClickById(Driver.WebDriver, "postpaid_0");
            WB.CargaAjax(Driver.WebDriver);
            //WB.ClickById(Driver.WebDriver, "postpaid_0");
        }
        else if(Segmento.equals("PREPAGO"))
        {
            reporte.AddImageToReport("La opcion PREPAGO no es valida", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "La opcion PREPAGO no es valida" );
            //WB.ClickById(Driver.WebDriver, "prepaid_0");
            //WB.CargaAjax(Driver.WebDriver);
        }
        Thread.sleep(1000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        //
        
        SelenElem.ClickById(Driver.WebDriver, "submitbtn");
        SelenElem.CargaAjax(Driver.WebDriver);        
        Thread.sleep(8000); 
        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", "Carga de archivos Residenciales",true);
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Búsqueda");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));

        
    }
    
    
}
