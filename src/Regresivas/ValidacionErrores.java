
package Regresivas;

import Core.Report;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import Core.WebAction;
import Operaciones.CargaVariables;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebElement;

public class ValidacionErrores {
    
    
    public static void GenerarRFC(WebDriv Driver,WebAction SelenElem,Report reporte, String Tipo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement NombreW, APPW, APMW, FechaW, ID, RFC,Pasaporte;
        String RFCT,Tab = null,Nombre,APP,APM,FECHA;
        
        if(Tipo.contentEquals("Pymes"))
        {
            Tab = "Venta PYMES";
        }
        else if(Tipo.contentEquals("Pospago"))
        {
            Tab = "Venta Pospago";
        }
        
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas",Tab,reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Nuevo Cliente");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        //Se LLenan los datos para el RFC
        
        Nombre        = CargaVariables.LeerCSV(0, "ConfigOnix", "CliNuevo");
        APP           = CargaVariables.LeerCSV(1, "ConfigOnix", "CliNuevo");
        APM           = CargaVariables.LeerCSV(2, "ConfigOnix", "CliNuevo");
        FECHA         = CargaVariables.LeerCSV(3, "ConfigOnix", "CliNuevo");
        
        Pasaporte = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500017_input_select");
        SelenElem.SelenSelect(Pasaporte, "Value", "2");
        ID = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.idNumber");
        SelenElem.TypeText(ID, "123456788");
        NombreW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name1");
        SelenElem.TypeText(NombreW, Nombre);
        APPW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name2");
        SelenElem.TypeText(APPW, APP);
        APMW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name3");
        SelenElem.TypeText(APMW,APM);
        FechaW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500025_input_value");
        SelenElem.TypeText(FechaW,FECHA);
        
        /*ID = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.idNumber");
        SelenElem.TypeText(ID, "123456788");
        Nombre = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name1");
        SelenElem.TypeText(Nombre, "Automatizacion");
        APP = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name2");
        SelenElem.TypeText(APP, "Automatizacion");
        APM = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Name", "#BMEAttr.paramCustBaseInfo.custBriefInfo.name3");
        SelenElem.TypeText(APM, "Automatizacion");
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500025_input_value");
        Fecha = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500025_input_value");
        SelenElem.TypeText(Fecha, "09/09/1993");*/
        
        
        reporte.AddImageToReport("Se llenan los campos necesarios", OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500200");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se genera el RFC", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        RFC = SelenElem.FindElementByID(Driver.WebDriver, "field_500003_500019_input_value");
        RFCT = RFC.getAttribute("value");
        if(RFCT=="")
        {
            throw new NullPointerException( "ERROR no se genero el RFC" );
        }
        
    }
    
    public static void ValidaInfoCliPIPE(WebDriv Driver,WebAction SelenElem,Report reporte, String Tipo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Cuenta, Input1, Input2, Input3, Input4;
        List <WebElement> Busqueda, Chil1,Chil2,Chil3,Chil4, Val;
        String RFCT,Tab=null;
        Boolean val1,Val2,Val3,Val4;
        
         if(Tipo.contentEquals("Pymes"))
        {
            Tab = "Venta PYMES";
        }
        else if(Tipo.contentEquals("Pospago"))
        {
            Tab = "Venta Pospago";
        }
         
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas",Tab,reporte);        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Nuevo Cliente");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        
        if(Tipo.contentEquals("Pymes"))
        {
            //Se LLenan los datos para el RFC
            OPG.LlenaDatosClienteNuevoPymes(Driver, SelenElem, reporte);
        }
        else if(Tipo.contentEquals("Pospago"))
        {
            //Se LLenan los datos para el RFC
            OPG.LlenaDatosClienteNuevo(Driver, SelenElem, reporte);
        }
        OPG.BurodeCredito(Driver, SelenElem, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
        SelenElem.ClickById(Driver.WebDriver, "nextBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(4000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Busqueda = SelenElem.FindElements(Driver.WebDriver, "ClassName", "bc_grid_databody");
        Chil1 = SelenElem.GetChildren(Busqueda.get(0));
        Chil3 = SelenElem.GetChildren(Chil1.get(0));
        Chil2 = SelenElem.GetChildren(Chil3.get(3));
        Chil2.get(0).click();
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        SelenElem.ClickById(Driver.WebDriver, "accountInfo_title");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Cuenta = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "acctList_header");
        Chil4 = SelenElem.GetChildren(Cuenta);
        Chil4.get(0).click();
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        val1 = OPG.ValidaPipe(Driver, SelenElem, reporte, "field_1000_1003_input_value");
        Val2 = OPG.ValidaPipe(Driver, SelenElem, reporte, "field_1000_500506_input_value");
        Val3 = OPG.ValidaPipe(Driver, SelenElem, reporte, "field_1000_500507_input_value");
        if(val1==true && Val2 ==true && Val3==true)
        {
            reporte.AddImageToReport("Se Valido con exito", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "Fallo La validacion PIPE" );
        }
        
        
    }
    
    public static void ValidaInfoDireccionPIPE(WebDriv Driver,WebAction SelenElem,Report reporte, String Tipo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Cuenta;
        List <WebElement> Busqueda, Chil1,Chil2,Chil3,Chil4, Val;
        Boolean val1;
        String Tab=null;
        
         if(Tipo.contentEquals("Pymes"))
        {
            Tab = "Venta PYMES";
        }
        else if(Tipo.contentEquals("Pospago"))
        {
            Tab = "Venta Pospago";
        }
         
         
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas",Tab,reporte);
        //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Nuevo Cliente");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        if(Tipo.contentEquals("Pymes"))
        {
            //Se LLenan los datos para el RFC
            OPG.LlenaDatosClienteNuevoPymes(Driver, SelenElem, reporte);
        }
        else if(Tipo.contentEquals("Pospago"))
        {
            //Se LLenan los datos para el RFC
            OPG.LlenaDatosClienteNuevo(Driver, SelenElem, reporte);
        }
        
        OPG.BurodeCredito(Driver, SelenElem, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
        SelenElem.ClickById(Driver.WebDriver, "nextBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(6000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Busqueda = SelenElem.FindElements(Driver.WebDriver, "ClassName", "bc_grid_databody");
        Chil1 = SelenElem.GetChildren(Busqueda.get(0));
        Chil3 = SelenElem.GetChildren(Chil1.get(0));
        Chil2 = SelenElem.GetChildren(Chil3.get(3));
        Chil2.get(0).click();
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        SelenElem.ClickById(Driver.WebDriver, "accountInfo_title");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Cuenta = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "acctList_header");
        Chil4 = SelenElem.GetChildren(Cuenta);
        Chil4.get(0).click();
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        //Se ocultan los div
        SelenElem.ClickById(Driver.WebDriver, "set_1000_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "billMedium_titlebar");
        
        val1 = OPG.ValidaPipe(Driver, SelenElem, reporte, "name1_input_value");
        if(val1==true)
        {
            reporte.AddImageToReport("Se Valido con exito", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "Fallo La validacion PIPE" );
        }
        
        
    }
    
    public static void ValidaInfoCalleDireccionPIPE(WebDriv Driver,WebAction SelenElem,Report reporte, String Tipo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Cuenta;
        List <WebElement> Busqueda, Chil1,Chil2,Chil3,Chil4, Val;
        Boolean val1,Val2;
        String Tab=null;
        
         if(Tipo.contentEquals("Pymes"))
        {
            Tab = "Venta PYMES";
        }
        else if(Tipo.contentEquals("Pospago"))
        {
            Tab = "Venta Pospago";
        }
         
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas",Tab,reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Nuevo Cliente");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        if(Tipo.contentEquals("Pymes"))
        {
            //Se LLenan los datos para el RFC
            OPG.LlenaDatosClienteNuevoPymes(Driver, SelenElem, reporte);
        }
        else if(Tipo.contentEquals("Pospago"))
        {
            //Se LLenan los datos para el RFC
            OPG.LlenaDatosClienteNuevo(Driver, SelenElem, reporte);
        }
        OPG.BurodeCredito(Driver, SelenElem, reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
        SelenElem.ClickById(Driver.WebDriver, "nextBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(6000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Busqueda = SelenElem.FindElements(Driver.WebDriver, "ClassName", "bc_grid_databody");
        Chil1 = SelenElem.GetChildren(Busqueda.get(0));
        Chil3 = SelenElem.GetChildren(Chil1.get(0));
        Chil2 = SelenElem.GetChildren(Chil3.get(3));
        Chil2.get(0).click();
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        SelenElem.ClickById(Driver.WebDriver, "accountInfo_title");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Cuenta = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "acctList_header");
        Chil4 = SelenElem.GetChildren(Cuenta);
        Chil4.get(0).click();
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        //Se ocultan los div
        SelenElem.ClickById(Driver.WebDriver, "set_1000_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "billMedium_titlebar");
        
        val1 = OPG.ValidaPipe(Driver, SelenElem, reporte, "field_500010_500506_input_value");
        Val2 = OPG.ValidaPipe(Driver, SelenElem, reporte, "field_500010_500507_input_value");
        if(val1==true && Val2 ==true )
        {
            reporte.AddImageToReport("Se Valido con exito", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "Fallo La validacion PIPE" );
        }          
        
    }
    
    
    public static void RFCExistente(WebDriv Driver,WebAction SelenElem,Report reporte, String Tipo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement RFCW,ValRFC;
        List <WebElement> Busqueda, Chil1,Chil2,Chil3,Chil4, Val;
        String RFC,Tab = null;

        if(Tipo.contentEquals("Pymes"))
        {
            Tab = "Venta PYMES";
        }
        else if(Tipo.contentEquals("Pospago"))
        {
            Tab = "Venta Pospago";
        }
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        OPG.NavegaMapaSitio(Driver,SelenElem,"Atención al Cliente","Punto de Ventas",Tab,reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "tabpage_items", Tab,true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Nuevo Cliente");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        RFC = CargaVariables.LeerCSV(9, "ConfigOnix", "CliNuevo");
        RFCW = SelenElem.FindElement(Driver.WebDriver, SelenElem, "Id", "field_500003_500019_input_value");
        SelenElem.TypeText(RFCW,RFC);
        reporte.AddImageToReport("Se ingresa el RFC" , OPG.ScreenShotElement(Driver));
        SelenElem.ClickById(Driver.WebDriver, "field_500003_500200");
        SelenElem.CargaAjax(Driver.WebDriver);
        
        OPG.IframeActual(Driver);
        ValRFC = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        if(ValRFC!=null)
        {
            reporte.AddImageToReport("El RFC Existe", OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "FALLO la validacion del RFC" );
        }
    }
    
    
    
}
