package Regresivas;

import Core.Report;
import Core.WebAction;
import Core.WebDriv;
import Operaciones.CargaVariables;
import Operaciones.OperacionesGenerales;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;

/**
 *
 */
public class FacturacionCobranza {
    
    public static void ConsultaImprimir(WebDriv Driver,WebAction WB,Report reporte,String Ord,String FechIn, String FechFin,String Tipo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputTip,InputFech,InputFech2,Tbody,OrdPago,DivTipo,DivEstatus,DivImpr;
        String TxtDiv,TxtOrder,TxtTipo,Txtestatus;
        List<WebElement> Cont,Chil1,chil2;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Comercio de Negocio","Imprimir Factura",reporte); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Imprimir Factura",true);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Thread.sleep(2000);
        
        InputTip = WB.FindElement(Driver.WebDriver, WB, "Name", "conds.invoiceType");
        WB.SelenSelect(InputTip, "Text", "Factura");
        WB.CargaAjax(Driver.WebDriver);
        InputFech = WB.FindElementByID(Driver.WebDriver, "startTime_input_value");
        InputFech.clear();
        WB.TypeText(InputFech, FechIn+" 00:00:00");
        InputFech2 = WB.FindElementByID(Driver.WebDriver, "endTime_input_value");
        WB.TypeText(InputFech2, FechFin+" 00:00:00");
        
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Buscar");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        Tbody = WB.FindElementByID(Driver.WebDriver, "resultArea_databody");
        TxtDiv = Tbody.getText();
        if(TxtDiv.equals("No hay registros"))
        {
            reporte.AddImageToReport("No se encontro ninguna factura", OperacionesGenerales.ScreenShotElement(Driver));
            throw new NullPointerException( "Factura no encontrada" );
        }
        Cont = WB.GetChildren(Tbody);
        Boolean Ban=false;
        for(int i=0;i<Cont.size();i++)
        {
            OrdPago = WB.FindElementByID(Driver.WebDriver, "resultArea_"+i+"_3");
            TxtOrder = OrdPago.getText();
            DivTipo = WB.FindElementByID(Driver.WebDriver, "resultArea_"+i+"_9");
            TxtTipo = DivTipo.getText();
            DivEstatus = WB.FindElementByID(Driver.WebDriver, "resultArea_"+i+"_12");
            Txtestatus = DivEstatus.getText();
            if(TxtOrder.equals(Ord) && TxtTipo.equals(Tipo) && Txtestatus.equals("Finalizado") )
            {
                
                 DivImpr = WB.FindElementByID(Driver.WebDriver, "resultArea_"+i+"_13");
                 Chil1 = WB.GetChildren(DivImpr);
                 chil2 = WB.GetChildren(Chil1.get(0));
                 chil2.get(0).click();
                 Thread.sleep(4000);
                 OPG.CambiaVentana(Driver.WebDriver);
                 reporte.AddImageToReport("Se muestra la factura", OPG.ScreenShotElement(Driver));
                 Ban = true;
            }
            if(Ban == true)
                break;
        }
        if (Ban == false)
            throw new NullPointerException( "Factura no encontrada" );
    }
    
    public static void CambioCiclo(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod,String Ciclo) throws InterruptedException, IOException
    {
        WebElement DivSelen,InpUser,InpPass,Dominio;
        String Value="",Folio,User,Password;
        Boolean Apagar;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        //Driver.WebDriver.findElement(By.id("firstname-placeholder")).sendKeys(Keys.F5);
        //Driver.WebDriver.navigate().refresh();
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"DN",DN,Cod);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cuenta",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Información de Cuenta",false);
        Thread.sleep(5000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "acctInformation_titlebar");
        WB.ClickById(Driver.WebDriver, "address_titlebar"); 
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));  
        WB.ClickById(Driver.WebDriver, "chgBillcycleBtn"); 
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Modificar Ciclo de Facturación",false);
        reporte.AddImageToReport("Se cambia el ciclo", OPG.ScreenShotElement(Driver)); 
        //Seleccionamos el ciclo
        DivSelen = WB.FindElementByID(Driver.WebDriver, "newBillCycleField_input_select");
        WB.SelenSelect(DivSelen, "Text", Ciclo);
        reporte.AddImageToReport("Se cambia el ciclo", OPG.ScreenShotElement(Driver)); 
        //Autorizacion
        WB.ClickById(Driver.WebDriver, "btnAuthorizeNewBillcycle");
        WB.CargaAjax(Driver.WebDriver);
        OPG.BuscaPOP(Driver);
        Thread.sleep(4000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        InpUser = WB.FindElementByID(Driver.WebDriver, "UserName_input_value");
        InpPass = WB.FindElementByID(Driver.WebDriver, "UserPassword_input_value");
        Dominio = WB.FindElementByID(Driver.WebDriver, "UserDomain_input_select");
        WB.SelenSelect(Dominio, "Value", "Local");
        
        User      = CargaVariables.LeerCSV2(0,"ConfigOnix","Users");
        Password  = CargaVariables.LeerCSV2(1,"ConfigOnix","Users");
        WB.TypeText(InpUser, User);
        WB.TypeText(InpPass, Password);
        reporte.AddImageToReport("Seingresan las credenciales", OPG.ScreenShotElement(Driver)); 
        WB.ClickById(Driver.WebDriver, "okBtn");
        WB.CargaAjax(Driver.WebDriver);
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Modificar Ciclo de Facturación",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Siguiente");
        WB.CargaAjax(Driver.WebDriver); 
        Thread.sleep(2000);
        reporte.AddImageToReport("Se valida la orden", OPG.ScreenShotElement(Driver));
        // Internet 25 Dias 3.5 GB
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, WB);
        OPG.EnviarOrden(Driver,WB,ParentPri,"custTabPanel_items","Modificar Ciclo de Facturación", reporte);
        Thread.sleep(3000);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "" );
        }
        
    }
    
    public static void CambioPagoaTarjeta(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod) throws InterruptedException, IOException
    {
        WebElement DivSelen,InpUser,InpPass,Dominio;
        String Value="",Folio,User,Password;
        Boolean Apagar;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"DN",DN,Cod);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cuenta",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Información de Cuenta",false);
        Thread.sleep(5000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "acctInformation_titlebar");
        WB.ClickById(Driver.WebDriver, "address_titlebar");
        WB.ClickById(Driver.WebDriver, "chgAcctBtn");
        Thread.sleep(5000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        WB.ClickById(Driver.WebDriver, "set_1007_titlebar");
        
        OPG.AgregaTarjeta(Driver, WB, reporte);
        try {
            OPG.SwitchIframe(Driver, ParentPri);
            OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cuenta",false);
            WB.ClickById(Driver.WebDriver, "nextBtn");  
            WB.CargaAjax(Driver.WebDriver);
        } catch (Exception e) {
            reporte.AddImageToReport("Error", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "" );
        }
        
        try {
            WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
            Driver.AssingNewDriver(nFrame);
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
             
        } catch (Exception e) {
        }
        Thread.sleep(3000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        reporte.AddImageToReport("Se envia la orden", OPG.ScreenShotElement(Driver)); 
        OPG.EnviarOrden(Driver, WB, ParentPri,"custTabPanel_items", "Cambiar Información de Cuenta", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
    } 
    
    public static void CambioMedioFact(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod,String Cambio) throws InterruptedException, IOException
    {
        WebElement RadioBtn,VentanaError,Btn,Dominio;
        String Value="",Folio,Elem=null;
        Boolean Apagar;
        List <WebElement> F;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"DN",DN,Cod);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));  
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cuenta",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Medio de facturación",false);
        Thread.sleep(5000);
        WB.WaitForElem(Driver.WebDriver, "billMediumList_body");
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        
        if (Cambio.equals("SMS"))
            Elem = "billMediumCheckBox_0";
        else if(Cambio.equals("Email"))
            Elem = "billMediumCheckBox_1";
        else if(Cambio.equals("Impresa"))
            Elem = "billMediumCheckBox_2"; //Papel
        else
        {
            reporte.AddImageToReport("Opcion no valida en el archivo: SMS, Email o Impresa", OPG.ScreenShotElement(Driver));
            throw new NullPointerException( "Opcion no valida" );
        }
            
        RadioBtn = WB.FindElementByID(Driver.WebDriver, Elem);
        RadioBtn.click();
        WB.CargaAjax(Driver.WebDriver);
        try {
            WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
            Driver.AssingNewDriver(nFrame);
            VentanaError = WB.FindElementByID(Driver.WebDriver, "winmsg0");
            if(VentanaError != null)
                throw new NullPointerException( "Error" );
            //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        } catch (Exception e) {
        }

        Driver.WebDriver.switchTo().frame(ParentPri);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cuenta",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Medio de facturación",false);
        
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Solicitar Cambio");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(3000);
        
        WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        Driver.WebDriver.switchTo().frame(ParentPri);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cuenta",false);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Medio de facturación",false);
        reporte.AddImageToReport("Se envia la orden", OPG.ScreenShotElement(Driver)); 
         
        OPG.EnviarOrden(Driver, WB, ParentPri,"custTabPanel_items", "Cuenta", reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Medio de facturación",false);
        Thread.sleep(5000);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
    }
    
    
    public static void CambioTarjetaAEfectivo(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod) throws InterruptedException, IOException
    {
        WebElement EfectSelect,InpUser,InpPass,Dominio;
        String Value="",Folio,User,Password;
        Boolean Apagar;
          
        OperacionesGenerales OPG = new OperacionesGenerales();
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Validación de sesión", OPG.ScreenShotElement(Driver));
        
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"DN",DN,Cod);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cuenta",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Información de Cuenta",false);
        Thread.sleep(5000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "acctInformation_titlebar");
        WB.ClickById(Driver.WebDriver, "address_titlebar");
        WB.ClickById(Driver.WebDriver, "chgAcctBtn");
        Thread.sleep(5000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        WB.WaitForElem(Driver.WebDriver, "field_507003_507081_input_select");
        WB.ClickById(Driver.WebDriver, "set_1007_titlebar");
        
        EfectSelect = WB.FindElementByID(Driver.WebDriver, "field_507003_507081_input_select");
        WB.SelenSelect(EfectSelect, "Value", "CASH");
        WB.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("Se Cambia el metodod e pago", OPG.ScreenShotElement(Driver)); 
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        WB.ClickById(Driver.WebDriver, "nextBtn");  
        WB.CargaAjax(Driver.WebDriver);

        try {
            WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
            Driver.AssingNewDriver(nFrame);
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
             
        } catch (Exception e) {
        }
        try {
            Thread.sleep(3000);
            WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
            Driver.AssingNewDriver(nFrame);
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "Aceptar");
             
        } catch (Exception e) {
        }
        Thread.sleep(3000);
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        reporte.AddImageToReport("Se envia la orden", OPG.ScreenShotElement(Driver)); 
        OPG.EnviarOrden(Driver, WB, ParentPri,"custTabPanel_items", "Cambiar Información de Cuenta", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        if(Folio==null)
        {
            throw new NullPointerException( "No se encontro el Folio de la Orden" );
        }
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
    } 
    
    
    public static void NotaCrediMenor(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod,String Importe) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputDN,SelectAjuste,SelectCategoria,SelectRazon,Tbody = null,InputImporte;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Cuentas por Cobrar","Gestión de Ajuste","Ajuste de notas de crédito",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Ajuste de notas de crédito",true);
        InputDN = WB.FindElementByID(Driver.WebDriver, "msisdn_condition_input_value");
        WB.TypeText(InputDN, DN);
        reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotAll());
        WB.ClickById(Driver.WebDriver, "queryUserInfoButton");
        WB.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Ajuste de notas de crédito",true);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        
        SelectAjuste = WB.FindElementByID(Driver.WebDriver, "settingPanelAdjLevel_input_select");
        WB.SelenSelect(SelectAjuste, "Value", "D");
        Thread.sleep(2000);
        SelectCategoria = WB.FindElementByID(Driver.WebDriver, "settingPanelReasonCategroy_input_select");
        WB.SelenSelect(SelectCategoria, "Value", "1668541828053858683");
        SelectRazon = WB.FindElementByID(Driver.WebDriver, "settingPanelReason_input_select");
        WB.SelenSelect(SelectRazon, "Value", "CNT217");
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        
        Tbody = WB.FindElementByID(Driver.WebDriver, "invoiceInfoIOList2_databody");
        if(Tbody != null)
        {
            WB.ClickById(Driver.WebDriver, "selected2_1");
            Thread.sleep(3000);
            InputImporte = WB.FindElementByID(Driver.WebDriver, "dispinput2_1_value");
            InputImporte.clear();
            WB.TypeText(InputImporte, Importe);
            reporte.AddImageToReport("", OPG.ScreenShotAll());
            
            WB.ClickById(Driver.WebDriver, "submitInvoiceDetailButton");
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(1000);
            reporte.AddImageToReport("", OPG.ScreenShotAll());
            
            OPG.BuscaPOP2(Driver);
            WB.ClickById(Driver.WebDriver, "confirmSubmitBtn");
            Thread.sleep(5000);
            reporte.AddImageToReport("", OPG.ScreenShotAll());
            OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Ajuste de notas de crédito",true);
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "OK");
        }
        else
        {
            throw new NullPointerException( "No se encontro informacion" );
        }
        
        
    }
    
     public static void NotaCrediExacto(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputDN,SelectAjuste,SelectCategoria,SelectRazon,Tbody = null,InputImporte;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Cuentas por Cobrar","Gestión de Ajuste","Ajuste de notas de crédito",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Ajuste de notas de crédito",true);
        InputDN = WB.FindElementByID(Driver.WebDriver, "msisdn_condition_input_value");
        WB.TypeText(InputDN, DN);
        reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotAll());
        WB.ClickById(Driver.WebDriver, "queryUserInfoButton");
        WB.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Ajuste de notas de crédito",true);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        
        SelectAjuste = WB.FindElementByID(Driver.WebDriver, "settingPanelAdjLevel_input_select");
        WB.SelenSelect(SelectAjuste, "Value", "D");
        Thread.sleep(2000);
        SelectCategoria = WB.FindElementByID(Driver.WebDriver, "settingPanelReasonCategroy_input_select");
        WB.SelenSelect(SelectCategoria, "Value", "1668541828053858683");
        SelectRazon = WB.FindElementByID(Driver.WebDriver, "settingPanelReason_input_select");
        WB.SelenSelect(SelectRazon, "Value", "CNT217");
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        
        Tbody = WB.FindElementByID(Driver.WebDriver, "invoiceInfoIOList2_databody");
        if(Tbody != null)
        {
            WB.ClickById(Driver.WebDriver, "selected2_1");
            Thread.sleep(3000);
            /*InputImporte = WB.FindElementByID(Driver.WebDriver, "dispinput2_1_value");
            InputImporte.clear();
            WB.TypeText(InputImporte, Importe);*/
            reporte.AddImageToReport("", OPG.ScreenShotAll());
            
            WB.ClickById(Driver.WebDriver, "submitInvoiceDetailButton");
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(1000);
            reporte.AddImageToReport("", OPG.ScreenShotAll());
            
            OPG.BuscaPOP2(Driver);
            WB.ClickById(Driver.WebDriver, "confirmSubmitBtn");
            Thread.sleep(5000);
            reporte.AddImageToReport("", OPG.ScreenShotAll());
            OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Ajuste de notas de crédito",true);
            WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "OK");
        }
        else
        {
            throw new NullPointerException( "No se encontro informacion" );
        }
        
    }
    public static void NotaCrediMayor(WebDriv Driver,WebAction WB,Report reporte,String DN,String Cod,String Importe) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement InputDN,SelectAjuste,SelectCategoria,SelectRazon,Tbody = null,InputImporte,BTN;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Cuentas por Cobrar","Gestión de Ajuste","Ajuste de notas de crédito",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Ajuste de notas de crédito",true);
        InputDN = WB.FindElementByID(Driver.WebDriver, "msisdn_condition_input_value");
        WB.TypeText(InputDN, DN);
        reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotAll());
        WB.ClickById(Driver.WebDriver, "queryUserInfoButton");
        WB.CargaAjax(Driver.WebDriver);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Ajuste de notas de crédito",true);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        
        SelectAjuste = WB.FindElementByID(Driver.WebDriver, "settingPanelAdjLevel_input_select");
        WB.SelenSelect(SelectAjuste, "Value", "D");
        Thread.sleep(2000);
        SelectCategoria = WB.FindElementByID(Driver.WebDriver, "settingPanelReasonCategroy_input_select");
        WB.SelenSelect(SelectCategoria, "Value", "1668541828053858683");
        Thread.sleep(2000);
        SelectRazon = WB.FindElementByID(Driver.WebDriver, "settingPanelReason_input_select");
        WB.SelenSelect(SelectRazon, "Value", "CNT217");
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        
        Tbody = WB.FindElementByID(Driver.WebDriver, "invoiceInfoIOList2_databody");
        if(Tbody != null)
        {
            WB.ClickById(Driver.WebDriver, "selected2_1");
            Thread.sleep(3000);
            InputImporte = WB.FindElementByID(Driver.WebDriver, "dispinput2_1_value");
            InputImporte.clear();
            WB.TypeText(InputImporte, Importe);
            reporte.AddImageToReport("", OPG.ScreenShotAll());
            
            WB.ClickById(Driver.WebDriver, "submitInvoiceDetailButton");
            WB.CargaAjax(Driver.WebDriver);
            Thread.sleep(2000);
            try 
            {
                OPG.NavegaIframe(Driver, "popwin0");
                throw new NullPointerException( "No se encontro error al ingresar un monto mayor" );
            } catch (Exception e)
            {
                reporte.AddImageToReport("Se valida el error", OPG.ScreenShotAll());
            }
        }
        
    }
    
    public static void Refacturacion(WebDriv Driver,WebAction WB,Report reporte,String DN,String FechaINI,String FechaFIN,String Monto) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Chil;
        WebElement InpFechFIN,InputDN,InpFechIN,Tab,DivMonto,Detalle;
        String Folio=null;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Invoicing","Bill Run Management","Refactura",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Refactura",true);
        
        Thread.sleep(8000);
        WB.WaitForElem(Driver.WebDriver, "msisdn_condition_input_value");
        InputDN = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.basicManager.IO.searchConditionIO.basicQueryCondition.msisdn");
        //InputDN = WB.FindElementByID(Driver.WebDriver, "msisdn_condition_input_value");
        WB.TypeText(InputDN, DN);
        //reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotElement(Driver)); 
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        WB.ClickById(Driver.WebDriver, "queryUserInfoButton");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        
        InpFechIN = WB.FindElementByID(Driver.WebDriver, "invoiceGenerationDate_input_value");
        WB.TypeText(InpFechIN, FechaINI);
        InpFechFIN = WB.FindElementByID(Driver.WebDriver, "settingPanelDueDate_input_value");
        WB.TypeText(InpFechFIN, FechaFIN);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotElement(Driver));
        
        Tab = WB.FindElementByID(Driver.WebDriver, "toobar");
        Chil =WB.GetChildren(Tab);
        Chil.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        
        Thread.sleep(4000);
        OPG.BuscaPOP2(Driver);
        Detalle = WB.FindElementByID(Driver.WebDriver, "conceptSelect_input_select");
        WB.SelenSelect(Detalle, "Value", "000000091");
        Thread.sleep(1000);
        DivMonto = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEModel.refacturaConceptIO.conceptUnitAmountStr");
        WB.TypeText(DivMonto, Monto);  
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "saveRefacturaConcept");
        //WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Refactura",true);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "submitButton");     
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        OPG.BuscaPOP2(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "confirmSubmitBtn");
        Thread.sleep(6000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Refactura",true);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "OK");
        reporte.AddImageToReport("", OPG.ScreenShotAll());

    }
    
    public static void RefacturacionDocCiclo(WebDriv Driver,WebAction WB,Report reporte,String DN,String Codigo,String FechaINI,String Monto) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Chil,Chil2;
        WebElement TBody,InputDN,InpFechIN,Tbody,Tbody2,DivMonto,Detalle,TD,DivReferencia,Divclick,InputMonto;
        String Folio=null, TxtTbody,TxtTbody2,TxtTD1=null,TxtTD3,FechaINI2;
        Boolean Bandera=false;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver)); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Facturación",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Factura y pago de factura",false);
        Thread.sleep(5000);
        InpFechIN = WB.FindElement(Driver.WebDriver, WB, "Name", "dateBegin");
        InpFechIN.clear();
        FechaINI2 = FechaINI+" 13:18:45";
        WB.TypeText(InpFechIN, FechaINI2);
        WB.ClickById(Driver.WebDriver, "searchBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        //Se busca en la tabla la factura 
        TBody = WB.FindElementByID(Driver.WebDriver, "invoiceInfoList_databody");
        TxtTbody = TBody.getText();
        if (TxtTbody == "No hay registros") 
        {
            reporte.AddImageToReport("No se encuentran facturas", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro error al ingresar un monto mayor" );
        }
        Chil = WB.GetChildren(TBody);
        for (int i = 0; i < Chil.size(); i++)
        {
            TD = WB.FindElementByID(Driver.WebDriver, "invoiceInfoList_"+i+"_1");
            TxtTD1 = TD.getText();
            TxtTD3 = TxtTD1.substring(0, 3);
            if(TxtTD3.equals( "PCS"))
            {
                Bandera = true;
                break;
            }
        }
        if (Bandera == false)
        {
            reporte.AddImageToReport("No se encuentran facturas viables para refacturar", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro error al ingresar un monto mayor" );
        }
        WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        OPG.CierraPestanas(Driver);
        Thread.sleep(2000);
        
        OPG.NavegaMapaSitio(Driver,WB,"Invoicing","Bill Run Management","Refactura",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Refactura",true);
        
        Thread.sleep(8000);
        WB.WaitForElem(Driver.WebDriver, "msisdn_condition_input_value");
        InputDN = WB.FindElement(Driver.WebDriver, WB, "Name", "#BMEAttr.basicManager.IO.searchConditionIO.basicQueryCondition.msisdn");
        //InputDN = WB.FindElementByID(Driver.WebDriver, "msisdn_condition_input_value");
        WB.TypeText(InputDN, DN);
        //reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotElement(Driver)); 
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        WB.ClickById(Driver.WebDriver, "queryUserInfoButton");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        
        
        InpFechIN = WB.FindElementByID(Driver.WebDriver, "invoiceGenerationDate_input_value");
        FechaINI = FechaINI.replace("/", ".");
        WB.TypeText(InpFechIN, FechaINI);
        DivReferencia = WB.FindElementByID(Driver.WebDriver, "referenceToInvoice_input_value");
        WB.TypeText(DivReferencia, TxtTD1);
        //InpFechFIN = WB.FindElementByID(Driver.WebDriver, "settingPanelDueDate_input_value");
        //WB.TypeText(InpFechFIN, FechaFIN);     
        //reporte.AddImageToReport("Se busca el DN", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "queryInvoiceDetailInfoButton");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        
        Tbody2 = WB.FindElementByID(Driver.WebDriver, "invoiceInfoIOList_databody");
        TxtTbody2 = Tbody2.getText();
        if (TxtTbody.equals("No se encontrarón archivos"))
        {
            reporte.AddImageToReport("No se encontrarón archivos", OPG.ScreenShotAll());
            throw new NullPointerException( "No se encontrarón archivos" );
        }
        
        Divclick = WB.FindElementByID(Driver.WebDriver, "invoiceInfoIOList_2_0");
        Chil2 = WB.GetChildren(Divclick);
        Chil2.get(0).click();
        WB.CargaAjax(Driver.WebDriver);
        InputMonto = WB.FindElementByID(Driver.WebDriver, "dispinput2_2_value");
        InputMonto.clear();
        WB.TypeText(InputMonto, Monto);
        
        
        
        
        WB.ClickById(Driver.WebDriver, "submitInvoiceDetailButton");     
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        OPG.BuscaPOP2(Driver);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickById(Driver.WebDriver, "confirmSubmitBtn");
        Thread.sleep(6000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Refactura",true);
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        //reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
        WB.ClickButtonDinamic(Driver.WebDriver, WB, "ClassName", "bc_btn", "OK");
        reporte.AddImageToReport("", OPG.ScreenShotAll());
        
    }
    
    public static void OrdenesDCDeuda(WebDriv Driver,WebAction WB,Report reporte,String DN,String Codigo,String FechaINI) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Chil,Chil2;
        WebElement TBody,InputDN,InpFechIN,Tbody,Tbody2,DivMonto,Detalle,TD,DivReferencia,Divclick,DivEstado;
        String Folio=null, TxtTbody,TxtTbody2,TxtTD1=null,TxtTD3,FechaINI2,TxtEstado;
        Boolean Bandera=false;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,WB,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver)); 
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Facturación",false);
        Thread.sleep(2000);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "ccbmTabPanel_items", "Factura y pago de factura",false);
        Thread.sleep(5000);
        InpFechIN = WB.FindElement(Driver.WebDriver, WB, "Name", "dateBegin");
        InpFechIN.clear();
        FechaINI2 = FechaINI+" 13:18:45";
        WB.TypeText(InpFechIN, FechaINI2);
        WB.ClickById(Driver.WebDriver, "searchBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        //Se busca en la tabla la factura 
        TBody = WB.FindElementByID(Driver.WebDriver, "invoiceInfoList_databody");
        TxtTbody = TBody.getText();
        if (TxtTbody == "No hay registros") 
        {
            reporte.AddImageToReport("No se encuentran facturas", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro error al ingresar un monto mayor" );
        }
        Chil = WB.GetChildren(TBody);
        for (int i = 0; i < Chil.size(); i++)
        {
            TD = WB.FindElementByID(Driver.WebDriver, "invoiceInfoList_"+i+"_1");
            TxtTD1 = TD.getText();
            TxtTD3 = TxtTD1.substring(0, 3);
            if(TxtTD3.equals( "PCS"))
            {
                DivEstado = WB.FindElementByID(Driver.WebDriver, "invoiceInfoList_2_2");
                TxtEstado = DivEstado.getText();
                if (TxtEstado.equals("Abierta"))
                {
                    Bandera = true;
                    break;
                }
                
            }
        }
        if (Bandera == false)
        {
            reporte.AddImageToReport("No se encuentran facturas vencidas", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro factura vencida" );
        }
        WebDriver nFrame = Driver.WebDriver.switchTo().defaultContent();
        Driver.AssingNewDriver(nFrame);
        OPG.CierraPestanas(Driver);
        Thread.sleep(2000);
        
        OPG.NavegaMapaSitio(Driver,WB,"Cobro de Deuda","Gestión de Orden de Tranajo","Consulta de Simulación de Orden de Trabajo",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Consulta de Simulación de Orden de Trabajo",true);
        Thread.sleep(8000);
        
        InputDN = WB.FindElementByID(Driver.WebDriver, "msisdnId_input_value_disp");
        WB.TypeText(InputDN, DN);
        WB.ClickById(Driver.WebDriver, "queryButton");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        try {
            OPG.NavegaIframe(Driver, "popwin0");
            reporte.AddImageToReport("No se encuentran facturas vencidas", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro factura vencida" );
        } catch (Exception e) 
        {
        }
        TBody = WB.FindElementByID(Driver.WebDriver, "queryResultGrid_databody");
        reporte.AddImageToReport("", OPG.ScreenShotElement2(TBody));
        
    }
    
     public static void ValidacionDiasReloj(WebDriv Driver,WebAction WB,Report reporte,String DN,String Codigo) throws InterruptedException, IOException
    {
        OperacionesGenerales OPG = new OperacionesGenerales();
        List <WebElement> Chil,Chil2;
        WebElement TBody,InputDN,Divinfo,Tbody,Tbody2,DivMonto,Detalle,TD,DivReferencia,Divclick,DivEstado;
        String Folio=null, TxtTbody,TxtDiv,TxtTD1=null,TxtTD3,FechaINI2,TxtEstado;
        Boolean Bandera=false;
                
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Cobro de Deuda","Gestión de Orden de Tranajo","Consulta de Simulación de Orden de Trabajo",reporte);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "tabpage_items", "Consulta de Simulación de Orden de Trabajo",true);
        Thread.sleep(8000);
        
        InputDN = WB.FindElementByID(Driver.WebDriver, "msisdnId_input_value_disp");
        WB.TypeText(InputDN, DN);
        WB.ClickById(Driver.WebDriver, "queryButton");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(1000);
        try {
            OPG.NavegaIframe(Driver, "popwin0");
            reporte.AddImageToReport("No se encuentran facturas vencidas", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro factura vencida" );
        } catch (Exception e) 
        {
        }
        TBody = WB.FindElementByID(Driver.WebDriver, "queryResultGrid_databody");
        reporte.AddImageToReport("", OPG.ScreenShotElement2(TBody));
        
        Chil = WB.GetChildren(TBody);
        for (int i = 0; i < Chil.size(); i++)
        {
            Divinfo = WB.FindElementByID(Driver.WebDriver, "queryResultGrid_"+i+"_8");
            TxtDiv = Divinfo.getText();
            if (TxtDiv != "")
            {
                TBody = WB.FindElementByID(Driver.WebDriver, "queryResultGrid_databody");
                reporte.AddImageToReport("DN con Ordenes de Trabajo", OPG.ScreenShotElement2(TBody));
                Bandera = true;
                break;
                
            }
        }
        if (Bandera == false)
        {
            reporte.AddImageToReport("No se encuentran ordenes de trabajo", OPG.ScreenShotElement(Driver)); 
            throw new NullPointerException( "No se encontro factura vencida" );
        }
    }
} 

