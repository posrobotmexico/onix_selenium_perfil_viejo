
package Regresivas;

import Core.Report;
import Core.WebDriv;
import Operaciones.OperacionesGenerales;
import Core.WebAction;
import java.io.IOException;
import java.util.List;
import org.openqa.selenium.WebElement;


public class InfoCliente {
    
    public static void CambiaInfoCuenta(WebDriv Driver,WebAction SelenElem,Report reporte, String DN,String Codigo) throws InterruptedException, IOException
    {
       OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,Input,CP,NUMEX,CALLE,FECHA,TITULO, BTNpop=null,Selen;
        Boolean Apagar=false;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cuenta",false);
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
         
        SelenElem.ClickById(Driver.WebDriver, "acctInformation_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "address_titlebar");
        
        
        SelenElem.ClickById(Driver.WebDriver, "chgAcctBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Información de Cuenta",false);
       
        //CP = SelenElem.FindElementByID(Driver.WebDriver,"field_1007_500508_input_value");
        //CP.clear();
        //SelenElem.TypeText(CP,"11560"); 
        SelenElem.ClickById(Driver.WebDriver, "field_1007_500506_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        NUMEX = SelenElem.FindElementByID(Driver.WebDriver, "field_1007_500506_input_value");
        //SelenElem.TypeText(NUMEX,"2");
        CALLE = SelenElem.FindElementByID(Driver.WebDriver, "field_1007_500505_input_value");
        SelenElem.TypeText(CALLE,"Practia");
        Input = SelenElem.FindElementByID(Driver.WebDriver, "field_1007_508006_input_value");
        Input.clear();
        SelenElem.TypeText(Input,"04/01/1992");        
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "set_1007_titlebar");


        //Direccion de entrega de factura
        TITULO = SelenElem.FindElementByID(Driver.WebDriver, "title_input_select");
        SelenElem.SelenSelect(TITULO, "Value", "1");
        SelenElem.CargaAjax(Driver.WebDriver);
        CALLE = SelenElem.FindElementByID(Driver.WebDriver, "name1_input_value");
        SelenElem.TypeText(CALLE,"Robot");
        CALLE = SelenElem.FindElementByID(Driver.WebDriver, "name2_input_value");
        SelenElem.TypeText(CALLE,"Robotsin");
        CALLE = SelenElem.FindElementByID(Driver.WebDriver, "name3_input_value");
        SelenElem.TypeText(CALLE,"Garcia");
        //CP = SelenElem.FindElementByID(Driver.WebDriver,"field_500010_500508_input_value");
        //SelenElem.TypeText(CP,"11560"); 
        SelenElem.ClickById(Driver.WebDriver, "field_500010_500506_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        NUMEX = SelenElem.FindElementByID(Driver.WebDriver, "field_500010_500506_input_value");
        SelenElem.TypeText(NUMEX,"2");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        NUMEX = SelenElem.FindElementByID(Driver.WebDriver, "field_500010_500505_input_value");
        SelenElem.TypeText(NUMEX,"Centenario");   
        
        
        
        SelenElem.ClickById(Driver.WebDriver, "nextBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        OPG.IframeActual(Driver);
        BTNpop = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        if(BTNpop!=null)
        {
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
            SelenElem.CargaAjax(Driver.WebDriver);
        }
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Información de Cuenta", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
        
        
        
    }
    
    public static void CambiaInfoCuentaOrdPend(WebDriv Driver,WebAction SelenElem,Report reporte, String DN,String Codigo) throws InterruptedException, IOException
    {
       OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Elem,Input,CP,NUMEX,CALLE,FECHA,TITULO, BTNpop=null,Selen;
        Boolean Apagar=false;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,SelenElem);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,SelenElem);
        OPG.CierraBoletin(Driver,SelenElem);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        
        String ParentPri = OperacionesGenerales.Busqueda360(Driver,SelenElem,"DN",DN,Codigo);
        reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cuenta",false);
        Thread.sleep(4000);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "ccbmTabPanel_items", "Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver));
         
        SelenElem.ClickById(Driver.WebDriver, "acctInformation_titlebar");
        SelenElem.ClickById(Driver.WebDriver, "address_titlebar");
        
        
        SelenElem.ClickById(Driver.WebDriver, "chgAcctBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        Thread.sleep(4000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Información de Cuenta",false);
       
        CP = SelenElem.FindElementByID(Driver.WebDriver,"field_500006_500508_input_value");
        SelenElem.TypeText(CP,"11560"); 
        SelenElem.ClickById(Driver.WebDriver, "field_1007_500506_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        NUMEX = SelenElem.FindElementByID(Driver.WebDriver, "field_500006_500506_input_value");
        SelenElem.TypeText(NUMEX,"2");
        CALLE = SelenElem.FindElementByID(Driver.WebDriver, "field_500006_500505_input_value");
        SelenElem.TypeText(CALLE,"Practia");
        Input = SelenElem.FindElementByID(Driver.WebDriver, "birthDate_input_value");
        Input.clear();
        SelenElem.TypeText(Input,"04/01/1992");    
        TITULO = SelenElem.FindElementByID(Driver.WebDriver, "acctUsoCfdi_input_select");
        SelenElem.SelenSelect(TITULO, "Value", "G03");
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        SelenElem.ClickById(Driver.WebDriver, "acctInformation_titlebar");
        


        //Direccion de entrega de factura
        //CP = SelenElem.FindElementByID(Driver.WebDriver,"field_500006_500508_input_value");
        //SelenElem.TypeText(CP,"11560"); 
        SelenElem.ClickById(Driver.WebDriver, "field_500006_500506_input_value");
        SelenElem.CargaAjax(Driver.WebDriver);
        

        CALLE = SelenElem.FindElementByID(Driver.WebDriver, "field_500006_500505_input_value");
        SelenElem.TypeText(CALLE,"2");
        CALLE = SelenElem.FindElementByID(Driver.WebDriver, "field_500006_500506_input_value");
        SelenElem.TypeText(CALLE,"2");  
        reporte.AddImageToReport("Se modifican los datos", OPG.ScreenShotElement(Driver)); 
        
        
        SelenElem.ClickById(Driver.WebDriver, "nextBtn");
        SelenElem.CargaAjax(Driver.WebDriver);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        OPG.IframeActual(Driver);
        BTNpop = SelenElem.FindMultiElem(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
        if(BTNpop!=null)
        {
            SelenElem.ClickButtonDinamic(Driver.WebDriver, SelenElem, "ClassName", "bc_btn", "Aceptar");
            SelenElem.CargaAjax(Driver.WebDriver);
        }
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, SelenElem, "custTabPanel_items", "Cambiar Información de Cuenta",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        
        Apagar = OPG.ValidaImpPago(Driver.WebDriver, SelenElem);
        
        if(Apagar.equals(false))
        {
            OPG.EnviarOrden(Driver,SelenElem,ParentPri,"custTabPanel_items","Cambiar Información de Cuenta", reporte);
            Folio= OPG.BuscaFolio(Driver,SelenElem);
            reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver));
        }
        else
        {
            throw new NullPointerException( "La orden tiene Costo" );
                
        }
        
        
        
        
    }
    
    
    public static void CambiaInfoSuscriptor(WebDriv Driver,WebAction WB,Report reporte, String DN,String Codigo) throws InterruptedException, IOException
    {
       OperacionesGenerales OPG = new OperacionesGenerales();
        WebElement Correo,Input,CP,NUMEX,CALLE,NIF,TITULO, BTNpop=null,Selen;
        Boolean Apagar=false;
        String Folio;
        
        reporte.AddImageToReport("Pagina principal", OPG.ScreenShotElement(Driver));
        OPG.Login(Driver,WB);
        reporte.AddImageToReport("Login", OPG.ScreenShotElement(Driver));
        OPG.ValidaSesion(Driver,WB);
        OPG.CierraBoletin(Driver,WB);
        reporte.AddImageToReport("Pantalla pricipal de Onix", OPG.ScreenShotElement(Driver));
        OPG.NavegaMapaSitio(Driver,WB,"Atención al Cliente","Servicios del Cliente","Cambiar Información de suscriptor",reporte);      
        Thread.sleep(2000);
        String ParentPri = OperacionesGenerales.BusquedaCliente(Driver,WB,"DN",DN,"");
        //reporte.AddImageToReport("Busqueda de cliente", OPG.ScreenShotElement(Driver));        
        
        OPG.SwitchIframe(Driver, ParentPri);
        OPG.NavegaTab(Driver,Driver.WebDriver, WB, "custTabPanel_items", "Cambiar Información de suscriptor",false);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        WB.ClickById(Driver.WebDriver, "displayActualCustInfo_input_0");
        Thread.sleep(1000);
        reporte.AddImageToReport("Datos sin modificar", OPG.ScreenShotElement(Driver)); 
        
        OPG.LlenaDatosClienteNuevoConRFC(Driver, WB, reporte, false);
        
        WB.ClickById(Driver.WebDriver, "nextBtn");
        WB.CargaAjax(Driver.WebDriver);
        Thread.sleep(2000);
        reporte.AddImageToReport("", OPG.ScreenShotElement(Driver)); 
        OPG.EnviarOrden(Driver, WB, ParentPri, "custTabPanel_items", "Cambiar Información de suscriptor", reporte);
        Folio= OPG.BuscaFolio(Driver,WB);
        reporte.AddImageToReport("Numero de orden: "+Folio, OPG.ScreenShotElement(Driver)); 
    }
     
     
     
}
